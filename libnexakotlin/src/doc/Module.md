# Module libnexakotlin

A multiplatform library to provide wallet-type operations for the nexa blockchain, including:
* connectivity to the peer-to-peer and electrum protocol networks.
* script creation and simulation
* address conversion
* wallet operation

## Add to your Gradle Project

[Find the latest library version here.](https://gitlab.com/api/v4/projects/48545045/packages)

In your build.grade.kts:

```gradle
repositories {
    maven { url = uri("https://gitlab.com/api/v4/projects/48545045/packages/maven") } // LibNexaKotlin
}

dependencies {
    // typically this library is used in test code so the example shown uses "testImplementation"
    implementation("org.nexa:libnexakotlin:0.0.1")  // Update this version with the latest
}
```


# Package org.nexa.libnexakotlin

This library is comprehensive and therefore complex.  Learning it is best accomplished via example code, with this documentation used as a reference.  The library contains, Wallet, Blockchain, Network and Scripting sections.  All are needed to implement a fully-functional Nexa wallet, but portions can be
used separately if full functionality is not needed.

Multiple blockchains are supported, via the *[ChainSelector]* object.  Currently this includes Nexa and Bitcoin Cash, and all of the secondary (i.e. regtest and testnet) networks for these two blockchains.
This is accomplished using a class hierarchy, where interfaces designated by iXXXX (for example *[iBlockHeader]*, *[iBlock]*, *[iTransaction]*, *[iTxInput]*, *[iTxOutput]*, *[iTxOutpoint]*) are the parent for a class that implements common functionality "CommonBlockHeader".
This is the parent of blockchain-specific derived classes (*[NexaBlockHeader]*, *[BchBlockHeader]*).

## Blockchain

The *[Blockchain]* class represents and sync blockchains, storing block headers to a database.

### Blockchain objects (Blocks and Transactions)
Although one may create Nexa or Bch objects directly, it is recommended that construction of blockchain objects occur via global helper functions:
* *[txFor]*, *[txFromHex]*, *[txInputFor]*, *[txOutputFor]*
* *[blockHeaderFor]*, *[blockHeaderFromHex]*, and *[merkleBlockFor]*

These functions will create the correct blockchain-specific derived class, returning the parent iXXXX object.  By doing this whenever possible, you maximize the likelihood that your source code will apply to multiple blockchains.
To use blockchain-specific functionality, you may downcast "(header as NexaBlockHeader)" when needed.

### Blockchain Network access

You may access either P2P or Electrum nodes in the network via the *[RequestMgr]* class.  This class automatically manages connection for you.
Going lower, you may directly connect to a specific node via the *[P2pClient]* or *[ElectrumClient]* classes.

## Scripting

Full scripting functionality is available via the *[OP]* class, which captures all opcodes, and via the *[SatoshiScript]* and *[ScriptTemplate]* classes.  
The *[SatoshiScript]* class is the generalized script language.  *[ScriptTemplate]* creates the Nexa-specific script template transaction output. Note that a partner library [Nexa Script Machine](https://nexa.gitlab.io/nexascriptmachinekotlin/index.html) can be used to execute and debug these scripts.

## Wallet

At the top level, the *[Bip44Wallet]* exists to create an manage a bip-44 compliant (meaning based on a 12-word recovery key) wallet.  If set up, this object automatically uses the Blockchain Network and Scripting APIs to automatically sync with the blockchain and parse and create transactions.
However, it is also possible to use this object in an offline mode, simply by not "hooking" it to a network object.  After creating this object, use the *[Wallet]* interface for general purpose functionality, to allow your code to work with any wallet type.


*Example*

``` kotlin
import org.nexa.libnexakotlin.*

class Test
{

    fun docExample()
    {
        // This is the Nexa genesis block
        val hdr = blockFromHex(ChainSelector.NEXA, "00000000000000000000000000000000000000000000000000000000000000000000011e0000000000000000000000000000000000000000000000000000000000000000cdafb522e41f4b94618c8ee546f0fab8aaae7057f80e4730ea32df145dec73910000000000000000000000000000000000000000000000000000000000000000c0b2b16200ffffff00000000000000000000000000000000000000000000000000000000007201000000000000010000000403001700010000020000000000000000000100000000000000000000a06a00023b1c4c99526575746572733a204a6170616e20504d204b697368696461206261636b7320424f4a20756c7472612d6561737920706f6c696379207768696c652079656e20776f7272696573206d6f756e74204254433a3734313731313a3030303030303030303030303030303030303037356634626330386531643738613361623361663832373464313333333463306163326465323533303937363800000000")
        println("Height ${hdr.height} Size ${hdr.size} Chain Work ${hdr.chainWork}")
        val tx = hdr.txes[0]
        println("Tx 0 inputs: ${tx.inputs.size}")
        println("Tx 0 outputs: ${tx.outputs.size}")

        // I'm cheating here a little because I know this is the genesis block so I know that the last output
        // of the last transaction is text within an OP_RETURN script.
        val output0Asm = tx.outputs.last().script.toAsm(" ")
        println("tx 0 output 0: $output0Asm")
        val stmts = tx.outputs.last().script.parsed()
        println("\nNexa genesis block says:" + OP.parse(stmts.last()).data!!.decodeUtf8())

        // Create a regtest address object
        val addr = PayAddress("nexareg:nqtsq5g53q3sqyhhp45a86792a7wkkm3gyw9gylhp7kr703l")
    }
}
```



