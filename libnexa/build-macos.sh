#!/usr/bin/env bash

set -e

[[ -z "$ARCH" ]] && echo "Please set the ARCH variable" && exit 1

echo "build-macos.sh"
echo "To reproduce run: ARCH=$ARCH ./build-macos.sh"
SDK="macosx"
if [ "$ARCH" == "arm64" ]; then
  SYSROOT=$(xcrun --sdk ${SDK} --show-sdk-path)
  # HOST_FLAGS="-arch arm64 -arch arm64e -miphoneos-version-min=13.0 -isysroot ${SYSROOT}"
  HOST_FLAGS="-arch arm64 -isysroot ${SYSROOT}"
  CHOST="arm-apple-darwin"
elif [ "$ARCH" == "x86_64" ]; then
  SYSROOT=$(xcrun --sdk ${SDK} --show-sdk-path)
  HOST_FLAGS="-arch x86_64 -isysroot ${SYSROOT}"
  # CHOST="x86_64-apple-darwin"
else
  echo "Unsupported ARCH: $ARCH"
  exit 1
fi

# export CMAKE_OSX_SYSROOT=${SYSROOT}
# export CMAKE_OSX_DEPLOYMENT_TARGET="10.4"
# export MACOSX_DEPLOYMENT_TARGET="10.4"
export CC=$(xcrun --find --sdk "${SDK}" clang)
export CXX=$(xcrun --find --sdk "${SDK}" clang++)
export CPP=$(xcrun --find --sdk "${SDK}" cpp)
export AR=$(xcrun --find --sdk "${SDK}" ar)
export LD=$(xcrun --find --sdk "${SDK}" ld)
export CFLAGS="${HOST_FLAGS} -O3 -g3 -fembed-bitcode -I${SYSROOT}/usr/include"
export CXXFLAGS="${HOST_FLAGS} -O3 -g3 -fembed-bitcode -Wno-deprecated-declarations -I${SYSROOT}/usr/include"
# -I${SYSROOT}/usr/include/machine"
export LDFLAGS="${HOST_FLAGS}"

export TARGET=${CHOST}
export PLATFORM=MACOS
echo "FLAGS:"
echo CC=${CC}
echo CXX=${CXX}
echo CFLAGS=${CFLAGS}
echo CXXFLAGS=${CXXFLAGS}

echo "Executing: make -j 10 build/macos/${ARCH}/libnexalight.a"
# Macos needs to build static library.
cp nexa-config-macos.h nexa/src/cashlib/nexa-config.h
make -j 10 build/macos/${ARCH}/libnexalight.a
exit 0

