# Lib Nexa Kotlin

Multiplatform light client and Nexa/BCH access library.

## Inclusion in Gradle Kotlin DSL projects

[Find the latest library version here.](https://gitlab.com/api/v4/projects/48545045/packages)

In your build.grade.kts:

```gradle
repositories {
    maven { url = uri("https://gitlab.com/api/v4/projects/48545045/packages/maven") } // LibNexaKotlin
}

dependencies {
    // typically this library is used in test code so the example shown uses "testImplementation"
    implementation("org.nexa:libnexakotlin:0.0.1")  // Update this version with the latest
}
```

## Package Maintainer Notes

### Building the C libraries

Gradle sync builds the libnexalight C++ code.  To do this, you need to symlink libnexa/nexa to the Nexa full node project.
Building on linux cannot include Apple targets due to Apple being uncooperative with respect to crossbuilds.  But we need those
targets to be defined, or they are not put into the Maven multiplatform inventory file.  Therefore, the build.gradle.kts file
does an ugly thing -- it defines the Apple targets on Linux hosts, but "forgets" about their dependency on the libnexa subproject on Linux.

To publish, you need to publish on linux (./gradlew publish), and then publish on macos (same command).

### Build all libraries

```bash
./gradlew assemble
```

### Publish to Maven

```bash
./gradlew publish
```

```bash
./gradlew publishToMavenLocal
```