import org.nexa.threads.*
import kotlin.concurrent.Volatile
import kotlin.test.*

class ThreadTests
{
    @Test
    fun test()
    {
        val p = platformName()
        check(p.isNotEmpty())
        println("Common tests on: " + p)
    }

    @Test
    fun testThreads()
    {
        //println("thread test")
        var cnt = 0
        val q = mutableListOf<Long>(0)
        val th = Thread {
            //println("thunk is running")
            cnt += 1
            q.add(cnt.toLong())
        }
        th.join()
        //println(cnt)
        check(cnt == 1)
        check(q.size == 2)   // check appending to a list in the thread
    }

    @Test
    fun testMutex()
    {
        // println("testMutex")
        val mutex = Mutex()

        var cnt = 0
        var cnt2 = 0
        val th = Thread {
            // println("thunk is running")
            for (i in 1..100000) mutex.lock {
                cnt = cnt + 1
                yield()
                cnt2 = cnt2 + 1
            }

        }
        for (i in 1..100000) mutex.lock {
            cnt = cnt + 1
            yield()
            cnt2 = cnt2 + 1
            }
        th.join()
        check(cnt == cnt2)
        check(cnt2 == 200000)

        check(mutex.timedSync(250) {
            true
        } != null)

        mutex.lock {
            val t = Thread {
                check(mutex.timedSync(250) {
                    check(false)
                } == null)
            }
            t.join()
        }
    }


    @Test
    fun testSynchronizer()
    {
        var done = false
        val lock = Gate()
        val q = mutableListOf<Int>()
        var processed:Int = -2
        val th = Thread {
            //println("thunk is running")
            while(processed < 50) {
                lock.wlock {
                    while (q.size > 0) {
                        val s = q.removeFirst()
                        //println("Consumed: $s")
                        processed = s
                        lock.wake()
                    }
                    if (processed < 50 && !done) await()
                }
            }
            //println("thread completed")
        }

        for(i in 1..50)
        {
            lock.lock {
                q.add(i)
                lock.wake()
            }
            //println("added $i")
        }

        lock.waitfor({ processed == 50 }) {
            done = true
            lock.wake()
        }

        th.join()
    }

    @Test
    fun testSynchronizerWaitLoop()
    {
        done=false
        val lock = Gate()
        val q = mutableListOf<Int>()
        var processed:Int = -2
        val th = Thread {
            //println("thunk is running")
            lock.loopwhile({!done}) {
                while (q.size > 0)
                {
                    val s = q.removeFirst()
                    //println("Consumed: $s")
                    processed = s
                    lock.wake()
                }
            }

        }

        for(i in 1..50)
        {
            lock.lock {
                q.add(i)
                lock.wake()
            }
            //println("added $i")
        }

        lock.waitfor({ processed == 50 }) {
            done = true
            lock.wake()
        }

        th.join()
    }

    @Volatile
    var done = false
    @Test
    fun testSynchronizerWaitFor()
    {
        done = false
        val lock = Gate()
        val q = mutableListOf<Int>()
        var processed:Int = -1
        val th = Thread {
            // println("thunk is running")
            while(!done)
            {
                lock.waitfor({ q.size > 0 || done }) {
                    if (done) return@waitfor
                    while(q.size > 0)
                    {
                        val s = q.removeFirst()
                        // println("Consumed: $s")
                        processed = s
                        yield()
                        lock.wake()
                    }
                }
            }
        }

        for(i in 1..50)
        {
            lock.lock {
                q.add(i)
                lock.wake()
            }
            yield()
            //println("added $i")
        }

        lock.waitfor({ processed == 50 }) {
            // println("consumed all, setting done = true")
            done = true
            lock.wake()
        }

        //println("joining thread")
        th.join()
    }

    @Test fun leakyBucketTest()
    {
        val lb = LeakyBucket(50, 10, 20)
        try {
            lb.take(51)
            check(false)
        }
        catch (e: IllegalArgumentException)  // taking more than bucket max -> infinite wait
        {
            check(true)
        }

        val start = org.nexa.libnexakotlin.millinow()
        lb.take(10)
        val rightaway = millinow()
        check(rightaway - start < 2)
        lb.take(30)
        val elapsed = millinow() - start
        println(elapsed)
        check(elapsed > 1900 && elapsed < 3100)  // total leaked 40, started at 20, filling 10 per sec, so 2 seconds wait
    }

    @Test
    fun repeatTest()
    {
        for(i in 0..1)
        {
            println("Iteration $i")
            testMutex()
            println("testLock")
            testSynchronizer()
            println("testLockWaitFor")
            testSynchronizerWaitFor()
            println("testLockWaitLoop")
            testSynchronizerWaitLoop()
        }
    }
}
