import com.android.build.gradle.LibraryExtension
import org.gradle.internal.os.OperatingSystem
import kotlin.io.path.Path
import kotlin.io.path.absolutePathString

val currentOs = OperatingSystem.current()
val bash = if (currentOs.isWindows) "bash.exe" else "bash"

val buildLibnexa by tasks.creating { group = "build" }

val buildLibnexaIos by tasks.creating {
    group = "build"
    buildLibnexa.dependsOn(this)
}

/*
// Print a bunch of properties that we can access (to use as gradle documentation)
println("root project extension properties")
for (ext in rootProject.properties)
{
    println("  " + ext.key + ": " + ext.value)
}
println("project extension properties")
for (e in project.properties)
{
    println("  " + e.key + ": " + e.value)
}
 */


fun expandedPath(s:String?):String
{
    var tmp = s!!.trim()
    if ((tmp.length > 0)&&(tmp[0] == '~')) tmp = System.getenv("HOME") + tmp.drop(1)
    return Path(tmp).absolutePathString()
}

fun createLibnexalight(platform: String, arch: String, block:(Exec.()->Unit)?=null) = tasks.creating(Exec::class) {
    group = "build"
    buildLibnexa.dependsOn(this)

    val script = "build-$platform.sh"
    val outdir = "$projectDir/build/$platform/$arch"

    inputs.files(script, fileTree("$projectDir/libnexa/src") {
        include("*.c", "*.h")
        exclude("*-config.h")
    })

    outputs.dir(outdir)
    var toolchain = when
    {
        currentOs.isLinux -> if (arch == "host") expandedPath(rootProject.ext["linux.toolchain"] as String?) else "linux-x86_64"
        currentOs.isMacOsX -> "darwin-x86_64"
        currentOs.isWindows -> "windows-x86_64"
        else -> error("No toolchain defined for this OS: $currentOs")
    }
    if (arch == "host")
    {
        var target = when
        {
            currentOs.isLinux -> rootProject.ext["linux.target"]
            currentOs.isMacOsX -> null
            currentOs.isWindows -> null
            // TODO fill in other OSes default compiler location
            else -> error("No target defined for this OS: $currentOs")
        }
        if (target != null) environment("TARGET", target)
    }
    environment("TOOLCHAIN", toolchain)
    environment("ARCH", arch)
    println("Building libnexalight for $platform on $arch")
    println("into: $outdir")
    println("executing: $script")
    commandLine(bash, script)
    if (block != null) block()
}

val libnexalightLinuxX64 by createLibnexalight("linux", "host") {
    onlyIf { currentOs.isLinux }
}

val libnexalightMacosX64 by createLibnexalight("macos", "x86_64") {
    onlyIf { currentOs.isMacOsX }
}

val libnexalightMacosArm64 by createLibnexalight("macos", "arm64") {
    onlyIf { currentOs.isMacOsX }
}

val libnexalightMingwX64 by createLibnexalight("mingw", "x86_64") {
    onlyIf { currentOs.isWindows }
}

val libnexalightIosX64 by createLibnexalight("ios", "x86_64-sim") {
    onlyIf { currentOs.isMacOsX }
}
val libnexalightIosSimulatorArm64 by createLibnexalight("ios", "arm64-sim") {
    onlyIf { currentOs.isMacOsX }
}
val libnexalightIosArm64 by createLibnexalight("ios", "arm64") {
    onlyIf { currentOs.isMacOsX }
}


//val buildLibnexaLinuxX64 by creatingBuildLibnexaLinux("x86_64")

fun creatingBuildLibnexaIos(arch: String) = tasks.creating(Exec::class) {
    group = "build"
    buildLibnexaIos.dependsOn(this)

    onlyIf { currentOs.isMacOsX }

    inputs.files("$projectDir/build-ios.sh", fileTree("$projectDir/libnexa/nexa") {
        include("*.c", "*.h")
        exclude("*-config.h")
    })
    outputs.dir("$projectDir/build/ios/$arch")

    environment("ARCH", arch)
    commandLine(bash, "build-ios.sh")
}

val buildLibnexaIosArm64 by creatingBuildLibnexaIos("arm64")
val buildLibnexaIosArm64Sim by creatingBuildLibnexaIos("arm64-sim")
val buildLibnexaIosX86_64Sim by creatingBuildLibnexaIos("x86_64-sim")

val buildLibnexaAndroid by tasks.creating {
    group = "build"
    buildLibnexa.dependsOn(this)
}

fun getCompileSdk():Int
{
    return rootProject.extensions.findByType<com.android.build.gradle.BaseExtension>()!!.compileSdkVersion!!.split("-")[1].toInt()
}

fun createLibnexalightAndroid(arch: String) = tasks.creating(Exec::class) {
    group = "build"
    buildLibnexaAndroid.dependsOn(this)

    inputs.files("build-android.sh", fileTree("$projectDir/libnexa/src") {
        include("*.c", "*.h")
        exclude("*-config.h")
    })
    outputs.dir("$projectDir/build/android/$arch")

    workingDir = projectDir

    val toolchain = when {
        currentOs.isLinux -> "linux-x86_64"
        currentOs.isMacOsX -> "darwin-x86_64"
        currentOs.isWindows -> "windows-x86_64"
        else -> error("No Android toolchain defined for this OS: $currentOs")
    }
    environment("TOOLCHAIN", toolchain)
    environment("ARCH", arch)
    environment("COMPILE_SDK_VER", getCompileSdk())
    //val le = (rootProject.extensions["android"] as LibraryExtension)
    val prj = project(":libnexakotlin")
    logger.info("log: Project object is: " + prj.extensions)
    println("Project object is: " + prj.toString())
    //val le = (project(":libnexakotlin").extensions["android"] as LibraryExtension)
    //environment("ANDROID_NDK", le.ndkDirectory)
    environment("ANDROID_NDK", (rootProject.ext["androidNdkDir"] as File).toString())
    commandLine(bash, "build-android.sh")
}
val buildLibnexaAndroidX86_64 by createLibnexalightAndroid("x86_64")
val buildLibnexaAndroidX86 by createLibnexalightAndroid("x86")
val buildLibnexaAndroidArm64v8a by createLibnexalightAndroid("arm64-v8a")
val buildLibnexaAndroidArmeabiv7a by createLibnexalightAndroid("armeabi-v7a")

val clean by tasks.creating {
    group = "build"
    doLast {
        delete("$projectDir/build")
        exec() { commandLine("make", "cleanall") }
    }
    //println("libnexa cleaning")
    //task<Exec>("clean_nexa_all") { commandLine("make cleanall") }
}
