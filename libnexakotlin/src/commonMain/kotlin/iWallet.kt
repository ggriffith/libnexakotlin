package org.nexa.libnexakotlin

import kotlinx.coroutines.runBlocking
import org.nexa.threads.millisleep
import com.ionspin.kotlin.bignum.decimal.BigDecimal
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

class AddressDerivationKey
{
    companion object
    {
        val BIP44 = 0x2C.toLong()  // 44
        val BIP32 = 0.toLong()  // BIP 32 HD wallet layout recommends "account" as the first field starting at 0

        /** BIP-44 coin types are described (here)[https://github.com/satoshilabs/slips/blob/master/slip-0044.md] */
        val BTC = 0.toLong()
        val TESTNET = 1.toLong()
        val BCH = 0x91.toLong()
        val ION = 0x737978.toLong()
        val NEXA = 0x7227.toLong()

        /** This is used for identity and payments on any blockchain (there is value in having the same address across blockchains sometimes, but its also potentially
        dangerous if two blockchains use exactly the same tx format, sighash scheme, and a reproducible (non-UTXO) input form.  This seems unlikely.
         */
        val ANY = 0x1c3b1c3b.toLong()

        fun hardened(v: Long): Long
        {
            return 0x800000000 or v
        }

        fun hardened(v: Int): Long
        {
            return 0x800000000 or v.toLong()
        }
    }
}


/** Let's be blunt: Kotlin enums are unusable */
class TxCompletionFlags
{
    companion object
    {
        const val USE_GROUP_AUTHORITIES = 1  // Note baton qualification below
        const val FUND_GROUPS = 2  // adds any inputs needed to for groups that are output.
        const val FUND_NATIVE = 4  // adds the right amount of NEXA to fund this tx including fee
        const val SIGN = 8  // Signs the tx
        const val BIND_OUTPUT_PARAMETERS = 0x10  // Looks for unfinished output scripts and binds them
        const val PARTIAL = 0x20 // use sighash single or 0thru to create a partial transaction
        const val NO_BATON_AUTHORITIES = 0x40  // if this is not set, authorities that are batons will not be used.
        const val SPEND_ALL_NATIVE = 0x80
        const val DEDUCT_FEE_FROM_OUTPUT = 0x100  // Indicate that the fee should be deducted from an output (independently passed)

        const val ALL = 0x7f
    }
}

@cli(Display.Simple, "A record of each transaction that interests a wallet")
class TransactionHistory(
    val chainSelector: ChainSelector,
    @property:cli(Display.Simple, "The transaction itself", 2) var tx: iTransaction
) : BCHserializable
{
    companion object
    {
        const val SERIALIZATION_VERSION = 1.toByte()
    }

    @cli(Display.Simple, "Confirmation block's height, or -1 meaning unconfirmed")
    var confirmedHeight: Long = -1

    @cli(Display.Simple, "Confirmation block's hash, or null if unconfirmed")
    var confirmedHash: Hash256? = null

    @cli(Display.Simple, "Price of the crypto in fiat when this transaction was issued")
    var priceWhenIssued: BigDecimal = BigDecimal.ZERO

    @cli(Display.Simple, "What fiat currency the priceWhenIssued field is denominated in")
    var priceWhatFiat: String = ""

    var basisOverride: BigDecimal? = null
    var saleOverride: BigDecimal? = null

    @cli(Display.Simple, "Additional information the user may have attached to this payment")
    var note: String = ""

    @cli(Display.Simple, "Date this transaction was issued or confirmed in epoch milliseconds")
    var date: Long = Clock.System.now().toEpochMilliseconds()

    /** The wallet spent this quantity in this transaction */
    @cli(Display.Simple, "The wallet spent this amount")
    var outgoingAmt: Long = 0

    /** The wallet received this quantity in this transaction */
    @cli(Display.Simple, "The wallet received this amount")
    var incomingAmt: Long = 0

    /** Which indexes in the tx are this wallet paying someone else */
    @cli(Display.Dev, "Which tx input indexes are mine (and being spent)")
    val outgoingIdxes: MutableList<Long> = mutableListOf()
    @cli(Display.Dev, "The output corresponding to outgoingIdxes")
    val spentTxos: MutableList<iTxOutput> = mutableListOf()

    /** Which indexes in this tx are this wallet receiving coins */
    @cli(Display.Dev, "Which tx outputs indexes are receives (to this wallet either incoming or change)")
    val incomingIdxes: MutableList<Long> = mutableListOf()

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector, txFor(chainSelector))
    {
        BCHdeserialize(stream)
    }

    override fun toString():String
    {
        val s = StringBuilder()

        if (confirmedHeight == -1L) s.append("unconfirmed")
        else
            s.append("height: " + confirmedHeight + " block hash: " + (confirmedHash ?: ""))
        s.append(" date: " + date)
        s.append(" tx: " + tx.toString())
        return s.toString()
    }

    /** Calculate the capital gain or loss of this transaction based on info from the provided transaction history */
    @cli(Display.Simple, "Calculate the capital gain or loss of this payment (only call for outgoing payments)")
    fun capGains(): BigDecimal
    {
        assert(incomingAmt < outgoingAmt)  // No cap gains on the buy side
        assert(basisOverride != null)  // any sale must have a basis carried forward
        val amount = outgoingAmt - incomingAmt
        val bo = basisOverride!!

        if (saleOverride != null)
        {
            return saleOverride!! - bo
        }

        return (priceWhenIssued * CurrencyDecimal(amount)) - bo
    }


    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        assert(format == SerializationType.DISK)
        val ret = BCHserialized(format) + SERIALIZATION_VERSION + date + confirmedHeight + (confirmedHash ?: Hash256()) + priceWhenIssued.toString() + priceWhatFiat + outgoingAmt + incomingAmt +
            note + (basisOverride?.toString() ?: "") + (saleOverride?.toString() ?: "")
        ret.add(tx)
        ret.add(BCHserialized.list(outgoingIdxes, { BCHserialized.int32(it) }, format))
        ret.add(BCHserialized.list(incomingIdxes, { BCHserialized.int32(it) }, format))
        ret.add(BCHserialized.list(spentTxos, { it.BCHserialize(format) }, format))
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        assert(stream.format == SerializationType.DISK)
        try
        {
            val ver = stream.deuint8()
            if (ver == 1)
            {
                date = stream.deint64()
                confirmedHeight = stream.deint64()
                confirmedHash = stream.denullHash()
                priceWhenIssued = CurrencyDecimal(stream.deString())
                priceWhatFiat = stream.deString()
                outgoingAmt = stream.deuint64()
                incomingAmt = stream.deuint64()
                note = stream.deString()
                basisOverride = BigDecimal.fromNullString(stream.denullString())
                saleOverride = BigDecimal.fromNullString(stream.denullString())
                tx = txFor(chainSelector, stream)

                outgoingIdxes.clear()
                outgoingIdxes += stream.delist { it.deuint32() }
                incomingIdxes.clear()
                incomingIdxes += stream.delist { it.deuint32() }
                spentTxos.clear()
                spentTxos += stream.delist { txOutputFor(chainSelector, it) }
            }
            else throw DeserializationException("Invalid version for stored TransactionHistory")
            return stream
        }
        catch (e: NumberFormatException)
        {
            throw DeserializationException("Deserialization error in TransactionHistory")
        }
    }
}



/** This class defines a wallet interface.  A wallet is a set of payment destinations that are being tracked on a particular blockchain.
 */
@cli(Display.Simple, "Wallet interface")
abstract class Wallet(val chainSelector: ChainSelector)
{
    /** Install a callback handler for whenever this wallet changes */
    abstract fun setOnWalletChange(callback: ((wallet: Wallet) -> Unit)?)

    /** Return identity domain data if this domain has previously been used */
    @cli(Display.Simple, "Get information about an entity that has requested identity information")
    abstract fun lookupIdentityDomain(name: String): IdentityDomain?

    /** Add or update identity domain data */
    @cli(Display.Simple, "Insert or replace identity information relevant to a particular entity")
    abstract fun upsertIdentityDomain(id: IdentityDomain)

    /** Remove identity domain data */
    @cli(Display.Simple, "Remove identity information relevant to a particular entity")
    abstract fun removeIdentityDomain(name: String)

    /** Return all configured identity domains */
    @cli(Display.Simple, "Get all entities that we have provided identity to")
    abstract fun allIdentityDomains(): Collection<IdentityDomain>

    @cli(Display.Simple, "Get information about an entity that has requested identity information")
    abstract fun lookupIdentityInfo(id: PayAddress): IdentityInfo?

    /** Add or update identity data */
    @cli(Display.Simple, "Insert or replace identity information")
    abstract fun upsertIdentityInfo(id: IdentityInfo)

    /** Remove identity information */
    @cli(Display.Simple, "Remove identity information")
    abstract fun removeIdentityInfo(id: PayAddress)


    /** Get a new address to receive funds.  Different wallets may support different payment destination types.  This API returns whatever is the "default" type for this wallet.  This allows generic algorithms to be created that
     * can be applied to many different wallet types.  When this API returns, the destination is ready for use (monitoring is installed in remote nodes, IF any remote nodes exist).  This API may pre-generate destinations.
     * @return A payment destination
     */
    @cli(Display.Dev, "Get a new address where money can be received (suspends)")
    abstract suspend fun newDestination(): PayDestination

    /** Tell the wallet that now is a good time to top-up a cache of unused destinations */
    abstract fun prepareDestinations(minAmt: Int = MIN_UNUSED_ADDRESSES, chunk: Int = GEN_ADDRESS_CHUNK_SIZE)

    /** Gets a new address: convenience function that is functionally similar to @newDestination but mirrors the bitcoin-cli command
     */
    fun getnewaddress(): PayAddress = getNewAddress()  // for the classic bitcoin RPC capitalization compatibility

    @cli(Display.Simple, "Get a new address where money can be received")
    fun getNewAddress(): PayAddress
    {
        val dest = runBlocking { newDestination() }
        return dest.address!! // !! Technically a destination may not have an address, but not ones that we create
    }

    /** Get a new address to receive funds.  Different wallets may support different payment destination types.  This API returns whatever is the "default" type for this wallet.  This allows generic algorithms to be created that
     * can be applied to many different wallet types.  When this API returns, the destination is ready for use (monitoring is installed in remote nodes, IF any remote nodes exist).  This API may pre-generate destinations.
     * @return A payment destination
     */
    @cli(Display.User, "Get a new address where money can be received (blocks)")
    fun getNewDestination(): PayDestination
    {
        val dest = runBlocking(exceptionHandler) { newDestination() }
        return dest
    }

    /** Low level API to generate a new address to receive funds, called by newDestination.  Use "newDestination()" in almost all cases.  This API does not pre-generate (so may be slow), and
     * does not install the destination into bloom filters, etc, before returning.  This means that there may be a race condition between the use of the destination
     * returned here and the wallet's monitoring of that destination which could cause funds to be received but not noticed by this wallet.
     * @return A payment destination
     */
    @cli(Display.Dev, "Create a new payment destination, rather then using a pre-allocated one")
    abstract fun generateDestination(): PayDestination

    /** Get a repeatable destination, generally used for identity purposes.
     *  Given 2 different seeds, this API should at a minimum be statistically unlikely to produce the same address
     *  The seed may be public data (a domain name, for example) so wallet security must not depend on it being a secret.
     *  @param seed Unique data per destination.  Use the empty string "" to get the "common" destination
     * @return A payment destination
     */
    @cli(Display.Dev, "Create a new payment destination, based on the given seed")
    abstract fun destinationFor(seed: String): PayDestination

    /** Wallet implementations may allow access to addresses generated from specific private keys or nonstandard HD derivation paths.
     *  The wallet will never offer these destinations as current payment targets.  It will only spend them (and show their balance).

     *  To inject "retrieve only" type of destination into the core wallet operation, override and implement this function.
     *  This function is only called during wallet initialization and rediscovery.  Dynamic addition of destinations is not supported.
     *
     *  (note to future dev: dynamic addition is not hard in this architecture.  Use electrum to find the UTXOs (and the TXOs if you want
     *  to import history) and add them and the destination to the appropriate CommonWallet data structures.  Save and regenerate bloom.
     */
    open fun getRetrieveOnlyDestinations(): MutableList<PayDestination>
    {
        return mutableListOf()
    }

    /** Send funds to this destination.  This function will select input coins from the wallet to fill the passed quantity
     * @param amountSatoshis Provide how many coins to send, denominated in the fundamental (smallest possible) unit of this currency
     * @param destScript The output (constraint) script
     * @param deductFeeFromAmount Set to true to reduce the amount sent by the fee needed
     * @param note Some information the sender may privately associate with this send
     * @return a signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Simple, "Send money")
    abstract fun send(amountSatoshis: Long, destScript: SatoshiScript, deductFeeFromAmount: Boolean = false, sync: Boolean = false,
        @cli(Display.Simple, "Some information the sender may privately associate with this send") note: String? = null): iTransaction

    /** Send funds to this destination.  This function will select input coins from the wallet to fill the passed quantity
     * @param amountSatoshis Provide how many coins to send, denominated in the fundamental (smallest possible) unit of this currency
     * @param destAddress The destination address
     * @param deductFeeFromAmount Set to true to reduce the amount sent by the fee needed
     * @param note Some information the sender may privately associate with this send
     * @return a signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Simple, "Send money")
    abstract fun send(amountSatoshis: Long, destAddress: PayAddress, deductFeeFromAmount: Boolean = false, sync: Boolean = false,
        @cli(Display.Simple, "Some information the sender may privately associate with this send") note: String? = null): iTransaction

    /** Send funds to this destination.  This function will select input coins from the wallet to fill the passed quantity
     * @param amountSatoshis Provide how many coins to send, denominated in the fundamental (smallest possible) unit of this currency
     * @param destAddress The destination address as a string
     * @param deductFeeFromAmount Set to true to reduce the amount sent by the fee needed
     * @param sync If true, do not return until this transaction has been sent to some nodes
     * @param note Some information the sender may privately associate with this send
     * @return a signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Simple, "Send money")
    abstract fun send(amountSatoshis: Long, destAddress: String, deductFeeFromAmount: Boolean = false, sync: Boolean = false,
        @cli(Display.Simple, "Some information the sender may privately associate with this send") note: String? = null): iTransaction

    /** Send funds to multiple destinations.  This function will select input coins from the wallet to fill the passed quantity
     * @param addrAmt Provide a list of how many coins to send to which addresses denominated in the fundamental (smallest possible) unit of this currency
     * @param sync If true, do not return until this transaction has been sent to some nodes
     * @param note Some information the sender may privately associate with this send
     * @return a signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Simple, "Send money to a several addresses")
    abstract fun send(addrAmt:List<Pair<PayAddress, Long>>, sync: Boolean = false,
        @cli(Display.Simple, "Some information the sender may privately associate with this send") note: String? = null
    ): iTransaction

    /** Send funds to multiple destinations (native coin only).
     * This function will select input coins from the wallet to fill the passed quantity
     * @param outputs Destination information
     * @param sync If true, do not return until this transaction has been sent to some nodes
     * @param note Some information the sender may privately associate with this send
     * @return a signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Simple, "Send money to a several addresses")
    fun sendNative(vararg outputs:iTxOutput, sync: Boolean = false,
        @cli(Display.Simple, "Some information the sender may privately associate with this send") note: String? = null
    ): iTransaction
    {
        val ret = prepareSend(outputs.toMutableList(), 0, false)
        send(ret, sync, note)
        return ret
    }

    /* Send any Group token or plain NEXA, to multiple destinations.
     * This function will use any authority (e.g. MINT) to accomplish the send, unless the send flags are overridden.
     * The transaction is forwarded to the network.
     * @param outputs Destination information
     * @param sync If true, do not return until this transaction has been sent to some nodes
     * @param note Some information the sender may privately associate with this send
     * @param flags: bit map of TxCompletionFlags that defines what this function can use in the wallet.  By default permissions are given to fund groups and native coins, with authorities if needed.
     * @return a signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Simple, "Send money to a several addresses")
    fun send(vararg outputs:iTxOutput, sync: Boolean = false,
        flags: Int = TxCompletionFlags.BIND_OUTPUT_PARAMETERS or TxCompletionFlags.FUND_GROUPS or TxCompletionFlags.FUND_NATIVE or TxCompletionFlags.USE_GROUP_AUTHORITIES or TxCompletionFlags.SIGN,
        @cli(Display.Simple, "Some information the sender may privately associate with this send") note: String? = null
    ): iTransaction
    {
        val tx = txFor(chainSelector)
        for (out in outputs) tx.add(out)
        txCompleter(tx,0, flags)
        send(tx, sync, note)
        return tx
    }


    /** Creates an unsigned transaction that sends to a list of outputs.  This function will select input coins from the wallet to fill the passed quantity
     * and sign the transaction, but will not relay the transaction to the network.
     * @param outputs A list of amounts and output (constraint) scripts
     * @param minConfirms: (Int = 0) minimum depth in the blockchain inputs must have to be eligible for inclusion in this transaction
     * @param deductFeeFromAmount Set to true to reduce the amount sent by the fee needed, rather than add more for the fee
     * @return A signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Dev, "Create a transaction, but do not send it")
    abstract fun prepareSend(outputs: MutableList<iTxOutput>, minConfirms: Int = 1, deductFeeFromAmount: Boolean = false): iTransaction

    /** Modify the passed transaction to complete it to the extent possible by this wallet, including:
     * Find inputs needed to supply satoshis and/or group tokens for this transaction.
     * If change outputs are required, add them.
     * If mint baton passing outputs are possible then add them if equalizeAuthorities=true
     * If outputs are not fully specified, supply the missing constraints.
     * @param [tx]: The transaction to complete
     * @param [inputAmount]: if inputAmount is non-null, assume existing inputs supply this number of satoshis (do not look up these inputs)
     * @param [flags]: bit map of TxCompletionFlags that must be set
     * @param [useAuthorities]: If useAuthorities = true, pull in authorities if needed (and available) to handle (mint/melt) operations
     * @param [fund]: If fund = true, add native crypto inputs to pay for the transaction
     * @param [adjustableOutput]: Based on the flags, this output may be adjusted for fee or surplus (coming out of tokens).  Pass a null if any adjustment should error.
     * @param[destinationAddress]: If null, any missing output templates will substitute a new address from the wallet.  If non-null output substitutions will use this address. */
    @cli(Display.Dev, "Fund the passed possibly-partial transaction.  Does not send it")
    abstract fun txCompleter(tx: iTransaction, minConfirms: Int, flags: Int, inputAmount: Long? = null, adjustableOutput: Int?=null, destinationAddress:PayAddress?=null)

    /** Post this transaction and update the wallet based on any inputs spent.  Typically the provided tx came from calling [prepareSend]
     * @param tx The transaction to be sent
     * @param note Some information the sender may privately associate with this send
     * @return A signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Simple, "Send money")
    abstract fun send(tx: iTransaction, sync: Boolean = false, note: String? = null)

    /** Abort this transaction, whose inputs were reserved by "prepareSend".  Update the wallet to release all inputs reserved by this transaction
     * @param tx The transaction to be aborted
     * @return Nothing
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Dev, "return all inputs used by this transaction to the wallet for subsequent use")
    abstract fun abortTransaction(tx: iTransaction)

    /** Calculate a suggested fee for this transaction
     * @param tx The transaction that needs a fee
     * @param pad Presume that the transaction is actually this much bigger
     * @param priority Some blockchain defined measure of the importance of this tx.  0 is the highest priority.  Irrelevant in BCH
     * @return The fee in satoshis
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.User, "Suggest a fee for the passed transaction")
    abstract fun suggestFee(tx: iTransaction, pad: Int = 0, priority: Int = 0): Long

    /** @return the current balance in the supplied address
     */
    @cli(Display.Simple, "Get the balance in the supplied address.  If unspent is true, the current balance is provided.  Otherwise the total ever received is provided.")
    abstract fun getBalanceIn(dest: PayAddress, unspent: Boolean = true): Long

    /** @return the current token balance
     * @param groupId The group identifier for this token
     * @param dest If non-null, only return balances in this address
     * @param minConfirms If nonzero, the funding transaction must be at least this deep.
     * @param unspent If unspent is true, the current balance is provided.  Otherwise the total ever received is provided.
     */
    @cli(Display.Simple, "Get the balance of the supplied token (at the supplied address).  If minConfirms is nonzero, the funding transaction must be at least this deep. If unspent is true, the current balance is provided.  Otherwise the total ever received is provided.")
    abstract fun getBalanceIn(groupId: GroupId, dest: PayAddress?=null, minConfirms: Int = 0, unspent: Boolean = true): Long


    /** @return true if the passed address belongs to this wallet
     */
    @cli(Display.Simple, "true if the passed address belongs to this wallet")
    abstract fun isWalletAddress(dest: PayAddress): Boolean

    /** @return Return the PayDestination if passed address belongs to this wallet
     */
    @cli(Display.Simple, "Return the PayDestination if passed address belongs to this wallet")
    abstract fun walletDestination(dest: PayAddress): PayDestination?

    /** @return true if the passed address belongs to this wallet, and is currently active
     */
    @cli(Display.Simple, "true if the passed address belongs to this wallet and contains some balance")
    abstract fun isUnspentWalletAddress(dest: PayAddress): Boolean

    /** @return the sum of the amounts in all confirmed unspent outputs spendable by this wallet
     */
    @cli(Display.Simple, "Amount of ungrouped satoshis in this wallet (confirmed and unconfirmed)")
    abstract val balance: Long

    @cli(Display.Simple, "Confirmed amount of ungrouped satoshis in this wallet")
    abstract val balanceConfirmed: Long

    /** @return the sum of the amounts in all unconfirmed unspent outputs spendable by this wallet
     *  Unconfirmed spends are removed from this balance
     */
    @cli(Display.Simple, "Unconfirmed amount of ungrouped satoshis in this wallet")
    abstract val balanceUnconfirmed: Long

    /** This wallet finds its coins on this blockchain
     */
    @cli(Display.Simple, "Access the wallet's blockchain")
    abstract val blockchain: Blockchain

    /** Any name you may want to call this wallet */
    @cli(Display.Simple, "Wallet name")
    abstract val name: String

    /** Returns the current position of this wallet in the blockchain.  Since a fork may have occurred, this information does not unambiguously locate the wallet, but it is valuable for UI.
     */
    @cli(Display.Simple,
        "Returns the current position of this wallet in the blockchain.  The wallet's state will not reflect any changes beyond this position")
    abstract val syncedHeight: Long

    /** Return whether this wallet is synced with its underlying blockchain.  If it is not synced, properties like balance and balanceUnconfirmed express some previous state.
     * In the unsynced case, this API will wait for it to do so, but no more than the provided time in milliseconds.
     * If a height is provided, this API returns true if the wallet is synced up to or beyond this height
     * If a height is not provided (or is -1), this API assumes you mean up to "now".  This is special cased with an extra check that the blockchain's tip timestamp is within an hour of
     * now.
     * Of course, since blocks can be discovered at any time, and connected nodes can be slow at processing blocks
     * one cannot ever absolutely know whether the wallet is really synced up to "now", so this function is more accurately described as "nearly synced" for any "now" call.
     */
    @cli(Display.Simple, "Return whether this wallet is synced with its underlying blockchain")
    abstract fun synced(epochTimeinMsOrBlockHeight: Long = -1L): Boolean

    /** Return whether this wallet is synced with its underlying blockchain.  If it is not synced, properties like balance and balanceUnconfirmed express some previous state.
     * In the unsynced case, this API will wait for it to do so, but no more than the provided time in milliseconds.
     * If a height is provided, this API returns true if the wallet is synced up to or beyond this height
     * If a height is not provided (or is -1), this API assumes you mean up to "now".  This is special cased with an extra check that the blockchain's tip timestamp is within an hour of
     * now.
     * Of course, since blocks can be discovered at any time, and connected nodes can be slow at processing blocks
     * one cannot ever absolutely know whether the wallet is really synced up to "now", so this function is more accurately described as "nearly synced" for any "now" call.
     */
    @cli(Display.Simple, "Wait until this wallet is synced with its underlying blockchain")
    fun sync(maxWait: Long = 10L*86400L*365L, epochTimeinMsOrBlockHeight: Long = -1L): Boolean
    {
        val start = epochMilliSeconds()
        val end = start + maxWait

        do
        {
            if (synced(epochTimeinMsOrBlockHeight)) return true
            millisleep(500U)
        } while (epochMilliSeconds() < end)
        return false
    }


    /** Forget all transaction and blockchain state, and asynchronously redo the search for wallet transactions.
     * This is intended for testing and debug
     */
    @cli(Display.Dev, "Forget all transaction and blockchain state, and asynchronously redo the search for wallet transactions.  forgetAddresses deletes any allocated addresses (a deterministic wallet will recalculate them), noPrehistory=true searches before the creation of this wallet for tx")
    abstract fun rediscover(forgetAddresses: Boolean = false, noPrehistory: Boolean = false): Unit

    /** Pause blockchain processing if true */
    @cli(Display.Dev, "Pause blockchain processing")
    abstract var pause: Boolean

    /** wallet history by transaction */
    @cli(Display.User, "wallet history by transaction")
    abstract val txHistory: MutableMap<Hash256, TransactionHistory>

    /** all UTXOs that this wallet is capable of spending (and how to spend them)
     * READ ONLY */
    @cli(Display.Dev, "all UTXOs that this wallet is capable of spending or has already spent")
    abstract val txos: Map<iTxOutpoint, Spendable>

    @cli(Display.Dev, "find and return the secret corresponding to the passed pubkey.")
    abstract fun pubkeyToSecret(pubkey: ByteArray): Secret?

    /** Control whether saving the wallet to underlying storage is automatic (true, default), or initiated by API calls
     * Wallet performance could improve if automaticFlush is false and the wallet is saved rarely.  However state may be lost! */
    @cli(Display.User, "Control whether saving the wallet to underlying storage is automatic or initiated by the save() API call")
    var automaticSave: Boolean = true

    /** Write this wallet's changes to underlying storage. Called internally if @automaticSave is true, and/or explicitly called.
     * Synchronous.  An inability to save the wallet is a severe problem so exceptions are thrown. */
    @cli(Display.User, "Save wallet state to underlying storage")
    abstract fun save(force: Boolean = false)

    /** Give this wallet access to the spot price of its token in the provided fiat currency code */
    var spotPrice: ((String) -> BigDecimal)? = null

    /** Give this wallet access to the historical price (epoch seconds) of its token in the provided fiat currency code */
    var historicalPrice: ((String, Long) -> BigDecimal)? = null

    /** @return true if the passed address belongs to this wallet, and is currently active
     */
    @cli(Display.Simple, "sign the provided string with the provided address (this wallet must have the private key for that address), or pass null to use the common identity")
    abstract fun signMessage(message: ByteArray, addr: PayAddress?=null): ByteArray

    fun signMessage(message: String, addr: PayAddress?=null): String
    {
        val ba = signMessage(message.toByteArray(), addr)
        val ret:String = Codec.encode64(ba)
        return ret
    }

    @cli(Display.Simple, "Verify that the provided message was signed properly")
    fun verifyMessage(message: String, addr: PayAddress, sigS: String): Boolean
    {
        val sig = Codec.decode64(sigS)
        val pubkey = libnexa.verifyMessage(message.toByteArray(), addr.data, sig)
        if (pubkey == null) return false
        return (libnexa.hash160(pubkey) contentEquals addr.data)
    }


    companion object
    {
        /** Network-defined bloom filter maximum size
         * TODO: associate this value with the blockchain
         */
        val MAX_BLOOM_SIZE = 36000

        /**  Bloom filter update flags
         * Must be consistent with same named fields in C++ code in bloom.h:bloomflags
         * */
        enum class BloomFlags(val v: Int)
        {
            BLOOM_UPDATE_NONE(0),
            BLOOM_UPDATE_ALL(1),
            BLOOM_UPDATE_P2PUBKEY_ONLY(2)
        }
        //val BLOOM_UPDATE_MASK = 3

        /** verify signed message helper function */
        fun verifyMessage(message: String, addr: PayAddress, sigS: String): Boolean
        {
            val sig = Codec.decode64(sigS)
            val pubkey = libnexa.verifyMessage(message.toByteArray(), addr.data, sig)
            if (pubkey == null) return false
            return (libnexa.hash160(pubkey) contentEquals addr.data)
        }

        /** verify signed message helper function */
        fun verifyMessage(message: ByteArray, addr: PayAddress, sigS: String): Boolean
        {
            val sig = Codec.decode64(sigS)
            val pubkey = libnexa.verifyMessage(message, addr.data, sig)
            if (pubkey == null) return false
            return (libnexa.hash160(pubkey) contentEquals addr.data)
        }

        /** verify a signed message */
        fun verifyMessage(message: ByteArray, addr: PayAddress, sig: ByteArray): Boolean
        {
            val pubkey = libnexa.verifyMessage(message, addr.data, sig)
            if (pubkey == null) return false
            return (libnexa.hash160(pubkey) contentEquals addr.data)
        }

    }
}
