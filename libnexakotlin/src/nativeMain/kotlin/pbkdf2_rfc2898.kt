package org.nexa.libnexakotlin

import okio.Buffer
import okio.ByteString.Companion.toByteString
import okio.use
import kotlin.experimental.xor
import kotlin.math.ceil

class FallbackProvider

class SecretKeyFactoryCommon()
{
    fun generateSecret(pbeKeySpec: PBEKeySpecCommon): SecretKeyCommon
    {
        val encoded = pbeKeySpec.run {
            Pbkdf2Sha512.derive(password!!, salt!!, iterationCount, keyLength)
        }
        return SecretKeyCommon(encoded)
    }

    companion object
    {
        fun getInstance(algorithm: String, provider: FallbackProvider): SecretKeyFactoryCommon = SecretKeyFactoryCommon()
    }
}

class SecretKeyCommon(val encoded : ByteArray)


/**
 *
 * This is a clean-room implementation of PBKDF2 using RFC 2898 as a reference.
 *
 *
 * RFC 2898: http://tools.ietf.org/html/rfc2898#section-5.2
 *
 *
 * This code passes all RFC 6070 test vectors: http://tools.ietf.org/html/rfc6070
 *
 *
 * http://cryptofreek.org/2012/11/29/pbkdf2-pure-java-implementation/<br></br>
 * Modified to use SHA-512 - Ken Sedgwick ken@bonsai.com
 * Modified to for Kotlin - Kevin Gorham anothergmale@gmail.com
 */
object Pbkdf2Sha512
{

    /**
     * Generate a derived key from the given parameters.
     *
     * @param p the password
     * @param s the salt
     * @param c the iteration count
     * @param dkLen the key length in bits
     */
    fun derive(p: CharArray, s: ByteArray, c: Int, dkLen: Int): ByteArray {
        Buffer().use { baos ->
            val dkLenBytes = dkLen / 8
            val pBytes = p.foldIndexed(ByteArray(p.size)) { i, acc, c ->
                acc.apply { this[i] = c.code.toByte() }
            }
            val hLen = 20.0
            // note: dropped length check because it's redundant, given the size of an int in kotlin
            val l = ceil(dkLenBytes / hLen).toInt()
            for (i in 1..l) {
                F(pBytes, s, c, i).let { Tn ->
                    baos.write(Tn)
                }
            }
            return ByteArray(dkLenBytes).apply {
                baos.read(this)
            }
        }
    }

    internal fun F(
        p: ByteArray,
        s: ByteArray,
        c: Int,
        i: Int
    ): ByteArray {
        val key = p.toByteString()
        val bU = ByteArray(s.size + 4)

        // concat s and i into array bU w/o additional allocations
        s.copyInto(bU, 0, 0, s.size)
        repeat(4) { j ->
            bU[s.size + j] = (i shr (24 - 8 * j)).toByte()
        }

        val uXor = bU.toByteString().hmacSha512(key).toByteArray()
        var uLast = uXor

        repeat(c - 1) {
            val baU = uLast.toByteString().hmacSha512(key).toByteArray()
            uXor.forEachIndexed { k, b ->
                uXor[k] = (b.xor(baU[k]))
            }
            uLast = baU
        }
        return uXor
    }
}


class PBEKeySpecCommon {
    /**
     * Returns the iteration count or 0 if not specified.
     *
     * @return the iteration count.
     */
    var iterationCount: Int = 0
        private set

    /**
     * Returns the to-be-derived key length or 0 if not specified.
     *
     *
     *  Note: this is used to indicate the preference on key length
     * for variable-key-size ciphers. The actual key size depends on
     * each provider's implementation.
     *
     * @return the to-be-derived key length.
     */
    var keyLength = 0
        private set

    /**
     * Constructor that takes a password. An empty char[] is used if
     * null is specified.
     *
     *
     *  Note: `password` is cloned before it is stored in
     * the new `PBEKeySpec` object.
     *
     * @param password the password.
     */
    constructor(password: CharArray?) {
        if (password == null || password.size == 0) {
            this.password = CharArray(0)
        } else {
            this.password = password.copyOf()
        }
    }

    /**
     * Constructor that takes a password, salt, iteration count, and
     * to-be-derived key length for generating PBEKey of variable-key-size
     * PBE ciphers.  An empty char[] is used if null is specified for
     * `password`.
     *
     *
     *  Note: the `password` and `salt`
     * are cloned before they are stored in
     * the new `PBEKeySpec` object.
     *
     * @param password the password.
     * @param salt the salt.
     * @param iterationCount the iteration count.
     * @param keyLength the to-be-derived key length.
     * @exception NullPointerException if `salt` is null.
     * @exception IllegalArgumentException if `salt` is empty,
     * i.e. 0-length, `iterationCount` or
     * `keyLength` is not positive.
     */
    constructor(
        password: CharArray?, salt: ByteArray?, iterationCount: Int,
        keyLength: Int
    ) {
        if (password == null || password.size == 0) {
            this.password = CharArray(0)
        } else {
            this.password = password.copyOf()
        }
        if (salt == null) {
            throw NullPointerException(
                "the salt parameter " +
                    "must be non-null"
            )
        } else require(salt.size != 0) {
            "the salt parameter " +
                "must not be empty"
        }
        this.salt = salt.copyOf()
        require(iterationCount > 0) { "invalid iterationCount value" }
        require(keyLength > 0) { "invalid keyLength value" }
        this.iterationCount = iterationCount
        this.keyLength = keyLength
    }

    /**
     * Constructor that takes a password, salt, iteration count for
     * generating PBEKey of fixed-key-size PBE ciphers. An empty
     * char[] is used if null is specified for `password`.
     *
     *
     *  Note: the `password` and `salt`
     * are cloned before they are stored in the new
     * `PBEKeySpec` object.
     *
     * @param password the password.
     * @param salt the salt.
     * @param iterationCount the iteration count.
     * @exception NullPointerException if `salt` is null.
     * @exception IllegalArgumentException if `salt` is empty,
     * i.e. 0-length, or `iterationCount` is not positive.
     */
    constructor(password: CharArray?, salt: ByteArray?, iterationCount: Int) {
        if (password == null || password.size == 0) {
            this.password = CharArray(0)
        } else {
            this.password = password.copyOf()
        }
        if (salt == null) {
            throw NullPointerException(
                "the salt parameter " +
                    "must be non-null"
            )
        } else require(salt.size != 0) {
            "the salt parameter " +
                "must not be empty"
        }
        this.salt = salt.copyOf()
        require(iterationCount > 0) { "invalid iterationCount value" }
        this.iterationCount = iterationCount
    }

    /**
     * Clears the internal copy of the password.
     *
     */
    fun clearPassword() {
        if (password != null) {
            password!!.fill(' ')
            password = null
        }
    }

    /**
     * Returns a copy of the password.
     *
     *
     *  Note: this method returns a copy of the password. It is
     * the caller's responsibility to zero out the password information after
     * it is no longer needed.
     *
     * @exception IllegalStateException if password has been cleared by
     * calling `clearPassword` method.
     * @return the password.
     */
    var password: CharArray?
        get() {
            checkNotNull(field) { "password has been cleared" }
            return field!!.copyOf()
        }
    /**
     * Returns a copy of the salt or null if not specified.
     *
     *
     *  Note: this method should return a copy of the salt. It is
     * the caller's responsibility to zero out the salt information after
     * it is no longer needed.
     *
     * @return the salt.
     */
    var salt: ByteArray? = null
        get() {
            return if (field != null) {
                field!!.copyOf()
            } else {
                null
            }
        }
}