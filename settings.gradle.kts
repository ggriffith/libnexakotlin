pluginManagement {
    repositories {
        mavenLocal()
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

dependencyResolutionManagement {
    repositories {
        mavenLocal()
        google()
        mavenCentral()
        maven { url = uri("https://gitlab.com/api/v4/projects/48544966/packages/maven") }  // mpThreads
        maven { url = uri("https://gitlab.com/api/v4/projects/38119368/packages/maven") }  // Nexa RPC into nexad
    }
}

rootProject.name = "lnk"
include(":libnexakotlin")
include(":libnexa")
// include(":aars")