// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin

import org.nexa.libnexakotlin.libnexa
import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign
import com.ionspin.kotlin.bignum.integer.util.fromTwosComplementByteArray
import okio.Buffer
import okio.internal.commonToUtf8String

val CHOP_SIZE: Int = 8192

open class DeserializationException(msg: String, shortMsg: String? = null) : LibNexaException(msg, shortMsg, ErrorSeverity.Expected)


enum class SerializationType
{
    NETWORK, DISK, HASH, SCRIPTHASH, UNKNOWN
}

class Codec
{
    companion object
    {
        /** Convert a byte array to a string using 64 values per character.  This format is used in Bitcoin to convert message signatures to a printable representation */
        fun encode64(data: ByteArray): String = libnexa.encode64(data)

        /** Convert an encoded string (using 64 values per character) to bytes.  This format is used in Bitcoin to convert message signatures to a printable representation */
        fun decode64(data: String): ByteArray = libnexa.decode64(data)
    }
}

/** This class behaves like a function and its sole purpose is to wrap a bytearray into another class so that when we serialize it we know not to include the length */
class exactBytes(val data: ByteArray)

/** This class behaves like a function and its sole purpose is to wrap a bytearray into another class so that when we serialize it we know to include the length */
class variableSized(val data: ByteArray)

// Base class allowing derived classes to specify BCH serialization capability
// For deserialization, create a constructor that accepts a BCHSerialized object
interface BCHserializable
{
    // Store this object into a BCHserialized stream and return it
    fun BCHserialize(format: SerializationType = SerializationType.UNKNOWN): BCHserialized
    // Load this object with data from the passed stream
    fun BCHdeserialize(stream: BCHserialized): BCHserialized
}

// This class serializes or deserializes bitcoin cash data
class BCHserialized(_format: SerializationType) // = SerializationType.UNKNOWN)
{
    var data = MutableList<ByteArray>(0, { _ -> ByteArray(0) })
    var format: SerializationType = _format
    var deserPos: Int = 0
    var size: Int = 0

    constructor(contents: ByteArray, _format: SerializationType) : this(_format) // =SerializationType.UNKNOWN): this()
    {
        format = _format
        data.add(contents)
        size = contents.size
    }

    constructor(contents: MutableList<ByteArray>, _format: SerializationType) : this(_format)
    {
        add(contents)
    }

    constructor(contents: BCHserialized) : this(contents.format)
    {
        add(contents)
    }

    /** Return how many bytes are left to be read */
    fun availableBytes(): Int = size - deserPos

    // Converts the internal representation to a single ByteArray
    fun flatten(): ByteArray
    {
        if (data.size > 1)
            data = MutableList<ByteArray>(1, { _ -> data.join() })
        if (data.size == 0) return ByteArray(0)

        if (deserPos > CHOP_SIZE)
        {
            // reclaim memory by chopping off data that we've already consumed
            data[0] = data[0].sliceArray(deserPos..(data[0].size - 1))
            deserPos = 0
            size = data[0].size
        }
        return data[0]
    }

    // Serialize a 32 bit integer into this object
    fun addUint8(num: Int): BCHserialized
    {
        var data = ByteArray(1)
        data[0] = num.toByte()
        return add(data)
    }

    // Serialize a 32 bit integer into this object
    fun addUint32(num: Long): BCHserialized
    {
        var data = ByteArray(4)
        var cur = num
        for (i in 0..3)
        {
            data[i] = (cur and 0xFF).toByte()
            cur = cur ushr 8
        }
        return add(data)
    }

    // Serialize a 32 bit integer into this object
    fun addInt32(num: Long): BCHserialized
    {
        var data = ByteArray(4)
        var cur = num
        for (i in 0..3)
        {
            data[i] = (cur and 0xFF).toByte()
            cur = cur ushr 8
        }
        return add(data)
    }

    /** Serialize a 32 bit integer into this object */
    fun addInt32(num: Int): BCHserialized
    {
        var data = ByteArray(4)
        var cur = num
        for (i in 0..3)
        {
            data[i] = (cur and 0xFF).toByte()
            cur = cur ushr 8
        }
        return add(data)
    }

    /** Serialize a 64 bit integer into this object */
    fun addUint64(num: ULong): BCHserialized = addInt64(num.toLong())
    /** Serialize a 64 bit integer into this object */
    fun addUint64(num: Long): BCHserialized = addInt64(num)

    /** Serialize a 64 bit integer into this object */
    fun addInt64(num: Long): BCHserialized
    {
        var data = ByteArray(8)
        var cur = num
        for (i in 0..7)
        {
            data[i] = (cur and 0xFF).toByte()
            cur = cur ushr 8
        }
        return add(data)
    }

    /** Append a serialized object to this serialized object */
    fun add(contents: BCHserializable) = add(contents.BCHserialize(this.format))

    /** Append a serialized object to this serialized object
     * Network formats do not support this, but occasionally we serialize into a network format to store as a binary blob on disk
     * */
    fun addNullable(contents: BCHserializable?): BCHserialized
    {
        if (contents == null) addUint8(0)
        else
        {
            addUint8(1)
            add(contents)
        }
        return this
    }

    /** Append a serialized object to this serialized object */
    fun add(contents: BCHserialized): BCHserialized
    {
        assert((this.format == SerializationType.UNKNOWN) || (contents.format == SerializationType.UNKNOWN) || (contents.format == this.format))
        if (this.format == SerializationType.UNKNOWN) this.format = contents.format  // grab a known format if I don't know mine
        // Need to copy anyway, might as well concat while doing so
        val content = contents.data.join()
        if (content.size == 0) return this
        this.add(content)
        this.size += content.size
        // add without compaction
        // contents.data.forEach { this.data.add(it); this.size += it.size }
        return this
    }

    /** Append a serialized object to this serialized object */
    fun plusAssign(contents: BCHserialized) = add(contents)

    fun add(contents: MutableList<ByteArray>): BCHserialized
    {
        contents.forEach { this.data.add(it); this.size += it.size }
        return this
    }

    // Append the serialization of the parameter into this object and return this object
    fun addlist(b: MutableList<out BCHserializable>): BCHserialized
    {
        add(BCHserialized.compact(b.size.toLong()))
        for (i in b)
        {
            add(i)
        }
        return this
    }

    // add some new data on to the end of this buffer
    fun add(newdata: ByteArray): BCHserialized
    {
        data.add(newdata)
        size += newdata.size
        return this
    }

    // add some new data on to the end of this buffer
    fun add(newdata: ByteArray, len: Int): BCHserialized
    {
        if (newdata.size == len) data.add(newdata)
        else data.add(newdata.sliceArray(0 until len))
        size += len
        return this
    }


    fun add(b: Boolean): BCHserialized
    {
        var data = byteArrayOf(0)
        if (b) data[0] = 1
        add(data)
        return this
    }


    // Start deserialization at a specified position (by default, restart)
    fun deserializeReset(pos: Int = 0)
    {
        deserPos = pos
    }

    /** Deserialize certain number of bytes as a ByteArray */
    fun debytes(len: Long): ByteArray
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val tmp = deserPos
        deserPos += len.toInt()
        try
        {
            return data[0].sliceArray(IntRange(tmp, (tmp + len - 1).toInt()))
        }
        catch (e: IndexOutOfBoundsException)
        {
            throw DeserializationException(e.toString())
        }
    }

    fun deboolean(): Boolean
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        val v: Byte = buf[deserPos]
        deserPos += 1
        return v != 0.toByte()
    }

    /** Deserialize an 8 bit integer */
    fun deuint8(): Int
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        val ret: Int = buf[deserPos].toPositiveInt()
        deserPos += 1
        return ret
    }
    /** Deserialize an 8 bits (as a signed value) */
    fun debyte(): Byte
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        val ret: Byte = buf[deserPos]
        deserPos += 1
        return ret
    }

    /** Deserialize a character */
    fun dechar(): Char
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        val ret: Byte = buf[deserPos]
        deserPos += 1
        return Char(ret.toInt())
    }

    /** Deserialize a 16 bit integer */
    fun deuint16(): Int
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        var ret: Int = 0
        for (i in 1 downTo 0)
        {
            val piece = buf[deserPos + i].toPositiveInt()
            ret = (ret shl 8) or piece

        }
        deserPos += 2
        return ret
    }

    /** Deserialize a 16 bit BigEndian unsigned integer */
    fun deBigEndianUint16(): UInt
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        var ret: UInt = (buf[deserPos].toUByte().toUInt() shl 8) + buf[deserPos + 1].toUByte()
        deserPos += 2
        return ret
    }

    /** Deserialize a 32 bit integer */
    fun deint32(): Int
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        var ret: Int = 0
        for (i in 3 downTo 0)
        {
            val piece = buf[deserPos + i].toPositiveInt()
            ret = (ret shl 8) or piece

        }
        deserPos += 4
        return ret
    }

    /** Deserialize a 32 bit unsigned integer -- must be returned as a long since kotlin integers are signed */
    fun deuint32(): Long
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        var ret: Long = 0
        for (i in 3 downTo 0)
        {
            val piece = buf[deserPos + i].toPositiveLong()
            ret = (ret shl 8) or piece

        }
        deserPos += 4
        return ret
    }

    // Deserialize a 64 bit unsigned integer
    fun deuint64(): Long
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        var ret: Long = 0
        for (i in 7 downTo 0)
        {
            val piece = buf[deserPos + i].toPositiveLong()
            ret = (ret shl 8) or piece

        }
        deserPos += 8
        return ret
    }

    /** Deserialize a 64 bit signed integer */
    fun deint64(): Long = deuint64()

    fun deuint256(): BigInteger
    {
        val tmp = debytes(32)
        tmp.reverse()
        return BigInteger.fromByteArray(tmp, sign = Sign.POSITIVE)  // converts from a big-endian byte array
    }

    fun addUint256(num: BigInteger): BCHserialized
        {
            val data = num.toByteArray()  // Note that this is not necessarily 32 bytes -- its the smallest # of bytes that the # fits in
            data.reverse()
            add(data)  // append the actual number
            add(ByteArray(32-data.size))  // add any MSB 0s to make it 32 bytes
            return this
        }

    // Deserialize a Bitcoin-style "compact" integer
    fun decompact(): Long
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        var sz: Int = buf[deserPos].toPositiveInt()
        deserPos++
        if (sz < 253)
        {
            return sz.toLong()
        }
        if (sz == 253)
        {
            return deuint16().toLong()
        }
        if (sz == 254)
        {
            return deuint32().toLong()
        }
        else return deuint64()
    }

    fun devarint(): Long
    {
        var ret = 0L
        while (true)
        {
            var chData: Long = deuint8().toLong()
            ret = (ret shl 7) or (chData and 0x7f)
            if (chData >= 0x80)
            {
                ret++;
            }
            else
            {
                return ret;
            }
        }
    }

    fun addVarint(num: Long): BCHserialized
    {
        var tmp = ByteArray((8*8 + 6)/7)
        var n = num
        var pos:Int = 0
        while (true)
        {
            tmp[pos] = ((n and 0x7F) or ( if (pos != 0) 0x80L else 0L)).toByte()
            if (n <= 0x7F) break
            n = (n shr 7) - 1
            pos += 1
        }
        // Now write it backwards
        val tmp1:ByteArray = tmp.slice(pos downTo 0).toByteArray()
        add(tmp1)
        return this
    }

    fun deString(): String
    {
        val length = decompact()
        val ba = debytes(length)
        //val buf = Buffer()
        //buf.write(ba)
        return ba.decodeUtf8()
        //return ba.commonToUtf8String()
    }

    fun denullString(): String?
    {
        val length = decompact()
        if (length == 0L) return null
        //return debytes(length).commonToUtf8String()
        val buf = Buffer()
        buf.write(debytes(length))
        return buf.readUtf8()
    }

    /** Deserialize a list of some object.  You must pass a factory function that takes a buffer
    and returns an instance of the object (consuming some of the buffer) */
    fun <T> delist(Tfactory: (BCHserialized) -> T): MutableList<T>
    {
        val len = decompact().toInt()
        var ret = MutableList<T>(len, { _ -> Tfactory(this) })
        return ret
    }

    /** Deserialize a map of key value pair objects.  You must pass 2 factory functions that each take a buffer
    and returns an instance of the key or value object (consuming some of the buffer) */
    fun <K, V> demap(Kfactory: (BCHserialized) -> K, Vfactory: (BCHserialized) -> V): MutableMap<K, V>
    {
        val len = decompact().toInt()
        var ret: MutableMap<K, V> = mutableMapOf()
        var count = 0
        while (count < len)
        {
            count += 1
            val k = Kfactory(this)
            val v = Vfactory(this)
            ret[k] = v
        }
        return ret
    }


    /** Deserialize a variable length array (vector) of bytes
     * Note that the opposite of this (serializing a array of bytes) is ambiguous
     * Do you want to serialize the exact bytes passed, or serialize an array of bytes
     * (that is, indicate the length in the serialization).
     * To serialize an array of bytes (the opposite of this) use the "variableSized" object like this:
     * satoshiSerializedObject + variableSized(yourByteArray)
     * */
    fun deByteArray(): ByteArray
    {
        val len = decompact().toLong()
        var ret = debytes(len)
        return ret
    }

    /** calculate the double SHA256 of the data in this object */
    fun hash256():Hash256
    {
        return Hash256(libnexa.hash256(this.flatten()))
    }
    /** calculate the SHA256 of the data in this object */
    fun sha256():Hash256
    {
        return Hash256(libnexa.sha256(this.flatten()))
    }

    companion object
    {
        //val SER_NETWORK = (1 shl 0)
        //val SER_DISK = (1 shl 1)
        //val SER_GETHASH = (1 shl 2)

        fun <T> list(lst: List<T>, Tserializer: (T) -> BCHserialized, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(lst.size, format)
            for (i in lst)
            {
                ret.add(Tserializer(i))
            }
            return ret
        }

        fun list(lst: List<BCHserializable>, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(lst.size, format)
            for (i in lst)
            {
                ret.add(i.BCHserialize(format))
            }
            return ret
        }

        /** Serializes a map object with the provided key and value serialization functions */
        fun <K, V> map(mp: Map<K, V>, Kserializer: (K) -> BCHserialized, Vserializer: (V) -> BCHserialized, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(mp.size, format)
            for (i in mp)
            {
                ret.add(Kserializer(i.key))
                ret.add(Vserializer(i.value))
            }
            return ret
        }

        /** Serializes a map object that contains serializable keys and values */
        fun map(mp: Map<out BCHserializable, BCHserializable>, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(mp.size, format)
            for (i in mp)
            {
                ret.add(i.key.BCHserialize(format))
                ret.add(i.value.BCHserialize(format))
            }
            return ret
        }

        /** Serializes a map object that contains serializable keys and values */
        fun mutableMap(mp: MutableMap<out BCHserializable, out BCHserializable>, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(mp.size, format)
            for (i in mp)
            {
                ret.add(i.key.BCHserialize(format))
                ret.add(i.value.BCHserialize(format))
            }
            return ret
        }

        // Serialize into a 32 bit integer
        fun int32(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var data = ByteArray(4)
            var cur = num
            for (i in 0..3)
            {
                data[i] = (cur and 0xFF).toByte()
                cur = cur ushr 8;
            }
            return BCHserialized(data, format)
        }

        fun int32(num: Int, format: SerializationType = SerializationType.UNKNOWN): BCHserialized = int32(num.toLong(), format)

        // Serialize into a 16 bit integer
        fun uint16(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var data = ByteArray(2)
            var cur = num
            for (i in 0..1)
            {
                data[i] = (cur and 0xFF).toByte()
                cur = cur ushr 8;
            }
            return BCHserialized(data, format)
        }

        fun boolean(b: Boolean): BCHserialized
        {
            var data = byteArrayOf(0)
            if (b) data[0] = 1
            return BCHserialized(data, SerializationType.UNKNOWN)
        }

        /** Serialize into an unsigned 8 bit number */
        fun uint8(num: Long): BCHserialized
        {
            var data = ByteArray(1)

            data[0] = (num and 0xFF).toByte()
            return BCHserialized(data, SerializationType.UNKNOWN)
        }
        /** Serialize into an unsigned 8 bit number */
        fun uint8(num: Int) = uint8(num.toLong())

        /** Serialize into an 8 bit number */
        fun uint8(num: Byte, format: SerializationType = SerializationType.UNKNOWN): BCHserialized = BCHserialized(byteArrayOf(num), format)

        /** Serialize into a 32 bit unsigned number */
        fun uint32(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            return int32(num, format)
        }

        fun uint256(num: BigInteger, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            val data = num.toByteArray()
            data.reverse()
            val ret = BCHserialized(data, format).add(ByteArray(32-data.size))
            return ret
        }

        /** Serialize a bitcoin-style "compact" integer */
        fun compact(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            if (num < 253)
            {
                return uint8(num.toByte(), format)
            }
            if (num < 0x10000)
            {
                return uint8(253.toByte(), format) + uint16(num)
            }
            if (num < 0x100000000)
            {
                return uint8(254.toByte(), format) + uint32(num)
            }
            return uint8(255.toByte()).addInt64(num)
        }

        fun compact(num: Int, format: SerializationType = SerializationType.UNKNOWN): BCHserialized = compact(num.toLong(), format)

        fun varint(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            return BCHserialized(format).addVarint(num)
        }
    }

    /** Return a hex string of this serialized data */
    fun toHex(): String
    {
        val ret = StringBuilder()
        for (array in data)
        {
            for (b in array)
            {
                //ret.append(String.format("%02x", b))
                ret.append(HEX_CHARS[b.toPositiveInt() shr 4])
                ret.append(HEX_CHARS[b.toPositiveInt() and 0xf])
            }
        }
        return ret.toString()
    }

    @Deprecated("Use toHex")
    fun ToHex(): String = toHex()

    @Suppress("UNUSED_PARAMETER")
    @Deprecated("Ambiguous: Use '+ exactBytes(array)' to append the ByteArray directly into the serialization, or '+ variableSized(array)' to append as a sized array", level = DeprecationLevel.ERROR)
    operator fun plus(b: ByteArray): BCHserialized
    {
        assert(false);
        return this
    }
    /** Return a new serialization object that contains this and the passed parameter */
    operator fun plus(b: exactBytes): BCHserialized
    {
        var ret = BCHserialized(this)
        ret.add(b.data)
        return ret
    }

    /** Return a new serialization object that contains this and the passed parameter serialized as a 64 bit number */
    operator fun plus(num: Long): BCHserialized = plus(num.toULong())

    /** Return a new serialization object that contains this and the passed parameter serialized as a 64 bit number */
    operator fun plus(num: ULong): BCHserialized
    {
        var data = ByteArray(8)
        var cur = num
        for (i in 0..7)
        {
            data[i] = (cur and 0xFF.toULong()).toByte()
            cur = cur shr 8;
        }
        var ret = BCHserialized(this)
        ret.data.add(data)
        return ret
    }

    /** Return a new serialization object that contains this and the passed parameter serialized as a 32 bit number */
    operator fun plus(num: Int): BCHserialized
    {
        return BCHserialized(this) + BCHserialized.int32(num.toLong(), this.format)
    }

    /** Return a new serialization object that contains this and the passed parameter serialized as a 32 bit number */
    operator fun plus(num: UInt): BCHserialized
    {
        return BCHserialized(this) + BCHserialized.uint32(num.toLong(), this.format)
    }

    /** Return a new serialization object that contains this and the passed parameter serialized as a 32 bit number */
    operator fun plus(value: Boolean): BCHserialized
    {
        return BCHserialized(this) + BCHserialized.uint8(if (value == true) 1 else 0, this.format)
    }

    /** Construct containing a serialized string */
    constructor(s: String, format: SerializationType) : this(format)
    {
        this += s
    }

    /** Append a serialized string to this object */
    operator fun plusAssign(s: String): Unit
    {
        add(s)
    }

    /** Append a serialized string to this object */
    fun add(s: String?): BCHserialized
    {
        if (s == null)
            add(compact(0))
        else
        {
            val ba = s.encodeUtf8()
            add(compact(ba.size))
            add(ba)
        }
        return this
    }

    /** Append a single character to this object */
    fun add(c: Char): BCHserialized
    {
        data.add(byteArrayOf(c.code.toByte()))
        size += 1
        return this
    }

    /** Return a new serialization object that contains this and the passed parameter serialized */
    operator fun plus(s: String?): BCHserialized
    {

        var ret = BCHserialized(this)
        if (s==null) ret.plusAssign("")
        else ret.plusAssign(s)
        return ret
    }

    /** Return a new serialization object that contains this and the passed parameter serialized */
    operator fun plus(s: variableSized): BCHserialized
    {
        var ret = BCHserialized(this)
        ret.add(compact(s.data.size))
        ret.add(s.data)
        return ret
    }

    fun add(s: variableSized): BCHserialized
    {
        add(compact(s.data.size))
        add(s.data)
        return this
    }

    /** Return a new serialization object that contains this and the passed parameter */
    operator fun plus(b: BCHserialized): BCHserialized
    {
        // Get the format from the incoming object if it has one and I don't
        if (this.format == SerializationType.UNKNOWN) this.format = b.format
        assert((b.format == SerializationType.UNKNOWN) || (b.format == this.format))
        var ret = BCHserialized(this)
        ret.add(b)
        return ret
    }

    /** Return a new serialization object that contains this and the passed parameter */
    operator fun plus(b: BCHserializable): BCHserialized = this + b.BCHserialize(format)

    /** Return a new serialization object that contains this and the serialization of an array of serializable objects */
    operator fun plus(b: Array<out BCHserializable>): BCHserialized
    {
        var ret = BCHserialized(this)
        ret = ret + BCHserialized.compact(b.size.toLong())
        for (i in b)
        {
            ret = ret + i.BCHserialize(format)
        }
        return ret
    }

    // Return a new serialization object that contains this and the serialization of
    // a MutableList of serializable objects
    operator fun plus(b: MutableList<out BCHserializable>): BCHserialized
    {
        var ret = BCHserialized(this)
        ret = ret + BCHserialized.compact(b.size.toLong())
        for (i in b)
        {
            ret = ret + i.BCHserialize(format)
        }
        return ret
    }

    operator fun plus(b: Byte): BCHserialized
    {
        return BCHserialized(this) + BCHserialized.uint8(b, this.format)
    }


}

/** Deserialize any object that is nullable, but prepending a byte indicating whether this object is null or not.
 * This API should only be used for DISK serialization because this nullable technique is not part of the network protocol.
 * However, in one case we store to disk the network serialization of an object (if it exists).
 * */
fun <T : BCHserializable> BCHserialized.deNullable(Tfactory: (BCHserialized) -> T): T?
{
    if (deuint8() == 0) return null
    val ret = Tfactory(this)
    return ret
}

