package org.nexa.libnexakotlin
import com.ionspin.kotlin.bignum.decimal.BigDecimal
import com.ionspin.kotlin.bignum.decimal.DecimalMode
import com.ionspin.kotlin.bignum.decimal.toJavaBigDecimal
import java.net.InetAddress
import java.nio.charset.Charset

import java.nio.charset.StandardCharsets
import java.text.DecimalFormat
import java.text.NumberFormat
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.Locale
import java.lang.String as JString


actual fun ByteArray.decodeUtf8():String
{
    // this.decodeToString()
    return String(JString(this, StandardCharsets.UTF_8).toCharArray())
}

/** Convert a string into a utf-8 encoded byte array. */
actual fun String.encodeUtf8():ByteArray
{
    return this.toByteArray(Charset.defaultCharset())
    //return this.encodeToByteArray()
}


/** convert a domain name into an address
 * @return The resolved name.  The type of the resolution e.g. ipv4, ipv6, tor, is implied by its length. */
actual fun resolveDomain(domainName: String, port: Int?): List<ByteArray>
{
    var dn = domainName
    if (dn.length == 0) return listOf()
    if (dn[0] == '/')
    {
        dn = dn.drop(1)
    }
    if (dn.contains(':'))
    {
        dn = dn.split(':')[0]
    }
    val addrs = InetAddress.getAllByName(dn).toMutableSet()
    if (addrs.size == 0) throw UnknownHostException("$domainName parsed into $dn and not found")
    return addrs.map { it.address }
}

actual class DecimalFormat actual constructor(fmtSpec: String)
{
    val fmt: DecimalFormat = DecimalFormat(fmtSpec)
    actual fun format(num: BigDecimal): String
    {
        val jnum = num.toJavaBigDecimal()
        return fmt.format(jnum)
    }
}

actual fun epochToDate(epochSeconds: Long): String
{
    val fmt = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withZone(ZoneId.systemDefault())
    val epochSec = Instant.ofEpochSecond(epochSeconds)
    return fmt.format(epochSec)
}

fun BigDecimal.Companion.fromJavaBigDecimal(bd: java.math.BigDecimal, decimalMode: DecimalMode?=null):BigDecimal
{
    return BigDecimal.parseStringWithMode(bd.toPlainString(), decimalMode)
}

fun ChainSelector.decimalMode(): DecimalMode
{
    return currencyMath
}

actual fun String.toCurrency(chainSelector: ChainSelector?): BigDecimal
{
    val nf = NumberFormat.getInstance(Locale.getDefault()) as java.text.DecimalFormat
    nf.setParseBigDecimal(true)
    val jbd = (nf.parseObject(this) as java.math.BigDecimal)
    return BigDecimal.fromJavaBigDecimal(jbd, chainSelector?.decimalMode() ?: currencyMath)
}

/*
fun BigDecimal.setCurrency(chainSelector: ChainSelector): BigDecimal
{
    when (chainSelector)
    {
        ChainSelector.BCHTESTNET, ChainSelector.BCHREGTEST, ChainSelector.BCH -> setScale(uBchDecimals)
        ChainSelector.NEXA, ChainSelector.NEXAREGTEST, ChainSelector.NEXATESTNET -> setScale(NexDecimals)
    }
    return this
}

 */

val ChainSelector.currencyDecimals: Int
    get()
    {
        return when (this)
        {
            ChainSelector.BCHTESTNET, ChainSelector.BCHREGTEST, ChainSelector.BCH -> uBchDecimals
            ChainSelector.NEXA, ChainSelector.NEXAREGTEST, ChainSelector.NEXATESTNET -> NexaDecimals
        }
    }