// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin

import io.ktor.network.sockets.SocketTimeoutException
import io.ktor.network.sockets.isClosed
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.coroutines.runBlocking
import org.nexa.threads.Gate
import org.nexa.threads.LeakyBucket
import org.nexa.threads.Mutex
import org.nexa.threads.Thread
import org.nexa.threads.iThread
import org.nexa.threads.millisleep
import kotlin.concurrent.Volatile
import kotlin.coroutines.CoroutineContext
import kotlin.time.ExperimentalTime

private val LogIt = GetLog("BU.cnxnmgr")

/** Indicate why we chose not to connect to a node */
enum class BanReason
{
    NOT_BANNED,
    TOO_SLOW,
    EARLY_DISCONNECT,
    CANNOT_CONNECT,  // Server is probably down, wrong port, or never existed
    BAD_DATA,
    INCORRECT_BLOCKCHAIN
}

/** Data related to why we banned a node */
class BanData(val reason: BanReason)
{
    val start = millinow()
}


//? The CnxnMgr handles discovery, connection, and maintenance of all connections into the Peer-2-Peer network
//  This allows higher layers to treat peers as a grab bag of nodes, ignoring issues like reconnecting, periodically
//  cycling connections, and searching for new nodes.
@cli(Display.Simple, "Handles connections to the blockchain network")
abstract class CnxnMgr(val netType: ChainSelector)
{
    /** returns the blockchain's name (for logging/debug) */
    @cli(Display.Simple, "blockchain name")
    abstract val chainName: String

    /** Returns a random connection from the connection manager's node set */
    abstract suspend fun getp2p(): P2pClient

    /** Returns a random connection from the connection manager's node set */
    @cli(Display.Simple, "Get a p2p connection")
    abstract fun getNode(block: Boolean = true): P2pClient

    /** Returns a random connection from the connection manager's node set, but not one of these.
     * Returns null if no connections left */
    abstract fun getAnotherNode(notThese: Set<P2pClient>): P2pClient?

    /** Returns a random connection from the connection manager's node set */
    @cli(Display.Simple, "Get an electrum connection")
    abstract fun getElectrum(): ElectrumClient

    /** Returns a random connection from the connection manager's node set, but not one of these.
     * Returns null if no connections left */
    abstract fun getAnotherElectrum(notThese: Set<ElectrumClient>): ElectrumClient?

    /** Returns a random connection from the connection manager's node set, but not one of these.
     * Returns null if no connections left */
    //abstract suspend fun coGetAnotherNode(notThese: Set<BCHp2pClient>): BCHp2pClient?

    /** Returns the number of currently connected peers */
    @cli(Display.Simple, "Return the number of connected peers")
    @Deprecated("use .size")
    abstract fun numPeers(): Int

    /** Returns the number of currently connected peers */
    @cli(Display.Simple, "Return the number of connected peers")
    abstract val size: Int

    // It is better to use the connection getter functions, rather than accessing this list.
    // This is meant for display, health and statistics, rather than access
    @cli(Display.Simple, "Return list of connected peers")
    abstract val p2pCnxns: List<P2pClient>

    // It is better to use the connection getter functions, rather than accessing this list.
    // This is meant for display, health and statistics, rather than access
    @cli(Display.Simple, "Return list of connected peers")
    abstract val electrumCnxns: List<ElectrumClient>

    /** Returns a list of results of your function applied to each connection in this list.
     * Return null to not include this connection in the list.
     * Note that the underlying data structure is synchronized so your function is called with this lock taken */
    abstract fun <T> mapConnections(mapfilter: (P2pClient) -> T?): List<T>

    /** Returns true if we are connected to this node/port */
    abstract fun connectedTo(ip: String, port: Int): Boolean

    /** wake up and analyze the health of connections, especially the optional passed connection */
    abstract suspend fun coReport(problemCnxn: P2pClient? = null)

    /** wake up and analyze the health of connections, especially the optional passed connection */
    abstract fun report(problemCnxn: P2pClient? = null)

    /** Add a potential node into the list of possible connections
     * @param ip IP address or domain name of the node
     * @param port TCP port of the node
     * @param priority higher number is higher priority (try it first).
     * @param keepForever do not delete this entry, even if the connection fails
     */
    @cli(Display.Simple, "Add a potential p2p node")
    abstract fun add(ip: String, port: Int, priority:Int = 0, keepForever: Boolean = false)


    /** Exclusively connect to a single node.  All other existing connections will be dropped
     * @param ip IP address or domain name of the node, or null to remove any exclusive node
     * @param port TCP port of the node (default or -1 uses the default port for this blockchain)
     */
    @cli(Display.Simple, "Exclusively connect to this node")
    abstract fun exclusiveNodes(nodes: Set<String>?)

    /** Prefer a particular node.  An existing connections may be dropped to make room
     * @param ip IP address or domain name of the node, or null to remove any exclusive node
     * @param port TCP port of the node
     */
    @cli(Display.Simple, "Prefer this node")
    abstract fun preferNodes(nodes: Set<String>?)

    /** Start connection manager operation */
    abstract fun start()

    /** Restart connection manager operation after suspend may have killed threads */
    abstract fun restart()

    /** Stop connection manager operation */
    abstract fun stop()

    /** clear all incoming messages, drop all connections */
    abstract fun clear()

    /** clear all bans (if param is null) or clear any ban of the specified IP address */
    @cli(Display.Simple, "Clear all banned nodes")
    abstract fun clearBanned(ip: String? = null)


    /** tell all nodes, including future connections, that we are only interested in things that match this bloom filter.
     * To enable multiple wallets to use the same Blockchain, do NOT use this API directly.  Instead use the blockchain API */
    abstract fun setAddressFilter(bloom: ByteArray, onBloomInstalled: (() -> Unit)?)

    // which bloom filter has been installed (increments every time setAddressFilter is called)
    var bloomCount: Int = -1

    // Add handler callbacks for incoming data
    // The connection manager will try to (but not guarantee) eliminate duplicates

    /** Add a callback to handle incoming transactions */
    abstract fun addTxHandler(cb: (List<out iTransaction>) -> Unit)

    /** Add a callback to handle incoming blocks -- downcast the parameter to either BCHBlock or MerkleBlock */
    abstract fun addBlockHandler(cb: (List<out iBlockHeader>) -> Unit)

    /** Add a callback to handle incoming incomplete MerkleBlocks */
    abstract fun addPartialBlockHandler(cb: (List<out iMerkleBlock>) -> Unit)

    /** Add a callback to handle incoming inventory (right now, only blocks, the CnxnMgr auto-handles tx INVs) */
    abstract fun addInvHandler(cb: (List<Inv>) -> Unit)

    /** Add a callback to handle incoming block headers */
    abstract fun addBlockHeadersHandler(cb: (List<out iBlockHeader>) -> Unit)

    /** Send a transaction to one node */
    @cli(Display.Simple, "Send transaction to one (random) node")
    abstract suspend fun sendTransaction(tx: ByteArray)

    /* Broadcast a transaction to all connected nodes */
    @cli(Display.Simple, "Send transaction to all connected nodes")
    abstract fun broadcastTransaction(tx: ByteArray)

    /** Broadcast a set of transactions to the network */
    abstract fun sendTransactions(txes: List<ByteArray>)

    /** report a node as having a problem */
    abstract fun misbehaving(cnxn: P2pClient, reason: BanReason, count: Int = 100)

    /** If the connection state changes, this function will be called */
    var changeCallback: ((CnxnMgr, P2pClient?) -> Unit)? = null


    /** Derived classes must override and implement their continuous connection management.  This function does not return until stop() is called.
     * Do not call directly -- called by start() */
    protected abstract fun run()
}


abstract class CommonCnxnMgr(netType: ChainSelector) : CnxnMgr(netType)
{
    var processingThread: iThread? = null

    protected val coCtxt: CoroutineContext = newFixedThreadPoolContext(4, "ConnectionManager")
    protected val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)
    protected val cocond = CoCond<Boolean>(coScope)

    val wakingDelay = Gate()
    val OUT_OF_NODES_DELAY = 200.toLong()
    val SIMULTANEOUS_CNXN_INIT = 5  //?< Try this many initial connection attempts simultaneously

    /** True if the connection manager should stop operating & clean up any threads */
    @Volatile
    var done = false

    /** Tell peers to only be interested in things that match this bloom */
    var addressBloomFilter: ByteArray? = null

    /** If a message of a certain type comes in, call this callback with the message for processing */
    var txCallback: ((List<out iTransaction>) -> Unit)? = null

    /** a block has arrived.  This could be a BCHBlock or a MerkleBlock */
    var blockCallback: ((List<iBlockHeader>) -> Unit)? = null

    /** A partially complete MerkleBlock arrived */
    var partialBlockCallback: ((List<iMerkleBlock>) -> Unit)? = null

    /** a block header has arrived */
    var headersCallback: ((List<out iBlockHeader>) -> Unit)? = null

    /** a notification of an object's existence has arrived */
    var invCallback: ((List<Inv>) -> Unit)? = null

    /** All current connections */
    val cnxnLock = Gate()
    @Volatile
    var cnxns: MutableList<P2pClient> = mutableListOf()
    @Volatile
    var ecnxns: MutableList<ElectrumClient> = mutableListOf()

    /* offer these connection lists to the API */
    override val p2pCnxns: List<P2pClient>
        get()
        {
            return synchronized(cnxnLock)
            {
                cnxns.toList()
            }
        }

    override val electrumCnxns: List<ElectrumClient>
        get()
        {
            return synchronized(cnxnLock)
            {
                ecnxns.toList()
            }
        }


    /** if this is not null, try all communications to this connection first */
    @Volatile
    var preferredCnxn: P2pClient? = null

    /** Don't reconnect to these nodes */
    val bannedLock = Mutex()
    val bannedNodes: MutableMap<String, BanData> = mutableMapOf()

    /** used to offer a different node each time to callers */
    protected var cnxnRoundRobin = 0

    /** supply electrum server ip and ports if we need them */
    var getElectrumServerCandidate: ((ChainSelector) -> IpPort)? = null


    /** Returns a random connection from the connection manager's node set */
    override fun getElectrum(): ElectrumClient
    {
        throw ElectrumNoNodesException()
    }

    /** Returns a random connection from the connection manager's node set, but not one of these.
     * Returns null if no connections left */
    override fun getAnotherElectrum(notThese: Set<ElectrumClient>): ElectrumClient?
    {
        throw ElectrumNoNodesException()
    }

    /** Returns a list of connected peers.  This API works simultaneously as map (transformation) and filter functions and calls the passed function on every item in the connection list.
     * @param mapfilter: Return T to add it to the returned list, return null skip
     * @return List of whatever your transformation/filter function returned, skipping null */
    override fun <T> mapConnections(mapfilter: (P2pClient) -> T?): List<T>
    {
        val ret = mutableListOf<T>()
        synchronized(cnxnLock)
        {
            for (c in cnxns)
            {
                mapfilter(c)?.let { ret.add(it) }
            }
        }
        return ret
    }

    /** remove a node (or all nodes if ip==null) from the ban table */
    override fun clearBanned(ip: String?)
    {
        if (ip == null) bannedNodes.clear()
        else bannedNodes.remove(ip)
    }

    override fun clear()
    {
        LogIt.warning(sourceLoc() + ": CLOSE -- Disconnecting everything.")
        synchronized(cnxnLock)
        {
            for (c in cnxns)
                c.close()
            for (c in cnxns)
                c.clear()
            cnxns.clear()
        }
    }

    /** Returns true if we are connected to this node/port */
    override fun connectedTo(ip: String, port: Int): Boolean
    {
        var ret = false
        cnxnLock.lock {
            for (c in cnxns)
            {
                if ((c.port == port) && (c.name == ip)) { ret=true; break }
            }
        }
        return ret
    }

    /** Start the connection manager processing thread */
    override fun start()
    {
        restart()
    }

    override fun restart()
    {
        cnxnLock.synchronized {
            LogIt.info(sourceLoc() + chainToURI[netType] + ": restart connection manager requested")
            if (processingThread == null)  // || (processingThread?.state == Thread.State.TERMINATED))
            {
                LogIt.info(sourceLoc() + chainToURI[netType] + ": restart connection manager -- launching threads")
                done = false
                processingThread = Thread(chainName + "_cnxnMgr") { run() }
            }
        }
    }

    /** Stop the connection manager processing thread asynchronously */
    override fun stop()
    {
        done = true
    }

    /** wake up and analyze my connections, especially the optional passed connection */
    override suspend fun coReport(problemCnxn: P2pClient?)
    {
        //cnxnEvent.send(problemCnxn)
        wakingDelay.wake()
    }

    /** wake up and analyze my connections, especially the optional passed connection */
    override fun report(problemCnxn: P2pClient?)
    {
        wakingDelay.wake()
    }

    @Deprecated("use size")
    override fun numPeers(): Int = synchronized(cnxnLock) { cnxns.size.toInt() }
    override val size: Int
        get() = cnxns.size.toInt()

    override fun sendTransactions(txes: List<ByteArray>)
    {
        val node = getNode()

        for (tx in txes)
        {
            //LogIt.info(" Raw Tx:")
            //val hex = tx.toHex();
            // DEBUG
            //val decodedTx = Transaction(netType, BCHserialized(tx, SerializationType.NETWORK))
            //decodedTx.debugDump()
            //for (c in hex.chunked(900))
            //{
            //    LogIt.info(c)
            //}
            /*
                        if (tx.size < 100)  // TODO use consensus param
                        {
                            throw TransactionException("Transaction is too small at ${tx.size} bytes")
                        }
             */
            node.sendTx(tx)
        }
    }

    override suspend fun sendTransaction(tx: ByteArray)
    {
        val node = getNode()

        //LogIt.info(sourceLoc() + " " + node.logName + " " + "Sending tx: " + tx.toHex())
        /*
        if (tx.size < 100)  // TODO use consensus param
        {
            throw TransactionException("Transaction is too small at ${tx.size} bytes")
        }
         */
        node.sendTx(tx)
    }

    override fun broadcastTransaction(tx: ByteArray)
    {
        if (tx.size < 100)  // TODO use consensus param
        {
            throw TransactionException("Transaction is too small at ${tx.size} bytes")
        }
        synchronized(cnxnLock)
        {
            if (cnxns.size == 0)
            {
                LogIt.warning(sourceLoc() + " " + chainName + ": No available connections to broadcast tx")
            }
            else
            {
                for (c in cnxns)
                {
                    try
                    {
                        if (c.isAlive()) c.sendTx(tx)
                    }
                    catch (e: P2PException)  // Its possible that the connection is bad at that moment
                    {
                        LogIt.warning(sourceLoc() + ": " + e.toString())
                    }
                }
            }
        }
    }

    override fun addTxHandler(cb: (List<out iTransaction>) -> Unit)
    {
        txCallback = cb
    }

    /** Add a callback to handle incoming incomplete MerkleBlocks */
    override fun addPartialBlockHandler(cb: (List<out iMerkleBlock>) -> Unit)
    {
        partialBlockCallback = cb
    }

    override fun addBlockHandler(cb: (List<out iBlockHeader>) -> Unit)
    {
        blockCallback = cb
    }

    override fun addInvHandler(cb: (List<Inv>) -> Unit)
    {
        invCallback = cb
    }

    override fun addBlockHeadersHandler(cb: (List<out iBlockHeader>) -> Unit)
    {
        headersCallback = cb
    }

    override fun setAddressFilter(bloom: ByteArray, onBloomInstalled: (() -> Unit)?)
    {
        addressBloomFilter = bloom
        bloomCount += 1
        synchronized(cnxnLock)
        {
            if (cnxns.size == 0)
                onBloomInstalled?.invoke()
            else
            {
                val cnxnSnap = cnxns.toList()
                val can = CallAfterN(onBloomInstalled, cnxnSnap.size)
                for (c in cnxnSnap)
                {
                    launch {
                        try
                        {
                            c.sendBloomFilter(bloom, bloomCount)
                            delay(500)  // give time for the filter to get to the node, since the protocol does not ACK messages, there's not much else we can do
                        }
                        catch (e: P2PDisconnectedException)
                        {
                        }
                        can() // If we disconnect, then we still need to indicate that the filter is installed, because the idea is to wait to call this until its been installed on all CONNECTED nodes
                    }
                }
            }
        }
    }


    suspend fun processInvMessage(invs: MutableList<Inv>, node: P2pClient)
    {
        // for (fn in onHeadersCallback) fn(hdrs, node)
        val txes: MutableList<Inv> = mutableListOf()
        val blocks: MutableList<Inv> = mutableListOf()
        for (inv in invs)
        {
            // LogIt.info(sourceLoc() + " " + node.logName + ": received INV for " + inv.type + " " + inv.id.toHex())

            if (inv.type == Inv.Types.TX) txes.add(inv)
            if (inv.type == Inv.Types.BLOCK)
            {
                blocks.add(inv)
            }
        }

        try
        {
            if (txes.size > 0)
                node.sendGetData(txes)
            if (blocks.size > 0)
                invCallback?.invoke(blocks)
        }
        catch (e: P2PDisconnectedException)
        {
            LogIt.info(sourceLoc() + ": Disconnect while requesting data from INV")
        }
    }

    fun processTxMessage(txs: MutableList<out iTransaction>, node: P2pClient)
    {
        for (tx in txs)
        {
            LogIt.info(sourceLoc() + " " + node.logName + ": received TX " + tx.idem.toHex() + " contents: " + tx.toHex())
        }
        txCallback?.let { it(txs) }
    }

    @Suppress("UNUSED_PARAMETER")
    fun processBlockMessage(blocks: MutableList<out iBlock>, node: P2pClient)
    {
        var blksAsHeaders: MutableList<iBlockHeader> = mutableListOf()
        for (blk in blocks)
        {
            LogIt.info(sourceLoc() + " " + node.logName + ": received block " + blk.hash.toHex())
            blksAsHeaders.add(blk)
        }
        blockCallback?.let { it(blksAsHeaders) }
    }

    @Suppress("UNUSED_PARAMETER")
    fun processMerkleBlockMessage(blocks: MutableList<out iMerkleBlock>, node: P2pClient)
    {
        // LogIt.info("Process ${blocks.size} Merkle Blocks")
        for (m in blocks)
        {
            if ((m.txes.size > 0) or (m.txHashes.size > 0))  // Only log something if the block is interesting to me
                LogIt.info(sourceLoc() + " " + node.logName + ": Received MerkleBlock ${m.hash.toHex()} with ${m.txCount} tx containing ${m.txes} full tx and ${m.txHashes} hashes")
        }
        val readyBlksAsHeaders: MutableList<iBlockHeader> = mutableListOf()
        val unreadyBlks: MutableList<iMerkleBlock> = mutableListOf()
        for (blk in blocks)
        {
            if (blk.txHashes.size == 0)  // If we have all the tx (generally this happens when there are no interesting txes) then go ahead and pass the merkle block onwards now
            {
                //LogIt.info("merkle block is ready")
                readyBlksAsHeaders.add(blk)
            }
            else
                unreadyBlks.add(blk)
        }

        if (readyBlksAsHeaders.size > 0)
        {
            blockCallback?.let {
                //LogIt.info("have a block callback")
                it(readyBlksAsHeaders)
            } ?: LogIt.info("no block callback")
        }
        if (unreadyBlks.size > 0) partialBlockCallback?.let { launch { it(unreadyBlks) } }

    }

    @Suppress("UNUSED_PARAMETER")
    fun processHeaderMessage(hdrs: List<out iBlockHeader>, node: P2pClient)
    {
        headersCallback?.invoke(hdrs)
    }

    /** Loops looking for an alive connection, returns null if no connections are left  */
    fun grabAndCheckConnection(): P2pClient?
    {
        return synchronized(cnxnLock) {
            while (true)
            {
                if (cnxns.size == 0) return@synchronized null
                val idx = cnxnRoundRobin % cnxns.size
                val cnxn = cnxns[idx]
                cnxnRoundRobin += 1

                if (!cnxn.isAlive())
                {
                    LogIt.warning(sourceLoc() + " " + cnxn.logName + ": CLOSE -- Disconnecting, connection isn't alive")
                    cnxn.close()
                    cnxns.removeAt(idx)
                    report()
                }
                else
                {
                    return@synchronized cnxn
                }
            }
            return@synchronized null
        }
    }

    override fun getNode(block: Boolean): P2pClient
    {
        if (!block)
        {
            val c = grabAndCheckConnection()
            if (c == null) throw P2PNoNodesException()
            return c
        }
        else
        {
            var ret: P2pClient? = null
            cnxnLock.loopwhile({ ret == null }) {

                if (cnxns.size > 0)
                {
                    val c = grabAndCheckConnection()
                    if (c != null) ret = c
                }
            }
            return ret ?: throw P2PNoNodesException()
        }
    }

    // Returns a random connection from my list
    override suspend fun getp2p(): P2pClient
    {
        var ret: P2pClient? = null
        cnxnLock.loopwhile({ ret == null }) {
            preferredCnxn?.let { if (it.isAlive()) ret = it }
            if (cnxns.size > 0)
                ret = grabAndCheckConnection()
        }
        return ret!!  // It can't get out of the loopwhile unless ret != null
    }

    /** Returns a random connection from the connection manager's node set, but not one of these.
     * Returns null if no connections left */
    override fun getAnotherNode(notThese: Set<P2pClient>): P2pClient?
    {
        var tries = 0
        var ret: P2pClient? = null
        return cnxnLock.synchronized {
            val csize = cnxns.size
            preferredCnxn?.let { if (it.isAlive() && !notThese.contains(it)) return@synchronized it }
            while (tries < csize && ret == null) // tries < csize: I've tried everything in cnxns
            {
                tries += 1
                val c = grabAndCheckConnection()
                if ((c != null) && (!notThese.contains(c))) return@synchronized c // don't give it multiple times
            }
            null
        }
    }
}

/** This class tracks possible blockchain nodes -- we have learned about this node from some source but the data may not be accurate */
class PossibleConnection() : BCHserializable
{
    // This matches the format of the ADDR message
    var time: Long = 0.toLong();  // 32 bits on wire
    var services: Long = 0.toLong()  // 64 bits on wire
    var port: Int = 0.toInt() // Short on wire
    var ip: ByteArray = ByteArray(16) // 16 bytes on wire

    // Disk only
    var name: String = ""
    var priority: Int = 0  // connection attempt order (you may set)
    var keep: Boolean = false

    /** If the ipp string contains a port number in the format ip:port, then that port overrides the portp parameter */
    constructor(ipp: String, portp: Int, keepForever: Boolean = false) : this()
    {
        val (ipString, portInt) = splitIpPort(ipp, portp)
        LogIt.info("Possible Connection: $ipString $portInt")
        val inetAddr = resolveDomain(ipString, portInt)
        name = ipp
        ip = inetAddr.random()  // pick a random choice from the address options
        port = portInt
        keep = keepForever
    }

    // Serialization
    constructor(stream: BCHserialized) : this()
    {
        BCHdeserialize(stream)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        // Network serialization must match ADDR message
        var ret = BCHserialized(format) + services + exactBytes(ip) + BCHserialized.uint16(port.toLong())
        if (format == SerializationType.DISK)
        {
            ret.add(keep)
            ret.addInt64(time)
            ret.addInt32(priority)
            ret.add(name)
        }
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        // Network serialization must match ADDR message
        services = stream.deuint64()
        ip = stream.debytes(16)
        port = stream.deBigEndianUint16().toInt()
        if (stream.format == SerializationType.DISK)
        {
            keep = stream.deboolean()
            time = stream.deint64()
            priority = stream.deint32()
            name = stream.deString()
        }
        else
        {
            keep = false
            time = millinow()/1000
            priority = 0  // All choices coming from network are low priority
            name = ""
        }
        return stream
    }

    fun ipAsString(): String
    {
        /*
        val inet = InetAddress.getByAddress(ip)
        val nodeIp: String = inet.hostAddress!!
        return nodeIp
         */
        return ipAsString(ip)
    }

    fun endpoint(): String
    {
        return ipAsString() + ":" + port.toString()
    }
}

@cli(Display.Simple, "Manage connections into the blockchain network")
class MultiNodeCnxnMgr(override val chainName: String, netType: ChainSelector, seed: Array<String>) : CommonCnxnMgr(netType)
{
    val dataLock = Gate()
    val prospects: MutableMap<String, PossibleConnection> = mutableMapOf()
    /** What index in the sorted list of prospects are we on */
    var curProspect = 0
    /** once we get into 0 priority prospects, if the length of prospects is > this number then loop back to 0.
     * This stops us from trying more than a few unlikely connections (provided by ADDR messages) */
    var prospectLimit = 10

    /** schedule connection retries: we'll take 5 per retry, so this results in a try every 5 seconds on average, but you can build up
     * a max of 5 quick tries.
     */
    val reconnectTimer = LeakyBucket(25, 1, 25)

    /** If a seeder was supplied, store its domain name here */
    var originalDnsSeeds: Array<String>? = null

    /** Active seeders */
    var dnsSeederAddress: Array<String>? = null

    /** Maintain this many connections to the bitcoin network */
    @cli(Display.Simple, "Maintain this many connections to the bitcoin network")
    var desiredConnectionCount = 4

    /** This connection is coming up */
    @Volatile
    var initializingCnxns: MutableList<P2pClient> = mutableListOf()

    @cli(Display.Simple, "Only connect to these nodes.  Specify ports with ip:port style.  Use null for non-exclusive mode")
    var exclusiveNodeSet: MutableSet<String>? = null


    @cli(Display.Simple, "Always try connecting to this node, but also connect to other nodes.")
    var preferredNodeSet: MutableSet<String>? = null

    init // parse seed and if its an IP add it to the list of possible nodes
    {
        launch { // Make async to speed up initialization
            for (s in seed)
            {
                try
                {
                    val pc = PossibleConnection(s, BlockchainPort[netType]!!, true)  // Keep any explicitly configured addresses forever in the possible connection list
                    synchronized(dataLock) { prospects[pc.endpoint()] = pc }
                }
                catch (e: Exception)  // Can get UnknownHostException or SecurityException
                {
                    // nothing to do
                }
            }
        }

        originalDnsSeeds = seed
        dnsSeederAddress = originalDnsSeeds?.copyOf()
    }

    /** Returns true if we are connected to this node/port */
    @cli(Display.User, "Returns true if we are connected to this node/port")
    override fun connectedTo(ip: String, port: Int): Boolean
    {
        for (c in initializingCnxns)
        {
            if ((c.port == port) && (c.name == ip)) return true
        }
        return super.connectedTo(ip, port)
    }

    /** report a node as having a problem */
    @cli(Display.Dev, "Report a node as having a problem")
    override fun misbehaving(cnxn: P2pClient, reason: BanReason, count: Int)
    {
        if (count >= 100)
        {
            ban(cnxn.name, BanData(reason))
            LogIt.warning(sourceLoc() + " " + cnxn.logName + ": CLOSE -- Disconnecting, misbehaving count is too large")
            cnxn.close()
        }
        launch { report(cnxn) }
    }

    @cli(Display.Dev, "Do not connect to this node again")
    fun ban(nodeIp: String, bd: BanData)
    {
        if (prospects[nodeIp]?.keep == true) return // Don't ban a hard-coded node
        bannedLock.synchronized {
            bannedNodes[nodeIp] = bd
        }
    }

    /** wake up and analyze my connections, especially the optional passed connection */
    override suspend fun coReport(problemCnxn: P2pClient?)
    {
        wakingDelay.wake()
    }

    /** wake up and analyze my connections, especially the optional passed connection */
    override fun report(problemCnxn: P2pClient?)
    {
        wakingDelay.wake()
    }

    @cli(Display.User, "Add a new possible P2P connection into the manager")
    override fun add(ip: String, port: Int, priority:Int, keepForever: Boolean)
    {
        val pc = try
        {
            PossibleConnection(ip, port, keepForever)
        }
        catch (e: Exception)
        {
            return
        }
        pc.priority = priority
        add(pc)
    }

    fun add(pc: PossibleConnection)
    {
        synchronized(dataLock) {
            prospects[pc.endpoint()] = pc
        }

        if (pc.keep)
        {
            val tmp = originalDnsSeeds?.toMutableList() ?: mutableListOf()
            tmp.add(0, pc.name)
            originalDnsSeeds = tmp.toTypedArray()
            dnsSeederAddress = originalDnsSeeds?.copyOf()
        }
    }

    /** Given a name and port, return the connection if we have one, or null if we don't.  This includes pending connections */
    @cli(Display.Dev, "Given a name and port, return the connection if we have one, or null if we don't.  This includes pending connections")
    fun find(name: String, portp: Int = -1): P2pClient?
    {
        val port = if (portp != -1) portp else BlockchainPort[netType]!!
        val p = try
        {
            PossibleConnection(name, port)
        }
        catch (e: Exception)
        {
            return null
        }
        val ips = p.ipAsString()
        return synchronized(cnxnLock)
        {

            for (c in cnxns)
            {
                // catch both FQDNs and IPs
                if (((c.name == name) || (c.name == ips)) && (c.port == p.port)) return@synchronized c
            }
            for (c in initializingCnxns)
            {
                if (((c.name == name) || (c.name == ips)) && (c.port == port)) return@synchronized c
            }
            null
        }
    }

    @cli(Display.Simple, "Configure preferred nodes.  Use null to remove an existing one.")
    override fun preferNodes(nodes: Set<String>?)
    {
        synchronized(cnxnLock)
        {
            preferredNodeSet = nodes?.toMutableSet()
        }
        if (nodes != null)
        {
            synchronized(cnxnLock)
            {
                // its a network operation to resolve the name
                launch()
                {
                    val pns = preferredNodeSet
                    if (pns != null) for (p in pns)
                    {
                        val c = find(p)
                        if (c != null)
                        {
                            preferredCnxn = c
                            break
                        }
                    }
                }  // If we happen to already be connected to it, then great
            }
        }
    }

    @cli(Display.Simple, "Configure an exclusive node.  Use null to remove an existing one.")
    override fun exclusiveNodes(nodes: Set<String>?)
    {
        exclusiveNodeSet = nodes?.toMutableSet()
        if (true)
        {
            if (nodes != null) LogIt.warning(sourceLoc() + chainName + ": CLOSE -- Disconnecting, exclusive node mode")
            synchronized(cnxnLock)
            {
                if (exclusiveNodeSet != null)  // already set
                {
                    // wipe out anything I'm trying to connect to
                    for (i in initializingCnxns)
                    {
                        if (exclusiveNodeSet?.contains(i.name) == true) continue
                        if (exclusiveNodeSet?.contains(i.name + ":" + i.port) == true) continue
                        i.close()
                    }
                    // wipe out any current connections (except to the chosen node)
                    for (i in cnxns)
                    {
                        if (exclusiveNodeSet?.contains(i.name) == true) continue
                        if (exclusiveNodeSet?.contains(i.name + ":" + i.port) == true) continue
                        i.close()
                    }
                }
            }
        }
    }

    /** Returns a possible peer from our prospects list */
    protected fun getProspectivePeer(): PossibleConnection
    {
        return synchronized(dataLock) {
            val useProspect = curProspect++
            val tmp = prospects.entries.sortedByDescending { it.value.priority }  // TODO: likely very inefficient to convert the map to a set every time
            val ret = tmp.elementAtOrElse(useProspect, {
                curProspect = 0
                tmp.elementAtOrElse(0) {
                    throw P2PNoNodesException()
                }
            }).value
            // If we get deep into random nodes provided by ADDR messages, then give up and start back with our high priority connections
            if ((ret.priority <= 0)&&(curProspect > prospectLimit)) curProspect=0
            ret
        }
    }

    /** Call if this connection didn't work out */
    protected fun deleteProspectivePeer(p: PossibleConnection)
    {
        synchronized(dataLock) {
            if ((prospects.size > 0) && (p.keep == false))  // In the case where we've configured just 1 connection, better to not remove it for any reason.
                prospects.remove(p.endpoint())
        }
    }

    var curElectrum: Int = 0

    /** Returns a random connection from the connection manager's node set */
    @cli(Display.Simple, "Get a electrum connection")
    override fun getElectrum(): ElectrumClient
    {
        if (ecnxns.size > 0)
        {
            curElectrum += 1
            return ecnxns[curElectrum % ecnxns.size]
        }
        else
        {
            while (true)
            {
                val get = getElectrumServerCandidate?.invoke(netType)
                if (get == null) throw ElectrumNoNodesException()

                val ec = try
                {
                    val useSSL = (get.port == DEFAULT_NEXA_SSL_ELECTRUM_PORT || get.port == DEFAULT_NEXATEST_SSL_ELECTRUM_PORT)
                    ElectrumClient(netType, get.ip, get.port, autostart = false, useSSL = useSSL)
                }
                catch (e: Exception)
                {
                    continue
                }
                ec.start()  // OK we got one
                ecnxns.add(ec)
                return ec
            }
        }
    }

    /** Returns a random connection from the connection manager's node set, but not one of these.
     * Returns null if no connections left */
    @cli(Display.Simple, "Returns a random connection from the connection manager's node set, but not one of the passed set.  Returns null if no connections left")
    override fun getAnotherElectrum(notThese: Set<ElectrumClient>): ElectrumClient?
    {
        throw NotImplementedError()
    }

    fun processAddrMessage(newPeers: MutableList<PossibleConnection>, source: P2pClient)
    {
        if (!MY_CNXNS_ONLY)  // debugging intervention to stop finding connections to external nodes
        {
            synchronized(dataLock) {
                for (p in newPeers)
                {
                    add(p)
                }
            }
        }
    }

    fun cleanupConnections()
    {
        var i = 0
        var removedCxn: P2pClient? = null
        synchronized(cnxnLock) {
            while (i < cnxns.size)
            {
                try
                {
                    val c = cnxns[i]
                    var disconnect = false
                    val elapsedRecvSec = (millinow() - c.lastReceiveTime)/1000
                    if (elapsedRecvSec > MAX_QUIET_TIME_SEC)
                    {
                        LogIt.info(sourceLoc() + " " + c.logName + ": Disconnecting, node is too quiet")
                        //changeCallback?.invoke(this, c)
                        disconnect = true
                    }
                    if (!c.isAlive())
                    {
                        if (c.closed) LogIt.info(sourceLoc() + " " + c.logName + ": I closed")
                        LogIt.info(sourceLoc() + " " + c.logName + ": Disconnecting, socket is already closed")
                        // changeCallback?.invoke(this, c)
                        disconnect = true
                    }

                    if (disconnect)
                    {
                        LogIt.info(sourceLoc() + " " + c.logName + ": CLOSE -- Cleanup closed connection")
                        report(c)
                        changeCallback?.invoke(this, c)
                        cnxns.removeAt(i)
                        removedCxn = c
                    }
                    else
                    {
                        i++
                    }
                }
                catch (e: P2PDisconnectedException)
                {
                    initializingCnxns.removeAt(i)
                }
                catch (e: IndexOutOfBoundsException)
                {
                    break
                }
            }
        }
        // Invoke the connection change outside of the lock since we don't know what the registered fns do.
        removedCxn?.let { changeCallback?.invoke(this, it) }
        removedCxn = null

        i = 0
        while (i < initializingCnxns.size)
        {
            try
            {
                synchronized(cnxnLock)
                {
                    if (i < initializingCnxns.size)
                    {
                        val c = initializingCnxns[i]
                        val elapsedBegin = millinow() - c.beginTime
                        if ((elapsedBegin > P2pClient.CONNECT_TIMEOUT + P2pClient.INITIAL_MESSAGE_XCHG_TIMEOUT))
                        {
                            LogIt.warning(sourceLoc() + " " + initializingCnxns[i].logName + ": CLOSE -- Disconnecting, initialization timeout")
                            c.close()
                            changeCallback?.invoke(this, c)
                            initializingCnxns.removeAt(i)
                        }
                        else if (!c.isAlive())
                        {
                            LogIt.warning(sourceLoc() + " " + initializingCnxns[i].logName + ": CLOSE -- Disconnecting, drop during initialization")
                            c.close()
                            changeCallback?.invoke(this, c)
                            initializingCnxns.removeAt(i)
                        }
                        else
                        {
                            i++
                        }
                    }
                }
            }
            catch (e: P2PDisconnectedException)
            {
                LogIt.warning(sourceLoc() + " " + initializingCnxns[i].logName + ": Disconnecting, disconnect")
                changeCallback?.invoke(this, initializingCnxns[i])
                initializingCnxns.removeAt(i)
            }
            catch (e: IndexOutOfBoundsException)
            {
                break
            }
        }

        try
        {
            val c = preferredCnxn
            if (c!=null)
            {
                val elapsedRecvSec = (millinow() - c.lastReceiveTime) / 1000
                if ((!c.isAlive()) || (elapsedRecvSec > MAX_QUIET_TIME_SEC))
                {
                    report(c)
                    preferredCnxn = null
                    changeCallback?.invoke(this, c)

                }
            }
        }
        catch (e: P2PDisconnectedException)
        {
            preferredCnxn?.let { changeCallback?.invoke(this, it) }
            preferredCnxn = null
        }

    }

    fun initialConnect(prospect: PossibleConnection, cnxn: P2pClient)
    {
        val nodeIpPort: String = prospect.endpoint()
        var cnx: P2pClient? = null
        try
        {
            //LogIt.info(sourceLoc() + " " + cnxn.logName + ": Connecting to " + nodeIpPort)
            cnxn.connect()
            //LogIt.info(sourceLoc() + " " + cnxn.logName + ": Socket connection established")
            cnx = cnxn
            cnxn.onInvCallback.add({ a, b -> launch { processInvMessage(a, b) } })
            cnxn.onTxCallback.add({ a, b -> processTxMessage(a, b) })
            cnxn.onBlockCallback.add({ a, b -> processBlockMessage(mutableListOf(a), b) })
            cnxn.onMerkleBlockCallback.add({ a, b -> processMerkleBlockMessage(mutableListOf(a), b) })
            cnxn.onHeadersCallback.add({ a, b -> processHeaderMessage(a, b); false })
            cnxn.onAddrCallback.add({ a, b -> processAddrMessage(a, b) })
            changeCallback?.invoke(this, cnxn)

            try
            {
                //LogIt.info(sourceLoc() + ": wait for ready " + cnxn.logName)
                cnxn.waitForReady()
                //LogIt.info(sourceLoc() + ": sending GetAddr " + cnxn.logName)
                cnxn.sendGetAddr()  // for any new connection, let's get who it knows about
                addressBloomFilter?.let {
                    //LogIt.info(sourceLoc() + ": sending Bloom Filter " + cnxn.logName)
                    cnxn.sendBloomFilter(it)
                }
                // Don't add the connection into the general pool until its ready
                // This is very important because many messages (like bloom filter install) are ignored during the initial message exchange,
                // and may even screw it up (node will assume that BU XVERSION is not coming if other messages arrive).
                synchronized(cnxnLock)
                {
                    if (cnxn.isAlive())
                    {
                        LogIt.info(sourceLoc() + ": Added connection " + cnxn.logName)
                        cnxns.add(cnxn)
                    }
                }
                changeCallback?.invoke(this, cnxn)
            }
            catch (e: P2PDisconnectedException)  // Connection worked by responding too slowly
            {
                LogIt.info(sourceLoc() + " " + cnxn.logName + ": CLOSE -- Banning node on connect attempt -- too slow\n")
                cnxn.close()
                changeCallback?.invoke(this, cnxn)
                ban(nodeIpPort, BanData(BanReason.TOO_SLOW))
                deleteProspectivePeer(prospect)
            }
        }
        catch (e: SocketTimeoutException)
        {
            LogIt.info(sourceLoc() + " " + cnxn.logName + ": CLOSE -- socket timeout " + e.toString() + "\n")
            cnxn.close()
            ban(nodeIpPort, BanData(BanReason.TOO_SLOW))
            deleteProspectivePeer(prospect)
        }

        synchronized(cnxnLock)
        {
            // Worked or failed, initialization is done
            if (cnx != null) initializingCnxns.remove(cnx)
        }
    }

    @ExperimentalTime
    fun reassessBannedNodes()
    {
        bannedLock.synchronized {
            val it = bannedNodes.iterator()
            for (d in it)
            {
                // Retry too slow connections whenever we need nodes
                if (d.value.reason == BanReason.TOO_SLOW) it.remove()
                // Retry these every 5 minutes -- server may have been restarted
                if ((d.value.reason == BanReason.EARLY_DISCONNECT) && ((millinow() - d.value.start) > 300 * 1000)) it.remove()
                if ((d.value.reason == BanReason.CANNOT_CONNECT) && ((millinow() - d.value.start) > 300 * 1000)) it.remove()
            }

        }
    }

    fun cnxnProcessor()
    {
        LogIt.info("Connection Manager Processor Thread")
        while (!done)
        {
            val cxn = mutableListOf<P2pClient>()
            synchronized(cnxnLock)
            {
                cxn.addAll(cnxns)
                cxn.addAll(initializingCnxns)
            }
            var didSomething = 0
            // LogIt.info("CnxnProcessor handling these connections: " + cxn.map { it.logName}.joinToString(", "))
            for (c in cxn)
            {
                try
                {
                    val tmp = c.process()
                    didSomething = didSomething or if (tmp) 1 else 0
                }
                catch (e: Throwable)
                {
                    handleThreadException(e, "Processing P2P messages", sourceLoc())
                    millisleep(500U)
                }
            }
            if (didSomething == 0) millisleep(20U)
            if (cxn.size == 0) millisleep(200U)
        }
    }

    /** disconnect from the lowest N performing peers */
    @cli(Display.Simple, "disconnect from the lowest N performing peers")
    fun dropWorstConnections(count: Int)
    {
        for (j in 0 until count)
        {
            // TODO use connection stats to pick a connection
            var removedCxn: P2pClient? = null
            synchronized(cnxnLock)
            {
                val i = cnxns.size - 1
                LogIt.info(sourceLoc() + ": CLOSE -- Disconnecting, worst connection " + cnxns[i].logName)
                cnxns[i].close()
                removedCxn = cnxns[i]
                cnxns.removeAt(i)
            }
            removedCxn?.let { changeCallback?.invoke(this, it) }
        }
    }

    @ExperimentalTime
    protected override fun run()
    {
        val th = Thread(chainName + "Cnxns") { cnxnProcessor() }
        LogIt.info("Cnxn manager node finder thread")
        while (!done)
        {
            try
            {
                if (dnsSeederAddress?.size == 0) dnsSeederAddress = originalDnsSeeds  // If I eliminate them all, try them all again
                cleanupConnections()
                // If we aren't connected to our preferred node, drop someone
                val pref = try { preferredNodeSet?.random() } catch (e: NoSuchElementException) { null }
                var needPrefConnect = if (pref == null)
                    false
                else
                {
                    if (preferredCnxn == null) true
                    else false
                }

                if (needPrefConnect && ((synchronized(cnxnLock) { cnxns.size }) >= desiredConnectionCount))
                {
                    dropWorstConnections(1)
                }

                val csize = synchronized(cnxnLock) { cnxns.size }
                // LogIt.info("Cnxn node finder, num connections: $csize")

                // Connect to the exclusive nodes (if configured)
                val ens = exclusiveNodeSet
                if (ens != null)
                {
                    // LogIt.info("Cnxn node finder, exclusive node mode if $csize + ${initializingCnxns.size} < ${ens.size}")
                    if (csize + initializingCnxns.size < ens.size)  // We need to connect to someone
                    {
                        for (enn in ens)
                        {
                            try
                            {
                                val prospect = PossibleConnection(enn, BlockchainPort[netType]!!)
                                val nodeIp: String = prospect.ipAsString()
                                val nodePort = prospect.port
                                if (!connectedTo(nodeIp, nodePort))
                                {
                                    LogIt.info(sourceLoc() +": Attempting to connect to $nodeIp:$nodePort")
                                    val cnxn = P2pClient(netType, nodeIp, nodePort, chainName + "@" + nodeIp + ":" + nodePort, coScope, cocond)
                                    initializingCnxns.add(cnxn)
                                    Thread("connect_${prospect.ipAsString()}") { initialConnect(prospect, cnxn) }
                                }
                            }
                            catch (e: Exception)  // not much to do if our exclusive host doesn't exist
                            {
                                logThreadException(e)
                                millisleep(500U)
                            }
                        }
                    }
                }
                // Connect to the preferred nodes (if configured)
                else if ((csize < desiredConnectionCount) && (initializingCnxns.size < SIMULTANEOUS_CNXN_INIT))
                {
                    try     // Reconnect to a node
                    {
                        val CnxnsToInit = kotlin.math.min(desiredConnectionCount.toInt() - csize, SIMULTANEOUS_CNXN_INIT.toInt() - initializingCnxns.size.toInt())
                        for (i in 0 until CnxnsToInit)
                        {
                            var delay = 0UL
                            synchronized(cnxnLock)
                            {
                                var prospect: PossibleConnection? = null
                                var preferred = false

                                if (needPrefConnect)
                                {
                                    try
                                    {
                                        prospect = PossibleConnection(pref!!, BlockchainPort[netType]!!)
                                        preferred = true
                                    }
                                    catch (e: Exception)  // drop thru with p == null
                                    {
                                        delay = 1000UL  // retry later
                                    }
                                }

                                if (prospect == null)
                                {
                                    for (count in 0..5)  // Loop to find a node that's not banned and we aren't connect to.  Try a couple per loop
                                    {
                                        val tmp = getProspectivePeer()
                                        if (!connectedTo(tmp.ipAsString(), tmp.port.toInt()))
                                            if (bannedLock.synchronized { !bannedNodes.contains(tmp.endpoint()) })
                                            {
                                                prospect = tmp
                                                break
                                            }
                                        deleteProspectivePeer(tmp)
                                    }
                                }

                                // If we got something, connect
                                if (prospect != null)
                                {
                                    val nodeIp: String = prospect.ipAsString()
                                    val nodePort = prospect.port.toInt()

                                    if (initializingCnxns.filter { (it.name == nodeIp && it.port == prospect.port) }.size == 0)
                                    {
                                        LogIt.info("Attempting to connect to $nodeIp:$nodePort")
                                        val cnxn = P2pClient(netType, nodeIp, nodePort, chainName + "@" + prospect.endpoint(), coScope, cocond)
                                        initializingCnxns.add(cnxn)
                                        if (preferred) preferredCnxn = cnxn
                                        Thread("connect_${prospect.ipAsString()}") { initialConnect(prospect, cnxn) }
                                        needPrefConnect = false // Once through would have picked the preferred if it is available
                                    }
                                    else
                                    {
                                        delay = 1000UL // If I'm already trying this prospect, slow down the retry
                                    }
                                }
                                else  // If I don't have new prospects, then slow down the retry
                                {
                                    delay = 2000UL
                                }
                            }
                            if (delay > 0UL) millisleep(delay)
                        }
                    }
                    catch (e: P2PNoNodesException)
                    {
                        val seed = dnsSeederAddress
                        if (seed != null)
                        {
                            for (s in seed)
                            {
                                try
                                {
                                    // Fill the prospects with some data from the seeder DNS
                                    withPeersFrom(s) {
                                        for (inet in it)
                                        {
                                            add(inet, BlockchainPort[netType] ?: NexaPort, 10)
                                            //val pc = PossibleConnection(it, (BlockchainPort[netType] ?: NexaPort).toInt())
                                            //synchronized(dataLock) { prospects[pc.endpoint()] = pc }
                                        }
                                    }
                                }
                                catch (e: Exception)  // UnknownHostException
                                {
                                    LogIt.warning(sourceLoc() + " " + chainName + ": " + s + " is unknown or offline")
                                    // Get rid of this seeder if its offline
                                    if (iHaveInternet() ?: true)
                                        dnsSeederAddress = dnsSeederAddress?.filterNot { it == s }?.toTypedArray()
                                }
                            }
                        }
                        reassessBannedNodes()
                    }
                }

                synchronized(cnxnLock)
                {
                    for (cnxn in cnxns)  // Periodic maintenance
                    {
                        try
                        {
                            val elapsedRecvSec = (millinow() - cnxn.lastReceiveTime)/1000

                            if ((elapsedRecvSec > PING_QUIET_TIME_SEC) && (cnxn.pingOutstanding == false))
                            {
                                LogIt.info(sourceLoc() + " " + cnxn.logName + ": ping ")
                                cnxn.sendPing()
                            }
                            if ((bloomCount != -1) && (cnxn.bloomCount != bloomCount))
                            {
                                LogIt.info(sourceLoc() + " " + cnxn.logName + ": periodic bloom update")
                                addressBloomFilter?.let { cnxn.sendBloomFilter(it, bloomCount) }
                            }
                        }
                        catch (e: IndexOutOfBoundsException)  // Happens if another thread deleted a connection under us, ignore
                        {

                        }
                        catch (e: P2PDisconnectedException)
                        {
                            LogIt.info(sourceLoc() + " " + cnxn.logName + ": CLOSE -- P2PDisconnectedException " + e.message)
                            cnxn.close()
                        }

                    }
                }

                wakingDelay.timedwaitfor(500, { false }, {})
                reconnectTimer.take(5)
            }
            catch (e: TimeoutCancellationException)
            {
                handleThreadException(e, sourceLoc() + " " + chainName + ": ")
                // nothing to do if various requests timed out. just loop around and try again
            }
            catch (e: Exception)
            {
                handleThreadException(e, sourceLoc() + " " + chainName + ": ")
            }
        }
        th.join()
    }

}


/*
open class RegTestCnxnMgr(override val chainName: String, val nodeIp: String, val nodePort: Int) : CommonCnxnMgr(ChainSelector.NEXAREGTEST)
{
    override fun add(ip: String, port: Int, priority:Int, keepForever: Boolean)
    {
        LogIt.info(sourceLoc() + "Not adding the potential peer $ip:$port to a regtest blockchain")
    }


    public override fun exclusiveNodes(nodes: Set<String>?)
    {
        LogIt.info(sourceLoc() + "Not adding exclusive nodes to a regtest blockchain")
    }

    public override fun preferNodes(nodes: Set<String>?)
    {
        LogIt.info(sourceLoc() + "Not adding the preferred nodes to a regtest blockchain")
    }

    /** report a node as having a problem */
    override fun misbehaving(cnxn: P2pClient, reason: BanReason, count: Int)
    {
        LogIt.warning(sourceLoc() + "Connection to regtest was indicated as misbehaving because: ${reason.name}")
    }

    // In the regtest mode, we just have to keep giving the same nodes
    override fun getAnotherNode(notThese: Set<P2pClient>): P2pClient?
    {
        var tries = 0
        while (true)
        {
            tries += 1
            if (tries > cnxns.size) return null // I've tried everything in cnxns
            if (cnxns.size > 0)
            {

                val c = grabAndCheckConnection()
                if (c != null)
                {
                    return c
                }
            }
            else wakingDelay.delay(OUT_OF_NODES_DELAY) { cnxns.size == 0 }
        }
    }

    override fun run()
    {
        while (!done)
        {
            var delay = 500
            for (i in 0..cnxns.size - 1)  // clean up dead connections
            {
                val c = cnxns[i]
                if (c.sock.isClosed || (c.inp?.isClosedForRead == true) || (c.out?.isClosedForWrite == true))
                {
                    LogIt.warning(sourceLoc() + ": CLOSE -- Disconnecting, connection is dropped " + c.logName)
                    cnxns.removeAt(i)
                    changeCallback?.invoke(this, c)
                }
            }

            if (cnxns.count() == 0)
            {
                try     // Reconnect to a node
                {
                    LogIt.info(sourceLoc() + " " + chainName + ": Connecting to " + nodeIp + ":" + nodePort.toString())
                    var cnxn = P2pClient(netType, nodeIp, nodePort, chainName + "@" + nodeIp + ":" + nodePort, coScope, cocond).connect()

                    cnxn.onInvCallback.add({ a, b -> launch { processInvMessage(a, b) } })
                    cnxn.onTxCallback.add({ a, b -> processTxMessage(a, b) })
                    cnxn.onBlockCallback.add({ a, b -> processBlockMessage(mutableListOf(a), b) })
                    cnxn.onMerkleBlockCallback.add({ a, b -> processMerkleBlockMessage(mutableListOf(a), b) })
                    cnxn.onHeadersCallback.add({ a, b -> processHeaderMessage(a, b); false })

                    Thread(nodeIp + ":" + nodePort + "_msgrecv") { cnxn.processForever() }
                    runBlocking { cnxn.waitForReady() }
                    addressBloomFilter?.let {
                        cnxn.sendBloomFilter(it)
                    }
                    // Don't add the connection into the general pool until its ready
                    // This is very important because many messages (like bloom filter install) are ignored during the initial message exchange,
                    // and may even screw it up (node will assume that BU XVERSION is not coming if other messages arrive).
                    cnxns.add(cnxn)
                    changeCallback?.invoke(this, cnxn)
                }
                catch (e: SocketTimeoutException)
                {
                    LogIt.info(sourceLoc() + " " + chainName + ": " +e.toString())
                    delay = 15000 // In regtest there's only 1 node to connect to, so don't spam reconnect if something's going wrong
                }
                catch (e: P2PDisconnectedException)  // Connection worked by responding too slowly
                {
                    LogIt.info(sourceLoc() + " " + chainName + ": " +e.toString())
                    delay = 15000 // In regtest there's only 1 node to connect to, so don't spam reconnect if something's going wrong
                }

            }

            for (i in 0..cnxns.size - 1)  // Periodic maintenance
            {
                if ((bloomCount != 0) && (cnxns[i].bloomCount != bloomCount))
                {
                    LogIt.info(sourceLoc() + " " + chainName + ": periodic bloom update")
                    addressBloomFilter?.let { cnxns[i].sendBloomFilter(it, bloomCount) }
                }

            }
            val ch = changeCount
            wakey.delay(delay.toLong()) { ch != changeCount }
        }
    }
}
*/