package org.nexa.libnexakotlin

/** Network-defined bloom filter maximum size
 * TODO: associate this value with the blockchain
 */
public val MAX_BLOOM_FILTER_SIZE:Int = 36000

/**  Bloom filter update flags
 * Must be consistent with same named fields in C++ code in bloom.h:bloomflags
 * */
public enum class BloomFlags(public val v: Int)
{
    // Must match the native code. DO NOT CHANGE!
    BLOOM_UPDATE_NONE(0),
    BLOOM_UPDATE_ALL(1),
    BLOOM_UPDATE_P2PUBKEY_ONLY(2)
}

// Payment Address abstraction
enum class PayAddressType(val v: Byte)
{
    // Must match C++ cashlib.cpp PayAddressType
    NONE(0),
    P2PUBKEY(1),
    P2PKH(2),
    P2SH(3),
    TEMPLATE(4), // Generalized pay to script template
    P2PKT(5)  // Pay to well-known script template 1 (pay-to-pub-key-template)
}

fun LoadPayAddressType(v: Byte): PayAddressType
{
    return when (v)
    {
        0.toByte() -> PayAddressType.NONE
        1.toByte() -> PayAddressType.P2PUBKEY
        2.toByte() -> PayAddressType.P2PKH
        3.toByte() -> PayAddressType.P2SH
        4.toByte() -> PayAddressType.TEMPLATE
        5.toByte() -> PayAddressType.P2PKT
        else -> throw PayAddressDecodeException("unknown address type")
    }
}

public interface LibNexa
{
    public companion object
    {
        public val GROUP_ID_MIN_SIZE:Int = 32
        public val GROUP_ID_MAX_SIZE:Int = 520  // stack size
    }
    /** Returns the minimum amount of native coins (finest unit) that must be in every UTXO for the passed blockchain */
    public fun minDust(chain: ChainSelector): Long

    public fun encode64(data: ByteArray): String
    public fun decode64(encoded: String): ByteArray

    /** Returns the double sha256 of data. Result is 32 bytes */
    public fun hash256(data: ByteArray): ByteArray
    /** Returns the sha256 of data. Result is 32 bytes */
    public fun sha256(data: ByteArray): ByteArray
    /** Calculates the RIPEMD160 of the SHA256 of data. Result is 20 bytes */
    public fun hash160(data: ByteArray): ByteArray

    /** Returns the double sha256 of data. Result is 32 bytes */
    public fun hash256(data: UByteArray): UByteArray
    /** Returns the sha256 of data. Result is 32 bytes */
    public fun sha256(data: UByteArray): UByteArray

    /** Calculates the RIPEMD160 of the SHA256 of data. Result is 20 bytes */
    public fun hash160(data: UByteArray): UByteArray

    /** Calculates the block hash given a serialized header.  Note that by convention hashes are displayed in byte-reverse order (as if a little-endian number was being displayed).
     * This return value is not reversed.  (hash.reversed().toHex() == "hex header from explorer") -> true
     */
    public fun blockHash(serializedBlockHeader: ByteArray): ByteArray
    public fun blockHash(serializedBlockHeader: UByteArray): UByteArray

    public fun txidem(serializedTx: ByteArray): ByteArray
    public fun txidem(serializedTx: UByteArray): UByteArray
    public fun txid(serializedTx: ByteArray): ByteArray
    public fun txid(serializedTx: UByteArray): UByteArray

    public fun verifyBlockHeader(chainSelector: ChainSelector, serializedBlockHeader: ByteArray): Boolean
    public fun verifyBlockHeader(chainSelector: ChainSelector, serializedBlockHeader: UByteArray): Boolean


    public fun signHashSchnorr(data: UByteArray, secret: UByteArray): UByteArray
    public fun signHashSchnorr(data: ByteArray, secret: ByteArray): ByteArray

    /** Given a private key, return the corresponding (secp256k1) public key -- that is
     * the privKey*G where G is the group generator */
    public fun getPubKey(privateKey: UByteArray): UByteArray
    public fun getPubKey(privateKey: ByteArray): ByteArray

    /** This function calculates the BIP44 key from the specified path.  BIP44 requires that purpose, coinType and account are "hardened".
     * This function will automatically harden those parameters if you pass unhardened values.
     * Therefore this function cannot be used for generalized (non-BIP44) child key derivation.
     */
    public fun deriveHd44ChildKey(secretSeed: ByteArray, purpose: Long, coinType: Long, account: Long, change: Boolean, index: Int): Pair<ByteArray, String>

    /** Decodes a private key provided in Satoshi's original fnormat */
    public fun decodeWifPrivateKey(chainSelector: ChainSelector, secretWIF: String): ByteArray

    /** Returns the work -- that is the expected number of hashes to find a solution -- given the difficulty expressed in Bitcoin's "bits" notation */
    public fun getWorkFromDifficultyBits(nBits: Long): ByteArray
    public fun getWorkFromDifficultyBits(nBits: UInt): ByteArray
    public fun getWorkFromDifficultyBits(nBits: ULong): ByteArray


    /** Given an array of items, creates a bloom filter and returns it serialized.
         * Typical items are addresses (just the raw 20 bytes), transaction hashes, and outpoints
         * @param items Array<ByteArray> of bitstrings to place into the bloom filter
         * @param falsePosRate Desired Bloom false positive rate
         * @param capacity Number of elements that can be placed into this bloom while maintaining the falsePosRate.  If this is < items.count(), items.count() is used.  The reason to provide a larger capacity is to allow items to be added into the bloom filter later
         * @param maxSize Maximum size of the bloom filter -- if the capacity and falsePosRate result in a bloom that's larger than this, this size is used instead
         * @param flags  See @BloomFlags for possible fields
         * @param tweak Change tweak to create a different bloom filter.  Used to ensure that collision attacks only work against one filter or node
         * @return Bloom filter serialized as in the P2P network format
         */
    public fun createBloomFilter(items: Array<Any>, falsePosRate: Double, capacity: Int, maxSize: Int, flags: Int = 0, tweak: Int = 1): ByteArray

    /** Create an ECDSA signature for the passed transaction
         * @param txData transaction serialized for signing
         * @param sigHashType signature hash algorithm selection
         * @param inputIdx what input to sign
         * @param inputAmount how many satoshis this input contains
         * @param prevoutScript the input's constraint script
         * @secret 32 byte private key
         * @return signature in binary format
         */
    public fun signBchTxOneInputUsingECDSA(txData: ByteArray, sigHashType: Int, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray

    /** Create a Schnorr signature for the passed transaction
         * @param txData transaction serialized for signing
         * @param sigHashType signature hash algorithm selection
         * @param inputIdx what input to sign
         * @param inputAmount how many satoshis this input contains
         * @param prevoutScript the input's constraint script
         * @secret 32 byte private key
         * @return signature in binary format
         */
    public fun signTxOneInputUsingSchnorr(txData: ByteArray, sigHashType: ByteArray, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray

    /** Create a Schnorr signature for the passed transaction
         * @param txData transaction serialized for signing
         * @param sigHashType signature hash algorithm selection
         * @param inputIdx what input to sign
         * @param inputAmount how many satoshis this input contains
         * @param prevoutScript the input's constraint script
         * @secret 32 byte private key
         * @return signature in binary format
         */
    public fun signBchTxOneInputUsingSchnorr(txData: ByteArray, sigHashType: Int, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray

    /** Sign a message using the same algorithm as the original bitcoin wallet's signmessage functionality
         * @message The raw bytes to be signed
         * @secret 32 byte private key
         * @return signature in binary format.  Call Codec.encode64 (BCHserialize.kt) to convert to the exact signature format used by the bitcoin wallet's signmessage */
    public fun signMessage(message: ByteArray, secret: ByteArray): ByteArray?

    /** Verify a message using the same algorithm as the original bitcoin wallet's signmessage functionality
         * @message The raw bytes of the message to be verified
         * @address The address raw bytes (without the type prefix)
         * @return The pubkey that was used to sign, if the signature is valid, otherwise a zero size array. */
    public fun verifyMessage(message: ByteArray, address: ByteArray, signature: ByteArray): ByteArray?


    // data is the flattened "raw" script bytes.  "raw" means not wrapped into a BCHserialized
    public fun encodeCashAddr(chainSelector: ChainSelector, type: PayAddressType, data: ByteArray): String

    // Returns an array with the first byte being the PayAddressType, and the rest the address bytes
    // This API is not stable.  It should be called only by the PayAddress wrapper class
    public fun decodeCashAddr(chainSelector: ChainSelector, addr: String): ByteArray

    public fun groupIdToAddr(chainSelector: ChainSelector, data: ByteArray): String

    public fun groupIdFromAddr(chainSelector: ChainSelector, addr: String): ByteArray

    public fun extractFromMerkleBlock(numTxes: Int, merkleProofPath: ByteArray, hashes: Array<ByteArray>): Array<ByteArray>?

    public fun secureRandomBytes(amt: Int): ByteArray

    // public fun GenerateBip39Seed(wordseed: String, passphrase: String, size: Int = 64): ByteArray

    //{
    //    return GenerateBip39Seed(wordseed,passphrase,size)
    //}

    public fun generateBip39SecretWords(
        bytes: ByteArray,
        wordList: Array<String> = englishWordList
    ) = GenerateBip39SecretWords(bytes, wordList)
}

// public expect fun initializeLibNexa(): LibNexa
public lateinit var libnexa: LibNexa
