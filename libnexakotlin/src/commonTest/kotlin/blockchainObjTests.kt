package org.nexa.libnexakotlin
import kotlin.test.*

import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign

private val LogIt = GetLog("BlockchainObjTests.kt")

val bip39TestVector = listOf(listOf("00000000000000000000000000000000","abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon about","c55257c360c07c72029aebc1b53c05ed0362ada38ead3e3e9efa3708e53495531f09a6987599d18264c1e1c92f2cf141630c7a3c4ab7c81b2f001698e7463b04","xprv9s21ZrQH143K3h3fDYiay8mocZ3afhfULfb5GX8kCBdno77K4HiA15Tg23wpbeF1pLfs1c5SPmYHrEpTuuRhxMwvKDwqdKiGJS9XFKzUsAF"),
    listOf("7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f","legal winner thank year wave sausage worth useful legal winner thank yellow","2e8905819b8723fe2c1d161860e5ee1830318dbf49a83bd451cfb8440c28bd6fa457fe1296106559a3c80937a1c1069be3a3a5bd381ee6260e8d9739fce1f607","xprv9s21ZrQH143K2gA81bYFHqU68xz1cX2APaSq5tt6MFSLeXnCKV1RVUJt9FWNTbrrryem4ZckN8k4Ls1H6nwdvDTvnV7zEXs2HgPezuVccsq"),
    listOf("80808080808080808080808080808080","letter advice cage absurd amount doctor acoustic avoid letter advice cage above","d71de856f81a8acc65e6fc851a38d4d7ec216fd0796d0a6827a3ad6ed5511a30fa280f12eb2e47ed2ac03b5c462a0358d18d69fe4f985ec81778c1b370b652a8","xprv9s21ZrQH143K2shfP28KM3nr5Ap1SXjz8gc2rAqqMEynmjt6o1qboCDpxckqXavCwdnYds6yBHZGKHv7ef2eTXy461PXUjBFQg6PrwY4Gzq"),
    listOf("ffffffffffffffffffffffffffffffff","zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo wrong","ac27495480225222079d7be181583751e86f571027b0497b5b5d11218e0a8a13332572917f0f8e5a589620c6f15b11c61dee327651a14c34e18231052e48c069","xprv9s21ZrQH143K2V4oox4M8Zmhi2Fjx5XK4Lf7GKRvPSgydU3mjZuKGCTg7UPiBUD7ydVPvSLtg9hjp7MQTYsW67rZHAXeccqYqrsx8LcXnyd"),
    listOf("000000000000000000000000000000000000000000000000","abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon agent","035895f2f481b1b0f01fcf8c289c794660b289981a78f8106447707fdd9666ca06da5a9a565181599b79f53b844d8a71dd9f439c52a3d7b3e8a79c906ac845fa","xprv9s21ZrQH143K3mEDrypcZ2usWqFgzKB6jBBx9B6GfC7fu26X6hPRzVjzkqkPvDqp6g5eypdk6cyhGnBngbjeHTe4LsuLG1cCmKJka5SMkmU"),
    listOf("7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f","legal winner thank year wave sausage worth useful legal winner thank year wave sausage worth useful legal will","f2b94508732bcbacbcc020faefecfc89feafa6649a5491b8c952cede496c214a0c7b3c392d168748f2d4a612bada0753b52a1c7ac53c1e93abd5c6320b9e95dd","xprv9s21ZrQH143K3Lv9MZLj16np5GzLe7tDKQfVusBni7toqJGcnKRtHSxUwbKUyUWiwpK55g1DUSsw76TF1T93VT4gz4wt5RM23pkaQLnvBh7"),
    listOf("808080808080808080808080808080808080808080808080","letter advice cage absurd amount doctor acoustic avoid letter advice cage absurd amount doctor acoustic avoid letter always","107d7c02a5aa6f38c58083ff74f04c607c2d2c0ecc55501dadd72d025b751bc27fe913ffb796f841c49b1d33b610cf0e91d3aa239027f5e99fe4ce9e5088cd65","xprv9s21ZrQH143K3VPCbxbUtpkh9pRG371UCLDz3BjceqP1jz7XZsQ5EnNkYAEkfeZp62cDNj13ZTEVG1TEro9sZ9grfRmcYWLBhCocViKEJae"),
    listOf("ffffffffffffffffffffffffffffffffffffffffffffffff","zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo when","0cd6e5d827bb62eb8fc1e262254223817fd068a74b5b449cc2f667c3f1f985a76379b43348d952e2265b4cd129090758b3e3c2c49103b5051aac2eaeb890a528","xprv9s21ZrQH143K36Ao5jHRVhFGDbLP6FCx8BEEmpru77ef3bmA928BxsqvVM27WnvvyfWywiFN8K6yToqMaGYfzS6Db1EHAXT5TuyCLBXUfdm"),
    listOf("0000000000000000000000000000000000000000000000000000000000000000","abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon art","bda85446c68413707090a52022edd26a1c9462295029f2e60cd7c4f2bbd3097170af7a4d73245cafa9c3cca8d561a7c3de6f5d4a10be8ed2a5e608d68f92fcc8","xprv9s21ZrQH143K32qBagUJAMU2LsHg3ka7jqMcV98Y7gVeVyNStwYS3U7yVVoDZ4btbRNf4h6ibWpY22iRmXq35qgLs79f312g2kj5539ebPM"),
    listOf("7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f","legal winner thank year wave sausage worth useful legal winner thank year wave sausage worth useful legal winner thank year wave sausage worth title","bc09fca1804f7e69da93c2f2028eb238c227f2e9dda30cd63699232578480a4021b146ad717fbb7e451ce9eb835f43620bf5c514db0f8add49f5d121449d3e87","xprv9s21ZrQH143K3Y1sd2XVu9wtqxJRvybCfAetjUrMMco6r3v9qZTBeXiBZkS8JxWbcGJZyio8TrZtm6pkbzG8SYt1sxwNLh3Wx7to5pgiVFU"),
    listOf("8080808080808080808080808080808080808080808080808080808080808080","letter advice cage absurd amount doctor acoustic avoid letter advice cage absurd amount doctor acoustic avoid letter advice cage absurd amount doctor acoustic bless","c0c519bd0e91a2ed54357d9d1ebef6f5af218a153624cf4f2da911a0ed8f7a09e2ef61af0aca007096df430022f7a2b6fb91661a9589097069720d015e4e982f","xprv9s21ZrQH143K3CSnQNYC3MqAAqHwxeTLhDbhF43A4ss4ciWNmCY9zQGvAKUSqVUf2vPHBTSE1rB2pg4avopqSiLVzXEU8KziNnVPauTqLRo"),
    listOf("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff","zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo vote","dd48c104698c30cfe2b6142103248622fb7bb0ff692eebb00089b32d22484e1613912f0a5b694407be899ffd31ed3992c456cdf60f5d4564b8ba3f05a69890ad","xprv9s21ZrQH143K2WFF16X85T2QCpndrGwx6GueB72Zf3AHwHJaknRXNF37ZmDrtHrrLSHvbuRejXcnYxoZKvRquTPyp2JiNG3XcjQyzSEgqCB"),
    listOf("9e885d952ad362caeb4efe34a8e91bd2","ozone drill grab fiber curtain grace pudding thank cruise elder eight picnic","274ddc525802f7c828d8ef7ddbcdc5304e87ac3535913611fbbfa986d0c9e5476c91689f9c8a54fd55bd38606aa6a8595ad213d4c9c9f9aca3fb217069a41028","xprv9s21ZrQH143K2oZ9stBYpoaZ2ktHj7jLz7iMqpgg1En8kKFTXJHsjxry1JbKH19YrDTicVwKPehFKTbmaxgVEc5TpHdS1aYhB2s9aFJBeJH"),
    listOf("6610b25967cdcca9d59875f5cb50b0ea75433311869e930b","gravity machine north sort system female filter attitude volume fold club stay feature office ecology stable narrow fog","628c3827a8823298ee685db84f55caa34b5cc195a778e52d45f59bcf75aba68e4d7590e101dc414bc1bbd5737666fbbef35d1f1903953b66624f910feef245ac","xprv9s21ZrQH143K3uT8eQowUjsxrmsA9YUuQQK1RLqFufzybxD6DH6gPY7NjJ5G3EPHjsWDrs9iivSbmvjc9DQJbJGatfa9pv4MZ3wjr8qWPAK"),
    listOf("68a79eaca2324873eacc50cb9c6eca8cc68ea5d936f98787c60c7ebc74e6ce7c","hamster diagram private dutch cause delay private meat slide toddler razor book happy fancy gospel tennis maple dilemma loan word shrug inflict delay length","64c87cde7e12ecf6704ab95bb1408bef047c22db4cc7491c4271d170a1b213d20b385bc1588d9c7b38f1b39d415665b8a9030c9ec653d75e65f847d8fc1fc440","xprv9s21ZrQH143K2XTAhys3pMNcGn261Fi5Ta2Pw8PwaVPhg3D8DWkzWQwjTJfskj8ofb81i9NP2cUNKxwjueJHHMQAnxtivTA75uUFqPFeWzk"),
    listOf("c0ba5a8e914111210f2bd131f3d5e08d","scheme spot photo card baby mountain device kick cradle pact join borrow","ea725895aaae8d4c1cf682c1bfd2d358d52ed9f0f0591131b559e2724bb234fca05aa9c02c57407e04ee9dc3b454aa63fbff483a8b11de949624b9f1831a9612","xprv9s21ZrQH143K3FperxDp8vFsFycKCRcJGAFmcV7umQmcnMZaLtZRt13QJDsoS5F6oYT6BB4sS6zmTmyQAEkJKxJ7yByDNtRe5asP2jFGhT6"),
    listOf("6d9be1ee6ebd27a258115aad99b7317b9c8d28b6d76431c3","horn tenant knee talent sponsor spell gate clip pulse soap slush warm silver nephew swap uncle crack brave","fd579828af3da1d32544ce4db5c73d53fc8acc4ddb1e3b251a31179cdb71e853c56d2fcb11aed39898ce6c34b10b5382772db8796e52837b54468aeb312cfc3d","xprv9s21ZrQH143K3R1SfVZZLtVbXEB9ryVxmVtVMsMwmEyEvgXN6Q84LKkLRmf4ST6QrLeBm3jQsb9gx1uo23TS7vo3vAkZGZz71uuLCcywUkt"),
    listOf("9f6a2878b2520799a44ef18bc7df394e7061a224d2c33cd015b157d746869863","panda eyebrow bullet gorilla call smoke muffin taste mesh discover soft ostrich alcohol speed nation flash devote level hobby quick inner drive ghost inside","72be8e052fc4919d2adf28d5306b5474b0069df35b02303de8c1729c9538dbb6fc2d731d5f832193cd9fb6aeecbc469594a70e3dd50811b5067f3b88b28c3e8d","xprv9s21ZrQH143K2WNnKmssvZYM96VAr47iHUQUTUyUXH3sAGNjhJANddnhw3i3y3pBbRAVk5M5qUGFr4rHbEWwXgX4qrvrceifCYQJbbFDems"),
    listOf("23db8160a31d3e0dca3688ed941adbf3","cat swing flag economy stadium alone churn speed unique patch report train","deb5f45449e615feff5640f2e49f933ff51895de3b4381832b3139941c57b59205a42480c52175b6efcffaa58a2503887c1e8b363a707256bdd2b587b46541f5","xprv9s21ZrQH143K4G28omGMogEoYgDQuigBo8AFHAGDaJdqQ99QKMQ5J6fYTMfANTJy6xBmhvsNZ1CJzRZ64PWbnTFUn6CDV2FxoMDLXdk95DQ"),
    listOf("8197a4a47f0425faeaa69deebc05ca29c0a5b5cc76ceacc0","light rule cinnamon wrap drastic word pride squirrel upgrade then income fatal apart sustain crack supply proud access","4cbdff1ca2db800fd61cae72a57475fdc6bab03e441fd63f96dabd1f183ef5b782925f00105f318309a7e9c3ea6967c7801e46c8a58082674c860a37b93eda02","xprv9s21ZrQH143K3wtsvY8L2aZyxkiWULZH4vyQE5XkHTXkmx8gHo6RUEfH3Jyr6NwkJhvano7Xb2o6UqFKWHVo5scE31SGDCAUsgVhiUuUDyh"),
    listOf("066dca1a2bb7e8a1db2832148ce9933eea0f3ac9548d793112d9a95c9407efad","all hour make first leader extend hole alien behind guard gospel lava path output census museum junior mass reopen famous sing advance salt reform","26e975ec644423f4a4c4f4215ef09b4bd7ef924e85d1d17c4cf3f136c2863cf6df0a475045652c57eb5fb41513ca2a2d67722b77e954b4b3fc11f7590449191d","xprv9s21ZrQH143K3rEfqSM4QZRVmiMuSWY9wugscmaCjYja3SbUD3KPEB1a7QXJoajyR2T1SiXU7rFVRXMV9XdYVSZe7JoUXdP4SRHTxsT1nzm"),
    listOf("f30f8c1da665478f49b001d94c5fc452","vessel ladder alter error federal sibling chat ability sun glass valve picture","2aaa9242daafcee6aa9d7269f17d4efe271e1b9a529178d7dc139cd18747090bf9d60295d0ce74309a78852a9caadf0af48aae1c6253839624076224374bc63f","xprv9s21ZrQH143K2QWV9Wn8Vvs6jbqfF1YbTCdURQW9dLFKDovpKaKrqS3SEWsXCu6ZNky9PSAENg6c9AQYHcg4PjopRGGKmdD313ZHszymnps"),
    listOf("c10ec20dc3cd9f652c7fac2f1230f7a3c828389a14392f05","scissors invite lock maple supreme raw rapid void congress muscle digital elegant little brisk hair mango congress clump","7b4a10be9d98e6cba265566db7f136718e1398c71cb581e1b2f464cac1ceedf4f3e274dc270003c670ad8d02c4558b2f8e39edea2775c9e232c7cb798b069e88","xprv9s21ZrQH143K4aERa2bq7559eMCCEs2QmmqVjUuzfy5eAeDX4mqZffkYwpzGQRE2YEEeLVRoH4CSHxianrFaVnMN2RYaPUZJhJx8S5j6puX"),
    listOf("f585c11aec520db57dd353c69554b21a89b20fb0650966fa0a9d6f74fd989d8f","void come effort suffer camp survey warrior heavy shoot primary clutch crush open amazing screen patrol group space point ten exist slush involve unfold","01f5bced59dec48e362f2c45b5de68b9fd6c92c6634f44d6d40aab69056506f0e35524a518034ddc1192e1dacd32c1ed3eaa3c3b131c88ed8e7e54c49a5d0998","xprv9s21ZrQH143K39rnQJknpH1WEPFJrzmAqqasiDcVrNuk926oizzJDDQkdiTvNPr2FYDYzWgiMiC63YmfPAa2oPyNB23r2g7d1yiK6WpqaQS"))


// Note switch your build variant to debug to run these tests if you get and error about signingConfig
class BlockchainObjTests
{
    companion object
    {
        init
        {
            initializeLibNexa()
        }
    }

    @Test
    fun testBlockHeaderSerialize()
    {
        val regGbData = "0000000000000000000000000000000000000000000000000000000000000000ffff7f20000000000000000000000000000000000000000000000000000000000000000049317c27d6893f423308ccd46e1f51ec0310892a49b8e6645e176e7235ba04c8000000000000000000000000000000000000000000000000000000000000000027ffee60000200000000000000000000000000000000000000000000000000000000000000e700000000000000010000000105".fromHex()
        val regGb = NexaBlockHeader(BCHserialized(regGbData, SerializationType.NETWORK))

        check(regGb.hashPrevBlock == Hash256())
        check(regGb.diffBits == 0x207fffffL)
        check(regGb.hashAncestor == Hash256())
        check(regGb.hashMerkleRoot == Hash256("c804ba35726e175e64e6b8492a891003ec511f6ed4cc0833423f89d6277c3149"))
        check(regGb.time == 1626275623L)
        check(regGb.height == 0L)
        check(regGb.chainWork == BigInteger.fromInt(2))
        check(regGb.size == 231L)
        check(regGb.txCount == 1L)

        check(regGb.feePoolAmt == 0L)
        check(regGb.utxoCommitment.size == 0)
        check(regGb.minerData.size == 0)
        check( regGb.nonce contentEquals byteArrayOf(5))

        val gbSer = BCHserialized(SerializationType.NETWORK) + regGb
        val hex = gbSer.toHex()
        check (hex == regGbData.toHex())
    }

    @Test
    fun testBlockSerialize()
    {
        val regGbData = "0000000000000000000000000000000000000000000000000000000000000000ffff7f20000000000000000000000000000000000000000000000000000000000000000049317c27d6893f423308ccd46e1f51ec0310892a49b8e6645e176e7235ba04c8000000000000000000000000000000000000000000000000000000000000000027ffee60000200000000000000000000000000000000000000000000000000000000000000e700000000000000010000000105010000020000000000000000000151000000000000000000156a00023b1c0f54686973206973207265677465737400000000".fromHex()
        val regGb = NexaBlock(ChainSelector.NEXAREGTEST, BCHserialized(regGbData, SerializationType.NETWORK))
        check(regGb.hashPrevBlock == Hash256())
        check(regGb.diffBits == 0x207fffffL)
    }

    @Test
    fun TestTxSerialize()
    {
        val tx1 = NexaTransaction.fromHex(ChainSelector.NEXAREGTEST,"000100696b069545cb31f6a53c822c3efca739a29256a34de181b541381ad22e242d3964222102f2a709a523d3cb03c168acac1e22dfe5c026904860eb3de3a9483858e94cb2e440cd1f5c4a12c89b1aa992b341ac3e167175197e25ac6439e4ead0318f9c09651a11e9b663d9669e98ae98270e816e0538cd0f37ca5cca5496e6f8ad33b8f04f5afeffffff75547f2c000000000201809698000000000017005114a23baa184b33c3ee244ea736d73dd37447f638da011abde62b0000000017005114de5c3eb8b94a27932509df76df63d0fe2ab311be7c000000")
        println(tx1.toString())
        check(tx1.id.toHex() == "188fc9f1b33afb968753b6cd642afc5a28c59fb244841f7ba7378972c30563c6")
        check(tx1.idem.toHex() == "882b5de933360a36c1ee38590579de3ee69655bad72bf442a3b4b16a7f196cc5")
        val tx1Op0 = NexaTxOutpoint(tx1.idem, 0)
        check(tx1Op0.hash.toHex() == "82dee5899e586f934b2f23f0324f2fbd3f6d9f33edaff98b46278ba3e24772cf")
        val tx1Op1 = NexaTxOutpoint(tx1.idem, 1)
        check(tx1Op1.hash.toHex() == "fcdb86c435e9cd98a0966097374d100e93d70bda5be6fb64c437649ecc3c10bc")

        // Generated from
        // ./nexa-cli sendmany "" "{\"nexareg:nqtsq5g5hxqrjecul0dq3z30f7z9ensky7sqgvve04d7nncq\":2000, \"nexareg:qqclf2cerrv0h9a2de9f7xf63d33zyzvggyas9upun\":2345.67}"
        // e1cbdc430d412fb499bca38eb813610c394f7faad1571628ecf93282058b195d
        // ./nexa-cli getrawtransaction e1cbdc430d412fb499bca38eb813610c394f7faad1571628ecf93282058b195d
        val txHex = "0001003f38fff13ed68c13c277de28ba1a5012966bc12f01c3c1c5cea7f19d2d9d62d764222103e647e3609fa7faaf708f2126eeb87899990c6afcb0f024eff46dce8111743de3401346ba950ab51dbb39137b0d65cc7e188c4881e570f1b99aaa9f9b8e07d543df9b5e09c24bdeae914a708582cd51409254370c6fb5209d38608db75bb376037ffeffffff1dc69a3b000000000301400d03000000000017005114b98039671cfbda088a2f4f845cce1627a00431990047940300000000001976a91431f4ab1918d8fb97aa6e4a9f193a8b6311104c4288ac019823943b00000000170051147a5592a573f528d5e81fe467029166c7213faa4067000000"
        val txData = txHex.fromHex()
        val tx = NexaTransaction(ChainSelector.NEXAREGTEST, BCHserialized(txData, SerializationType.NETWORK))
        check(tx.outputs.size == 3)
        check(tx.inputs.size == 1)
        check(tx.version == 0)
        check(tx.lockTime == 103L)
        check(tx._inputs[0].type == 0.toByte())
        check((tx._inputs[0].spendable.outpoint as NexaTxOutpoint).hash == Hash256("d7629d2d9df1a7cec5c1c3012fc16b9612501aba28de77c2138cd63ef1ff383f"))
        check(tx._inputs[0].sequence == 4294967294)
        check(tx._inputs[0].spendable.amount ==  999999005L)

        check(tx._outputs[0].amount == 200000L)
        check(tx._outputs[0].type == NexaTxOutput.Type.TEMPLATE)
        check(tx._outputs[0].script.toHex() == "005114b98039671cfbda088a2f4f845cce1627a0043199")
        check(tx._outputs[1].amount == 234567L)
        check(tx._outputs[1].type == NexaTxOutput.Type.SATOSCRIPT)
        check(tx._outputs[1].script.toHex() == "76a91431f4ab1918d8fb97aa6e4a9f193a8b6311104c4288ac")
        check(tx._outputs[2].amount == 999564184L)
        check(tx._outputs[2].type == NexaTxOutput.Type.TEMPLATE)
        check(tx._outputs[2].script.toHex() == "0051147a5592a573f528d5e81fe467029166c7213faa40")

        // Check re-serialization
        val txSer = BCHserialized(SerializationType.NETWORK) + tx
        val hex = txSer.toHex()
        check (hex == txHex)
    }

    @Test
    fun testSerialize()
    {
        val chain = ChainSelector.NEXAREGTEST
        val outpoint = NexaTxOutpoint(Hash256("1f443a6340d2f805e0046b3bcea0d93830844b356edd72da053044dd3fb09f54"), 32)
        var sp = Spendable(chain)
        sp.secret = UnsecuredSecret(byteArrayOf(1,2,3))
        sp.outpoint = outpoint
        sp.priorOutScript = SatoshiScript(chain) + OP.DUP + OP.HASH160 + OP.push(ByteArray(20, { 0})) + OP.EQUALVERIFY + OP.CHECKSIG
        sp.addr = PayAddress("nexareg:qpaj30le3wqz04ldwnsj94x75u9kv782kvh6hgf5e0")
        sp.amount = 4567
        sp.redeemScript = SatoshiScript(chain) + OP.push(byteArrayOf(7,8))
        sp.commitHeight = 987654321
        sp.commitBlockHash = Guid(Hash256("1c2f4377f2222f167a9015c0ee2ca47200b368d5c17e3698962d0f307e565881"))
        sp.spentHeight = 5739243
        sp.spentBlockHash = Guid()

        val rawAddr = (sp.addr as PayAddress).data
        check(rawAddr.size == 20)

        val serScr = sp.priorOutScript.BCHserialize(SerializationType.DISK).flatten()
        val scr2 = SatoshiScript(chain)
        scr2.BCHdeserialize(BCHserialized(serScr, SerializationType.DISK))
        check(scr2.flatten().contentEquals(sp.priorOutScript.flatten()))

        val ser = sp.BCHserialize(SerializationType.DISK).flatten()

        var ser2 = BCHserialized(ser, SerializationType.DISK)
        val sp2 = Spendable(chain, ser2)

        check(sp.secret?.getSecret().contentEquals(sp2.secret!!.getSecret()))
        check(sp.outpoint == sp2.outpoint)
        check(sp.priorOutScript == sp.priorOutScript)
        check(sp.addr == sp2.addr)
        check(sp.amount == sp2.amount)
        check(sp.redeemScript.contentEquals(sp2.redeemScript))
        check(sp.commitHeight == sp2.commitHeight)
        check(sp.commitBlockHash == sp2.commitBlockHash)
        check(sp.spentHeight == sp2.spentHeight)
        check(sp.spentBlockHash == sp2.spentBlockHash)
        check(sp.spentUnconfirmed == sp2.spentUnconfirmed)
        check(sp.spendableUnconfirmed == sp2.spendableUnconfirmed)


    }

    @Test
    fun testHDderivation()
    {
        val secret = ByteArray(32, {_ -> 1})
        val index = 0
        val newSecret = libnexa.deriveHd44ChildKey(secret, 27, AddressDerivationKey.ANY, 0, true, index.toInt())
        LogIt.info("HD key: " + newSecret.first.toHex())
        assertEquals(newSecret.first.toHex(), "9c219cb84e3199b076aaa0f954418407b1e8d54f79103612fbaf04598bc8be55")

        val wdb = try
        {
            openKvpDB("testHDerivationWallet")!!
        }
        catch (e: org.nexa.libnexakotlin.LibNexaException)
        {
            if (e.message?.contains("with the Android application context") ?: false) return
            else throw e
        }

        // Test vector for wallet derivation path (MUST be BCH so address derivation matches hard coded addresses)
        val wal = Bip44Wallet(wdb,"bch", ChainSelector.BCH,"night wing zebra gate juice absorb student disease gospel maple depth trap")
        val knownAddrs = mutableListOf(
            "bitcoincash:qpwxcdytyswysm7qmvlnmu7pw4zhdgtzrctr5plkfn",
            "bitcoincash:qpxzpeh39jhduns9srpjc5jzhsmx9x95rsqeeyfask",
            "bitcoincash:qqauxecsjlk2y23ssnvljgpdd60p53sfkgear23ct3",
            "bitcoincash:qrs0muk7f48tzj8fkn5qze62vd6jad80fs6rwvsrs2",
            "bitcoincash:qply459hr7q2x9yxg7k53ge3hxykyvq635t4ahaj27",
            "bitcoincash:qqqxnsm3ku23nhhv353kzatrdqqq3pzfaqn3am8m9x",
            "bitcoincash:qqqxqj7wvyhgewqrxvc86k70tm4er83pxsn979qhe2",
            "bitcoincash:qztmzqp70fv69nuc2axvxq0fzjv8rpq0nyh8qglnax",
            "bitcoincash:qqhv59yk4dg25mmr7ntantdge97gw9lgwgzk7yu44v",
            "bitcoincash:qqhz9jpyqh386lqm2k0hunraxlpvf3w25sqt5qnmp4",
            "bitcoincash:qzmkzaau9c2qsh6nnch9n80re265f3x4cc4zcq4hkp",
            "bitcoincash:qr2f65qgg6pjqjd3e6r0gmu5xvjrtwpfdcvcremzgr",
            "bitcoincash:qz8zvka9vjdygyargww8w7ax6h55u5uzag9v4jrva9",
            "bitcoincash:qz2057l2t9mu97mv7xl28s2xv7xkjh5xwsylgaz0x2",
            "bitcoincash:qqfh83y0ynkgg893gvctjh4kfr03ew72vqef6npejl",
            "bitcoincash:qz0qkex8r0jcz5zx3s67fpeme26esfrm8yy0axhxm7",
            "bitcoincash:qpggr3sjzkm44xmktzdsjkjg7yapqtntj5jyt0cwm8",
            "bitcoincash:qr2v54yh7qehgl79y7lvmfc5248q6x7dsq5rqxhh29",
            "bitcoincash:qrja9sckkrckptkckpk6p0pwsuarm5qyr5w4fwvtn8",
            "bitcoincash:qq406tnvlgl52f93ruu9l7uqqu0gnxmk9u02kk28py")
        for (addr in knownAddrs)
        {
            val d = wal.generateDestination()
            assertEquals(d.address.toString(), addr)
        }

        val waln = Bip44Wallet(wdb,"nexa", ChainSelector.NEXA,"night wing zebra gate juice absorb student disease gospel maple depth trap")
        val knownAddrsn = mutableListOf(
          "nexa:nqtsq5g5fxz9qyup04g288qy2pxf9aemxjysnzqnn2nky4xw",
          "nexa:nqtsq5g55t9699mcue00frjqql5275r3et45c3dqtxzfz8ru",
          "nexa:nqtsq5g5skc5xfw7dzu2jw7hktf3tg053drevxx94yx44gjc",
          "nexa:nqtsq5g53zpuftghudhhg6szwwerymtrhnwzu7ufrh8yg8t5",
          "nexa:nqtsq5g5pehl9a45ne68sgzjkdneq2lw3qumyuzjlams0jf4")
        for (addr in knownAddrsn)
        {
            val d = waln.generateDestination()
            //println(d)
            assertEquals(d.address.toString(), addr)
        }
    }

    @Test
    fun testBlockchain()
    {
        val workBytes = libnexa.getWorkFromDifficultyBits(0x172c4e11)
        val work = BigInteger.fromByteArray(workBytes, Sign.POSITIVE)
        check(work == BigInteger.parseString("5c733e87890743fed65",16))
    }


    @Test
    fun testAddressConversion()
    {
        run {
            val p = PayAddress("bchreg:qr02hs6rq52uvkz7fkum28hd0zqewgzhlgjjedf05z")
            check(p.type == PayAddressType.P2PKH)
            check(p.blockchain == ChainSelector.BCHREGTEST)
            check(p.data.toHex() == "deabc3430515c6585e4db9b51eed7881972057fa")
            val q = PayAddress(ChainSelector.BCHREGTEST, p.type, p.data)
            check(p == q)
        }

        run {
            val p = PayAddress("bchtest:qpm4a0mxj3gcmyj38xe2uss50lzh2ww2qqhgf8gzm3")
            check (p.type == PayAddressType.P2PKH)
            check (p.blockchain == ChainSelector.BCHTESTNET)
            check (p.data.toHex() == "775ebf6694518d925139b2ae42147fc57539ca00")
        }

        run {
            val p = PayAddress("nexareg:qr02hs6rq52uvkz7fkum28hd0zqewgzhlgeakv8hkz")
            check(p.type == PayAddressType.P2PKH)
            check(p.blockchain == ChainSelector.NEXAREGTEST)
            check(p.data.toHex() == "deabc3430515c6585e4db9b51eed7881972057fa")
            val q = PayAddress(ChainSelector.NEXAREGTEST, p.type, p.data)
            check(p == q)
        }
        run {
            val p = PayAddress("nexatest:qzyk3wt08a9gn2lrfv5k699yj72y8cdusvazcyd4ju")
            check (p.type == PayAddressType.P2PKH)
            check (p.blockchain == ChainSelector.NEXATESTNET)
            check (p.data.toHex() == "8968b96f3f4a89abe34b296d14a4979443e1bc83")
        }
        run {
            val addr = "nexatest:nqtsq5g5ev7nfvl29wwza00s5gjw7v4yvgzrfg5kce2qxrmy"
            val p = PayAddress(addr)
            check (p.type == PayAddressType.TEMPLATE)
            check (p.blockchain == ChainSelector.NEXATESTNET)
            check (p.toString() == addr)
            val s = p.outputScript()
            check(s.toAsm() == "0 1 cb3d34b3ea2b9c2ebdf0a224ef32a4620434a296h")
        }

        run {
            val gid = GroupId("nexareg:trc3kvg0r90jwdrglm7kgsu52xzasltwp7pvy5f5wddwgdw82sqqqw5250xt7")
            check(gid.isSubgroup() == false)
            check(gid.data.toHex() == "f11b310f195f273468fefd6443945185d87d6e0f82c25134735ae435c7540000")
            val gid2 = GroupId(ChainSelector.NEXAREGTEST,"f11b310f195f273468fefd6443945185d87d6e0f82c25134735ae435c7540000".fromHex())
            check(gid == gid2)
        }

    }

    @Test
    fun address()
    {
        val cs = ChainSelector.NEXAREGTEST
        val scr = SatoshiScript.ungroupedP2pkt(cs, byteArrayOf(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23))  // byte array would normally be a pubkey
        val pa = PayAddress(cs, PayAddressType.P2PKT, scr.asSerializedByteArray() )
        check(pa.toString() == "nexareg:nqtsq5g5qfsfy8qe9zqlm5gz96d7nf3eawm7d8h4a2ke9h0k")
    }

    @Test
    fun testTransaction()
    {
        val ch = ChainSelector.NEXAREGTEST
        var tx = NexaTransaction(ch)
        var in1 = Spendable(ch,NexaTxOutpoint("00112233445566778899aabbccddeeff000102030405060708090a0b0c0d0e0f"), 10001)
        tx._inputs.add(NexaTxInput(ch, in1, SatoshiScript(ch), 0xffffffff))
        var out1 = NexaTxOutput(ch,10001, SatoshiScript(ch, "76a914431ecec94e0a920a7972b084dcfabbd69f61691288ac"))
        tx._outputs.add(out1)
        check(tx.outpoints.size == 1)
        var ser = tx.BCHserialize(SerializationType.NETWORK)
        LogIt.info("tx: " + ser.toHex())

        ser.flatten()
        var tx2 = NexaTransaction(ch, ser)
        var ser2 = tx2.BCHserialize(SerializationType.NETWORK)
        ser2.flatten()
        LogIt.info("tx: " + ser2.toHex())
        check(ser.toHex() == ser2.toHex())
    }

    @Test
    fun testKeys()
    {
        val ch = ChainSelector.NEXAREGTEST
        // Test that BCHoutpoint is a proper map key
        val paymentHistory: MutableMap<NexaTxOutpoint, Int> = mutableMapOf()
        val testOutpoint = NexaTxOutpoint(Hash256("00112233445566778899aabbccddeeff000102030405060708090a0b0c0d0e0f"), 0)
        paymentHistory[testOutpoint] = 1
        check(paymentHistory[testOutpoint] == 1)

        val testOutpoint2 = NexaTxOutpoint(Hash256("00112233445566778899aabbccddeeff000102030405060708090a0b0c0d0e0f"), 0)
        check(paymentHistory[testOutpoint2] != null)
        val testOutpoint3 = NexaTxOutpoint(Hash256("00112233445566778899aabbccddeeff000102030405060708090a0b0c0d0e0f"), 1)
        check(paymentHistory[testOutpoint3] == null)
        val testOutpoint4 = NexaTxOutpoint(Hash256("00112233445566778899aabbccddeeff000102030405060708090a0b0c0d0e00"), 0)
        check(paymentHistory[testOutpoint4] == null)


        // Test that PayAddress is a proper map key
        var receiving: MutableMap<PayAddress, Int> = mutableMapOf()

        val inserted = PayAddress(ch, PayAddressType.P2PKH,"0123456789abcdef01230123456789abcdef0123".fromHex())
        receiving[inserted] = 1

        check(receiving.contains(inserted))
        check(receiving.containsKey(PayAddress(ch, PayAddressType.P2PKH,"0123456789abcdef01230123456789abcdef0123".fromHex())))
    }

    @Test
    fun testSignVerifyMessage()
    {
        val testMessage = "This is a test"

        val sbytes = ByteArray(32, {_ -> 2})

        val identityDest = Pay2PubKeyHashDestination(ChainSelector.NEXAREGTEST, UnsecuredSecret(sbytes))

        val secret = identityDest.secret
        val address = identityDest.address

        val sig = libnexa.signMessage(testMessage.toByteArray(), secret.getSecret())
        check(sig != null)

        if (true)
        {

            val verify = libnexa.verifyMessage(testMessage.toByteArray(), address.data, sig)
            check(verify != null)
            check(verify.size != 0)
            check(verify contentEquals identityDest.pubkey)

            // Try bad verifications

            if (true)
            {
                val badaddr = ByteArray(10, { _ -> 3 })
                val badVerify = libnexa.verifyMessage(testMessage.toByteArray(), badaddr, sig)
                check(badVerify == null)
            }

            if (true)
            {
                val badaddr = ByteArray(20, { _ -> 3 })
                val badVerify = libnexa.verifyMessage(testMessage.toByteArray(), badaddr, sig)
                check(badVerify == null)
            }

            if (true)
            {
                val badaddr = address.data.copyOf()
                badaddr[0] = (badaddr[0].toByte() + 1.toByte()).toByte()  // Change the address a little
                val badVerify = libnexa.verifyMessage(testMessage.toByteArray(), badaddr, sig)
                check(badVerify == null)
            }

            if (true)
            {
                val differentMessage = "This is another test"
                val badVerify = libnexa.verifyMessage(differentMessage.toByteArray(), address.data, sig)
                check(badVerify == null)
            }

            if (true)
            {
                val badSig = sig.copyOf()
                badSig[0] = (badSig[0] + 1.toByte()).toByte()
                val badVerify = libnexa.verifyMessage(testMessage.toByteArray(), address.data, badSig)
                check(badVerify == null)
            }
        }
    }

    @Test
    fun testScript()
    {
        val ch = ChainSelector.NEXAREGTEST
        val fakepubkey = "0123456789abcdef01230123456789abcdef0123".fromHex()

        val s1 = SatoshiScript(ch, "0051147faf7c2303f8f5c68715ffc690ccdefc20c36f58", SatoshiScript.Type.TEMPLATE)
        val addr = s1.address
        check(addr.toString() == "nexareg:nqtsq5g507hhcgcrlr6udpc4llrfpnx7lssvxm6cks6tkyt5")
        // P2PKH

        // Basic match
        val P2PKH1 = SatoshiScript(ch) + OP.DUP + OP.HASH160 + OP.push(ByteArray(20, { 0})) + OP.EQUALVERIFY + OP.CHECKSIG
        check(P2PKH1.match() != null)

        // A few bad matches
        val P2PKH2 = SatoshiScript(ch) + OP.DUP + OP.HASH160 + OP.push(ByteArray(20, { 0})) + OP.EQUALVERIFY
        check(P2PKH2.match() == null)

        val P2PKH3 = SatoshiScript(ch) + OP.HASH160 + OP.push(ByteArray(20, { 0})) + OP.EQUALVERIFY + OP.CHECKSIG
        check(P2PKH3.match() == null)

        val P2PKH4 = SatoshiScript(ch) + OP.DUP + OP.HASH160 + OP.push(ByteArray(21, { 0})) + OP.EQUALVERIFY + OP.CHECKSIG
        check(P2PKH4.match() == null)

        val P2PKH5 = SatoshiScript(ch) + OP.DUP + OP.HASH160 + OP.push(ByteArray(19, { 0})) + OP.EQUALVERIFY + OP.CHECKSIG
        check(P2PKH5.match() == null)


        // Test different constructions
        val P2PKH6 = SatoshiScript(ch, SatoshiScript.Type.SATOSCRIPT, OP.DUP, OP.HASH160, OP.push("0123456789abcdef01230123456789abcdef0123".fromHex()), OP.EQUALVERIFY, OP.CHECKSIG)
        val P2PKH7 = SatoshiScript(ch, SatoshiScript.Type.SATOSCRIPT, OP.DUP, OP.HASH160, OP.PUSHDATA1, OP(byteArrayOf(20)), OP("0123456789abcdef01230123456789abcdef0123".fromHex()), OP.EQUALVERIFY, OP.CHECKSIG)

        if (true)
        {
            val (type, params) = P2PKH6.match() ?: throw AssertionError("should have matched P2PKH template")
            check(type == PayAddressType.P2PKH)
            check(params[0].contentEquals(fakepubkey))
        }

        if (true)
        {
            val (type, params) = P2PKH7.match() ?: throw AssertionError("should have matched P2PKH template")
            check(type == PayAddressType.P2PKH)
            check(params[0].contentEquals(fakepubkey))
        }

        val P2PKH8 = SatoshiScript(ch) + OP.DUP + OP.HASH160 + OP.push("0123456789abcdef01230123456789abcdef0123".fromHex()) + OP.EQUALVERIFY + OP.CHECKSIG
        check(P2PKH6.contentEquals(P2PKH8))

        val (type1, params1) = P2PKH6.match() ?: throw AssertionError("should have matched P2PKH template")
        check (type1 == PayAddressType.P2PKH)
        check(params1[0].contentEquals(fakepubkey))
    }

    @Test
    fun testBip39()
    {
        // LogIt.info("testBip39")
        val result = GenerateEntropy(128)
        check(result.size == 128/8)
        // just check a common error if the random is bad (0s)
        check(result[0] != 0.toByte() || result[1] != 0.toByte() || result[2] != 0.toByte() || result[3] != 0.toByte())

            for (tv in bip39TestVector)
        {
            val b = tv[0].fromHex()
            val words = GenerateBip39SecretWords(b)
            check(words == tv[1])
            val seed = generateBip39Seed(words,"TREZOR", 64)
            // LogIt.info(seed.toHex())
            check(seed.toHex() == tv[2])
        }

    }
}
