import org.nexa.libnexakotlin.*
import org.nexa.threads.*
import kotlin.test.*
import kotlinx.serialization.Serializable

private val LogIt = GetLog("ElectrumProtocolTests.kt")

// If you are running on a real device, you may need to talk to the host via your local network.  There is no way to figure out that that is since no code
// is running on the host.
val TEST_HOST_IP = "127.0.0.1"

// The IP address of the host machine: Android sets up a fake network with the host hardcoded to this IP
val EMULATOR_HOST_IP = "10.0.2.2"

val ELECTRUM_POSSIBLE_IPS = listOf("127.0.0.1", EMULATOR_HOST_IP, TEST_HOST_IP)

@Serializable
data class BannerReply(val result: String)

class ElectrumProtocolTests
{
    lateinit var cnxn:ElectrumClient
    @BeforeTest
    fun beforeMethod()
    {
        initializeLibNexa()

        for (ip in ELECTRUM_POSSIBLE_IPS)
        {
            try
            {
                LogIt.info("Trying Electrum@${ip}:${DEFAULT_NEXAREG_TCP_ELECTRUM_PORT}")
                cnxn = ElectrumClient(ChainSelector.NEXAREGTEST,
                    ip,
                    DEFAULT_NEXAREG_TCP_ELECTRUM_PORT,
                    "Electrum@${ip}:${DEFAULT_NEXAREG_TCP_ELECTRUM_PORT}", useSSL = false, connectTimeoutMs = 500)
                cnxn.start()
                return
            }
            catch (e: ElectrumConnectError) // cannot connect
            {
                LogIt.info("Cannot find Rostrum (electrum protocol server) at ${ip}:${DEFAULT_NEXAREG_TCP_ELECTRUM_PORT}")
                continue
            }
            catch (e: Exception)
            {
                LogIt.info("This test requires an electrum cash server running on regtest at ${EMULATOR_HOST_IP}:${DEFAULT_NEXAREG_TCP_ELECTRUM_PORT}")
                logThreadException(e)
                LogIt.warning("Skipping Electrum tests: ${e}")
                throw e
            }
        }
        LogIt.info("This test requires an electrum cash server running on regtest at ${EMULATOR_HOST_IP}:${DEFAULT_NEXAREG_TCP_ELECTRUM_PORT}")
        throw IllegalStateException("No Rostrum servers available")
    }

    @Test
    fun testelectrumclient()
    {
        val ret = cnxn.call("server.version", listOf("4.0.1", "1.4"), 1000)
        if (ret!=null) LogIt.info(sourceLoc() + ": Server Version returned: " + ret)

        val version = cnxn.version()
        LogIt.info(sourceLoc() + ": Version API call returned: " + version.first + " " + version.second)

        val features = cnxn.features()
        LogIt.info(sourceLoc() + ": genesis block hash:" + features.genesis_hash)
        check("d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371" == features.genesis_hash)
        check("sha256" == features.hash_function)
        check(features.server_version.contains("Rostrum"))  // Clearly this may fail if you connect a different server to this regression test

        val ret2 = cnxn.call("blockchain.block.header", listOf(100, 102), 1000)
        LogIt.info(ret2 ?: "nothing")

        try {
            cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d00", 5000)  // doesn't exist
            assert(false)
        }
        catch(e: ElectrumNotFound)
        {
            if (e.message!!.contains("still syncing"))
            {
                LogIt.warning("This test is more effective if you generate 101 blocks on regtest")
            }
            else assert(e.message!!.contains("tx not in blockchain or mempool"))
        }

        try
        {
            cnxn.getTx("zz5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d", 5000) // bad hash
            assert(false)
        }
        catch(e: ElectrumIncorrectRequest)
        {
        }

        try
        {
            cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d", 5000) // bad hash (short)
            assert(false)
        }
        catch(e: ElectrumIncorrectRequest)
        {
        }

        try
        {
            cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d".repeat(10), 1000) // bad hash (large)
            assert(false)
        }
        catch(e: ElectrumIncorrectRequest)
        {
        }

        val (name, ver) = cnxn.version(1000)
        LogIt.info("Server name $name, server version $ver")

        cnxn.call("server.banner", null) {
            LogIt.info("Server Banner reply is: " + it)
        }

        cnxn.parse("server.banner", null, BannerReply.serializer() ) {
            LogIt.info("Server Banner reply is: " + it!!.result)
        }

/*
        cnxn.subscribe("blockchain.headers.subscribe") {
            LogIt.info("Received blockchain header notification: ${it}")
        }
*/
        // Find the tip
        var tipHeight = -1
        cnxn.subscribeHeaders {
            tipHeight = it.height.toInt()
        }
        while (tipHeight == -1) millisleep(200U)

        val header = cnxn.getHeader(10000000)  // beyond the tip
        LogIt.info(header.toString())

        try
        {
            cnxn.getHeader(-1)  // beyond the tip
            assert(false)
        }
        catch (e: ElectrumIncorrectRequest)
        {
            LogIt.info(e.toString())
            assert(true)
        }
        LogIt.info(header.toString())


        // This code gets the first coinbase transaction and then checks its history.  Based on the normal regtest generation setup, there should be at least 100
        // blocks that generate to this same output.
        val firsttx = cnxn.getTxAt(1, 0)
        LogIt.info(firsttx.toHex())
        firsttx.debugDump()

        val txBlkHeader = NexaBlockHeader(BCHserialized(cnxn.getHeader(1), SerializationType.HASH))

        // TODO: check server capabilities
        val firstUse = cnxn.getFirstUse(firsttx.outputs[0].script)
        LogIt.info("first use in block ${firstUse.block_hash}:${firstUse.block_height}, transaction ${firstUse.tx_hash}")
        check(firstUse.tx_hash != null)
        check(firstUse.block_height!! == 1)
        check(firsttx.id.toHex() == firstUse.tx_hash!!)
        check(txBlkHeader.hash.toHex() == firstUse.block_hash)

        // doesn't exist
        try
        {
            val firstUse2 = cnxn.getFirstUse("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d00")
            LogIt.info(firstUse2.toString())
            assert(false)
        }
        catch(e: ElectrumNotFound)
        {
            // expected
        }

        // Test getUtxo

        // First check an unspent tx
        val lastcb = cnxn.getTxAt(tipHeight.toInt(), 0)
        val unspentUtxo = cnxn.getUtxo(lastcb.outpoints[0])
        LogIt.info(unspentUtxo.status)
        check(unspentUtxo.height == tipHeight)
        check(unspentUtxo.spent.height == null)
        check(unspentUtxo.status == "unspent")

        // next test getUtxo with a nonexistent tx
        try
        {
            val noTx = cnxn.getUtxo("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d00" )
            LogIt.info(noTx.toString())
            assert(false)
        }
        catch(e: ElectrumNotFound)
        {
            // expected
        }

        // Finally check a spent tx
        try
        {
            cnxn.getUtxo(firsttx.idem.toHex())
            assert(false)
        }
        catch (e: ElectrumNotFound)
        {
            assert(true)
        }

        val uses = cnxn.getHistory(firsttx.outputs[0].script)
        for (use in uses)
        {
            LogIt.info("used in block ${use.first} tx ${use.second}")
        }

        assert(uses.size >= 100)  // Might be wrong if the regtest chain startup is changed.

        val headers = cnxn.getHeadersFor(ChainSelector.NEXAREGTEST, 0, 1000, 10000)
        for (i in headers)
        {
            val hdr = cnxn.getHeader(i.height.toInt(), 1000)
            check(hdr.toHex() contentEquals i.BCHserialize(SerializationType.NETWORK).toHex())
        }

        cnxn.close()
    }
}
