// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin
import org.nexa.threads.*

private val LogIt = GetLog("BU.BCHtransaction")

data class BchTxOutpoint(val txid: Hash256, var idx: Long) : iTxOutpoint
{
    constructor(stream: BCHserialized) : this(Hash256(stream), stream.deuint32())
    {
    }

    constructor(txhex: String, index: Long) : this(Hash256(txhex), index)

    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        return BCHserialized(format) + txid + BCHserialized.uint32(idx)
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        txid.BCHdeserialize(stream)
        idx = stream.deuint32()
        return stream
    }

    fun getTransactionOrder(): Long
    {
        return idx
    }

    override fun toString(): String
    {
        return "BCHoutpoint(\"" + txid.toHex() + "\"," + idx + ")"
    }
    override fun toHex(): String
    {
        return BCHserialize(SerializationType.NETWORK).flatten().toHex()
    }
}

/** Spend a coin (aka UTXO, prevout) in a transaction */
@cli(Display.Simple, "Input of a bitcoin transaction")
class BchTxInput(val chainSelector: ChainSelector) : iTxInput
{
    @cli(Display.Simple, "What UTXO is being spent")
    override var spendable = Spendable(chainSelector) //!< What prevout to spend

    @cli(Display.Simple, "Satisfier script")
    override var script = SatoshiScript(chainSelector) //!< Satisfier script that proves you can spend this prevout

    @cli(Display.Dev, "enables locktime if not 0xffffffff")
    var sequence: Long = 0xffffffff //!< enable locktime if not 0xffffffff

    override fun toString(): String
    {
        return """{ "utxo" : ${spendable.toString()}, "satisfier" : "${script.toHex()}" }"""
    }

    override fun copy(): iTxInput = BchTxInput(this)

    constructor(copy: iTxInput): this(copy as BchTxInput)
    /** Copy constructor */
    constructor(copy: BchTxInput) : this(copy.chainSelector)
    {
        spendable = Spendable(copy.chainSelector, copy.BCHserialize(SerializationType.NETWORK))
        val ser = copy.script.BCHserialize(SerializationType.NETWORK)
        ser.flatten()
        script = SatoshiScript(copy.chainSelector, ser)
        sequence = copy.sequence
    }

    @cli(Display.Dev, "constructor")
    constructor(chainSelector: ChainSelector, spend: Spendable, scriptp: SatoshiScript, seq: Long = 0xffffffff) : this(chainSelector)
    {
        spendable = spend
        script = scriptp
        sequence = seq
    }

    @cli(Display.Dev, "constructor")
    constructor(spend: Spendable, scriptp: SatoshiScript, seq: Long = 0xffffffff) : this(scriptp.chainSelector)
    {
        spendable = spend
        script = scriptp
        sequence = seq
    }

    @cli(Display.Dev, "constructor, fill in the satisfier script later")
    constructor(spend: Spendable, seq: Long = 0xffffffff) : this(spend.chainSelector)
    {
        spendable = spend
        script = SatoshiScript(spend.chainSelector)
        sequence = seq
    }

    /** deserialization constructor */
    @cli(Display.Dev, "deserialization constructor")
    constructor(chainSelector: ChainSelector, buf: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    @cli(Display.Dev, "serialization")
    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        var ret = spendable.BCHserialize(format) + script + BCHserialized.uint32(sequence)
        return ret
    }

    @cli(Display.Dev, "deserialization")
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized  //!< Deserializer
    {
        spendable = Spendable(chainSelector, stream)
        script = SatoshiScript(chainSelector, stream)
        sequence = stream.deuint32()
        return stream
    }
}

//@Serializable
/** Output of a bitcoin transaction */
@cli(Display.Simple, "Output of a bitcoin transaction")
class BchTxOutput(val chainSelector: ChainSelector) : iTxOutput
{
    @cli(Display.Simple, "Spend quantity in satoshis")
    override var amount: Long = 0 //!< Amount in satoshis assigned to this output

    @cli(Display.Simple, "Constraint script")
    override var script = SatoshiScript(chainSelector)  //!< The "predicate" script that controls spendability

    /** explicit constructor */
    @cli(Display.Dev, "Constructor")
    constructor(chainSelector: ChainSelector, amt: Long, scr: SatoshiScript = SatoshiScript(chainSelector)) : this(chainSelector)
    {
        amount = amt
        script = scr
    }

    @cli(Display.Dev, "Constructor")
    constructor(amt: Long, scr: SatoshiScript) : this(scr.chainSelector)
    {
        amount = amt
        script = scr
    }

    /** Deserialization constructor */
    @cli(Display.Dev, "Deserialization constructor")
    constructor(chainSelector: ChainSelector, buf: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    /** Serializer */
    @cli(Display.Dev, "Serialize this output")
    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        var ret = BCHserialized(format) + amount + script
        return ret
    }

    @cli(Display.Dev, "Deserializer: Overwrite this object with the contents in the passed stream")
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized  //!< Deserializer
    {
        amount = stream.deint64()
        script = SatoshiScript(chainSelector, stream)
        return stream
    }

    @cli(Display.Dev, "Convert to a human readable form")
    override fun toString(): String
    {
        return amount.toString() + " to " + (script.address?.toString() ?: ("script " + script.toString()))
    }

    override fun hashCode(): Int
    {
        return amount.hashCode() xor script.hashCode()  // java 1.7 (android API 23-) does not contain Long.hashCode
    }

    override fun equals(other: Any?): Boolean
    {
        if (other !is NexaTxOutput) return false
        if (other.amount != amount) return false
        if (!other.script.contentEquals(this.script)) return false
        return true
    }

    @cli(Display.Simple, "Return group token information, or null if not grouped")
    fun groupInfo(): GroupInfo? = script.groupInfo(amount)
}

//@Serializable
/** Bitcoin transaction */
@cli(Display.Simple, "A blockchain transaction")
class BchTransaction(override val chainSelector: ChainSelector) : iTransaction
{
    protected val mutex = Mutex()
    protected var hashData: Hash256? = null  //!< cached hash of this transaction
    protected var sizeData: Long? = null  //!< cached size of this transaction

    var _inputs = mutableListOf<BchTxInput>()
    var _outputs = mutableListOf<BchTxOutput>()

    @cli(Display.Simple, "inputs to the transaction")
    override val inputs: MutableList<out iTxInput>
        get()
        {
            return _inputs
        }
    override fun setInputs(inputs: MutableList<out iTxInput>):iTransaction
    {
        _inputs.clear()
        for (i in inputs)
        {
            _inputs.add(i as BchTxInput)  // throw an exception if an input of the wrong type is in the list
        }
        return this
    }


    @cli(Display.Simple, "transaction outputs")
    override val outputs: MutableList<out iTxOutput>
        get()
        {
            return _outputs
        }
    override fun setOutputs(outputs: MutableList<out iTxOutput>):iTransaction
    {
        _outputs.clear()
        for (i in outputs)
        {
            _outputs.add(i as BchTxOutput)  // throw an exception if an input of the wrong type is in the list
        }
        return this
    }

    @cli(Display.Simple, "version")
    override var version: Int = 1  //!< transaction version

    @cli(Display.Simple, "transaction lock time")
    override var lockTime: Long = 0  //!< transaction lock time

    override fun add(input: iTxInput): iTransaction
    {
        assert(input is BchTxInput)
        assert(input.spendable.outpoint is BchTxOutpoint)
        _inputs.add(input as BchTxInput)
        return this
    }
    override fun add(output: iTxOutput):iTransaction
    {
        assert(output is BchTxOutput)
        _outputs.add(output as BchTxOutput)
        return this
    }

    companion object
    {
        fun fromHex(chain: ChainSelector, hex: String, serializationType: SerializationType = SerializationType.UNKNOWN): BchTransaction
        {
            val bytes = hex.fromHex()
            return BchTransaction(chain, BCHserialized(bytes, serializationType))
        }
    }

    /** Return the outpoints in this transaction */
    @cli(Display.Dev, "transaction outpoints")
    override val outpoints: Array<BchTxOutpoint>
        get()
        {
            val txhash = hash
            val ret = mutableListOf<BchTxOutpoint>()
            for (count in 0..outputs.size)
            {
                ret.add(BchTxOutpoint(txhash, count.toLong()))
            }
            return ret.toTypedArray()
        }

    @cli(Display.Dev, "Satoshis being spent by this transaction")
    val inputSatoshis: Long
        get()
        {
            return inputs.fold(0L) { sum, inp -> sum + inp.spendable.amount }
        }

    @cli(Display.Dev, "transaction outpoints")
    fun spendable(index: Int): Spendable
    {
        if (index >= outputs.size || index < 0)
        {
            throw IndexOutOfBoundsException("illegal transaction output index")
        }
        val ret = Spendable(chainSelector, BchTxOutpoint(hash, index.toLong()), outputs[index].amount)
        ret.priorOutScript = outputs[index].script
        ret.commitTxIdem = Guid(this.hash)
        ret.commitTx = this
        return ret
    }

    @cli(Display.Dev, "return the output index corresponding to this constraint script")
    fun findOutput(s: SatoshiScript): Int
    {
        for ((idx, out) in outputs.withIndex())
        {
            if (out.script.contentEquals(s)) return idx
        }
        throw NoSuchElementException("output script not found")
    }

    @cli(Display.Dev, "return the output index corresponding to this constraint script")
    fun findOutput(addr: PayAddress): Int = findOutput(addr.constraintScript())

    /** Force recalculation of hash. To access the hash just use #hash */
    fun calcHash(): Hash256
    {
        return mutex.synchronized {
            val ser = BCHserialize(SerializationType.HASH)
            val serbytes = ser.flatten()
            val hash = Hash256(libnexa.hash256(serbytes))
            hashData = hash
            sizeData = serbytes.size.toLong()
            hash
        }
    }

    /** If you change transaction data, call this function to force lazy recalculation of the hash */
    override fun changed()
    {
        return mutex.synchronized {
            hashData = null
            sizeData = null
        }
    }


    @cli(Display.Simple, "transaction size in bytes")
    override val size: Long
        get()
        {
            return mutex.synchronized {
                val ret = sizeData
                if (ret != null) ret
                else
                {
                    calcHash()
                    val ret2 = sizeData
                    ret2!!  // must have been calced and I'm locked
                }
            }
        }

    @cli(Display.Simple, "transaction fee")
    override val fee: Long
        get()
        {
            var fee: Long = 0
            for (i in inputs)
            {
                fee += i.spendable.amount
            }
            for (out in outputs)
            {
                fee -= out.amount
            }
            return fee
        }

    /** Return this transaction's hash.  Uses a cached value if one exists.  If you change the
     * transaction be sure to call calcHash() or invalidateHash() to update this value either greedily or lazily respectively */
    @cli(Display.Simple, "transaction id")
    val hash: Hash256
        get()
        {
            return mutex.synchronized {
                val temp = hashData
                if (temp != null) temp
                else calcHash()
            }
        }
        //set(value)
        //{
        //    hashData = value
        //}

    @cli(Display.Simple, "transaction id")
    override val id: Hash256
        get()
        {
            return mutex.synchronized {
                val temp = hashData
                if (temp != null) temp
                else calcHash()
            }
        }

    /** Return this transaction's id  Uses a cached value if one exists.  If you change the
     * transaction be sure to call changed() to update this value */
    @cli(Display.Simple, "transaction id")
    override val idem: Hash256
        get()
        {
            return mutex.synchronized {
                val temp = hashData
                if (temp != null) temp
                else calcHash()
            }
        }


    /** Returns true if this transaction is a coinbase tx */
    @cli(Display.Dev, "is this transaction a coinbase?")
    override fun isCoinbase(): Boolean
    {
        // As per satoshi codebase
        if (inputs.size != 1) return false  // there must be exactly 1 input
        val opit = inputs[0].spendable.outpoint as BchTxOutpoint
        if (opit.txid == Hash256() && opit.idx == 0xffffffff) return true  // whose prevout must have a 0 hash and -1 idx
        return false
    }

    @cli(Display.Simple, "Return true if this is an ownership challenge transaction.  This means that this transaction is invalid and so can never be actually committed to the blockchain.")
    override fun isOwnershipChallenge(): Boolean
    {
        throw UnsupportedInBlockchain("Ownership challenge transactions are not supported in BCH")
    }


    /** Serialize this transaction and return it as a hex encoded string */
    @cli(Display.Simple, "return the serialized hex representation")
    override fun toHex(): String
    {
        var ser = BCHserialize(SerializationType.NETWORK)
        return ser.flatten().toHex()
    }

    /** Default display: print the transaction hash */
    override fun toString(): String = hash.toHex()

    override fun debugDump()
    {
        LogIt.info("size: $size fee: $fee feerate: ${feeRate} inputs: ${inputs.size} outputs: ${outputs.size}")
        LogIt.info("INPUTS:")
        for (i in inputs)
        {
            LogIt.info("  ${i.spendable.amount} ${i.spendable} confirmed at: ${i.spendable.commitHeight}")
        }
    }

    /** Deserialization constructor */
    constructor(chainSelector: ChainSelector, buf: ByteArray, format: SerializationType) : this(chainSelector, BCHserialized(buf, format))

    /** Deserialization constructor */
    constructor(chainSelector: ChainSelector, buf: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized  //!< Serializer
    {
        for (i in _inputs)
        {
            assert(i.spendable.outpoint is BchTxOutpoint)
        }
        // If HASH serialization differs from NETWORK, then the "size" calculation will be incorrect!
        var ret = BCHserialized(format) + version + _inputs + _outputs + BCHserialized.uint32(lockTime)
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized  //!< Deserializer
    {
        version = stream.deint32()
        _inputs = stream.delist({ b -> BchTxInput(chainSelector, b) })
        _outputs = stream.delist({ b -> BchTxOutput(chainSelector, b) })
        lockTime = stream.deuint32()
        changed()
        return stream
    }

    override fun appendableSighash(extendInputs:Boolean, extendOutputs:Boolean): ByteArray
    {
        throw UnimplementedException("BCH sighash for appendable transactions not implemented")
    }

}
