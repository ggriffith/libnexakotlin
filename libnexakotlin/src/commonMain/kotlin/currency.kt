package org.nexa.libnexakotlin

import com.ionspin.kotlin.bignum.decimal.BigDecimal
import com.ionspin.kotlin.bignum.decimal.DecimalMode
import com.ionspin.kotlin.bignum.decimal.RoundingMode

const val SATperNEX = 100L
const val SATperUBCH = 100L
const val SATperBCH = 100L * 1000L * 1000L
const val bchDecimals = 8   // !< The number of decimal places needed to express 1 satoshi in the "normal" currency units
const val uBchDecimals = 2  //!< The number of decimal places needed to express 1 Satoshi (or equivalent) in the units used in the GUI
const val NexaDecimals = 2  //!< The number of decimal places needed to express 1 Satoshi (or equivalent) in the units used in the GUI
const val currencyScale = 16  //!<  How many decimal places we need to do math without creating cumulative rounding errors
val fiatFormat = DecimalFormat("##,##0.00")  //!< How all fiat currencies are displayed (2 decimal places)
val uBchFormat = DecimalFormat("##,###,##0.00")  //!< How the mBCH crypto is displayed (5 optional decimal places)
val mBchFormat = DecimalFormat("##,##0.#####")  //!< How the mBCH crypto is displayed (5 optional decimal places)
val nexFormat = DecimalFormat("##,###,##0.00")  //!< How the NEX crypto is displayed (2 optional decimal places)
val bchFormat = DecimalFormat("#######0.########")  //!< How to display BCH crypto
val serializeFormat = DecimalFormat("###########0.########")  //!< How convert BigDecimals to strings in preparation for serialization/deserialization
val currencyMath = DecimalMode(32L, RoundingMode.ROUND_HALF_AWAY_FROM_ZERO,currencyScale.toLong())  //!< tell the system details about how we want bigdecimal math handled
val nexaMathMode = DecimalMode(32L, RoundingMode.ROUND_HALF_AWAY_FROM_ZERO, NexaDecimals.toLong())  //!< tell the system details about how we want bigdecimal math handled
val uBchMathMode = DecimalMode(32L, RoundingMode.ROUND_HALF_AWAY_FROM_ZERO, uBchDecimals.toLong())  //!< tell the system details about how we want bigdecimal math handled

val CURRENCY_ZERO = CurrencyDecimal(0)
val CURRENCY_0 = CURRENCY_ZERO
val CURRENCY_NEG1 = CurrencyDecimal(-1)
val CURRENCY_1 = CurrencyDecimal(1)

/** Create a BigDecimal that is appropriate for currency mathematics (with lots of decimal places) */
fun CurrencyDecimal(a: Long) = BigDecimal.fromLong(a, currencyMath)

/** Create a BigDecimal that is appropriate for currency mathematics (with lots of decimal places) */
fun CurrencyDecimal(a: Int) = BigDecimal.fromInt(a, currencyMath)

/** Create a BigDecimal that is appropriate for Nexa currency mathematics (with 2 decimal places) */
fun NexaDecimal(a: Long) = BigDecimal.fromLong(a, nexaMathMode)

/** Create a BigDecimal that is appropriate for Nexa currency mathematics (with 2 decimal places) */
fun NexaDecimal(a: Int) = BigDecimal.fromInt(a, nexaMathMode)


/** Create a BigDecimal that is appropriate for u-BCH (micro bitcoin cash) currency mathematics (with 2 decimal places) */
fun UbchDecimal(a: Long) = BigDecimal.fromLong(a, uBchMathMode)


fun BigDecimal.Companion.fromString(s: String, dm: DecimalMode):BigDecimal
{
    return parseStringWithMode(s, dm)
}

fun BigDecimal.Companion.fromNullString(s:String?, dm: DecimalMode=currencyMath):BigDecimal?
{
    if (s == null) return null
    return BigDecimal.fromString(s, dm)
}

/** Create a BigDecimal that is appropriate for currency mathematics (with lots of decimal places) */
fun CurrencyDecimal(a: String) = BigDecimal.fromString(a, currencyMath)

fun BigDecimal.toLong(): Long
{
    return this.longValue(false)
}

fun BigDecimal.toInt(): Int
{
    return this.longValue(false).toInt()
}

fun BigDecimal.toDouble(): Double
{
    return this.doubleValue(false)
}

/** Get this string as a BigDecimal currency value (using your default locale's number representation) */
expect fun String.toCurrency(chainSelector: ChainSelector? = null): BigDecimal
