package org.nexa.libnexakotlin

import java.io.File
import kotlin.jvm.Synchronized
import java.security.SecureRandom
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import kotlin.io.path.Path
import kotlin.io.path.exists

private class Native  // All the native functions that do not use an object (which is all of them right now)
{
    companion object
    {
        @JvmStatic fun SecureRandomBytes(data: ByteArray)
        {
            val random = SecureRandom()
            random.nextBytes(data)
        }
        @JvmStatic external fun initializeLibNexa(): Boolean

        // Provide secure random numbers to the C++ layer
        // Android native does not have access to a good secure random number generator so it will ask
        // the java layer for data
        @JvmStatic external fun encode64(data: ByteArray): String
        @JvmStatic external fun decode64(encoded: String): ByteArray

        @JvmStatic external fun hash256(data: ByteArray): ByteArray
        @JvmStatic external fun sha256(data: ByteArray): ByteArray
        @JvmStatic external fun hash160(data: ByteArray): ByteArray
        @JvmStatic external fun blockHash(serializedBlockHeader: ByteArray): ByteArray
        @JvmStatic external fun txidem(serializedTx: ByteArray): ByteArray
        @JvmStatic external fun txid(serializedTx: ByteArray): ByteArray

        /** checks that POW matches target and a few other context-free things */
        @JvmStatic external fun verifyBlockHeader(chainSelector: Byte, serializedBlockHeader: ByteArray): Boolean

        /** Given an array of items, creates a bloom filter and returns it serialized.
         * Typical items are addresses (just the raw 20 bytes), transaction hashes, and outpoints
         * @param items Array<ByteArray> of bitstrings to place into the bloom filter
         * @param falsePosRate Desired Bloom false positive rate
         * @param capacity Number of elements that can be placed intgetPo this bloom while maintaining the falsePosRate.  If this is < items.count(), items.count() is used.  The reason to provide a larger capacity is to allow items to be added into the bloom filter later
         * @param maxSize Maximum size of the bloom filter -- if the capacity and falsePosRate result in a bloom that's larger than this, this size is used instead
         * @param flags  See @BloomFlags for possible fields
         * @param tweak Change tweak to create a different bloom filter.  Used to ensure that collision attacks only work against one filter or node
         * @return Bloom filter serialized as in the P2P network format
         */
        @JvmStatic
        external fun createBloomFilter(items: Array<Any>, falsePosRate: Double, capacity: Int, maxSize: Int, flags: Int = 0, tweak: Int = 1): ByteArray

        /** Create an ECDSA signature for the passed transaction
         * @param txData transaction serialized for signing
         * @param sigHashType signature hash algorithm selection
         * @param inputIdx what input to sign
         * @param inputAmount how many satoshis this input contains
         * @param prevoutScript the input's constraint script
         * @secret 32 byte private key
         * @return signature in binary format
         */
        @JvmStatic
        external fun signOneInputUsingECDSA(txData: ByteArray, sigHashType: Int, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray

        /** Create a Schnorr signature for the passed transaction
         * @param txData transaction serialized for signing
         * @param sigHashType signature hash algorithm selection
         * @param inputIdx what input to sign
         * @param inputAmount how many satoshis this input contains
         * @param prevoutScript the input's constraint script
         * @secret 32 byte private key
         * @return signature in binary format
         */
        @JvmStatic
        external fun signOneInputUsingSchnorr(txData: ByteArray, sigHashType: ByteArray, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray

        /** Create a Schnorr signature for the passed transaction
         * @param txData transaction serialized for signing
         * @param sigHashType signature hash algorithm selection
         * @param inputIdx what input to sign
         * @param inputAmount how many satoshis this input contains
         * @param prevoutScript the input's constraint script
         * @secret 32 byte private key
         * @return signature in binary format
         */
        @JvmStatic external fun signOneBchInputUsingSchnorr(txData: ByteArray, sigHashType: Int, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray

        /** Sign a message using the same algorithm as the original bitcoin wallet's signmessage functionality
         * @message The raw bytes to be signed
         * @secret 32 byte private key
         * @return signature in binary format.  Call Codec.encode64 (BCHserialize.kt) to convert to the exact signature format used by the bitcoin wallet's signmessage */
        @JvmStatic external fun signMessage(message: ByteArray, secret: ByteArray): ByteArray

        /** Verify a message using the same algorithm as the original bitcoin wallet's signmessage functionality
         * @message The raw bytes of the message to be verified
         * @address The address raw bytes (without the type prefix)
         * @return The pubkey that was used to sign, if the signature is valid, otherwise a zero size array. */
        @JvmStatic external fun verifyMessage(message: ByteArray, address: ByteArray, signature: ByteArray): ByteArray?

        @JvmStatic external fun getWorkFromDifficultyBits(nBits: Long): ByteArray

        @JvmStatic
        /** Data must be 32 bytes and MUST be a cryptographic hash of the data you really want to sign EVEN IF THAT DATA IS 32 BYTES for security reasons
         * With a compatible cryptographic hash, this function produces a signature compatible with OP_CHECKDATASIGVERIFY */
        external fun signHashSchnorr(data: ByteArray, secret: ByteArray): ByteArray

        @JvmStatic external fun getPubKey(privateKey: ByteArray): ByteArray

        @JvmStatic external fun groupIdToAddr(chainSelector: Byte, data: ByteArray): String
        @JvmStatic external fun groupIdFromAddr(chainSelector: Byte, addr: String): ByteArray

        // data is the flattened "raw" script bytes.  "raw" means not wrapped into a BCHserialized
        @JvmStatic external fun encodeCashAddr(chainSelector: Byte, type: Byte, data: ByteArray): String

        // data is the flattened "raw" script bytes.  "raw" means not wrapped into a BCHserialized
        @JvmStatic external fun decodeCashAddr(chainSelector: Byte, addr: String): ByteArray

        /** Decodes a private key provided in Satoshi's original format */
        @JvmStatic
        external fun decodeWifPrivateKey(chainSelector: Byte, secretWIF: String): ByteArray

        /** This function calculates the BIP44 key from the specified path.  BIP44 requires that purpose, coinType and account are "hardened".
         * This function will automatically harden those parameters if you pass unhardened values.
         * Therefore this function cannot be used for generalized (non-BIP44) child key derivation.
         */
        @JvmStatic external fun deriveHd44ChildKey(masterSecret: ByteArray, purpose: Long, coinType: Long, account: Long, change: Int, index: Int): ByteArray

        @JvmStatic external fun extractFromMerkleBlock(numTxes: Int, merkleProofPath: ByteArray, hashes: Array<ByteArray>): Array<ByteArray>?
    }
}

private class LibNexaJvm : LibNexa
{
    override fun minDust(chain: ChainSelector): Long = 546L  // All the supported chains currently have the same dust limit

    override fun encode64(data: ByteArray): String = Native.encode64(data)
    override fun decode64(encoded: String): ByteArray = Native.decode64(encoded)
    override fun hash256(data: ByteArray): ByteArray
    {
        return Native.hash256(data)
    }
    /** Returns the sha256 of data. Result is 32 bytes */
    override fun sha256(data: ByteArray): ByteArray = Native.sha256(data)
    /** Calculates the RIPEMD160 of the SHA256 of data. Result is 20 bytes */
    override fun hash160(data: ByteArray): ByteArray = Native.hash160(data)

    @kotlin.ExperimentalUnsignedTypes
    /** Returns the double sha256 of data. Result is 32 bytes */
    override fun hash256(data: UByteArray): UByteArray = Native.hash256(data.toByteArray()).toUByteArray()

    @kotlin.ExperimentalUnsignedTypes
    /** Returns the sha256 of data. Result is 32 bytes */
    override fun sha256(data: UByteArray): UByteArray = Native.sha256(data.toByteArray()).toUByteArray()

    @kotlin.ExperimentalUnsignedTypes
    /** Calculates the RIPEMD160 of the SHA256 of data. Result is 20 bytes */
    override fun hash160(data: UByteArray): UByteArray = Native.hash160(data.toByteArray()).toUByteArray()

    override fun txidem(serializedTx: ByteArray): ByteArray = Native.txidem(serializedTx)

    @kotlin.ExperimentalUnsignedTypes
    override fun txidem(serializedTx: UByteArray): UByteArray = Native.txidem(serializedTx.toByteArray()).toUByteArray()

    override fun txid(serializedTx: ByteArray): ByteArray  = Native.txid(serializedTx)

    @kotlin.ExperimentalUnsignedTypes
    override fun txid(serializedTx: UByteArray): UByteArray  = Native.txid(serializedTx.toByteArray()).toUByteArray()

    override fun blockHash(serializedBlockHeader: ByteArray): ByteArray = Native.blockHash(serializedBlockHeader)

    @kotlin.ExperimentalUnsignedTypes
    override fun blockHash(serializedBlockHeader: UByteArray): UByteArray = Native.blockHash(serializedBlockHeader.toByteArray()).toUByteArray()

    override fun verifyBlockHeader(chainSelector: ChainSelector, serializedBlockHeader: ByteArray): Boolean = Native.verifyBlockHeader(chainSelector.v, serializedBlockHeader)

    @kotlin.ExperimentalUnsignedTypes
    override fun verifyBlockHeader(chainSelector: ChainSelector, serializedBlockHeader: UByteArray): Boolean  = Native.verifyBlockHeader(chainSelector.v, serializedBlockHeader.toByteArray())

    @kotlin.ExperimentalUnsignedTypes
    override fun signHashSchnorr(data: UByteArray, secret: UByteArray): UByteArray
    {
         return Native.signHashSchnorr(data.toByteArray(), secret.toByteArray()).toUByteArray()
    }
    override fun signHashSchnorr(data: ByteArray, secret: ByteArray): ByteArray
    {
         return Native.signHashSchnorr(data, secret)
    }

    @kotlin.ExperimentalUnsignedTypes
    /** Given a private key, return the corresponding (secp256k1) public key -- that is
     * the privKey*G where G is the group generator */
    override fun getPubKey(privateKey: UByteArray): UByteArray
    {
        return Native.getPubKey(privateKey.toByteArray()).toUByteArray()
    }
    override fun getPubKey(privateKey: ByteArray): ByteArray
    {
        return Native.getPubKey(privateKey)
    }

    /** This function calculates the BIP44 key from the specified path.  BIP44 requires that purpose, coinType and account are "hardened".
     * This function will automatically harden those parameters if you pass unhardened values.
     * Therefore this function cannot be used for generalized (non-BIP44) child key derivation.
     */

    override fun deriveHd44ChildKey(secretSeed: ByteArray, purpose: Long, coinType: Long, account: Long, change: Boolean, index: Int): Pair<ByteArray,String>
    {
        val derivedSecret = Native.deriveHd44ChildKey(secretSeed, purpose, coinType, account, if (change) 1 else 0, index)
        return Pair(derivedSecret, "")
    }

    /** Decodes a private key provided in Satoshi's original format */
    override fun decodeWifPrivateKey(chainSelector: ChainSelector, secretWIF: String): ByteArray = Native.decodeWifPrivateKey(chainSelector.v, secretWIF)
    override fun getWorkFromDifficultyBits(nBits: UInt): ByteArray
    {
        return Native.getWorkFromDifficultyBits(nBits.toLong())
    }
    override fun getWorkFromDifficultyBits(nBits: ULong): ByteArray
    {
        return getWorkFromDifficultyBits(nBits.toLong())
    }
    override fun getWorkFromDifficultyBits(nBits: Long): ByteArray
    {
        return Native.getWorkFromDifficultyBits(nBits)
    }

    /** Given an array of items, creates a bloom filter and returns it serialized.
     * Typical items are addresses (just the raw 20 bytes), transaction hashes, and outpoints
     * @param items Array<ByteArray> of bitstrings to place into the bloom filter
     * @param falsePosRate Desired Bloom false positive rate
     * @param capacity Number of elements that can be placed into this bloom while maintaining the falsePosRate.  If this is < items.count(), items.count() is used.  The reason to provide a larger capacity is to allow items to be added into the bloom filter later
     * @param maxSize Maximum size of the bloom filter -- if the capacity and falsePosRate result in a bloom that's larger than this, this size is used instead
     * @param flags  See @BloomFlags for possible fields
     * @param tweak Change tweak to create a different bloom filter.  Used to ensure that collision attacks only work against one filter or node
     * @return Bloom filter serialized as in the P2P network format
     */
    @kotlin.ExperimentalUnsignedTypes
    override fun createBloomFilter(items: Array<Any>, falsePosRate: Double, capacity: Int, maxSize: Int, flags: Int, tweak: Int): ByteArray
    {
        var needCopy = false
        for (i in items)
        {
            if (i !is ByteArray)
            {
                needCopy=true
                break
            }
        }
        if (needCopy)
        {
            val data = Array<Any>(items.size, {
                if (items[it] is ByteArray) items[it] as ByteArray
                else if (items[it] is UByteArray) (items[it] as UByteArray).toByteArray()
                else throw LibNexaException("every item must be a ByteArray or UByteArray")
            })
            return Native.createBloomFilter(data, falsePosRate, capacity, maxSize, flags, tweak)
        }
        else return Native.createBloomFilter(items, falsePosRate, capacity, maxSize, flags, tweak)
    }

    /** Create a Schnorr signature for the passed transaction
     * @param txData transaction serialized for signing
     * @param sigHashType signature hash algorithm selection
     * @param inputIdx what input to sign
     * @param inputAmount how many satoshis this input contains
     * @param prevoutScript the input's constraint script
     * @secret 32 byte private key
     * @return signature in binary format
     */
    override fun signTxOneInputUsingSchnorr(txData: ByteArray, sigHashType: ByteArray, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray
    {
         return Native.signOneInputUsingSchnorr(txData, sigHashType, inputIdx, inputAmount, prevoutScript, secret)
    }

    /** Create a Schnorr signature for the passed transaction
     * @param txData transaction serialized for signing
     * @param sigHashType signature hash algorithm selection
     * @param inputIdx what input to sign
     * @param inputAmount how many satoshis this input contains
     * @param prevoutScript the input's constraint script
     * @secret 32 byte private key
     * @return signature in binary format
     */
    override fun signBchTxOneInputUsingSchnorr(txData: ByteArray, sigHashType: Int, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray
    {
        return Native.signOneBchInputUsingSchnorr(txData, sigHashType, inputIdx, inputAmount, prevoutScript, secret)
    }

    /** Create an ECDSA signature for the passed transaction
     * @param txData transaction serialized for signing
     * @param sigHashType signature hash algorithm selection
     * @param inputIdx what input to sign
     * @param inputAmount how many satoshis this input contains
     * @param prevoutScript the input's constraint script
     * @secret 32 byte private key
     * @return signature in binary format
     */
    override fun signBchTxOneInputUsingECDSA(txData: ByteArray, sigHashType: Int, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray
    {
        return Native.signOneInputUsingECDSA(txData, sigHashType, inputIdx, inputAmount, prevoutScript, secret)
    }

    /** Sign a message using the same algorithm as the original bitcoin wallet's signmessage functionality
     * @message The raw bytes to be signed
     * @secret 32 byte private key
     * @return signature in binary format.  Call Codec.encode64 (BCHserialize.kt) to convert to the exact signature format used by the bitcoin wallet's signmessage */
    override fun signMessage(message: ByteArray, secret: ByteArray): ByteArray
    {
         return Native.signMessage(message, secret)
    }

    /** Verify a message using the same algorithm as the original bitcoin wallet's signmessage functionality
     * @message The raw bytes of the message to be verified
     * @address The hash160 of the pubkey raw bytes (binary address without the type prefix)
     * @return The pubkey that was used to sign, if the signature is valid, otherwise a zero size array. */
    override fun verifyMessage(message: ByteArray, address: ByteArray, signature: ByteArray): ByteArray?
    {
         return Native.verifyMessage(message, address, signature)
    }

    override fun encodeCashAddr(chainSelector: ChainSelector, type: PayAddressType, data: ByteArray): String = Native.encodeCashAddr(chainSelector.v, type.v, data)

    // data is the flattened "raw" script bytes.  "raw" means not wrapped into a BCHserialized
    override fun decodeCashAddr(chainSelector: ChainSelector, addr: String): ByteArray = Native.decodeCashAddr(chainSelector.v, addr)

    override fun groupIdToAddr(chainSelector: ChainSelector, data: ByteArray): String = Native.groupIdToAddr(chainSelector.v, data)

    override fun groupIdFromAddr(chainSelector: ChainSelector, addr: String): ByteArray
    {
        try
        {
            return Native.groupIdFromAddr(chainSelector.v, addr)
        }
        catch (e: java.lang.IllegalStateException)
        {
            throw LibNexaException("not a group identifier")
        }
    }

    override public fun extractFromMerkleBlock(numTxes: Int, merkleProofPath: ByteArray, hashes: Array<ByteArray>): Array<ByteArray>? =
        Native.extractFromMerkleBlock(numTxes, merkleProofPath, hashes)

    override public fun secureRandomBytes(amt: Int): ByteArray
    {
        val rng = SecureRandom()
        var bytes = ByteArray(amt)
        rng.nextBytes(bytes)
        kotlin.assert(bytes.find { it > 0 } != null)
        return bytes
    }
}

private var isInitialized = false

@Synchronized
actual fun initializeLibNexa(variant: String?): LibNexa
{
    if (!isInitialized)
    {
        if (variant != null)  // otherwise assume its already been loaded
        {
            val opSys = System.getProperty("os.name").lowercase()
            val arch = System.getProperty("os.arch").lowercase()
            val ext = if ("windows" in opSys) "dll" else if ("mac" in opSys) "dylib" else "so"
            val flavor = if (ext == "dylib")
            {
                if (arch == "x86_64") "_x86"
                else ""
            } else ""
            var libname = if (variant == "") "nexa" else variant
            val filename = "lib$libname$flavor.$ext"
            println("Looking for: $filename")
            val env = System.getenv()
            println("Environment:\n" + env + "\n")
            val javaLibPath = System.getProperty("java.library.path")
            println("java.library.path:\n" + javaLibPath + "\n")
            val userDir = System.getProperty("user.dir")
            println("User directory: " + userDir)
            val above = Path(userDir + "/../$filename").toAbsolutePath()
            val here = File(userDir + "/$filename")
            if (here.exists())
            {
                println("Loading from: " + here)
                System.load(here.absolutePath)
            }
            else if (above.exists())
            {
                println("Loading from: " + above)
                System.load(above.toString())
            }
            else
            {
                val p = System.getProperties()
                println("properties:")
                println(p)
                println("Trying to load from the system path")
                System.loadLibrary(libname)
            }
        }
        Native.initializeLibNexa()  // Initialize the C library first
        libnexa = LibNexaJvm()
        isInitialized = true
    }
    return libnexa
}
