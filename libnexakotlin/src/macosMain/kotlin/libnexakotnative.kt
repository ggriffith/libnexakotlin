// NOTE THIS IS A SHARED FILE
// The Nexa Kotlin Native API is the same across all native targets.  So this code can be the same.
// But the native compilation must produce different binary .a files, which means that the "commonMain" target
// cannot produce them all.  So the native production and this code must be in the ARCH specific targets, e.g.
// linuxMain, iosMain, macosMain
package org.nexa.libnexakotlin
import kotlinx.cinterop.*

@OptIn(ExperimentalForeignApi::class)
private class LibNexaLinux : LibNexa
{
    override fun minDust(chain: ChainSelector): Long = 546L  // All the supported chains currently have the same dust limit

    override fun encode64(data: ByteArray): String
    {
        val ub = data.toUByteArray()
        val cv = ub.toCValues()
        memScoped {
            val resultSz = data.size*2
            val result = this.allocArray<ByteVar>(resultSz)
            val ret = libnexalight.encode64(cv, cv.size, result, resultSz)
            if (ret > 0)
            {
                result[ret] = 0
                return result.toKString()
            }
            // Should never happen because we gave it plenty of data
            throw LibNexaException("unknown error")
        }
    }
    override fun decode64(encoded: String): ByteArray
    {
        memScoped {
            val resultSz = encoded.length // the bytes used will be less then the string length
            val result = this.allocArray<UByteVar>(resultSz)
            val ret = libnexalight.decode64(encoded, result, resultSz)
            if (ret > 0)
            {
                return result.readBytes(ret)
            }
            // Should never happen because we gave it plenty of data
            throw LibNexaException("unknown error")
        }
    }
    override fun hash256(data: ByteArray): ByteArray
    {
        val ub = data.toUByteArray()
        val cv = ub.toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(32)
            libnexalight.hash256(cv, cv.size.toUInt(), result)
            return result.readBytes(32)
        }
    }

    /** Returns the sha256 of data. Result is 32 bytes */
    override fun sha256(data: ByteArray): ByteArray
    {
        val ub = data.toUByteArray()
        val cv = ub.toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(32)
            libnexalight.sha256(cv, cv.size.toUInt(), result)
            return result.readBytes(32)
        }
    }
    /** Calculates the RIPEMD160 of the SHA256 of data. Result is 20 bytes */
    override fun hash160(data: ByteArray): ByteArray
    {
        val ub = data.toUByteArray()
        val cv = ub.toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(20)
            libnexalight.hash160(cv, cv.size.toUInt(), result)
            return result.readBytes(20)
        }
    }

    /** Returns the double sha256 of data. Result is 32 bytes */
    override fun hash256(data: UByteArray): UByteArray
    {
        val cv = data.toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(32)
            libnexalight.hash256(cv, cv.size.toUInt(), result)
            return result.readBytes(32).toUByteArray()
        }
    }
    /** Returns the sha256 of data. Result is 32 bytes */
    override fun sha256(data: UByteArray): UByteArray
    {
        val cv = data.toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(32)
            libnexalight.sha256(cv, cv.size.toUInt(), result)
            return result.readBytes(32).toUByteArray()
        }
    }
    /** Calculates the RIPEMD160 of the SHA256 of data. Result is 20 bytes */
    override fun hash160(data: UByteArray): UByteArray
    {
        val cv = data.toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(20)
            libnexalight.hash160(cv, cv.size.toUInt(), result)
            return result.readBytes(20).toUByteArray()
        }
    }

    /** Calculates the block hash given a serialized header.  Note that by convention hashes are displayed in byte-reverse order (as if a little-endian number was being displayed).
     * This return value is not reversed.  (hash.reversed().toHex() == "hex header from explorer") -> true
     */
    override fun blockHash(serializedBlockHeader: ByteArray): ByteArray =
        blockHash(serializedBlockHeader.toUByteArray()).toByteArray()
    override fun blockHash(serializedBlockHeader: UByteArray): UByteArray
    {
        val cv = serializedBlockHeader.toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(32)
            val sz:Int = cv.size
            libnexalight.blockHash(cv, sz, result)
            return result.readBytes(32).toUByteArray()
        }
    }
    override fun txidem(serializedTx: ByteArray): ByteArray = txidem(serializedTx.toUByteArray()).toByteArray()

    override fun txidem(serializedTx: UByteArray): UByteArray
    {
        val cv = serializedTx.toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(32)
            val sz:Int = cv.size.toInt()
            libnexalight.txidem(cv, sz, result)
            return result.readBytes(32).toUByteArray()
        }
    }
    override fun txid(serializedTx: ByteArray): ByteArray = txid(serializedTx.toUByteArray()).toByteArray()

    override fun txid(serializedTx: UByteArray): UByteArray
    {
        val cv = serializedTx.toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(32)
            libnexalight.txid(cv, cv.size, result)
            return result.readBytes(32).toUByteArray()
        }
    }
    override fun verifyBlockHeader(chainSelector: ChainSelector, serializedBlockHeader: ByteArray): Boolean
    {
        val cv = serializedBlockHeader.toUByteArray().toCValues()
        val sz = cv.size
        val result = libnexalight.verifyBlockHeader(chainSelector.v.toInt(),cv, sz)
        return result
    }
    override fun verifyBlockHeader(chainSelector: ChainSelector, serializedBlockHeader: UByteArray): Boolean
    {
        return verifyBlockHeader(chainSelector, serializedBlockHeader.toByteArray())
    }
    override fun signHashSchnorr(data: UByteArray, secret: UByteArray): UByteArray
    {
        if (data.size != 32) throw IllegalStateException("invalid hash size")
        if (secret.size != 32) throw IllegalStateException("invalid secret size")
        val mcv = data.toCValues()
        val scv = secret.toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(100)
            val retsize = libnexalight.SignHashSchnorr(mcv, scv, result)
            if (retsize <= 0)  throw IllegalStateException("sign one tx input error")
            return result.readBytes(retsize).toUByteArray()
        }
    }
    override fun signHashSchnorr(data: ByteArray, secret: ByteArray): ByteArray
    {
        return signHashSchnorr(data.toUByteArray(), secret.toUByteArray()).toByteArray()
    }
    /** Given a private key, return the corresponding (secp256k1) public key -- that is
     * the privKey*G where G is the group generator */
    override fun getPubKey(privateKey: UByteArray): UByteArray
    {
        if (privateKey.size != 32) throw IllegalStateException("incorrect private key size")
        val cv = privateKey.toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(128)
            val retsize = libnexalight.GetPubKey(cv, result, 128U)
            if (retsize <= 0)  throw IllegalStateException("public key derivation error")
            return result.readBytes(retsize).toUByteArray()
        }

    }
    override fun getPubKey(privateKey: ByteArray): ByteArray = getPubKey(privateKey.toUByteArray()).toByteArray()

    /** This function calculates the BIP44 key from the specified path.  BIP44 requires that purpose, coinType and account are "hardened".
     * This function will automatically harden those parameters if you pass unhardened values.
     * Therefore this function cannot be used for generalized (non-BIP44) child key derivation.
     */
    override fun deriveHd44ChildKey(secretSeed: ByteArray, purpose: Long, coinType: Long, account: Long, change: Boolean, index: Int): Pair<ByteArray, String>
    {
        if ((secretSeed.size < 16) || (secretSeed.size > 64)) throw LibNexaException("incorrect secret seed size")
        val ss = secretSeed.toUByteArray().toCValues()
        memScoped {
            val resultSecret = this.allocArray<UByteVar>(32)
            val resultDerivationPath = this.allocArray<ByteVar>(1024)
            val nextindex = libnexalight.hd44DeriveChildKey(ss, secretSeed.size.toUInt(), purpose.toUInt(), coinType.toUInt(), account.toUInt(), change, index.toUInt(), resultSecret, resultDerivationPath)
            if (nextindex <= 0)  throw LibNexaException("public key derivation error")
            return Pair(resultSecret.readBytes(32), resultDerivationPath.toKString())
        }

    }

    /** Decodes a private key provided in Satoshi's original fnormat */
    override fun decodeWifPrivateKey(chainSelector: ChainSelector, secretWIF: String): ByteArray
    {
        memScoped {
            val result = this.allocArray<UByteVar>(128)
            val retsize = libnexalight.decodeWifPrivateKey(chainSelector.v.toInt(), secretWIF, result, 128)
            if (retsize <= 0)  throw IllegalStateException("private key decode error")
            return result.readBytes(retsize)
        }
    }

    /** Returns the work -- that is the expected number of hashes to find a solution -- given the difficulty expressed in Bitcoin's "bits" notation */
    override fun getWorkFromDifficultyBits(nBits: Long): ByteArray  = getWorkFromDifficultyBits(nBits.toULong())

    override fun getWorkFromDifficultyBits(nBits: UInt): ByteArray = getWorkFromDifficultyBits(nBits.toULong())

    override fun getWorkFromDifficultyBits(nBits: ULong): ByteArray
    {
        memScoped {
            val result = this.allocArray<UByteVar>(32)
            libnexalight.getWorkFromDifficultyBits(nBits, result)
            return result.readBytes(32)
        }
    }

    /** Given an array of items, creates a bloom filter and returns it serialized.
     * Typical items are addresses (just the raw 20 bytes), transaction hashes, and outpoints
     * @param items Array<ByteArray> of bitstrings to place into the bloom filter
     * @param falsePosRate Desired Bloom false positive rate
     * @param capacity Number of elements that can be placed into this bloom while maintaining the falsePosRate.  If this is < items.count(), items.count() is used.  The reason to provide a larger capacity is to allow items to be added into the bloom filter later
     * @param maxSize Maximum size of the bloom filter -- if the capacity and falsePosRate result in a bloom that's larger than this, this size is used instead
     * @param flags  See @BloomFlags for possible fields
     * @param tweak Change tweak to create a different bloom filter.  Used to ensure that collision attacks only work against one filter or node
     * @return Bloom filter serialized as in the P2P network format
     */

    override fun createBloomFilter(items: Array<Any>, falsePosRate: Double, capacity: Int, maxSize: Int, flags: Int, tweak: Int): ByteArray
    {
        val itemLen = if (items[0] is ByteArray) (items[0] as ByteArray).size
        else if (items[0] is UByteArray) (items[0] as UByteArray).size
        else throw LibNexaException("createBloomFilter: invalid object type")

        val data = UByteArray(items.size * itemLen)
        var count = 0
        for (i in items)
        {
            if (i is ByteArray)
            {
                if (itemLen != i.size) throw LibNexaException("every bloom filter element must be the same size")
                i.toUByteArray().copyInto(data, count*itemLen)
                count++
            }
            else if (i is UByteArray)
            {
                if (itemLen != i.size) throw LibNexaException("every bloom filter element must be the same size")
                i.copyInto(data, count*itemLen)
                count++
            }
        }
        val cv = data.toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(maxSize+32)
            val resultSize = libnexalight.createBloomFilter(cv, cv.size.toUInt(), itemLen.toUInt(), falsePosRate, capacity, maxSize, flags, tweak, result)
            if (resultSize > 0)
            {
                return result.readBytes(resultSize)
            }
            throw LibNexaException("cannot create bloom filter")
        }
    }
    /** Create an ECDSA signature for the passed transaction
     * @param txData transaction serialized for signing
     * @param sigHashType signature hash algorithm selection
     * @param inputIdx what input to sign
     * @param inputAmount how many satoshis this input contains
     * @param prevoutScript the input's constraint script
     * @secret 32 byte private key
     * @return signature in binary format
     */

    override fun signBchTxOneInputUsingECDSA(txData: ByteArray, sigHashType: Int, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray
    {
        throw NotImplementedError()
    }
    /** Create a Schnorr signature for the passed transaction
     * @param txData transaction serialized for signing
     * @param sigHashType signature hash algorithm selection
     * @param inputIdx what input to sign
     * @param inputAmount how many satoshis this input contains
     * @param prevoutScript the input's constraint script
     * @secret 32 byte private key
     * @return signature in binary format
     */
    override fun signTxOneInputUsingSchnorr(txData: ByteArray, sigHashType: ByteArray, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray
    {
        val mcv = txData.toUByteArray().toCValues()
        val sigHashcv = sigHashType.toUByteArray().toCValues()
        val scv = secret.toUByteArray().toCValues()
        val prevoutcv = prevoutScript.toUByteArray().toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(100)
            val retsize = libnexalight.signTxOneInputUsingSchnorr(mcv, mcv.size.toInt(), inputIdx.toUInt(), inputAmount,
                prevoutcv, prevoutcv.size.toUInt(),
                sigHashcv, sigHashcv.size.toUInt(),
                scv,
                result, 100U)
            if (retsize <= 0)  throw IllegalStateException("sign one tx input error")
            return result.readBytes(retsize)
        }
    }

    /** Create a Schnorr signature for the passed transaction
     * @param txData transaction serialized for signing
     * @param sigHashType signature hash algorithm selection
     * @param inputIdx what input to sign
     * @param inputAmount how many satoshis this input contains
     * @param prevoutScript the input's constraint script
     * @secret 32 byte private key
     * @return signature in binary format
     */

    override fun signBchTxOneInputUsingSchnorr(txData: ByteArray, sigHashType: Int, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray
    {
        val mcv = txData.toUByteArray().toCValues()
        val scv = secret.toUByteArray().toCValues()
        val prevoutcv = prevoutScript.toUByteArray().toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(100)
            val retsize = libnexalight.signBchTxOneInputUsingSchnorr(mcv, mcv.size.toInt(), inputIdx.toUInt(), inputAmount,
                prevoutcv, prevoutcv.size.toUInt(),
                sigHashType.toUInt(),
                scv,
                result, 100U)
            if (retsize <= 0)  throw IllegalStateException("sign one tx input error")
            return result.readBytes(retsize)
        }
    }

    /** Sign a message using the same algorithm as the original bitcoin wallet's signmessage functionality
     * @message The raw bytes to be signed
     * @secret 32 byte private key
     * @return signature in binary format.  Call Codec.encode64 (BCHserialize.kt) to convert to the exact signature format used by the bitcoin wallet's signmessage */

    override fun signMessage(message: ByteArray, secret: ByteArray): ByteArray?
    {
        // SLAPI int signMessage(const unsigned char* message, unsigned int msgLen, const unsigned char* secret, unsigned int secretLen, unsigned char *result, unsigned int resultLen);

        val mcv = message.toUByteArray().toCValues()
        val scv = secret.toUByteArray().toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(128)
            val retsize = libnexalight.signMessage(mcv, message.size.toUInt(),  scv, scv.size.toUInt(), result, 128U)
            if (retsize <= 0)  throw IllegalStateException("sign message error")
            return result.readBytes(retsize)
        }
    }

    /** Verify a message using the same algorithm as the original bitcoin wallet's signmessage functionality
     * @message The raw bytes of the message to be verified
     * @address The address raw bytes (without the type prefix)
     * @return The pubkey that was used to sign, if the signature is valid, otherwise a zero size array. */
    override fun verifyMessage(message: ByteArray, address: ByteArray, signature: ByteArray): ByteArray?
    {
        //SLAPI bool verifyMessage(const unsigned char* message, unsigned int msgLen,
        //    const unsigned char* addr, unsigned int addrLen,
        //    const unsigned char* sig, unsigned int sigLen,
        //    unsigned char *result, unsigned int resultLen);
        val mcv = message.toUByteArray().toCValues()
        val scv = signature.toUByteArray().toCValues()
        val acv = address.toUByteArray().toCValues()
        memScoped {
            val result = this.allocArray<UByteVar>(128)
            val retsize = libnexalight.verifyMessage(mcv, message.size.toUInt(), acv, acv.size.toUInt(), scv, scv.size.toUInt(), result, 128U)
            if (retsize <= 0)  return null
            return result.readBytes(retsize)
        }
    }

    // data is the flattened "raw" script bytes.  "raw" means not wrapped into a BCHserialized
    override fun encodeCashAddr(chainSelector: ChainSelector, type: PayAddressType, data: ByteArray): String
    {
        val dc = data.toUByteArray().toCValues()
        val rlen = data.size*3
        val result = ByteArray(rlen)
        result.usePinned {
            val ret = libnexalight.encodeCashAddr(chainSelector.v.toInt(), type.v.toInt(), dc, dc.size, it.addressOf(0), rlen - 1)
            if (ret <= 0) throw LibNexaException("address encoding failed")
            return result.toKString(0, ret)
        }
    }
    // Returns an array with the first byte being the PayAddressType, and the rest the address bytes
    // This API is not stable.  It should be called only by the PayAddress wrapper class

    override fun decodeCashAddr(chainSelector: ChainSelector, addr: String): ByteArray
    {
        memScoped {
            val result = this.allocArray<UByteVar>(1024)
            val ret = libnexalight.decodeCashAddr(chainSelector.v.toInt(), addr, result, 1024)
            if (ret > 0)
            {
                return result.readBytes(ret)
            }
            throw LibNexaException("cannot decode address")
        }
    }
    override fun groupIdToAddr(chainSelector: ChainSelector, data: ByteArray): String
    {
        val dc = data.toUByteArray().toCValues()
        val rlen = data.size*3
        val result = ByteArray(rlen)
        result.usePinned {
            val ret = libnexalight.groupIdToAddr(chainSelector.v.toInt(), dc, dc.size, it.addressOf(0), rlen - 1)
            if (ret <= 0) throw IllegalStateException("address encoding failed")
            return result.toKString(0, ret)
        }
    }
    override fun groupIdFromAddr(chainSelector: ChainSelector, addr: String): ByteArray
    {
        memScoped {
            val result = this.allocArray<UByteVar>(1024)
            val ret = libnexalight.groupIdFromAddr(chainSelector.v.toInt(), addr, result, 1024)
            if (ret > 0)
            {
                return result.readBytes(ret)
            }
            throw IllegalStateException("cannot decode group")
        }
    }

    override fun extractFromMerkleBlock(numTxes: Int, merkleProofPath: ByteArray, hashes: Array<ByteArray>): Array<ByteArray>?
    {
        val mpp = merkleProofPath.toUByteArray().toCValues()
        //val b = Buffer()
        //println("hashes")
        var b = byteArrayOf()
        for (h in hashes)
        {
            if (h.size != 32) throw IllegalStateException("merkle proof hash is incorrect size ${h.size}")
            //b.write(h,h.size)
            b = b + h  // TODO terribly inefficient
            //println(h.toHex())
        }
        //b.flush()

        val hshba = b  // b.readByteArray()
        //println("hashes as bytearray")
        //println(hshba.toHex())
        val hsh = hshba.toUByteArray().toCValues()
        val ret = mutableListOf<ByteArray>()
        memScoped {
            val MAX_RESULT_SIZE = 256*1024
            val result = this.allocArray<UByteVar>(MAX_RESULT_SIZE)
            val nHashes = libnexalight.extractFromMerkleBlock(numTxes, mpp, mpp.size, hsh, hashes.size, result,  MAX_RESULT_SIZE)
            val joined = result.readBytes(nHashes*32)
            //println("result $nHashes")
            //println(joined.toHex())
            for (i in 0 until nHashes)
            {
                  ret.add(joined.sliceArray(i*32 until (i+1)*32))
            }
        }
        return ret.toTypedArray()
    }

    override fun secureRandomBytes(amt: Int): ByteArray
    {
        memScoped {
            val result = this.allocArray<UByteVar>(amt)
            val ret = libnexalight.RandomBytes(result, amt)
            return result.readBytes(ret)
        }
    }
}



private var libNexaisInitialized = false
actual fun initializeLibNexa(variant: String?): LibNexa
{
    if (!libNexaisInitialized)
    {
        libnexa = LibNexaLinux()
        libNexaisInitialized = true
    }
    return libnexa
}
