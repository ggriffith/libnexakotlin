// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin

import kotlinx.atomicfu.atomic
import kotlinx.coroutines.*
import kotlinx.serialization.json.JsonObject
import kotlin.coroutines.CoroutineContext
import kotlin.text.StringBuilder
import kotlin.time.TimeSource
import kotlin.math.ceil
import org.nexa.threads.*
import kotlin.concurrent.Volatile
import com.ionspin.kotlin.bignum.decimal.BigDecimal
import com.ionspin.kotlin.bignum.decimal.toBigDecimal

private val LogIt = GetLog("BU.wallet")

/** Helper class that glues a wallet to a blockchain
 * @property [chain] a reference to an active blockchain object.  The wallet will access this object whenever it needs blockchain data.
 */
@cli(Display.Simple, "Connection between this wallet and its blockchain.")
class GlueWalletBlockchain(val chain: Blockchain) : BCHserializable
{
    val mutex = Mutex()

    companion object
    {
        const val SERIALIZATION_VERSION = 1.toByte()
    }
    /** This wallet has synchronized its balances up to this block height.  Note, use syncedHash to be certain of a sync because the chain could have forks.
     */
    @Volatile
    @cli(Display.Simple, "This wallet has synchronized its balances up to this block height.")
    var syncedHeight: Long = chain.checkpointHeight

    @Volatile
    @cli(Display.Simple, "This wallet has synchronized its balances up to this date (as read from the block syncedHash/syncedHeight).  This is a convenience function so that the block header does not need to be accessed.  syncedHash is authoritative.")
    var syncedDate: Long = chain.checkpointHeight


    /** This wallet has synchronized its balances up to this block hash
     */
    @Volatile
    @cli(Display.Simple, "This wallet has synchronized its balances up to this block hash.")
    var syncedHash: Hash256 = chain.checkpointId

    /** Information about what happened at a particular block, allowing us to unwind that block in case of a reorg.
     * If there is no entry, nothing happened.  This data also allows the wallet to provide transaction history
     */
    @cli(Display.Dev,
      "Information about what happened at a particular block, allowing us to unwind that block in case of a reorg.")
    var blockRewind: MutableMap<Hash256, RewindData> = mutableMapOf()

    /** Any block before this one cannot have any transactions relevant to this wallet (the wallet was created after this block).
     *  The wallet uses this information to rapidly sync.
     */
    @cli(Display.Simple, "Any block before this date cannot have any transactions relevant to this wallet.")
    var prehistoryDate: Long = 0

    /** Any block before this one cannot have any transactions relevant to this wallet (the wallet was created after this block).
     *  The wallet uses this information to rapidly sync.
     */
    @cli(Display.Simple,
      "Any block before this one cannot have any transactions relevant to this wallet.  If zero we don't know the height.")
    var prehistoryHeight: Long = 0

    /** a handle to this wallet's bloom filter data installed in the Blockchain */
    var filterHandle: Int? = null

    /** Get or rewindData, create empty if nonexistent */
    fun getRewindData(blockHash: Hash256): RewindData
    {
        return mutex.lock {
            blockRewind.getOrPut(blockHash, { RewindData(chain.chainSelector) })
        }
    }

    init
    {
        chain.attachWallet()
    }

    @cli(Display.Simple,
      "Return true if this wallet is synchronized with the blockchain")
    fun isSynchronized(maxBlock: Int=0, maxSec: Int=60*60):Boolean
        {
            if (syncedHeight + maxBlock < chain.curHeight) return false  // We cannot be behind in blocks
            if (syncedDate + maxSec < epochSeconds()) return false  // If our last synced block is too old, we assume unsynced (blockchain may have to catch up as well).
            return true
        }


    fun delete()
    {
        mutex.lock {
            chain.detachWallet(filterHandle)
        }
    }

    /** Reset this wallet's blockchain state to the earliest point available in the blockchain */

    fun resetToCheckpoint()
    {
        mutex.lock {
            blockRewind.clear()
            syncedHeight = chain.checkpointHeight
            syncedHash = chain.checkpointId
            syncedDate = 0
        }
    }

    /** Reset this wallet's blockchain state to the wallet prehistory */

    fun resetToPrehistory()
    {
        mutex.lock {
            // force reset of prehistory
            //if (prehistoryHeight > 0) prehistoryHeight = 1
            //if (prehistoryDate > 0) prehistoryDate = 1
            if (prehistoryHeight != 0L)  // I know the prehistory block
            {
                blockRewind.clear()
                syncedHeight = prehistoryHeight
                syncedHash = chain.getBlockHeader(syncedHeight).hash
                syncedDate = 0
            }
            else resetToCheckpoint()  // I don't know it so reset do checkpoint to discover it by iteration
        }
    }


    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        LogIt.info(sourceLoc() + " " + chain.name + " prehistory written synced is $syncedHeight time is $prehistoryDate height is $prehistoryHeight hash is $syncedHash")
        return mutex.lock {
            BCHserialized(SerializationType.DISK).addUint8(SERIALIZATION_VERSION.toInt()).addUint64(syncedHeight.toULong()).addUint64(prehistoryDate.toULong()).addUint64(prehistoryHeight.toULong()) + syncedHash + syncedDate + BCHserialized.mutableMap(blockRewind, SerializationType.DISK)
        }
    }


    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        return mutex.lock {
            val ver = stream.deuint8()
            if (ver == 1)
            {
                syncedHeight = stream.deuint64()
                prehistoryDate = stream.deuint64()
                prehistoryHeight = stream.deuint64()
                syncedHash = Hash256(stream)
                syncedDate = stream.deuint64()
                blockRewind = stream.demap({ var v = Hash256(); v.BCHdeserialize(it); v },
                    { var v = RewindData(chain.chainSelector); v.BCHdeserialize(it); v })
                LogIt.info(sourceLoc() + " " + chain.name + " Loaded GlueWalletBlockchain.  prehistory read synced is $syncedHeight time is $prehistoryDate height is $prehistoryHeight hash is $syncedHash")
            }
            else throw DeserializationException("Invalid version for stored TransactionHistory")
            stream
        }
    }
}

/** If a block is rewound, this is how to undo it.  Also constitutes all the data needed to show transaction history.
 */
class RewindData(val chainSelector: ChainSelector) : BCHserializable
{
    /** If this block is rewound, these outputs become spendable again */
    var spent: MutableList<Spendable> = mutableListOf()

    /** If this block is rewound, these outputs disappear */
    var added: MutableList<Spendable> = mutableListOf()

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        return BCHserialized.list(spent, SerializationType.DISK) + BCHserialized.list(added, SerializationType.DISK)
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        spent = stream.delist({ Spendable(chainSelector, it) })
        added = stream.delist({ Spendable(chainSelector, it) })
        return stream
    }
}


fun chainStateDbKey(name: String, chainSelector: ChainSelector) = "wallet_" + name + "_chainstate_" + chainSelector
fun txStateDbKey(name: String) = "wallet_" + name + "_txstate"
fun unusedAddressesDbKey(name: String) = "wallet_" + name + "_unusedAddresses"
fun txHistoryDbKey(name: String) = "wallet_" + name + "_txHistory"
fun identityDomainsDbKey(name: String) = "wallet_" + name + "_identitydomains"
fun identityInfoDbKey(name: String) = "wallet_" + name + "_identityInfo"

/** Delete a wallet on disk */
fun deleteWallet(walletName: String, chainSelector: ChainSelector)
{
    val db = openKvpDB("wallet_" + walletName)
    if (db == null) return
    deleteWallet(db, walletName, chainSelector)
}

fun deleteWallet(db: KvpDatabase, walletName: String, chainSelector: ChainSelector)
{
    val dbkey = "wallet_" + walletName + "chainstate_" + chainSelector
    db.delete(dbkey)
    db.delete("bip44wallet_" + walletName)
    db.delete(chainStateDbKey(walletName, chainSelector))
    db.delete(txStateDbKey(walletName))
    db.delete(unusedAddressesDbKey(walletName))
    db.delete(txHistoryDbKey(walletName))
    db.delete(identityDomainsDbKey(walletName))
    db.delete(identityInfoDbKey(walletName))
}

// Returns a predicate function that accepts a BCHspendable.  This function returns true if the BCHspendable is in the passed
// group and has properties based on passed flags, and is not already reserved.  If "normal" is true, any non-authority utxo will return true.
// If "authority" is a bitmap, returns true if these bits are set in the authorityFlags.
// If "maskOff" is zero, maskOff is set to "authority" (and so does nothing).  Otherwise all set bits are part of the filter.  This parameter allows the caller require that a bit is zero, by
// setting it in "maskOff", but leaving it cleared in "authority".

fun groupedFilter(
  groupId: GroupId,
  normal: Boolean = true,
  authority: ULong = GroupAuthorityFlags.AUTHORITY,
  maskOff: ULong = 0.toULong()
): (Spendable) -> Boolean
{
    val mask = if (maskOff == 0.toULong()) authority else maskOff
    // returns a filter function that selected only grouped inputs, either normal or authority (or both) depending on the flags
    return {
        if (it.reserved != 0L) false
        else
        {
            val groupInfo: GroupInfo? = it.groupInfo()

            if (groupInfo == null) false
            else
            {
                if (groupInfo.groupId != groupId)
                {
                    LogIt.info("groupId doesn't match")
                    false
                }
                else
                {
                    if (groupInfo.isAuthority())
                    {
                        LogIt.info("Auth check: " + groupInfo.authorityFlags.toString(16) + " and " + authority.toString(16))
                        if ((groupInfo.authorityFlags and mask) == authority) true
                        else false
                    }
                    else
                    {
                        if (normal) true
                        else false
                    }
                }
            }
        }
    }
}

/** Calculate the Schnorr signature of the passed transaction and input index
 * This function does not update the transaction.
 * @returns The signature as a byte array (including sighashtype) */
fun calcSig(tx: iTransaction, inputIdx: Int, sigHashType: ByteArray, serializedTx: ByteArray? = null): ByteArray
{
    val inp = tx.inputs[inputIdx]
    val spendable = inp.spendable
    val secret = spendable.secret

    val flatTx = serializedTx ?: tx.BCHserialize(SerializationType.NETWORK).flatten()

    if (secret == null) throw IllegalArgumentException("Input spendable must have secret set")
    if (secret.getSecret().size == 0) throw IllegalArgumentException("Input spendable must have a nonempty secret")
    val sigSchnorr = libnexa.signTxOneInputUsingSchnorr(flatTx, sigHashType, inputIdx.toLong(), inp.spendable.amount, inp.spendable.priorOutScript.flatten(), secret.getSecret())
    return sigSchnorr
}

/** Sign (and create the satisfier script for) one input of the passed transaction
 * This function modifies the transaction.
 * @returns True if the input was successfully signed
 * */
fun signInput(tx: iTransaction, idx: Long, sigHashType: ByteArray, serializedTx: ByteArray? = null): Boolean
{
    val inp = tx.inputs[idx.toInt()]
    val spendable = inp.spendable
    val secret = spendable.secret

    val flatTx = serializedTx ?: tx.BCHserialize(SerializationType.NETWORK).flatten()

    // if we don't know how to sign this input, then don't sign it.
    // this is also how the tx creator communicates to the wallet that this input need not be signed
    if ((secret != null && secret.getSecret().size != 0) || (inp.spendable.backingPayDestination != null))
    {
        /*
            //LogIt.info("Signing tx " + flatTx.ToHex() + ", " + sigHashType + ", " + count + ", " + inp.spendable.amount + ", " + inp.spendable.priorOutScript.flatten().ToHex() + ", " + secret.ToHex())
            val sig = signOneInputUsingECDSA(flatTx, sigHashType, idx, inp.spendable.amount, inp.spendable.priorOutScript.flatten(), secret)
            //val sigSchnorr = signOneInputUsingSchnorr(flatTx, sigHashType, count, inp.spendable.amount, inp.spendable.priorOutScript.flatten(), secret)

            // TODO call a function on the spendable that produces the satisfier script.  For now assume P2PKH
            val pubkey = PayDestination.GetPubKey(secret)
            inp.script = BCHscript(chainSelector, OP.push(sig), OP.push(pubkey))
            // DEBUG break the sig:
            //inp.script = BCHscript(chainSelector, OP.NOP, OP.push(pubkey))
         */
        val pd = inp.spendable.payDestination
        if (pd != null)  // This will be null if we can't understand the script, and therefore can't sign this input
        {
            inp.script = pd.spendScript(flatTx, idx, sigHashType, inp.spendable.amount)
            return true
        }
    }
    return false
}

/** signs a transaction in place, with all inputs that have a BCHspendable with secrets (skips those that do not so others can sign)
 by default the sighashtype is ALL/ALL
*/
fun signTransaction(tx: iTransaction, sigHashType: ByteArray = byteArrayOf())
{
    val txSerialized = tx.BCHserialize(SerializationType.NETWORK)
    val flatTx = txSerialized.flatten()
    var changed = false

    for (idx in 0L until tx.inputs.size)
    {
        changed = signInput(tx, idx, sigHashType, flatTx) or changed
    }
    if (changed) tx.changed()  //  Clear out the hash (if it was requested) since it will have changed now that signatures are added.
}

/** This class provides implementations to for functions that are common to many blockchains and wallet types */
@cli(Display.Simple, "Most wallet functionality resides here")
abstract class CommonWallet(override val name: String, chainSelector: ChainSelector) : Wallet(chainSelector)
{
    companion object
    {
        // Constants defining the data version of the wallet
        val CHAIN_STATE_SERIALIZED_VERSION: Byte = 1
        val TX_STATE_SERIALIZED_VERSION: Byte = 1
        val UNUSED_ADDRESSES_SERIALIZED_VERSION: Byte = 1
        val TX_HISTORY_SERIALIZED_VERSION: Byte = 1
        val PAYMENT_HISTORY_SERIALIZED_VERSION: Byte = 1
    }

    /** rebroadcast historical unconfirmed transactions every hour (and when we first start up) */
    val timeToResendHistoricalTx = Periodically(60L * 60000L)

    /** rebroadcast wallet transaction every minute */
    val timeToResendWalletTx = Periodically(60000)

    val WALLET_FLUSH_PERIOD = 10000   //!< Minimum wallet save interval, for recoverable operations

    /** If the wallet's state changes, it will call this function.  Use [setOnWalletChange]. */
    protected var walletChangeCallback: ((Wallet) -> Unit)? = null

    //  These variables wake up a processing thread to handle periodic tasks like chainstate sync and tx submission.
    //  These tasks are not implemented via co-routines triggered by events because we want both event trigger and periodic checking.
    //w val lock = ReentrantLock()
    //protected val cond = lock.newCondition()
    protected val CHECK_PERIOD: Long = 10000  // 10 seconds

    /**  Set to true to quit */
    protected var done = false

    // Set to true to pause, false to resume
    @Volatile
    override var pause = false

    @Volatile
    var paused = false

    protected val oneTxConstructor = Mutex("1TxConstructor")
    /** protects the next 4 objects */
    protected val dataLock = Gate()

    // Addresses that have already been generated and are in the bloom filter, but are not yet used
    @cli(Display.Dev, "available but unused address list.  Remove from index 0 before using, add to the end.  Its important to use in order so that gaps between used addresses remain small.  Otherwise some wallets may not know to look beyond the gap.")
    protected val unusedAddresses: MutableList<PayAddress> = mutableListOf()

    /** Every receive address that has been generated (even if they have not been used or even provided to some external payer) */
    @cli(Display.Dev, "Every receive address that has been generated")
    protected var receiving: MutableMap<PayAddress, PayDestination> = mutableMapOf()

    /** all UTXOs that this wallet is capable of spending (and how to spend them) */
    @cli(Display.Dev, "All UTXOs that this wallet is capable of spending")
    protected var allTxos: MutableMap<iTxOutpoint, Spendable> = mutableMapOf()

    // offer only a read only version in the Wallet API because unspentByAddress, txHistory, paymentHistory, etc need to be
    // updated when a utxo is spent or received
    override val txos: Map<iTxOutpoint, Spendable>
        get() = allTxos.toMap()

    @cli(Display.Simple, "Manually insert spendable coins into this wallet, allowing the wallet to use them for subsequent spends.  This will NOT sweep these coins! This means they will not be restored by recovering the wallet via the recovery key.")
    fun injectUnspent(vararg spend: Spendable)
    {
        synchronized(dataLock)
        {
            for (sp in spend)
            {
                allTxos[sp.outpoint!!] = sp
                sp.addr?.let {
                    insertUnspentByAddress(sp.addr, sp.outpoint!!)
                }
            }
        }
    }

    /** all UTXOs that this wallet is capable of spending (like unspent), indexed by address */
    @cli(Display.Dev, "All UTXOs that this wallet is capable of spending, indexed by address")
    protected val unspentByAddress: MutableMap<PayAddress, MutableList<iTxOutpoint>> = mutableMapOf()

    /** All transactions that interest this wallet -- either sent or received */
    @cli(Display.User, "All transactions that interest this wallet, sent, received, or externally generated, indexed by transaction hash")
    override public var txHistory: MutableMap<Hash256, TransactionHistory> = mutableMapOf()

    data class WalletStatistics(
      val numUnusedAddrs: Int,
      val numUsedAddrs: Int,
      val numUnspentTxos: Int,
      val totalTxos: Int,
      val numTransactions: Int,
      val firstReceiveHeight: Long,
      val lastReceiveHeight: Long,
      val firstSendHeight: Long,
      val lastSendHeight: Long,
    )

    fun statistics(): WalletStatistics
    {
        var unspentTxos = 0
        var firstReceive = Long.MAX_VALUE
        var lastReceive = 0L
        var firstSpend = Long.MAX_VALUE
        var lastSpend = 0L
        for (i in allTxos)
        {
            if (i.value.spentHeight < 0) unspentTxos += 1
            if (i.value.commitHeight > 0 && i.value.commitHeight < firstReceive) firstReceive = i.value.commitHeight
            if (i.value.commitHeight > 0 && i.value.commitHeight > lastReceive) lastReceive = i.value.commitHeight

            if (i.value.spentHeight > 0 && i.value.spentHeight < firstSpend) firstSpend = i.value.spentHeight
            if (i.value.spentHeight > 0 && i.value.spentHeight > lastSpend) lastSpend = i.value.spentHeight
        }
        return WalletStatistics(unusedAddresses.size, receiving.size - unusedAddresses.size, unspentTxos, allTxos.size, txHistory.size, firstReceive, lastReceive, firstSpend, lastSpend)
    }

    /** After startup, chainstate should always be set.  It connects this wallet to a particular location in the blockchain */
    @cli(Display.User, "connection between this wallet and its blockchain")
    @Volatile
    var chainstate: GlueWalletBlockchain? = null

    var walletDb: KvpDatabase? = null

    /** Thread that handles keeping wallet synced
     */
    var processingThread: iThread? = null

    /** Get a set of every address in this wallet */
    // w@cli(Display.User, "every address (generated so far) in this wallet")
    val allAddresses: MutableSet<PayAddress>
        get() = receiving.keys

    /** transactions that this wallet has generated that have not yet been confirmed */
    @cli(Display.User, "New transactions that have not yet been confirmed")
    var pendingTx: MutableMap<Hash256, ByteArray> = mutableMapOf()

    override val blockchain: Blockchain
        get() = chainstate!!.chain

    override val syncedHeight: Long
        get() = chainstate?.syncedHeight ?: -1

    /** wake up the processing loop early */
    protected var wakey = Gate()
    @Volatile
    protected var wakeCount = 0L

    /** for inessential wallet operations (like syncing with the blockchain), we allow periodic flush.  If the program fails between flush periods, a few seconds of work must be redone */

    @Volatile protected var lastSavedSyncedHeight: Long = 0
    protected val flushPeriod = Periodically(WALLET_FLUSH_PERIOD.toLong())
    protected fun flushWalletPeriodically(): Boolean
    {
        if (lastSavedSyncedHeight + 50 < syncedHeight || flushPeriod())
        {
            lastSavedSyncedHeight = syncedHeight
            return true
        }
        else return false
    }

    var identityDomain: MutableMap<String, IdentityDomain> = mutableMapOf()
    @Volatile var identityDomainChanged = false

    var identityInfo: MutableMap<PayAddress, IdentityInfo> = mutableMapOf()
    @Volatile var identityInfoChanged = false

    // enable long delays to be changed, primarily for tests where activities happen rapidly
    var LONG_DELAY_INTERVAL = 5000L

    protected val coCtxt: CoroutineContext = newFixedThreadPoolContext(4, "ConnectionManager")
    protected val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)

    fun thisLaunch(scope: CoroutineScope? = null, fn: (suspend () -> Unit)?)
    {
        val s = if (scope != null) scope else coScope
        (::launch)(s, fn)
    }

    public override fun toString(): String
    {
        val ret = StringBuilder()
        ret.append("""{ "type":"CommonWallet", "name": "$name", "blockchain" : "$chainSelector", "pause" : "$pause", "timeToResendWalletTx" : $timeToResendWalletTx, """)
        ret.append(""" "unusedAddresses":"[...${unusedAddresses.size}...]", "receiving": "[...${receiving.size}...]", "unspent" : "[...${allTxos.size}...]", "txHistory" : "{...${txHistory.size}...}", """)
        ret.append(""" "pendingTx":"{...${pendingTx.size}...}", "identityDomain":"{...${identityDomain.size}...}", "identityInfo":"{...${identityInfo.size}...}"    }""")
        return ret.toString()
    }

    @cli(Display.Dev, "find and return the secret corresponding to the passed pubkey.")
    override fun pubkeyToSecret(pubkey: ByteArray): Secret?
    {
        for ((_, d) in receiving)
        {
            if ((d.pubkey.contentEquals(pubkey)) && (d.secret != null)) return d.secret
        }
        return null
    }

    /** Forget about all unconfirmed transactions in the wallet.  It the transactions are in the network and are confirmed they will be added to the wallet at that point.
     * This API is used to clear out tx that will never confirm for some reason.
     * This API causes the wallet to forget about the inputs that weren't confirmed.  "Rediscover" can get those back
     */
    @cli(Display.Dev, "Forget about unconfirmed transactions in the wallet")
    fun cleanUnconfirmed(before: Long = Long.MAX_VALUE)
    {
        val muit = txHistory.iterator()
        val forget = mutableListOf<TransactionHistory>()
        for ((hash, hist) in muit)
        {
            if (hist.confirmedHeight <= 0) // this tx is not confirmed yet
            {
                LogIt.info("TX ${hash} is unconfirmed since ${hist.date}")
                if (hist.date < before) forget.add(hist)
            }
        }
        cleanUnconfirmed(forget)
    }

    /** Forget about certain transactions in the wallet.  If the transactions are in the network and are later confirmed they will be added to the wallet then.
     * This API is used to clear out tx that will never confirm for some reason.
     * This API causes the wallet to forget about the inputs that weren't confirmed.  "Rediscover" can get those back
     */
    @cli(Display.Dev, "Forget about unconfirmed transactions in the wallet")
    fun cleanUnconfirmed(lst: MutableList<TransactionHistory>)
    {
        for (hist in lst)
        {
            LogIt.info("Cleaning up ${hist.tx.idem}")
            txHistory.remove(hist.tx.idem)
            pendingTx.remove(hist.tx.idem)

            for (out: iTxOutpoint in hist.tx.outpoints)
            {
                allTxos.remove(out)
            }
            for (out: iTxOutput in hist.tx.outputs)
            {
                unspentByAddress.remove(out.script.address)
            }
        }
        save()
    }

    /** If you clear the "receiving" addresses map, you need to fill them back up with derived class injected destinations using this function */
    fun fillReceivingWithRetrieveOnly()
    {
        val rods = getRetrieveOnlyDestinations()

        for (r in rods)
        {
            r.address?.let {
                receiving[it] = r
            }
        }
    }

    /** In case someone sends coins to one of your identities, the wallet should be able to spend them
     * It is not recommended to send coins to identities, as that makes the payments identifiable, and
     * also a subsequent payment may mix other incoming payments with identity payments.  So the identity address
     * is never provided as a payment address.  However, someone may use it as such.  */
    fun fillReceivingWithIdentities()
    {
        // hard code loading the common ID
        if (true)
        {
            val identityDest: PayDestination = destinationFor(Bip44Wallet.COMMON_IDENTITY_SEED)
            identityDest.address?.let {
                LogIt.info(sourceLoc() + name + ": receiving added " + it.toString())
                if (!receiving.contains(it))
                {
                    receiving[it] = identityDest
                }
            }
        }

        // then get the rest
        for (id in identityInfo)
        {
            id.value.identity?.let {
                if (!receiving.contains(it))
                {
                    val identityDest: PayDestination = destinationFor(id.value.identityKey)
                    receiving[it] = identityDest
                }
            }
        }
    }

    /** Forget all transaction and blockchain state, and the redo the search for wallet transactions.
     * This is intended for testing and debug
     */
    @cli(Display.Dev, "Forget all transaction and blockchain state, and the redo the search for wallet transactions.")
    override fun rediscover(forgetAddresses: Boolean, noPrehistory: Boolean): Unit
    {
        thisLaunch {
            // We want to pause all blockchain processing while switching the chain tip.  Otherwise we could be in the middle of processing a bunch of tx when we reset
            val origP = pause
            pause = true
            while (!paused) delay(100)

            val cs = chainstate

            if (cs != null)
            {
                if (noPrehistory)
                {
                    cs.prehistoryDate = 0
                    cs.prehistoryHeight = 0
                }

                synchronized(dataLock) {
                    pendingTx.clear()
                    txHistory.clear()
                    allTxos.clear()
                    unspentByAddress.clear()
                    if (forgetAddresses)
                    {
                        unusedAddresses.clear()
                        receiving.clear()
                        generateDestinationsInto(unusedAddresses)
                        fillReceivingWithRetrieveOnly()
                        fillReceivingWithIdentities()
                    }

                    try
                    {
                        cs.resetToPrehistory()
                    }
                    catch (_: Exception)
                    {
                        cs.resetToCheckpoint()
                    }
                }

                regenerateBloom { }
                save()
                walletChangeCallback?.invoke(this)
                pause = origP
                wakey.wake() { wakeCount++ }  // Wake up wallet processing
            }
        }
    }

    /** Insert a record into the unspentByAddress structure */
    fun insertUnspentByAddress(addr: PayAddress?, outpoint: iTxOutpoint?)
    {
        if (addr == null) return
        if (outpoint == null) return
        synchronized(dataLock) {
            var lst = unspentByAddress.getOrPut(addr) { mutableListOf() }
            if (!lst.contains(outpoint))
            {
                lst.add(outpoint)
            }
        }
    }

    /** Indicate that an address has been used (maybe it really has, or maybe not, but the point is that its reserved).
     * If we start to run low, we'll also make more */
    fun markAddressUsed(addr: PayAddress)
    {
        LogIt.info(sourceLoc() + name + ": address used ${addr.toString()}, ${unusedAddresses.size} unused of ${receiving.size} last generated index ${(this as? Bip44Wallet)?.maxAddress}")
        unusedAddresses.remove(addr)
        prepareDestinations(MIN_UNUSED_ADDRESSES, GEN_ADDRESS_CHUNK_SIZE)
    }

    /** Returns whether this address has been provided to the GUI (it may or may not have actually been used by someone) */
    fun isAddressGivenOut(addr: PayAddress): Boolean
    {
        if (receiving.containsKey(addr))
        {
            return !unusedAddresses.contains(addr)
        }
        return false  // its not even ours so not used!
    }

    /** Remove all unspent outputs.  DANGEROUS API: you will need to recover them by searching the blockchain */
    fun cleanUnspent()
    {
        unspentByAddress.clear()
        allTxos.clear()
    }

    override fun suggestFee(tx: iTransaction, pad: Int, priority: Int): Long
    {
        // This function just returns the size of the tx for BCH-style 1 sat/byte blockchains, but
        // really it should compute the size and other properties of the tx and pass this information to the underlying blockchain object for fee calculation.

        return tx.size.toLong() + pad
    }

    open fun delete()
    {
        dataLock.lock {
            val cs = chainSelector
            walletDb?.let { deleteWallet(it, name, cs) }
            chainstate?.delete()
            chainstate = null
        }
    }

    override fun save(force: Boolean)
    {
        dataLock.lock {
            saveWalletTo(walletDb)
        }
    }

    /** Save wallet transaction state to the database */

    open fun saveWalletTo(db: KvpDatabase?)
    {
        if (db == null) return
        dataLock.lock {
            val st = SerializationType.DISK
            chainstate?.let {
                LogIt.info(sourceLoc() + name + ": Save Chain State: Synced Height: ${it.syncedHeight} Hash: ${it.syncedHash.toHex()}  Prehistory Date: ${it.prehistoryDate}, Height: ${it.prehistoryHeight}")
                var unspentSize: Int = 0
                var receivingSize: Int = 0
                synchronized(dataLock)
                {
                    // Even though this doesn't use chainstate, don't store it if for some reason we don't have a blockchain yet. Because it makes sense to be consistent
                    // with chainstate
                    val txSer = BCHserialized.uint8(TX_STATE_SERIALIZED_VERSION, st) + synchronized(dataLock)
                    {
                        unspentSize = allTxos.size
                        receivingSize = receiving.size
                        // val realUnspent = unspent.filter{ it.value.amount != -1L }
                        val v = BCHserialized.map(allTxos, st)
                        v.add(BCHserialized.map(receiving, { BCHserialized(st).add(it) }, {
                            BCHserialized(st).addUint8(it.derivedType).add(it)
                        }))
                        v
                    }
                    assert(txSer.format == st)
                    val unusedAddrs = synchronized(dataLock)
                    {
                        BCHserialized.uint8(UNUSED_ADDRESSES_SERIALIZED_VERSION, st)
                            .add(BCHserialized.list(unusedAddresses, SerializationType.DISK))
                    }
                    assert(unusedAddrs.format == st)
                    val hist = synchronized(dataLock)
                    {
                        BCHserialized.uint8(TX_HISTORY_SERIALIZED_VERSION, st).add(BCHserialized.map(txHistory, SerializationType.DISK))
                    }
                    assert(hist.format == st)

                    val chainStateData = BCHserialized.uint8(CHAIN_STATE_SERIALIZED_VERSION, st).add(it.BCHserialize(st))
                    assert(chainStateData.format == st)

                    //val gwb = GlueWalletBlockchain(it.chain)
                    //val tmp = gwb.BCHdeserialize(BCHserialized(chainStateData, SerializationType.DISK))  // DEBUG test deserialize right away
                    db.set(chainStateDbKey(name, chainSelector), chainStateData.flatten())
                    db.set(txStateDbKey(name), txSer.flatten())
                    db.set(unusedAddressesDbKey(name), unusedAddrs.flatten())
                    val flathist = hist.flatten()
                    val result = db.set(txHistoryDbKey(name), flathist)
                    if (result == false)
                    {
                        LogIt.error(sourceLoc() + name + ": Wallet FAILED TO SAVE history of ${flathist.size} bytes")
                    }
                    else
                    {
                        LogIt.info(sourceLoc() + name + ": Wallet saved history of ${flathist.size} bytes")
                    }

                    if (identityDomainChanged)
                    {
                        identityDomainChanged = false
                        val identityDomainSerialized: ByteArray =
                            BCHserialized.map(identityDomain, { BCHserialized(SerializationType.DISK).add(it) }, { it.BCHserialize(SerializationType.DISK) }, SerializationType.DISK).flatten()
                        db.set(identityDomainsDbKey(name), identityDomainSerialized)

                    }

                    if (identityInfoChanged)
                    {
                        identityInfoChanged = false
                        val identityInfoSerialized: ByteArray =
                            BCHserialized.map(identityInfo, { BCHserialized(SerializationType.DISK).add(it) }, { it.BCHserialize(SerializationType.DISK) }, SerializationType.DISK).flatten()
                        db.set(identityInfoDbKey(name), identityInfoSerialized)
                    }
                }
                LogIt.info(sourceLoc() + name + ": Wallet state saved: ${unspentSize} utxos,  ${receivingSize} receiving addresses.")
            }
        }
    }

    open fun loadPendingTxFromUnspent(throwIllegal: Boolean = true)
    {
        for ((_, v) in allTxos)
        {
            if (v.spentHeight == -1L && (v.spendableUnconfirmed > 0) && (v.spentUnconfirmed == false))
            // no longer needed: caused by messy database && (commitTx != null) && (commitTx.inputs.size != 0))
            {
                val tx = v.commitTx   // TODO do I need to look this up ever, or is this superfluous ?: txHistory[k.txid]?.tx
                if (tx != null)
                {
                    val txbytes = tx.BCHserialize(SerializationType.NETWORK).flatten()
                    if (txbytes.size < 100)  // TODO: reference chain consensus parameter rather than 100
                    {
                        if (throwIllegal) throw TransactionException("Transaction is too small at ${txbytes.size} bytes")
                    }
                    else
                    {
                        LogIt.info("pending TX:")
                        tx.debugDump()
                        LogIt.info("Hex TX:")
                        val hex = txbytes.toHex()
                        for (c in hex.chunked(900))
                        {
                            LogIt.info(c)
                        }

                        if (v.commitTxIdem != Guid(tx.idem))
                        {
                            LogIt.error("inconsistent data structure: tx and its hash")
                        }

                        pendingTx[tx.idem] = txbytes
                    }
                }
                else
                {
                    // Should never happen, the database is inconsistent
                    LogIt.info(sourceLoc() + name + ": Pending tx unrecoverable, try rediscovering")
                }
            }
        }
    }

    fun loadChainState(cs: GlueWalletBlockchain, db: KvpDatabase, key: String)
    {
        val chData = db.get(key)
        val chStream = BCHserialized(chData, SerializationType.DISK)

        val chainstateVer = chStream.debytes(1)[0]
        if (chainstateVer != CHAIN_STATE_SERIALIZED_VERSION) throw DeserializationException("Invalid version in chain state")
        cs.BCHdeserialize(chStream)
        LogIt.info(sourceLoc() + name + ": Load Chain State: Synced Height: ${cs.syncedHeight} Hash: ${cs.syncedHash.toHex()}  Prehistory Date: ${cs.prehistoryDate}, Height: ${cs.prehistoryHeight}")
    }

    /** Load wallet transaction state from the database */

    open fun loadWalletTx(db: KvpDatabase): Boolean
    {
        return dataLock.lock {
            var ret = false
            try
            {
                if (true) // chainstate?.let
                {
                    var corrupt = false

                    try
                    {
                        LogIt.info(sourceLoc() + " " + name + ": Loading wallet")
                        synchronized(dataLock)
                        {
                            val txData = db.get(txStateDbKey(name))
                            val addrData = db.get(unusedAddressesDbKey(name))
                            val histData = db.get(txHistoryDbKey(name))

                            val addrStream = BCHserialized(addrData, SerializationType.DISK)
                            val txStream = BCHserialized(txData, SerializationType.DISK)
                            val histStream = BCHserialized(histData, SerializationType.DISK)

                            val unspentVer = txStream.debytes(1)[0]
                            if (unspentVer != TX_STATE_SERIALIZED_VERSION) throw DeserializationException("Invalid version in transaction state data")
                            allTxos = txStream.demap({ outpointFor(chainSelector, it) }, { Spendable(chainSelector, it) })
                            LogIt.info(sourceLoc() + " " + name + ": Loaded ${allTxos.size} unspent outpoints")
                            receiving = txStream.demap({ PayAddress(it) },
                                {
                                    val destType = it.debyte().toInt()
                                    when (destType)
                                    {
                                        Pay2PubKeyTemplateDestination.DEST_TYPE -> Pay2PubKeyTemplateDestination(chainSelector, it)
                                        Pay2PubKeyHashDestination.DEST_TYPE -> Pay2PubKeyHashDestination(chainSelector, it)
                                        // Pay2ScriptHashDestination.DEST_TYPE -> Pay2ScriptHashDestination(chainSelector, it)
                                        // MultisigDestination.DEST_TYPE -> MultisigDestination(chainSelector, it)
                                        else -> throw DeserializationException("destination type not handled")
                                    }
                                })
                            LogIt.info(sourceLoc() + " " + name + ": Loaded ${receiving.size} receiving")

                            val histVer = histStream.debytes(1)[0]
                            if (histVer != TX_HISTORY_SERIALIZED_VERSION) throw DeserializationException("Invalid version in transaction history")
                            txHistory = histStream.demap({ Hash256(it) }, { TransactionHistory(chainSelector, it) })
                            LogIt.info(sourceLoc() + " " + name + ": Loaded tx history: ${txHistory.size} entries")

                            unusedAddresses.clear()  // should be empty on load anyway
                            val uaVer = addrStream.debytes(1)[0]
                            if (uaVer != UNUSED_ADDRESSES_SERIALIZED_VERSION) throw DeserializationException("Invalid version in unused addresses")
                            var uaList = addrStream.delist { PayAddress(it) }
                            LogIt.info(sourceLoc() + " " + name + ": Loaded ${uaList.size} unused addresses")
                            unusedAddresses.addAll(uaList)

                            unspentByAddress.clear()
                            for (u in allTxos)
                            {
                                // patch up the Spendable transaction info from our tx history so we don't store it in 2 places.
                                val txh = txHistory[u.value.commitTxIdem.data]
                                u.value.commitTx = txh?.tx
                                insertUnspentByAddress(u.value.addr, u.key)
                            }
                            LogIt.info(sourceLoc() + " " + name + ": Wallet state loaded: ${allTxos.size} utxos,  ${receiving.size} receiving addresses.")
                        }
                    }
                    catch (e: DeserializationException)
                    {
                        LogIt.info(sourceLoc() + name + ": (expected if new wallet) Deserialization exception: " + e.message)
                        handleThreadException(e)
                        corrupt = true
                    }
                    catch (e: UnknownBlockchainException)
                    {
                        LogIt.info(sourceLoc() + name + ": Unknown blockchain: " + e.message)
                        handleThreadException(e)
                        corrupt = true
                    }
                    catch (e: DataMissingException)
                    {
                        LogIt.info(sourceLoc() + name + ": Data missing: " + e.message)
                        // handleThreadException(e)
                        corrupt = true
                    }

                    if (corrupt)
                    {
                        LogIt.info(sourceLoc() + name + ": Stored data is corrupt, rediscovering")
                        rediscover()
                    }
                    ret = !corrupt

                    corrupt = false // Identity data failure does not require wallet reset
                    try
                    {
                        val idData = db.get(identityDomainsDbKey(name))
                        val idStream = BCHserialized(idData, SerializationType.DISK)
                        identityDomain = idStream.demap({ val s = it.deString(); LogIt.info(s); s }, { IdentityDomain(it) })
                        identityDomainChanged = false
                    }
                    catch (e: DeserializationException)
                    {
                        LogIt.info("Deserialization exception: " + e.message)
                        handleThreadException(e)
                        corrupt = true
                    }
                    catch (e: UnknownBlockchainException)
                    {
                        LogIt.info("Unknown blockchain: " + e.message)
                        handleThreadException(e)
                        corrupt = true
                    }
                    catch (e: DataMissingException)  // This is normal; identity is linked to only one blockchain
                    {
                        //LogIt.info(sourceLoc() + name + ": Identity data missing: " + e.message)
                        corrupt = true
                    }
                    if (corrupt)
                    {
                        LogIt.info(sourceLoc() + name + ": Identitiy domain data is corrupt or missing, not loading")
                        // not critical info
                    }

                    corrupt = false // Identity data failure does not require wallet reset
                    try
                    {
                        val idData = db.get(identityInfoDbKey(name))
                        val idStream = BCHserialized(idData, SerializationType.DISK)
                        identityInfo = idStream.demap({ val s = PayAddress(it); LogIt.info(s.toString()); s }, { IdentityInfo(it) })
                        identityInfoChanged = false
                    }
                    catch (e: DeserializationException)
                    {
                        logThreadException(e, "Identity Info Deserialization exception", sourceLoc())
                        corrupt = true
                    }
                    catch (e: DataMissingException)
                    {
                        //logThreadException(e, "Identity Info Data Missing", sourceLoc())
                        corrupt = true
                    }
                    if (corrupt)
                    {
                        LogIt.info(sourceLoc() + name + ": Identity info data is corrupt or missing, not loading")
                        // not critical info
                    }
                }

                loadPendingTxFromUnspent(false)
            }
            catch (e: DataMissingException)
            {
                LogIt.warning("missing wallet chain and tx state data.")
                return@lock false
            }
            catch (e: IndexOutOfBoundsException)
            {
                LogIt.warning("corrupt wallet chain and tx state data.")
                return@lock false
            }
            catch (e: PayAddressDecodeException)
            {
                LogIt.warning("corrupt wallet chain and tx state data (PayAddress)")
                return@lock false
            }
            return@lock ret
        }
    }

    /** Install a change handler that will get called whenever this wallet's state changes */
    override fun setOnWalletChange(callback: ((wallet: Wallet) -> Unit)?)
    {
        walletChangeCallback = callback
    }

    override fun send(amountSatoshis: Long, destAddress: String, deductFeeFromAmount: Boolean, sync: Boolean, note: String?): iTransaction
    {
        val dest = PayAddress(destAddress)
        if (dest.blockchain != chainSelector) throw WalletIncompatibleAddress("Cannot send.  The destination address is for a different cryptocurrency")
        return send(amountSatoshis, dest, deductFeeFromAmount, sync, note)
    }

    override fun send(amountSatoshis: Long, destAddress: PayAddress, deductFeeFromAmount: Boolean, sync: Boolean, note: String?): iTransaction
    {
        if (destAddress.blockchain != chainSelector) throw WalletIncompatibleAddress("Cannot send.  The destination address is for a different cryptocurrency")
        return send(amountSatoshis, destAddress.outputScript(), deductFeeFromAmount, sync, note)
    }

    @cli(Display.Simple, "confirmed balance (of ungrouped coins)")
    override val balanceConfirmed: Long
        get()
        {
            var ret = 0.toLong()
            synchronized(dataLock) {
                for (u in allTxos.values)
                {
                    if (u.groupInfo() == null)
                    {
                        // Committed but not spent
                        if (u.spentHeight == -1L && u.commitHeight != -1L)
                        // if ((u.spentHeight == -1L) && (u.spentUnconfirmed == false) && (u.spendableUnconfirmed == 0.toLong()) && (u.groupInfo() == null))
                            ret += u.amount
                    }
                }
            }
            return ret
        }

    @cli(Display.Simple, "unconfirmed balance (of ungrouped coins)")
    override val balanceUnconfirmed: Long
        get()
        {
            var ret = 0.toLong()
            synchronized(dataLock)
            {
                for (u in allTxos.values)
                {
                    // Skip if the output was subsequently spent by an unconfirmed tx
                    if ((u.spentHeight == -1L) && (u.spendableUnconfirmed > 0) && (u.spentUnconfirmed == false)
                      // And if this is not a grouped output (yes this could skip grouped BCH or BCH just in the group output)
                      && (u.groupInfo() == null))
                        ret += u.amount
                }
            }
            return ret
        }

    @cli(Display.Simple, "total balance (of ungrouped coins)")
    override val balance: Long
        get()
        {
            var ret = 0.toLong()
            synchronized(dataLock) {
                for (u in allTxos.values)
                {
                    if (u.groupInfo() == null)
                    {
                        if ((u.spentHeight == -1L) && (u.spentUnconfirmed == false))  // unspent
                            ret += u.amount
                    }
                }
            }
            return ret
        }

    @cli(Display.User, "unconfirmed transaction list")
    fun getUnconfirmedTx(): MutableList<Spendable>
    {
        var ret = mutableListOf<Spendable>()
        synchronized(dataLock)
        {
            for (u in allTxos.values)
            {
                // Skip if the output was subsequently spent by an unconfirmed tx
                if ((u.spentHeight == -1L) && (u.spendableUnconfirmed > 0) && (u.spentUnconfirmed == false))
                    ret.add(u)
            }
        }
        return ret
    }

    /** Return identity domain data if this domain has previously been used */
    override fun lookupIdentityDomain(name: String): IdentityDomain?
    {
        return identityDomain[name]
    }

    /** Add or update identity domain data */
    override fun upsertIdentityDomain(id: IdentityDomain)
    {
        identityDomainChanged = true
        identityDomain[id.domain] = id
    }

    /** Remove identity domain data */
    override fun removeIdentityDomain(name: String)
    {
        identityDomainChanged = true
        identityDomain.remove(name)
    }

    override fun allIdentityDomains(): Collection<IdentityDomain>
    {
        return identityDomain.values
    }

    /** Return identity domain data if this domain has previously been used */
    override fun lookupIdentityInfo(id: PayAddress): IdentityInfo?
    {
        return identityInfo[id]
    }

    /** Add or update identity domain data */
    override fun upsertIdentityInfo(id: IdentityInfo)
    {
        val tmp = id.identity
        if (tmp != null)
        {
            identityInfoChanged = true
            identityInfo[tmp] = id
        }
    }

    /** Remove identity domain data */
    override fun removeIdentityInfo(id: PayAddress)
    {
        identityInfoChanged = true
        identityInfo.remove(id)
    }

    /** track this transaction and periodically resubmit until it has been committed.
     * @param tx: the wallet transaction to commit
     * @param txbytes: OPTIONAL: the network-serialized wallet transaction (for efficiency if the caller has already serialized it)
     * @throws */
    fun commitWalletTransaction(tx: iTransaction, txbytes: ByteArray? = null, note: String? = null)
    {
        val txb = txbytes ?: tx.BCHserialize(SerializationType.NETWORK).flatten()
        pendingTx[tx.idem] = txb
        val req = chainstate?.chain?.req

        if (tx.size < 100)
        {
            throw IllegalStateException("txbytes is too small")
        }

        try
        {
            // notify this wallet first so the note is stored if there is one
            interestingUnconfirmedTx(mutableListOf(tx), note)
            // other wallets in this device may not get notifications for this tx because I sent it by pushing this into the req mgr, it calls back into ALL the connected wallets
            if (req != null)  // send it out
                req.onUnconfirmedTx(mutableListOf(tx))
        }
        catch(e: Exception)
        {
            handleThreadException(e)
        }
        thisLaunch { save() }  // do this out-of-band so UI response is quicker

        chainstate?.chain?.net?.broadcastTransaction(txb)
    }

    override fun prepareSend(outputs: MutableList<iTxOutput>, minConfirms: Int, deductFeeFromAmount: Boolean): iTransaction
    {
        var total = 0L
        for (out in outputs)
        {
            total += out.amount
        }

        val (signedTx, _) = txConstructor(total, deductFeeFromAmount, minConfirms)
        { tx, _ ->
            assert(deductFeeFromAmount == false)  // TODO: actually deduct
            for (out in outputs)
            {
                tx.add(out)
            }
            deductFeeFromAmount
        }

        return signedTx
    }

    override fun abortTransaction(tx: iTransaction)
    {
        synchronized(dataLock) // Give all the unspent back to the general use pool since we've aborted this inprogress spend
        {
            for (inp in tx.inputs)
            {
                inp.spendable.reserved = 0
            }
        }
    }


    /** Send funds to multiple destinations.  This function will select input coins from the wallet to fill the passed quantity
     * @param addrAmt Provide a list of how many coins to send to which addresses denominated in the fundamental (smallest possible) unit of this currency
     * @param sync If true, do not return until this transaction has been sent to some nodes
     * @param note Some information the sender may privately associate with this send
     * @return a signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    override fun send(addrAmt: List<Pair<PayAddress, Long>>, sync: Boolean, note: String?): iTransaction
    {
        var amountSatoshis = 0L
        for (s in addrAmt) amountSatoshis += s.second
        val (signedTx, serializedTx) = try  // Try at least 1 confirmation first
        {
            txConstructor(amountSatoshis, false, 1) { tx, _ ->
                for (s in addrAmt)
                {
                    val output = txOutputFor(chainSelector)
                    output.amount = s.second
                    output.script = s.first.outputScript()
                    tx.add(output)
                }
                false
            }
        }
        catch (e: WalletNotEnoughBalanceException)  // Try unconfirmed
        {
            txConstructor(amountSatoshis, false, 0) { tx, _ ->
                for (s in addrAmt)
                {
                    val output = txOutputFor(chainSelector)
                    output.amount = s.second
                    output.script = s.first.outputScript()
                    tx.add(output)
                }
                false
            }
        }

        LogIt.info(sourceLoc() + " " + name + ": Sending TX " + signedTx.idem.toHex())
        LogIt.info(sourceLoc() + " " + name + ": TX hex " + serializedTx.toHex())
        if (sync)
            commitWalletTransaction(signedTx, serializedTx, note)
        else thisLaunch { commitWalletTransaction(signedTx, serializedTx, note) }
        return signedTx
    }


    //* Send funds to this destination
    override fun send(amountSatoshis: Long, destScript: SatoshiScript, deductFeeFromAmount: Boolean, sync: Boolean, note: String?): iTransaction
    {
        val (signedTx, serializedTx) = try  // Try at least 1 confirmation first
        {
            txConstructor(amountSatoshis, deductFeeFromAmount, 1) { tx, fee ->
                // Add the output that we are sending to
                val output = txOutputFor(chainSelector)
                output.amount = if (deductFeeFromAmount) amountSatoshis - fee else amountSatoshis
                output.script = destScript
                tx.add(output)
                deductFeeFromAmount
            }
        }
        catch (e: WalletNotEnoughBalanceException)  // Try unconfirmed
        {
            txConstructor(amountSatoshis, deductFeeFromAmount, 0) { tx, fee ->
                // Add the output that we are sending to
                val output = txOutputFor(chainSelector)
                output.amount = if (deductFeeFromAmount) amountSatoshis - fee else amountSatoshis
                output.script = destScript
                tx.add(output)
                deductFeeFromAmount
            }
        }

        LogIt.info(sourceLoc() + " " + name + ": Sending TX " + signedTx.idem.toHex())
        LogIt.info(sourceLoc() + " " + name + ": TX hex " + serializedTx.toHex())
        if (sync)
            commitWalletTransaction(signedTx, serializedTx, note)
        else thisLaunch { commitWalletTransaction(signedTx, serializedTx, note) }
        return signedTx
    }

    //* Send funds to this destination
    override fun send(tx: iTransaction, sync: Boolean, note: String?)
    {
        val serializedTx = tx.BCHserialize(SerializationType.NETWORK).flatten()
        LogIt.info(sourceLoc() + " " + name + ": Sending TX " + tx.idem.toHex())
        LogIt.info(sourceLoc() + " " + name + ": TX hex " + serializedTx.toHex())
        if (sync)
            commitWalletTransaction(tx, serializedTx, note)
        else thisLaunch { commitWalletTransaction(tx, serializedTx, note) }
    }

    //* publish any unconfirmed transactions to the network
    @cli(Display.User, "sends all tx marked unconfirmed to the network.")
    fun resendUnconfirmedTx()
    {
        val v = chainstate?.chain?.net
        if (v != null)
        {
            val txHist = unconfirmedTx()

            for (i in txHist)
            {
                if (i.confirmedHeight < 1)
                {
                    if (!i.tx.isCoinbase())  // If a coinbase somehow got into our history, if we ever relay it we will be banned.  So don't.
                    {
                        v.broadcastTransaction(i.tx.BCHserialize(SerializationType.NETWORK).flatten())
                        LogIt.info("Resending ${i.tx.idem} confirmedHeight is: ${i.confirmedHeight}")
                        LogIt.info("Hex: ${i.tx.toHex()}")
                    }
                }
            }
        }
    }

    /** This function re-checks all tx marked unconfirmed in this wallet to see if they are actually unconfirmed.
     * It uses an electrumx server to do so, so may be unavailable if electrumx is not supported or no servers are available. */
    @cli(Display.User, "re-checks all tx marked unconfirmed in this wallet to see if they are actually unconfirmed.")
    fun reassessUnconfirmedTx()
    {
        val reqMgr = chainstate?.chain?.req ?: throw ElectrumNoNodesException()
        val unTxes = unconfirmedTx()
        val forgetTxes: MutableList<TransactionHistory> = mutableListOf()
        for (unTx in unTxes)
        {
            val retmsg = reqMgr.getTxDetails(unTx.tx.idem)
            val result = (retmsg as JsonObject)["result"] as JsonObject?
            if (result != null)
            {
                val confBlock: String? = result.get("blockhash")?.toString()?.replace(""""""", "")
                val confHeight = result.get("height")?.toString()?.toLong()
                val confTime = result.get("time")?.toString()?.toLong()
                // TODO don't trust the server; get a merkle proof

                if ((confBlock != null) && (confHeight != null) && (confTime != null))
                {
                    interestingConfirmedTx(listOf(unTx.tx), Hash256(confBlock), confHeight, confTime)
                }
                continue
            }
            val error = retmsg["error"] as JsonObject?
            if (error != null)
            {
                LogIt.info("Tx ${unTx.tx.idem} reassess error: " + error["code"])
                if (error["code"].toString() == "-32603")  // This transaction does not exist
                {
                    forgetTxes.add(unTx)
                }
            }
        }

        cleanUnconfirmed(forgetTxes)
    }

    //* get unconfirmed tx sorted by date
    fun unconfirmedTx(): MutableList<TransactionHistory>
    {
        val ret = mutableListOf<TransactionHistory>()
        synchronized(dataLock) {
            for (i in txHistory.values)
            {
                if (i.confirmedHeight < 1) ret.add(i)
            }
        }
        ret.sortBy { it.date }
        return ret
    }

    // This function analyses a script for template op codes, and fills in wallet specific data where requested.
    fun bindScriptToWallet(s: SatoshiScript, overrideDest: PayDestination? = null): SatoshiScript
    {
        return s.replace {
            if (it.contentEquals(OP.TMPL_PUBKEYHASH.v))
            {
                val d = runBlocking { overrideDest ?: newDestination() }
                SatoshiScript(s.chainSelector) + OP.push(d.pkh()!!).v
            }
            else if (it.contentEquals(OP.TMPL_SCRIPT.v))
            {
                val d = runBlocking { overrideDest ?: newDestination() }
                d.grouplessConstraintScript()
            }
            else if (it.contentEquals(OP.TMPL_PUBKEY.v))
            {
                val d = runBlocking { overrideDest ?: newDestination() }
                SatoshiScript(s.chainSelector) + OP.push(d.pubkey!!).v
            }
            else
            {
                throw NotImplementedError()
            }
        }

    }

    /**
     * Checks the transaction's final fee.  Throws an exception and aborts the transaction if the fee is too big.
     */
    fun txSanityCheck(tx: iTransaction)
    {
        tx.changed()
        val sz = tx.size

        var outputAmount: Long = 0
        for (output in tx.outputs)
        {
            outputAmount += output.amount
        }

        // We don't need to check our input amounts against the blockchain for accuracy because if our values are incorrect then the signature hash that was generated
        // as part of signedTx will be incorrect.
        var inputAmount: Long = 0
        for (input in tx.inputs)
        {
            inputAmount += input.spendable.amount
        }

        val finalFee = inputAmount - outputAmount

        if (finalFee > MaxFee)
        {
            abortTransaction(tx)
            throw WalletFeeException(appI18n(RfeeExceedsFlatMax))
        }
        else if (sz * MaxFeePerByte < finalFee)
        {
            abortTransaction(tx)
            throw WalletFeeException(appI18n(RexcessiveFee))
        }
    }

    /**
    Find inputs needed to supply funds for this transaction.
    @param[minConfirms] Only fund with coins that have at least this many confirmations
    @param[inputAmount] If non-null, assume existing inputs supply this number of satoshis (do not look up these inputs)
    @param[flags] See [TxCompletionFlags]
     If change outputs are required, add them.
     If mint baton passing outputs are possible then add them if equalizeAuthorities=true
     If useAuthorities = true, pull in authorities if needed (and available) to handle (mint/melt) operations
     If fund = true, add native crypto inputs to pay for the transaction
    @param[adjustableOutput] Pass an output index if the fee should be deducted from this output.  Otherwise the fee must be taken from extra input
    @throws[WalletException],[WalletNotEnoughBalanceException]
    */
    override fun txCompleter(tx: iTransaction, minConfirms: Int, flags: Int, inputAmount: Long?, adjustableOutput: Int?, destinationAddress:PayAddress?)
    {
        val iSpent = mutableListOf<Spendable>()
        val DUST: Long = dust(chainSelector)  // Below this many satoshis not worth making an output -- just give to miner
        val MAX_FEE_OVERPAY: Long = DUST * 3
        val AVG_OUTPUT_SIZE: Long = 34

        // Track what's coming in and going out for each group
        data class GroupIO(
          var groupId: GroupId, var tokenI: Long, var tokenO: Long, var authorityFlagsI: ULong,
          var authorityFlagsO: ULong
        )

        val groupData = mutableMapOf<GroupId, GroupIO>()

        val destinationOverride: PayDestination? = if (destinationAddress != null)
        {
            val tmp = receiving[destinationAddress]
            if (tmp == null) throw WalletIncompatibleAddress("Not an address from this wallet")
            tmp
        }
        else null

        // Fill any parameterized scripts with addresses from this wallet and
        // Fill the groupData structure with all the output groups and token quantities.
        for (out in tx.outputs)
        {
            val constraint = if ((flags and TxCompletionFlags.BIND_OUTPUT_PARAMETERS) > 0)
            {
                val tmp = bindScriptToWallet(out.script, destinationOverride)
                LogIt.info(sourceLoc() + ": Bind script parameterization: ${out.script.toHex()} -> ${tmp.toHex()}")
                out.script = tmp
                tmp
            }
            else out.script

            val cgdata = constraint.groupInfo(out.amount) ?: continue  // If ungrouped, nothing more to do
            val d = groupData[cgdata.groupId]
            if (d == null) groupData[cgdata.groupId] = GroupIO(cgdata.groupId, 0, cgdata.tokenAmt, 0.toULong(), cgdata.authorityFlags)
            else
            {
                d.tokenO += cgdata.tokenAmt
                d.authorityFlagsO = d.authorityFlagsI or cgdata.authorityFlags
            }

        }

        // Figure out what caller has supplied.
        for (inp in tx.inputs)
        {
            val prevout = inp.spendable
            val cgdata = prevout.priorOutScript.groupInfo(prevout.amount) ?: continue  // continue if ungrouped
            val d = groupData[cgdata.groupId] ?: continue // Well, there's an existing input with no corresponding output... either tx is melting or won't validate (not our problem)

            d.tokenI += cgdata.tokenAmt
            d.authorityFlagsI = d.authorityFlagsI or cgdata.authorityFlags  // if the caller supplied an authority bit
        }

        oneTxConstructor.lock()
        {
            if ((flags and TxCompletionFlags.FUND_GROUPS) > 0)
            {
                // supply inputs and outputs for the difference between what's coming in and going out
                for ((gid, g) in groupData)
                {
                    if (g.tokenI < g.tokenO) // Supply token input or mint authority
                    {
                        val inputs =
                          filterInputs(minConfirms, groupedFilter(g.groupId, true, GroupAuthorityFlags.NO_AUTHORITY))
                        val gAmt = inputs.fold(0L,
                          { acc, utxo -> acc + utxo.groupInfo()!!.tokenAmt })  // how much do I have available
                        if (gAmt + g.tokenI >= g.tokenO)  // Ok don't need to mint
                        {
                            // TODO quantity-sensitive selection
                            while (g.tokenI < g.tokenO)
                            {
                                val utxo = inputs.removeAt(0)
                                utxo.reserved = epochSeconds()
                                val gi = utxo.groupInfo()!!  // filter did not work if null
                                tx.add(txInputFor(utxo))
                                g.tokenI += gi.tokenAmt
                            }

                        }
                        else
                        {
                            if ((flags and TxCompletionFlags.USE_GROUP_AUTHORITIES) == 0)
                            {
                                LogIt.info(sourceLoc() + ": Token ${gid.toString()} (${gid.toHex()}) input qty: ${g.tokenI} output qty: ${g.tokenO} and wallet has not enough balance: ${gAmt}")
                                throw WalletNotEnoughTokenBalanceException(appI18n(RsendMoreTokensThanBalance))
                            }
                            var mask = GroupAuthorityFlags.MINT
                            if ((flags and TxCompletionFlags.NO_BATON_AUTHORITIES) != 0)  // If baton authorities are not allowed then include BATON in the mask
                            {
                                mask = mask or GroupAuthorityFlags.BATON
                            }
                            var auths = filterInputs(minConfirms, groupedFilter(g.groupId, false, GroupAuthorityFlags.MINT, mask))
                            if (auths.isEmpty())
                            {
                                // If we have no mint auths, look to see if this is a subgroup and whether we have mint through the parent
                                if (g.groupId.isSubgroup())
                                {
                                    mask = mask or GroupAuthorityFlags.SUBGROUP
                                    auths = filterInputs(minConfirms,
                                      groupedFilter(g.groupId.parentGroup(),
                                        false,
                                        GroupAuthorityFlags.SUBGROUP or GroupAuthorityFlags.MINT, mask))
                                }
                                if (auths.isEmpty())
                                {
                                    LogIt.info(sourceLoc() + ": Token ${gid.toString()} (${gid.toHex()}) input qty: ${g.tokenI} output qty: ${g.tokenO} and wallet has not enough balance: ${gAmt} and no auths")
                                    throw WalletNotEnoughTokenBalanceException(appI18n(RsendMoreTokensThanBalance))
                                }
                            }

                            val auth = auths[0]
                            auth.reserved = epochSeconds()
                            val authGroupInfo = auth.groupInfo()
                            tx.add(txInputFor(auth))
                            g.authorityFlagsI = g.authorityFlagsI or authGroupInfo!!.authorityFlags  // We just added this into the inputs, so mark that we now have these new authorities
                            iSpent.add(auth)
                            // If this authority can be passed to a child, make an output so we don't lose this ability
                            if ((authGroupInfo.authorityFlags and GroupAuthorityFlags.BATON) > 0.toULong())
                            {
                                // TODO do not reuse the authority address, note that we need to handle subgroups here when fixing
                                tx.add(txOutputFor(chainSelector, dust(chainSelector), auth.priorOutScript))
                            }
                            else
                            {
                                LogIt.info(sourceLoc() + ": consuming authority without creating child authority because baton not set")
                            }
                        }
                    }
                    // The above code could include too many tokens, meaning we now need to make change,
                    // or the caller supplied tx could have done the same

                    if (g.tokenI > g.tokenO) // Create token change output
                    {
                        val co = createGroupedChangeOutput(gid, g.tokenI - g.tokenO)
                        tx.add(co)
                        g.tokenO = g.tokenI  // In case we use it later
                    }

                    // are any output authorities set that are not provided by the inputs?
                    // If so, we need to supply an authority
                    val missingAuthorities: ULong = g.authorityFlagsI.inv() and g.authorityFlagsO
                    if (missingAuthorities != 0.toULong())
                    {
                        if ((flags and TxCompletionFlags.USE_GROUP_AUTHORITIES) == 0)
                        {
                            throw WalletAuthorityException("API call must use authorities but not authorized to do so by caller")
                        }
                        var mask = missingAuthorities
                        if ((flags and TxCompletionFlags.NO_BATON_AUTHORITIES) != 0)  // If baton authorities are not allowed then include BATON in the mask
                        {
                            mask = mask or GroupAuthorityFlags.BATON
                        }
                        var auths = filterInputs(minConfirms, groupedFilter(g.groupId, false, missingAuthorities, mask))
                        if (auths.isEmpty())
                        {
                            // If we have no mint auths, look to see if this is a subgroup and whether we have the authorities through the parent
                            if (g.groupId.isSubgroup())
                            {
                                mask = mask or GroupAuthorityFlags.SUBGROUP
                                auths = filterInputs(minConfirms,
                                  groupedFilter(g.groupId.parentGroup(),
                                    false,
                                    GroupAuthorityFlags.SUBGROUP or missingAuthorities, mask))
                            }
                            if (auths.isEmpty())
                            {
                                throw WalletAuthorityException(appI18n(RneedNonexistentAuthority))
                            }
                        }

                        val auth = auths[0]
                        auth.reserved = epochSeconds()
                        val authGroupInfo = auth.groupInfo()
                        tx.add(txInputFor(auth))
                        iSpent.add(auth)
                        // If this authority can be passed to a child, make an output so we don't lose this ability
                        if ((authGroupInfo!!.authorityFlags and GroupAuthorityFlags.BATON) > 0.toULong())
                        {
                            // TODO do not reuse the authority address, note that we need to handle subgroups here when fixing
                            tx.add(txOutputFor(chainSelector, dust(chainSelector), auth.priorOutScript))
                        }
                        else
                        {
                            LogIt.info(sourceLoc() + ": consuming authority without creating child authority because baton not set")
                        }
                    }

                }
            }

            tx.changed()  // recalc size since the above probably changed the tx
            // Now that the groups are equalized, equalize the satoshis
            if ((flags and TxCompletionFlags.FUND_NATIVE) > 0)
            {
                var outAmt = tx.outputs.fold(0L, { a, b -> a + b.amount })
                var inAmt = inputAmount ?: tx.inputs.fold(0L, { a, b -> a + b.spendable.amount })

                // By setting outFee to 0 here if we intend to deduct the fee from an output, we will just fund the outputs without the fee
                var outFee: Long = if ((flags and TxCompletionFlags.DEDUCT_FEE_FROM_OUTPUT) > 0) 0L
                else feeForSize((tx.size + (APPROX_P2PKH_SIG_SCRIPT_LEN * tx.inputs.size)).toLong())

                // We are draining this account, grab all ungrouped UTXOs
                if ((flags and TxCompletionFlags.SPEND_ALL_NATIVE) > 0)
                {
                    val inputs = findInputs(Long.MAX_VALUE, minConfirms)  // findInputs automatically sets the utxos it selects to reserved
                    if (inputs != null)
                    {
                        while (!inputs.isEmpty())
                        {
                            val utxo = inputs.removeAt(0)
                            tx.add(txInputFor(utxo))
                            iSpent.add(utxo)
                            inAmt += utxo.amount
                            outFee += feeForSize((APPROX_P2PKH_SIG_SCRIPT_LEN + TX_SCRIPTLESS_INPUT_SIZE).toLong())
                        }
                    }
                }
                // Spend the right amount of native satoshis
                else if (inAmt < outAmt + outFee)  // Need to pull in more sats
                {
                    while (inAmt < outAmt + outFee)
                    {
                        val inputs = findInputs(outAmt + outFee - inAmt, minConfirms)  // findInputs automatically sets the utxos it selects to reserved

                        if (inputs == null)
                        {
                            LogIt.info(sourceLoc() + ": Wallet cannot find enough sats with ${minConfirms} confirms. Need ${outAmt} + ${outFee} - $inAmt = ${outAmt + outFee - inAmt}")
                            throw WalletNotEnoughBalanceException(appI18n(RsendMoreThanBalance))
                        }

                        while ((inAmt < outAmt + outFee) && !inputs.isEmpty())
                        {
                            val utxo = inputs.removeAt(0)
                            tx.add(txInputFor(utxo))
                            iSpent.add(utxo)
                            inAmt += utxo.amount

                            outFee += feeForSize((APPROX_P2PKH_SIG_SCRIPT_LEN + TX_SCRIPTLESS_INPUT_SIZE).toLong())
                        }

                        // looping again is unlikely; it means that the chosen input was close enough for outFee to exceed it
                        // In this case, just grab another utxo (which might be not-tx-size-efficient in the short run)
                    }
                }

                // Since the fee is increasing while we add inputs, its possible we'll run out of utxos, so double check that we did it
                if (inAmt < outAmt + outFee)
                {
                    LogIt.info(sourceLoc() + ": Even after looping Adding inputs caused too large fee. Provided ${inAmt}.  Needed ${outFee} + ${outAmt} = ${outAmt + outFee}")
                    throw WalletNotEnoughBalanceException(appI18n(RsendMoreThanBalance))
                }

                // Now add some change if needed.  We only add a change output if we aren't adjusting one of the outputs
                if (adjustableOutput == null)
                {
                    if (inAmt > outAmt + outFee + MAX_FEE_OVERPAY)  // TODO dump extra dust into group outputs for later fee paying
                    {
                        if ((flags and TxCompletionFlags.DEDUCT_FEE_FROM_OUTPUT) == 0)
                            outFee += feeForSize(AVG_OUTPUT_SIZE)  // Add fee for the change output
                        val co = createChangeOutput(inAmt - (outAmt + outFee))
                        tx.add(co)
                    }
                }
                else  // if we are adjusting one of the outputs, dump all the extra into there.
                {
                    tx.outputs[adjustableOutput].amount += inAmt - (outAmt + outFee)
                }

            }
        }


        val sighash = if ((flags and TxCompletionFlags.PARTIAL) > 0)
        // BCH: sign with sighash single and anyone can pay. NOTE: doesn't really do what we want -- we want a few outputs signed
        // actually sighash anyonecanpay would work, but payer would have to create 2 tx, the first makes an output with exact change for this tx
        // 0xc3  // actually sighash anyonecanpay would work, but payer would have to create 2 tx, the first makes an output with exact change for this tx
            tx.appendableSighash()
        else
            byteArrayOf() // ALL/ALL

        // Sign the transaction
        if ((flags and TxCompletionFlags.SIGN) > 0)
            signTransaction(tx, sighash)

        // By signing above, we can calculate the exact fee needed, but then we need to re-sign the transaction (which is poor performance)
        // Note that the most common use case for deductFeeFromOutput is to send the entire wallet's contents somewhere else, so performance probably
        // won't matter.
        //
        // Also note that if you don't sign the TX but select deductFeeFromOutput, that doesn't make sense because we don't know how much to deduct.
        if ((flags and TxCompletionFlags.DEDUCT_FEE_FROM_OUTPUT) > 0)
        {
            if (adjustableOutput == null) throw WalletImplementationException("If deducting a fee from an output, you MUST provide the output index")
            tx.changed()  // recalc size since the above probably changed the tx
            val outFee = feeForSize(tx.size)
            tx.outputs[adjustableOutput].amount -= outFee
            if (tx.outputs[adjustableOutput].amount < 0) throw WalletFeeException("Fee exceeds adjustable output's amount")
            if ((flags and TxCompletionFlags.SIGN) > 0)
                signTransaction(tx, sighash)
        }

        txSanityCheck(tx)
    }


    protected fun txConstructor(
      amountSatoshis: Long,
      deductFeeFromAmount: Boolean,
      minConfirms: Int = 1,
      outputFiller: ((iTransaction, Long) -> Boolean)
    ): Pair<iTransaction, ByteArray>
    {
        val AVG_INPUT_SIZE: Long = 147
        val AVG_OUTPUT_SIZE: Long = 34
        val TX_OVERHEAD_SIZE: Long = 16
        val DUST: Long = dust(chainSelector)

        if (amountSatoshis < DUST)
        {
            throw WalletDustException("Sending dust")
        }

        val ret: Pair<iTransaction, ByteArray> =  oneTxConstructor.lock {
            var NinputsGuestimate = 1
            var retin:Pair<iTransaction, ByteArray>? = null

            outerloop@ while (true)
            {
                // Find inputs
                val total =
                  if (deductFeeFromAmount) amountSatoshis else amountSatoshis + feeForSize(AVG_INPUT_SIZE * NinputsGuestimate + AVG_OUTPUT_SIZE * 2 + TX_OVERHEAD_SIZE)
                val inputs = findInputs(total, minConfirms)

                if (inputs != null)
                {
                    var newTx = txFor(chainSelector)
                    var inAmt: Long = 0

                    // Fill the TX with the chosen inputs
                    for (i in inputs)
                    {
                        LogIt.info(name + ": TX Input: " + (i.outpoint?.toHex() ?: "null") + "   Amount: " + i.amount)
                        var bchinput = txInputFor(i) // a spendable carries with it the ability to sign a tx.  This is how the secret (if one is needed) is communicated to the tx under construction
                        bchinput.script = SatoshiScript(chainSelector)
                        newTx.add(bchinput)
                        inAmt += i.amount
                    }

                    // How much do we want to pay for this tx?
                    var fee = feeForSize(newTx.inputs.size * AVG_INPUT_SIZE + AVG_OUTPUT_SIZE * 2 + TX_OVERHEAD_SIZE)

                    var signedTx: iTransaction?
                    var serializedTx: ByteArray?
                    while (true)  // Sign the TX and see if the fee is acceptable to the network for the real size of the tx.  If not try again with the new fee
                    {
                        newTx.outputs.clear()
                        val feeDeducted = outputFiller(newTx, fee)

                        // check the outputFiller to make sure it used what it claimed
                        var actualAmount = 0L
                        for (out in newTx.outputs)
                        {
                            if (out.amount < 0)
                            {
                                abortTransaction(newTx)
                                throw WalletFeeException(appI18n(RdeductedFeeLargerThanSendAmount))
                            }
                            actualAmount += out.amount
                        }
                        var oops = false
                        if (feeDeducted)
                        {
                            if (actualAmount + fee != amountSatoshis) oops = true
                        }
                        else if (actualAmount != amountSatoshis) oops = true
                        if (oops)
                        {
                            abortTransaction(newTx)
                            throw WalletImplementationException("Output filler filled the wrong number of satoshis")
                        }

                        if (inAmt - (actualAmount + fee) < 0)  // The quantity in all the inputs can't afford this tx.  We need to find another input
                        {
                            // By bumping the guess as to the number of inputs, we will increase the amount allocated for fees

                            NinputsGuestimate = max(inputs.size, NinputsGuestimate + 1)
                            abortTransaction(newTx)
                            continue@outerloop
                        }

                        // add change output only if needed
                        if (inAmt - (actualAmount + fee) > DUST)
                        {
                            val co = createChangeOutput(inAmt - amountSatoshis - fee)
                            newTx.add(co)
                        }

                        // TODO shuffle outputs
                        signedTx = signTransaction(newTx)

                        // SANITY CHECK for extremely large fee and abort if so.
                        txSanityCheck(signedTx)

                        serializedTx = signedTx.BCHserialize(SerializationType.NETWORK).flatten()
                        val serializedTxSize = serializedTx.size.toLong()

                        val minFee = minFeeForSize(serializedTxSize)

                        if (fee < minFee)   // If the fee we chose is lower than the minimum fee for a tx of this size, try again with minfee as the fee.
                        {
                            fee = minFee
                            newTx.outputs
                        }
                        else break  // Ok worked!
                    }

                    if ((signedTx == null) || (serializedTx == null))  // Should never happen
                    {
                        abortTransaction(newTx)
                        throw WalletNotEnoughBalanceException(appI18n(RsendMoreThanBalance))
                    }

                    val outputSize = signedTx.outputs[0].BCHserialize(SerializationType.NETWORK).flatten().size
                    val inputSize = signedTx.inputs[0].BCHserialize(SerializationType.NETWORK).flatten().size
                    val inputScriptSize = signedTx.inputs[0].script.BCHserialize(SerializationType.NETWORK).flatten().size
                    LogIt.info(sourceLoc() + " " + name + ": TX info: size ${serializedTx.size} fee $fee rate ${fee.toDouble() / serializedTx.size.toDouble()}  inputSize $inputSize (script $inputScriptSize) outputSize $outputSize ")

                    retin = Pair(signedTx, serializedTx)
                    break
                }
                throw WalletNotEnoughBalanceException(appI18n(RsendMoreThanBalance))
            }
            return@lock retin!!
        }
        return ret
    }

    fun signTransaction(tx: iTransaction, sigHashType: ByteArray = byteArrayOf()): iTransaction
    {
        val txSerialized = tx.BCHserialize(SerializationType.NETWORK)
        var count: Long = 0
        val flatTx = txSerialized.flatten()

        //LogIt.info(flatTx.ToHex())
        for (inp in tx.inputs)
        {
            signInput(tx, count, sigHashType, serializedTx = flatTx)
            count += 1
        }
        tx.changed()  //  Clear out the hash (if it was requested) since it will have changed now that signatures are added.
        return tx
    }

    @cli(Display.Simple, "sign the provided string with the provided address (this wallet must have the private key for that address), or pass null to use the common identity")
    override fun signMessage(message: ByteArray, addr: PayAddress?): ByteArray
    {
        val identityDest: PayDestination = destinationFor(Bip44Wallet.COMMON_IDENTITY_SEED)

        if (addr == null || addr == identityDest.address)
        {
            // This is a coding bug in the wallet
            val secret = identityDest.secret ?: throw WalletIncompatibleAddress("Wallet failed to provide an identity with a secret")
            return libnexa.signMessage(message, secret.getSecret()) ?: throw WalletIncompatibleAddress("Wallet signing failed")
        }
        else
        {
            val dest: PayDestination? = receiving[addr]
            val secret = identityDest.secret ?: throw WalletIncompatibleAddress("Wallet failed to provide an identity with a secret")
            if (dest == null) throw WalletAddressMissingException("This wallet is unaware of this address")
            return libnexa.signMessage(message, secret.getSecret()) ?: throw WalletIncompatibleAddress("Wallet signing failed")
        }
    }


    fun createChangeOutput(amtSatoshis: Long): iTxOutput
    {
        val d = runBlocking { newDestination() }
        var ret = txOutputFor(chainSelector)
        ret.amount = amtSatoshis
        ret.script = d.ungroupedOutputScript()
        return ret
    }

    // If the group holds native tokens, amtSatoshis is ignored
    fun createGroupedChangeOutput(groupId: GroupId, groupAmount: Long, amtSatoshis: Long = dust(chainSelector)): NexaTxOutput
    {
        val d = runBlocking { newDestination() }
        if (!chainSelector.isNexaFamily) throw UnsupportedInBlockchain("Group tokenization accessed in a non-Nexa chain")
        val ret = NexaTxOutput(chainSelector)

        if (groupId.isFenced())
        {
            assert(false)  // not implemented
        }
        else
        {
            ret.amount = amtSatoshis
            ret.script = d.groupedOutputScript(groupId, groupAmount)
        }
        return ret
    }

    fun feeForSize(txSize: Long): Long
    {
        return ceil(txSize.toDouble() * DesiredFeeSatPerByte).toLong()  // Guess 1 satoshi per byte
    }

    fun minFeeForSize(txSize: Long): Long
    {
        return ceil(txSize * MinFeeSatPerByte).toLong()
    }

    //? Find inputs in this wallet to use in a new transaction.  This does NOT mark them as used, so cannot be called repeatedly until consumeInputs is called
    //? Pass a filter to select a subset of the available UTXOs.
    fun filterInputs(minConfirms: Int = 0, filter: ((Spendable) -> Boolean)? = null): MutableList<Spendable>
    {
        val curHeight: Long = chainstate?.syncedHeight ?: if (minConfirms == 0) 0L else throw WalletDisconnectedException()
        var ret: MutableList<Spendable> = mutableListOf()

        for (i in allTxos.values)
        {
            // If an unconfirmed or confirmed spend of this utxo exists, then don't use it.
            if ((!i.spentUnconfirmed) &&
              (i.spentHeight == -1L) && (i.amount > 0) &&
              // subtracting 1 from minConfirms because "0 confirms" means unconfirmed so the "1th" block is the tip, not the "0th"
              ((minConfirms == 0) || ((i.commitHeight != -1L) && (i.commitHeight + (minConfirms - 1) <= curHeight))) &&  // Or we need some number of confirmations
              (i.reserved == 0L) &&
              ((filter != null) || (i.groupInfo() == null)) &&  // if filter is null, then only look for ungrouped coins
              ((filter == null) || filter(i))
            )  // or this inputs has been used by some other in-progress transaction
                ret.add(i)
        }

        return ret
    }

    /** Find inputs in this wallet to use in a new transaction.  This does NOT mark them as used, so cannot be called repeatedly until consumeInputs is called
     * @param amountSatoshis: The quantity of satoshis needed.  Pass LONG.MAX_VALUE to get all of them (otherwise the function will fail if there are not enough)
     * @param minConfirms: Minimum number of confirmations
     * @param filter: You can eliminate some inputs using this filter, by returning false
     Pass a filter to select a subset of the available UTXOs.
    */
    fun findInputs(amountSatoshis: Long, minConfirms: Int = 0, filter: ((Spendable) -> Boolean)? = null): MutableList<Spendable>?
    {
        // LogIt.info("Looking for: " + amountSatoshis)
        var ret = mutableListOf<Spendable>()
        var currentTotal: Long = 0
        // grab all of our UTXOs as a list that's sorted by quantity of satoshis
        val ok = synchronized(dataLock)
        {
            var amounts: MutableList<Spendable> = filterInputs(minConfirms, filter)

            amounts.sortBy { it.amount }

            // Loop, adding one utxo per iteration, until we've found enough satoshis or run out of UTXOs
            while ((currentTotal < amountSatoshis) && (amounts.size > 0))
            {
                var loc = amounts.binarySearchBy(amountSatoshis - currentTotal) { it.amount }
                if (loc < 0)  // means we missed but -loc-1 is near (its the insertion position if the elem was added)
                {
                    loc = -loc - 1
                }

                if (loc >= amounts.size)  // We need more coins than exist in a UTXO
                {
                    loc = amounts.size - 1
                }

                if ((loc < 0) || (loc >= amounts.size)) return@synchronized false  // loc is out of bounds because nothing left in the UTXO


                val utxo = amounts[loc]
                if (utxo.reserved == 0L)  // Final check because we released the lock
                {
                    // LogIt.info("found amount: " + utxo.amount)
                    ret.add(utxo)                // Add this utxo into our list of inputs
                    utxo.reserved = epochSeconds()
                    currentTotal += utxo.amount  // and add its quantity of satoshis into our running total
                }
                amounts.removeAt(loc)  // remove this from our list of possible utxos because we've used it
            }
            true
        }
        if (!ok) return null

        // We have enough we are done
        if ((currentTotal >= amountSatoshis)||(amountSatoshis == Long.MAX_VALUE)) return ret

        // We used all the UTXOs and didn't get enough.  Undo the reservations.  This is an uncommon case since the wallet can determine whether there's enough balance before calling findInputs
        synchronized(dataLock)
        {
            for (utxo in ret)
            {
                utxo.reserved = 0L
            }
        }
        return null
    }

    override fun synced(epochTimeinMsOrBlockHeight: Long): Boolean
    {
        val TIME_FUDGE = 60 * 60 * 1000  // 1 hour in milliseconds
        val now = (epochTimeinMsOrBlockHeight == -1L)
        var tim = if (now) millinow() else epochTimeinMsOrBlockHeight
        val bc = chainstate
        if (bc == null) return false  // can't be synced if we don't have a chainstate


        if (tim > 1262350000000) // Time stamp
        {
            if (bc.syncedHeight >= bc.chain.curHeight)  // Ok we are synced with our blockchain
            {
                val tip = bc.chain.nearTip ?: return false
                // Special case the "now" choice -- check if our blockchain up to date?
                if (now && (tip.time * 1000 < tim - TIME_FUDGE)) return false
                return true
            }
            else
            {
                if (now) return false; // we can't be synced if our blocks aren't even synced
                val hdr = bc.chain.getBlockHeader(bc.syncedHeight)
                if (hdr.time >= tim) return true  // Our synced header came in after the queried time
            }
        }
        else
        {
            if (tim <= bc.syncedHeight) return true
        }

        return false
    }

    //? undo 1 block's changes to this wallet
    fun rewind()
    {
        val cs = chainstate ?: throw WalletException("Cannot rewind with no blockchain")
        val syncedHash = cs.syncedHash

        LogIt.info(sourceLoc() + " " + name + ": Rewind away from ${cs.syncedHash.toHex()}:${cs.syncedHeight}")

        val header = cs.chain.blockHeader(syncedHash) ?: throw WalletException("Cannot rewind earlier than our header history")
        synchronized(dataLock)
        {
            val rewindData = cs.blockRewind[syncedHash]

            // before the prehistoryHeight by definition there is no wallet changes but rewindData will just return null
            if (rewindData != null)  // If I don't have data on this block, it did not change my wallet
            {
                for (spendable in rewindData.added)
                {
                    LogIt.info("Rewind removing: " + (spendable.outpoint?.toHex() ?: "null") + " amt: " + spendable.amount)
                    if (allTxos.remove(spendable.outpoint) == null)
                    {
                        LogIt.error("Missing utxo!")
                    }

                    spendable.addr?.let { addr ->
                        val v: MutableList<iTxOutpoint>? = unspentByAddress[addr]
                        if (v != null) v.remove(spendable.outpoint)
                    }

                }

                for (spendable in rewindData.spent)
                {
                    // Since I'm rewinding this block, I need to mark this spendable as unspent
                    if (spendable.spentBlockHash.data == syncedHash)
                    {
                        assert(spendable.spentHeight == header.height)
                        spendable.spentBlockHash = Guid()
                        spendable.spentHeight = -1
                        spendable.spentTxHash = Guid()
                        spendable.spentUnconfirmed = false
                    }
                    LogIt.info("Rewind returning: " + (spendable.outpoint?.toHex() ?: "null") + " amt: " + spendable.amount)
                    allTxos[spendable.outpoint!!] = spendable
                    insertUnspentByAddress(spendable.addr, spendable.outpoint!!)
                }
            }

            cs.syncedHeight -= 1
            if (cs.prehistoryHeight > cs.syncedHeight) cs.prehistoryHeight = cs.syncedHeight  // When we rock forward our new history will start where ever we had to rewind back to.
            if (cs.prehistoryDate > header.time) cs.prehistoryDate = header.time - PREHISTORY_SAFEFTY_FACTOR  // we set prehistory to the current block because we have it, but rewind from there.
            cs.syncedHash = header.hashPrevBlock
        }
        walletChangeCallback?.invoke(this)  // Update the GUI
    }

    fun generateDestinationsInto(newUnusedAddresses: MutableList<PayAddress>, minAmt: Int = GEN_ADDRESS_CHUNK_SIZE)
    {
        for (i in 0..minAmt)
        {
            val dest = generateDestination()
            val destaddr = dest.address
            //LogIt.info(i.toString() + ": destaddr: " + destaddr + " dest pubkey:" + dest.pubkey?.toHex())
            if (destaddr == null) throw WalletImplementationException("Generated destination needs to be representable as an address")
            synchronized(dataLock) {
                receiving[destaddr] = dest
                newUnusedAddresses.add(destaddr)  // I cannot add these addresses to the unused pile until they are installed into my bloom filters in connected nodes
            }
        }
        LogIt.info(sourceLoc() + name + ": receiving into ${receiving.size} addresses")
    }

    val preparingDestinationsSync = Mutex()
    override fun prepareDestinations(minAmt: Int, chunk: Int)
    {
        preparingDestinationsSync.trylock {
            // Only let one execution of this function happen at a time; ignore other calls to it
            val newUnusedAddresses = mutableListOf<PayAddress>()
            if (unusedAddresses.size < minAmt)
            {
                while(unusedAddresses.size + newUnusedAddresses.size < minAmt)
                {
                    // Put all the new addrs into a temporary array so nobody uses them yet
                    synchronized(dataLock) {
                        generateDestinationsInto(newUnusedAddresses, chunk)
                    }
                }
                    // Hand the new addrs to our connected nodes in the form of a bloom filter.
                regenerateBloom() {
                    // OK bloom is installed so finally we can release these addresses.
                    for (a in newUnusedAddresses) unusedAddresses.add(a)
                }
            }
        }
    }


    //* create a new address that funds can be sent to.  This function actually returns an existing pre-generated address, and generates new addresses in chunks of 'GEN_ADDRESS_CHUNK_SIZE when running out
    // This function makes sure that the destination is installed into bloom filters of connected nodes before it returns.  This adds complexity to this function, but eliminates a lot of problems where
    // a transaction is not seen because addresses are used before added to the bloom filter.
    override suspend fun newDestination(): PayDestination
    {
        var result: PayDestination? = null
        var prep = false
        while (result == null)
        {
            synchronized(dataLock) {
                if (unusedAddresses.size > 0)
                {
                    val ret = unusedAddresses.removeAt(0)
                    // The address must exist because all destinations are put into 'receiving at the time of creation
                    result = receiving[ret]!!
                }
                else
                {
                    if (!prep) { prep = true; prepareDestinations(1, GEN_ADDRESS_CHUNK_SIZE); }
                }
            }
            if (result == null) delay(20)  // Wait for destinations to be created
        }
        LogIt.info(sourceLoc() + " " + name + " Returning new address " + result?.address.toString())
        return result!!
    }


    // This function keeps this wallet synced up with the chainstate
    fun run()
    {
        try
        {
            var priorChainTip = Hash256()
            while (!done)
            {
                // Implements blockchain processing pause feature at the API level.  Normally no pause here.
                // Also allows us to stop processing when making a major change to the blockchain (like rewind or rediscover)
                while (pause)
                {
                    paused = true
                    wakey.delayuntil(100L) { pause == false }
                }
                paused = false
                LogIt.info(sourceLoc() + " " + name + ": wallet analysis at: " + (chainstate?.chain?.nearTip?.hash?.toHex() ?: "unavailable") + ":" + (chainstate?.chain?.nearTip?.height ?: "unavailable"))
                try
                {
                    val cs = chainstate
                    if (cs != null)
                    {
                        if (cs.syncedHash == Hash256())  // I'm not sure if the chain knows about my start location yet
                        {
                            val hdr = cs.chain.findOrRewindTo(cs.syncedHeight)
                            cs.syncedHash = hdr.hash  // ok progress forwards...
                        }

                        // Find the blockchain's most difficult tipFrac
                        cs.chain.nearTip?.let { tip: iBlockHeader ->
                            if (tip.hash != priorChainTip)  // Update UI elements that show the wallet's blockchain since it has changed.
                            {
                                priorChainTip = tip.hash
                                walletChangeCallback?.invoke(this)
                            }

                            if (tip.hash != cs.syncedHash)
                            {
                                LogIt.info(sourceLoc() +" " + name + ": Moving wallet to " + tip.hash.toHex() + ":" + tip.height + " from: " + cs.syncedHash.toHex() + ":" + cs.syncedHeight)
                                try  // Rewind to get onto the path to the most difficult tip
                                {
                                    // The if check makes this code work for regtest when the wallet starts not knowing the genesis block
                                    // And for when you "rediscover" the blockchain
                                    if (cs.syncedHeight != -1L)
                                    {
                                        while (!cs.chain.isInMostWorkChain(cs.syncedHeight, cs.syncedHash))
                                        {
                                            LogIt.info("rewind")
                                            try
                                            {
                                                rewind()
                                            }
                                            catch (e: WalletException)
                                            {
                                                LogIt.info("Wallet Exception:" + e.toString())
                                                millisleep(100U)
                                                break
                                            }
                                        }
                                    }
                                }
                                catch (e: RequestedPrehistoryHeader)  // Rewound everything; move forward from here
                                {
                                    logThreadException(e, "Prehistory header requested", sourceLoc())
                                }

                                val syncedH = cs.syncedHeight
                                if (cs.chain.curHeight > syncedH) // I need to sync with additional blocks
                                {
                                    // Load the headers from storage
                                    //var count = (syncedH.toInt() + 1000) and 511.inv()  // move by powers of 2 because ancestorchain prefers that
                                    //count = count - syncedH.toInt()
                                    //if (count <= 0) count = 1000
                                    val count = 1527  // a weird number makes the pauses seem random
                                    LogIt.info("wallet loading headers from ${syncedH}")
                                    val hdrs = cs.chain.getHeaderChain(syncedH + 1, cs.syncedHash, tip.hash, count)
                                    LogIt.info("header load completed")
                                    while ((hdrs.size > 0) && (hdrs[0].height < syncedH + 1)) // too many returned, chop what I don't want
                                    {
                                        // LogIt.info("Header at ${hdrs[0].height}")
                                        hdrs.removeAt(0)
                                    }

                                    if (hdrs.size > 0 && hdrs[0].height != syncedH + 1)
                                    {
                                        LogIt.error(sourceLoc() + " " + name + ": Out of order headers. Asking starting at ${syncedH + 1}, loaded ${hdrs.size} headers starting at ${hdrs[0].height} (${hdrs[0].hash.toHex()})")
                                        //it.chain.reacquireHeaders(it.syncedHeight+1, hdrs[0].hash)
                                    }
                                    else
                                    {
                                        val earlyReq = mutableListOf<Guid>()
                                        for (hdr in hdrs)  // Skip early request for prehistory blocks
                                        {
                                            if (!((hdr.time <= cs.prehistoryDate) || (hdr.height <= cs.prehistoryHeight))) earlyReq.add(Guid(hdr.hash))
                                            else LogIt.info(sourceLoc() + " " + name + ": Skipping prehistory block time ${hdr.time} <= ${cs.prehistoryDate} or height ${hdr.height} <= ${cs.prehistoryHeight}")
                                        }
                                        thisLaunch { cs.chain.earlyRequestTxInBlock(earlyReq) }

                                        for (hdr in hdrs)
                                        {
                                            if ((hdr.time <= cs.prehistoryDate) || (hdr.height <= cs.prehistoryHeight))  // skip accessing any blocks that happened before this wallet was created
                                            {
                                                LogIt.info("prehistory")
                                                cs.prehistoryHeight = max(cs.prehistoryHeight, hdr.height)
                                                onSync(hdr.hash, hdr.height, hdr.time)
                                                walletChangeCallback?.invoke(this)
                                            }
                                            else
                                            {
                                                if (hdr.height != cs.syncedHeight + 1)  // ensure that hdrs is in order
                                                {
                                                    LogIt.error(sourceLoc() + " " + name + ": Out of order headers received ${hdr.height} expecting ${cs.syncedHeight + 1}")
                                                    if (hdr.height > cs.syncedHeight + 1) break // and if they are beyond what we need then abort this work
                                                }
                                                else
                                                {
                                                    val txInBlock = cs.chain.getTxInBlock(Guid(hdr.hash))
                                                    if (txInBlock.size > 0)
                                                    {
                                                        LogIt.info(sourceLoc() + " " + name + ": Synced Wallet at ${cs.syncedHeight} with: " + txInBlock.size + " interesting tx. block " + hdr.height + " hash: " + hdr.hash.toHex())

                                                        synchronized(dataLock)
                                                        {
                                                            interestingConfirmedTx(txInBlock, hdr.hash, hdr.height, hdr.time * 1000)
                                                            onSync(hdr.hash, hdr.height, hdr.time)
                                                        }
                                                    }
                                                    else onSync(hdr.hash, hdr.height, hdr.time)  // Nothing to do but bump the synced at number
                                                    if (txInBlock.size > 0 || (hdr.height and 0x15L) == 0L || cs.syncedHeight >= cs.chain.curHeight)
                                                        walletChangeCallback?.invoke(this)
                                                    // Write the chainstate data so we can pick up where we left off if the app stops and restarts
                                                    if (flushWalletPeriodically()) save()
                                                }
                                            }
                                            // abort this sync if something else is going on (we'll just re-request the headers we skipped)
                                            if (pause) break
                                        }
                                        // Write the chainstate data so we can pick up where we left off if the app stops and restarts
                                        // either periodically, or at the end of sync
                                        if ((hdrs.size > 0) && flushWalletPeriodically() || (hdrs.size >= 16 && cs.syncedHeight >= cs.chain.curHeight)) save()
                                    }
                                }
                            }
                        }
                    }
                }
                catch (e: RequestedPrehistoryHeader)  // Blockchain tells us that it doesn't keep data that far back, so wallets must assume that they also have no old activity
                {
                    LogIt.info(sourceLoc() + " " + name + ": wallet asked for blockchain prehistory block.  Moving wallet to ${e.lastBlock?.height}")
                    if (e.lastBlock != null)
                    {
                        chainstate?.syncedHeight = e.lastBlock.height
                        chainstate?.syncedHash = e.lastBlock.hash
                    }
                }
                catch (e: BlockNotForthcoming)  // Nodes may not provide blocks to us if we request ones that are not on the main chain.  In that case loop around so that we move the wallet to the main chain
                {
                    LogIt.info(sourceLoc() + " " + name + ": Block is not being supplied, ${e.toString()}")
                    //if (blockchain.net.numPeers() > 0)  // no blocks makes sense  if we have no peers
                    //    chainstate?.chain?.findAnotherTip()  // This fixes a weird, likely never to be seen in mainnet, case where there's a tie but one of the blocks was invalidated and yet we keep requesting that block rather than the active one.
                }
                catch (e: P2PNoNodesException)  // No connectivity; nothing to do but wait
                {
                    val tmp = wakeCount
                    wakey.delayuntil(LONG_DELAY_INTERVAL) { wakeCount != tmp }
                }

                // Periodically resend all unconfirmed transactions to random nodes, most especially this happens once the wallet gets connected up
                val v = chainstate?.chain?.net
                if ((v != null) && (v.size > 0) && timeToResendHistoricalTx())
                {
                    resendUnconfirmedTx()
                }

                // Periodically resend pending transactions to random nodes
                // keep track of which nodes have INVed me with the tx, and don't send it to them
                if ((v != null) && (v.size > 0) && (pendingTx.size > 0))
                {
                    if (timeToResendWalletTx())
                    {
                        // Send all transactions to one node in case they are dependant
                        val pt = mutableListOf<ByteArray>()
                        for ((_, tx) in pendingTx)
                        {
                            pt.add(tx)
                        }
                        try
                        {
                            v.sendTransactions(pt)

                        }
                        catch (e: P2PDisconnectedException)  // Nothing to do but wait for another opportunity to send
                        {
                            timeToResendWalletTx.reset()
                        }
                        catch (e: P2PNoNodesException)
                        {
                            timeToResendWalletTx.reset()
                        }
                    }
                }

                val tmp = wakeCount
                wakey.delayuntil(5000)  { wakeCount != tmp } // Periodic delay before rechecking
            }
        }
        catch (e: Exception)
        {
            handleThreadException(e, sourceLoc() + " " + name)
        }
        done = true  // If the wallet aborts from its processing loop, set the done indicator
    }

    /** @return the current token balance
     * @param groupId The group identifier for this token
     * @param dest If non-null, only return balances in this address
     * @param minConfirms If nonzero, the funding transaction must be at least this deep.
     * @param unspent If unspent is true, the current balance is provided.  Otherwise the total ever received is provided.
     */
    @cli(Display.Simple, "Get the balance of the supplied token (at the supplied address).  If minConfirms is nonzero, the funding transaction must be at least this deep. If unspent is true, the current balance is provided.  Otherwise the total ever received is provided.")
    override fun getBalanceIn(groupId: GroupId, dest: PayAddress?, minConfirms: Int, unspent: Boolean): Long
    {
        return synchronized(dataLock)
        {
            val curHeight = syncedHeight
            var ret = 0L
            for (txo in txos)
            {
                val sp = txo.value
                // Check that unconfirmed are allowed or that the right number of confirms have passed
                if ((sp.commitHeight == -1L && minConfirms == 0) || (sp.commitHeight + minConfirms) >= curHeight)
                {
                    val grp = sp.groupInfo()
                    // group id must match
                    if ((grp != null) && !grp.isAuthority() && (grp.groupId == groupId))
                    {
                        if (dest == null || (dest == txo.value.addr))  // If an address is supplied it must match
                        {
                            if (unspent)
                            {
                                if (sp.isUnspent)
                                    ret += grp.tokenAmt
                            }
                            else
                            {
                                ret += grp.tokenAmt
                            }
                        }
                    }
                }
            }
            ret
        }
    }

    override fun getBalanceIn(dest: PayAddress, unspent:Boolean): Long
    {
        return synchronized(dataLock)
        {
            var ret = 0L
            if (unspent == true)
            {
                if (dest in unspentByAddress)
                {
                    unspentByAddress[dest]?.let {
                        for (outpt in it)
                        {
                            val sp = allTxos[outpt]
                            if (sp != null)
                            {
                                if ((unspent == false) || ((sp.spentHeight <= 0) && (sp.spentUnconfirmed == false)))
                                    ret += allTxos[outpt]?.amount ?: 0
                            }
                        }
                    }
                }
            }
            else
            {
                val os = dest.outputScript()
                for (txo in allTxos)
                {
                    if (txo.value.priorOutScript contentEquals os)
                    {
                        ret += txo.value.amount
                    }
                }
            }
            ret
        }
    }

    override fun isUnspentWalletAddress(dest: PayAddress): Boolean
    {
        return synchronized(dataLock)
        {
            val d = unspentByAddress[dest]
            (d != null)
        }
    }

    override fun isWalletAddress(dest: PayAddress): Boolean
    {
        return synchronized(dataLock)
        {
            val d = receiving[dest]
            (d != null)
        }
    }

    /** @return Return the PayDestination if passed address belongs to this wallet
     */
    @cli(Display.Simple, "Return the PayDestination if passed address belongs to this wallet")
    override fun walletDestination(dest: PayAddress): PayDestination?
    {
        return synchronized(dataLock)
        {
            receiving[dest]
        }
    }


    fun regenerateBloom(onBloomInstalled: (() -> Unit)?)
    {
        // without a backing blockchain, there's no reason for a bloom, so this function is a no-op
        val cs = chainstate
        if (cs == null)
        {
            onBloomInstalled?.invoke()
            return
        }

        val data = synchronized(dataLock)
        {
            val data = Array<Any>(receiving.size + allTxos.size, { Unit })
            var idx = 0
            // Install all known addresses, so we find incoming money
            LogIt.info(sourceLoc() + name + ": Regenerate bloom with ${receiving.size} addresses")
            for (r in receiving)
            {
                r.value.bloomFilterData?.let {
                    data[idx] = it
                    // need to create the log string here because it uses receiving
                    //logStr = name + ": Installing bloom with " + receiving.size + " elements: " + receiving.map { it.key.toString() }.joinToString() + "bloom size: " + bloom.size
                    assert(r.key == r.value.address)
                    // LogIt.info(sourceLoc() + name + ": bloom addr " + idx + " is " + r.value.address + " or " + it.toHex())
                    idx++
                }
            }

            // Install all known outpoints so that we find spends
            for (u in allTxos)
            {
                data[idx] = u.key.BCHserialize(SerializationType.NETWORK).flatten()
                idx++
            }

            // if a receiving object can't resolve to some bloom filter data this assertion could fail.  But that should never happen for a wallet (that must understand all its own scripts)
            assert(idx == receiving.size + allTxos.size)
            data
        }

        cs.filterHandle = cs.chain.setFilterObjects(data, cs.filterHandle, onBloomInstalled)
        LogIt.info(sourceLoc() + name + ": Regenerated bloom filter handle ${cs.filterHandle} with ${receiving.size} addresses and ${allTxos.size} outpoints")
    }

    fun addBlockchain(chain: Blockchain, earliestHeight: Long?, startPlace: Long?)
    {
        val cs = GlueWalletBlockchain(chain)
        val dbkey = chainStateDbKey(name, chain.chainSelector)
        var badChainState = false
        var noData = false
        try
        {
            loadChainState(cs, walletDb!!, dbkey)
        }
        catch (e: DataMissingException)
        {
            badChainState = true
            noData = true
        }
        catch (e: IndexOutOfBoundsException)
        {
            LogIt.info(sourceLoc() + " " + name + ": DB deserialization error for key: " + dbkey)
            walletDb?.delete(dbkey)
            badChainState = true
        }
        catch (e: IllegalArgumentException)  // illegal capacity
        {
            LogIt.warning(sourceLoc() + " " + name + ": database corruption " + dbkey + ". Deleting record")
            walletDb?.delete(dbkey)
            badChainState = true
        }
        catch (e: DeserializationException)
        {
            LogIt.info(sourceLoc() + " " + name + ": DB deserialization error for key: " + dbkey)
            walletDb?.delete(dbkey)
            badChainState = true
        }
        LogIt.info(sourceLoc() + " " + name + ": Loaded wallet blockchain state at: ${cs.syncedHash}:${cs.syncedHeight}")

        if (badChainState)
        {
            if (noData)
            {
                if ((startPlace == null)||(earliestHeight == null))
                {
                    cs.prehistoryDate = 0
                    cs.prehistoryHeight = 1
                }
                else
                {
                    // last prehistory block is 1 less than the first activity...
                    val ph = if (earliestHeight>0) earliestHeight-1 else 0
                    cs.prehistoryDate = startPlace
                    cs.prehistoryHeight = ph
                    cs.syncedHash = Hash256()  // I don't know it yet
                    cs.syncedHeight = ph
                }
            }
            LogIt.info(sourceLoc() + " " + name + ": Bad or no persisted chainstate, assuming wallet needs recovery and setting prehistory to " + cs.prehistoryDate + "(" + epochToDate(cs.prehistoryDate) + ")")
            if (cs.prehistoryHeight <= 0.toLong())  // If we couldn't set prehistoryHeight any other way, set it from the checkpoint
            {
                synchronized(dataLock)
                {
                    cs.prehistoryHeight = chain.checkpointHeight
                    cs.syncedHash = chain.checkpointId
                    cs.syncedHeight = chain.checkpointHeight
                }
            }
        }

        // Unconfirmed tx are communicated via callback.  Confirmed tx are handled by requesting a block and processing the result.
        chain.req.addUnconfirmedTxHandler({ txs -> interestingUnconfirmedTx(txs, null) })

        chainstate = cs
        try
        {
            if (receiving.size == 0)  // you didn't loadWalletTx yet!
            {
                LogIt.error(sourceLoc() + name + ": Load wallet addresses and transactions before adding a blockchain")
                throw WalletException("Load wallet addresses and transactions before adding a blockchain")
            }
            else
            {
                // Have a bunch of unused addresses ready and installed in the bloom filter
                if (unusedAddresses.size < MIN_UNUSED_ADDRESSES) generateDestinationsInto(unusedAddresses, MIN_UNUSED_ADDRESSES - unusedAddresses.size)
                // Restart will start the processing thread that will catch this wallet up to the current tip.  Don't start doing that until the bloom filter is properly installed or we might miss tx
                thisLaunch { regenerateBloom({}) }
            }
        }
        catch (e: DataMissingException)
        {
            rediscover(true)
        }

        restart()
    }

    fun restart()
    {
        if (processingThread == null)
        {
            processingThread = Thread(name + "_wallet") { run() }
        }
    }

    fun stop()
    {
        done = true
    }

    fun onSync(blockHash: Hash256, blockHeight: Long, blockTime: Long)
    {
        // LogIt.info(sourceLoc() + " " + "onSync set synced height to:" + blockHeight + " time: " + blockTime)
        chainstate?.syncedHeight = blockHeight
        chainstate?.syncedHash = blockHash
        chainstate?.syncedDate = blockTime
        if (blockHeight == blockchain.curHeight) // we caught up so force the wallet to be saved
        {
            save(true)
        }

        walletChangeCallback?.invoke(this)  // UPDATE the GUI
    }

    fun interestingConfirmedTx(txes: List<iTransaction>, blockHash: Hash256, blockHeight: Long, msSinceEpoch: Long)
    {
        // You can only give the wallet confirmed tx for the next block.
        // Otherwise portions of the transaction graph could be lost since we do not know to look for a certain Outpoint without previously seeing the output
        interestingTx(txes, blockHash, blockHeight, msSinceEpoch, null)
        for (tx in txes)
            pendingTx.remove(tx.idem)  // If one of our transactions was confirmed, then remove it from the pending list so we know not to keep broadcasting it
    }

    fun interestingUnconfirmedTx(txs: List<iTransaction>, note: String?)
    {
        interestingTx(txs, null, null, null, note)
    }

    /** Get the price of this crypto's finest unit in fiat, using some external service */
    fun getPrice(whenInMsSinceEpoch: Long, currencyCode: String): BigDecimal
    {
        val now = millinow()

        try
        {
            if (now - whenInMsSinceEpoch < 24 * 60 * 60 * 1000) // 24 hours (historical price doesn't work for today)
            {
                val spfn = spotPrice
                if (spfn != null)
                {
                    return (spfn(currencyCode))
                }
            }
            else
            {
                val hfn = historicalPrice
                if (hfn != null)
                {
                    return hfn(currencyCode, whenInMsSinceEpoch / 1000)
                }
            }
        }
        catch (e: Exception)
        {
            LogIt.info(sourceLoc() + " " + name + " " + ": Cannot get price in " + currencyCode + " Error: " + e.toString())
        }

        // I can't get the price
        LogIt.info(sourceLoc() + " " + name + " " + ": Cannot get price in " + currencyCode)
        return BigDecimal.ZERO
    }

    fun recoverDtorOrder(txUnsorted: List<iTransaction>): List<iTransaction>
    {
        return txUnsorted  // TODO
    }

    // Collect all the data we will need to figure out the money flow
    //class HRecord(val inflow: MutableList<iTxOutpoint> = mutableListOf(), val outflow: MutableList<PaymentHistory> = mutableListOf(), val spent: MutableList<PaymentHistory> = mutableListOf())

    fun interestingTx(txUnsorted: List<iTransaction>, blockHash: Hash256?, blockHeight: Long?, msSinceEpoch: Long?, note: String?)
    {
        var changed = false
        var idx: Long
        synchronized(dataLock)
        {
            val txs = recoverDtorOrder(txUnsorted)

            // Filter out any repeats
            val filteredTxes = mutableListOf<iTransaction>()

            // These are transactions that I may add to this wallet history by the end of this alg
            val possHistory: MutableMap<Hash256,TransactionHistory> = mutableMapOf()

            // Create a UTXO map for the outputs in all transactions
            // We use this to look up other tx that spend this.  These Tx are not necessarily mine, but we cant tell yet because we haven't analyzed the parent tx which could be
            // anywhere in this block.
            val blockUtxos: MutableMap<iTxOutpoint, iTransaction> = mutableMapOf()
            for (tx in txs)
            {
                var txoutidx = 0
                for (txo in tx.outputs)
                {
                    blockUtxos[outpointFor(chainSelector, tx.idem, txoutidx.toLong())] = tx
                    txoutidx++
                }
            }

            for (tx in txs)
            {
                if (blockHash != null)
                {
                    LogIt.info(sourceLoc() +" " + name + ": Confirmed tx " + tx.idem.toHex() + " in block " + blockHash.toHex() + ":" + blockHeight)
                }
                else
                {
                    LogIt.info(sourceLoc() +" " + name + ": Unconfirmed tx " + tx.idem.toHex())
                }

                // Let's see if we already have this tx in our history
                val handledTx = txHistory[tx.idem]

                var process = if (handledTx == null) true  // We've never seen this transaction
                else if (handledTx.confirmedHeight > 0.toLong()) false  // We already have a confirmation of this tx
                else if (blockHash != null) true  // This is confirmed, and we only have unconfirmed
                else false // double unconfirmed

                // If we've already put this tx in our history, populate our possible history cache with it so that we can just use possHistory as a superset of all relevant history
                // that is, possHistory may have extra tx we don't need, but it is guaranteed to have all the tx we may need.
                if (handledTx != null) possHistory[tx.idem] = handledTx

                // Discover if this tx really has something to do with this wallet, or is it a false positive or a different wallet's tx.
                var relevantToMe = false
                if (process)
                {
                    // Is this transaction spending a UTXO tracked by this wallet, or a UTXO created in this block?  (the wallet is sending money)
                    for (i in tx.inputs)
                    {
                        val unsp = allTxos[i.spendable.outpoint]
                        if (unsp != null)
                        {
                            LogIt.info(sourceLoc() +" " + name + ": block tx ${tx.idem.toHex()} spends my UTXO")
                            relevantToMe = true; break;
                        }

                        val blockSpend = blockUtxos[i.spendable.outpoint]  // If its spending a utxo in this block we need to do the OTI alg with it
                        if (blockSpend != null)
                        {
                            LogIt.info(sourceLoc() +" " + name + ": block tx ${tx.idem.toHex()} spends a UTXO created in the same block")
                            relevantToMe = true; break;
                        }
                    }
                }
                // If we haven't found a wallet input, then look to see if this tx outputs to this wallet (the wallet is receiving money)
                if (process && !relevantToMe)
                {
                    for (i in tx.outputs)
                    {
                        if (i.amount > 0)  // Ignore anything unspendable
                        {
                            val addr = i.script.address
                            if (addr == null)
                            {
                                LogIt.info(sourceLoc() +name + ": cannot understand tx: " + tx.idem.toHex() + " output: " + i.script.toHex())
                            }
                            else
                            {
                                val dest = receiving[addr]
                                if (dest != null)
                                {
                                    relevantToMe = true
                                    LogIt.info(sourceLoc() + name + ": block tx ${tx.idem} sending to my address " + addr)
                                    break
                                }
                                else
                                {
                                    // LogIt.info(sourceLoc() + name + ": block tx ${tx.idem} sending to foreign address " + addr)
                                }
                            }
                        }
                    }
                }

                if (process && relevantToMe)
                {
                    // Add this tx into the list that needs wallet processing
                    filteredTxes.add(tx)

                    // Add this tx into the possible history. I can't add it to the history yet, because it might just spend another tx in this block (this other tx or may not be mine)
                    if (handledTx == null)
                    {
                        val entry = TransactionHistory(chainSelector, tx)
                        if (blockHeight != null) entry.confirmedHeight = blockHeight
                        if (blockHash != null) entry.confirmedHash = blockHash
                        // In general, take "now" as the transaction time since it is more accurate than the block time (we'll create this record for unconfirmed tx)
                        // unless the current time is well after the block time.  In that case, assume we are doing an IBD and so use the block time rather than the current time
                        if ((msSinceEpoch != null) && (entry.date - msSinceEpoch > 2 * 60 * 60 * 1000)) // 2 hours
                        {
                            entry.date = msSinceEpoch
                        }
                        val cs = chainstate
                        if ((txHistory.size == 0)&&(cs != null)&&(blockHeight != null))  // This is the first transaction relevant to this wallet
                        {
                            // if the prehistory height is unspecified, set it to the block before this first transaction
                            if (cs.prehistoryHeight == 0L) cs.prehistoryHeight = blockHeight-1
                        }

                        if (note != null) entry.note = note
                        possHistory[tx.idem] = entry
                    }
                    else
                    {
                        if (blockHeight != null) handledTx.confirmedHeight = blockHeight
                        if (blockHash != null) handledTx.confirmedHash = blockHash
                    }
                }
            }

            // Now process all the tx in the list

            //val hData: MutableMap<Hash256, HRecord> = mutableMapOf()

            // TX don't come in in dependency order, so need to do outs then inputs algorithm (OTI)
            for (tx in filteredTxes)
            {
                var incomingAmt = 0L
                val walletIncomingIdxes = mutableListOf<Long>()
                idx = 0

                //val hrecord = HRecord()
                //hData[tx.idem] = hrecord

                //LogIt.info(name + ": looking for wallet output given ${receiving.size} addresses")
                for (utxo in tx.outputs)
                {
                    val addr = utxo.script.address  // If we can parse this UTXO into an address, then maybe its being sent to this wallet
                    val payDest = receiving[addr]

                    if (addr != null)  // If its a nonstandard payment give up any analysis
                    {
                        if (payDest != null)  // This utxo is interesting to us (its one of this wallet's destinations)
                        {
                            val outpoint = outpointFor(chainSelector, tx.idem, idx)

                            if (true)
                            {
                                var created = false
                                // If it doesn't exist, initialize it, assume its an unconfirmed and we'll modify later if confirmed
                                val sp: Spendable = allTxos.getOrPut(outpoint)
                                {
                                    created = true
                                    val sp = Spendable(chainSelector)
                                    sp.outpoint = outpoint
                                    sp.spendableUnconfirmed = 1
                                    sp
                                }

                                assert(sp.outpoint == outpoint)
                                sp.amount = utxo.amount

                                if (blockHash != null)
                                { // This is a confirmed spend.  Overwrite any unconfirmed data with that of the confirmed tx, just in case malleated tx or doublespend
                                    if (sp.commitHeight > 0)
                                    {
                                        LogIt.info("RECONFIRM")
                                    }
                                    sp.spendableUnconfirmed = 0  // We got confirmed
                                    sp.commitBlockHash = Guid(blockHash)
                                    sp.commitHeight = blockHeight!!  // blockHeight must be something if blockHash is something

                                    // update the rewind data in case this blockchain is re-orged
                                    val rewindData = chainstate?.getRewindData(blockHash) ?: throw WalletException("cannot happen because these vars are always inited")
                                    rewindData.added.add(sp)
                                }
                                else
                                {
                                    if (created == false)
                                    {
                                        if (sp.commitHeight <= 0)
                                        {
                                            LogIt.info("RE-UNCONFIRMED")
                                        }
                                        else
                                        {
                                            LogIt.info("UNCONFIRMED notification of CONFIRMED TX")
                                        }
                                    }
                                }

                                sp.secret = payDest.secret  // TODO encrypt or reference an encrypted secret rather than holding it in plaintext
                                sp.redeemScript = SatoshiScript(chainSelector) // TODO
                                sp.priorOutScript = utxo.script  // Hang onto the output script since its part of the sighash when spending
                                sp.addr = addr
                                // Note we could check for a double spend by comparing this to an existing value
                                if ((sp.commitTx == null) || (blockHash != null))  // If there's no info yet, or if this is the tx being commited to the blockchain then update the tx
                                    sp.commitTx = tx
                                sp.commitTxIdem = Guid(tx.idem)

                                incomingAmt += sp.amount
                                walletIncomingIdxes.add(idx)

                                LogIt.info(name + ": Wallet tx received " + sp.amount + " in block " + (blockHash?.toHex()
                                  ?: "Unconfirmed") + " TX " + tx.idem.toHex() + " outpoint " + sp.outpoint!!.toHex())

                                if (sp.spentBlockHash == Guid())
                                    insertUnspentByAddress(addr, sp.outpoint)
                                else
                                    LogIt.info("Output was already spent when received")

                                // Remove this address from the unused addresses list
                                markAddressUsed(addr)

                                //hrecord.inflow.add(outpoint)

                                // record must exist because we created it above
                                assert(allTxos[outpoint]!!.commitTx != null)
                            }
                            changed = true
                        }
                        else  // If this UTXO is unknown to this wallet, then it might be an outgoing payment if we signed the inputs of the tx.
                        {     // But we can't figure that out until later, so create outgoing histories but keep them locally
                            //val outpay = updateOutgoingPaymentHistory(tx, idx, outpoint, utxo.amount, addr, msSinceEpoch, note)
                            //hrecord.outflow.add(outpay)
                        }
                    }
                    idx += 1
                }

                if (incomingAmt > 0)
                {
                    LogIt.info(sourceLoc() + " " + name + ": adding ${tx.idem.toHex()} to history because received ${incomingAmt}")
                    val history = possHistory[tx.idem]
                    if (history != null)  // its got to be in this map b/c I added it just above
                    {
                        // if we know the block time now, then use it if its earlier than the first moment we became aware of this tx
                        if (msSinceEpoch != null && msSinceEpoch < history.date) history.date = msSinceEpoch
                        history.incomingAmt = incomingAmt
                        history.incomingIdxes.clear()
                        history.incomingIdxes.addAll(walletIncomingIdxes)
                        if (note != null) history.note = note
                        txHistory[tx.idem] = history  // This is a deposit into this wallet (that is, output is one of ours) so its part of the history
                    }
                    else
                    {
                        LogIt.info(sourceLoc() +name + " BUG should NEVER HAPPEN!")
                    }
                }
            }

            // We added all outputs, so now we can look for any inputs.  This catches any cases where we receive and then send in the same block and the txes are in the list in reverse causal order
            for (tx in filteredTxes)
            {
                val walletOutgoingIdxes = mutableListOf<Long>()
                val walletOutgoingOutputs = mutableListOf<iTxOutput>()

                //val hrecord = hData[tx.idem]!!  // Must be non-null because we create it in the loop above
                //val spentHistory = hrecord.spent

                // go thru outputs again tabulating what I received vs what someone else received
                var Ireceived = 0L
                var Oreceived = 0L
                for (utxo in tx.outputs)
                {
                    val addr = utxo.script.address
                    val payDest = receiving[addr]
                    if (payDest != null)
                    {
                        Ireceived += utxo.amount  // This wallet received
                    }
                    else Oreceived += utxo.amount  // Other wallet received
                }

                // Go through all the inputs, confirming that we spent our UTXOs
                idx = 0
                var Ispent = 0L  // This wallet spent
                var Ospent = 0L  // Other wallet spent
                idx = 0
                var spendsMyUtxo = false
                for (spent in tx.inputs)
                {
                    synchronized(dataLock)
                    {
                        val outpoint = spent.spendable.outpoint

                        // This code relies on us getting the output tx before we get the spend.  This may not be the case for unconfirmed tx at least in theory
                        val v: Spendable? = allTxos.get(outpoint)
                        if (v != null) // One of our inputs was spent
                        {
                            spendsMyUtxo = true
                            Ispent += v.amount
                            LogIt.info(name + ": Wallet tx spent " + v.amount + " in block " + (blockHash?.toHex()
                              ?: " Unconfirmed") + " TX " + tx.idem.toHex() + " outpoint " + spent.spendable.outpoint!!.toHex())

                            walletOutgoingIdxes.add(idx)
                            walletOutgoingOutputs.add(v.prevout)
                            changed = true
                            if (blockHash == null)  // This is an unconfirmed tx
                            {
                                // TODO check other stuff like that spentHeight == -1 (not yet spent)
                                v.spentUnconfirmed = true
                                v.spentTxHash = Guid(tx.idem)
                            }
                            else  // confirmation that our input was spent
                            {
                                v.spentUnconfirmed = false
                                val rewindData = chainstate?.getRewindData(blockHash) ?: throw WalletException("cannot happen because these vars are always inited")

                                rewindData.spent.add(v)
                                //  just mark it as spent: unspent.remove(spent.spendable.outpoint)
                                // and clean up much later
                                v.spentBlockHash = Guid(blockHash)
                                v.spentHeight = blockHeight ?: -1
                                v.spentTxHash = Guid(tx.idem)
                            }

                            // We should have prior payment history on every payment because the OTI (outs-then-ins) algorithm added all payments
                            //paymentHistory[outpoint]?.let { spentHistory.add(it) }
                        }
                        else
                        {
                            Ospent += spent.spendable.amount
                        }
                    }
                    idx += 1
                }

                if (spendsMyUtxo)
                {
                    LogIt.info(sourceLoc() + " " + name + ": adding ${tx.idem.toHex()} to history because spent ${Ispent}")
                    val history = possHistory[tx.idem]
                    if (history != null) // its got to be in this map b/c I added it in the processing before the OTI step
                    {
                        history.outgoingAmt = Ispent
                        history.outgoingIdxes.clear()
                        history.outgoingIdxes.addAll(walletOutgoingIdxes)
                        history.spentTxos.clear()
                        history.spentTxos.addAll(walletOutgoingOutputs)
                        if (note != null) history.note = note
                        txHistory[tx.idem] = history
                    }
                    else
                    {
                        LogIt.info(sourceLoc() +name + " BUG should NEVER HAPPEN!  (All relevant tx should have been put into the history)")
                    }
                }
            }

            // we want to track the cost basis for every tx (and only those tx) relevant to this wallet, which is anything that either the input or output processing above decided to put into
            // the wallet.
            for (tx in filteredTxes)
            {
                txHistory[tx.idem]?.let {
                    trackCostBasis(it)
                }
            }

        }

        if (changed) walletChangeCallback?.let { it(this) }
    }

    // TODO: This entire code is broken by partial transactions because it assumes that any tx I've signed are entirely "mine" (all outputs are change or outgoing flows)
    // and all inputs are mine if any are.
    fun trackCostBasis(th:TransactionHistory?)
    {
        return
        /*
        var giveUp = 0
        // Figure out what this transaction actually did and update the relevant history records
        // This code assumes that if this wallet spent any coins (i.e. signed any inputs) then self outputs are change

        while (hData.size > 0)
        {
            var progress = 0
            val txdataIter = hData.iterator()
            for (txdata in txdataIter)
            {
                // Look for a transaction whose inputs are all resolved with cost basis information
                var inputsResolved = true
                var inputAmount = 0L
                var totalPrice = BigDecimal(0, currencyMath).setScale(currencyScale)
                var priceWhatFiat = "USD"
                for (inp in txdata.value.spent)
                {
                    val priorSpend:PaymentHistory? = inp.outpoint?.let { paymentHistory[it] }
                    if (priorSpend == null) inputsResolved = false
                    else
                    {
                        if (priorSpend.priceWhatFiat == "") inputsResolved = false  // I haven't figured out the cost basis yet
                        else
                        {
                            priceWhatFiat = priorSpend.priceWhatFiat  // TODO make sure same fiat
                            inputAmount += priorSpend.amount
                            val basisOverride = priorSpend.basisOverride

                            if (basisOverride != null)
                            {
                                totalPrice += basisOverride
                            }
                            else
                            {
                                totalPrice = totalPrice + (priorSpend.priceWhenIssued * BigDecimal(priorSpend.amount))
                            }
                        }
                    }
                    if (!inputsResolved) break
                }

                // We have enough information to resolve this whole tx
                if (inputsResolved)
                {
                    if (inputAmount > 0)  // Wallet paid someone
                    {
                        val avgPrice = totalPrice / BigDecimal(inputAmount)  // fiat/satoshi

                        for (outflow in txdata.value.outflow)  // The outputs that actually went to someone
                        {
                            outflow.basisOverride = avgPrice * BigDecimal(outflow.amount)
                            outflow.priceWhatFiat = priceWhatFiat
                            outflow.priceWhenIssued = getPrice(outflow.date, priceWhatFiat)
                            paymentHistory[outflow.outpoint!!] = outflow  // Since this wallet signed some inputs in this tx, I'm going to assume that all the outputs are this wallet spending
                        }

                        for (inflow in txdata.value.inflow)  // The outputs that went to myself (change) should get the cost basis of the inputs
                        {
                            val inflowRecord = paymentHistory[inflow]!!
                            inflowRecord.priceWhenIssued = avgPrice
                            inflowRecord.priceWhatFiat = priceWhatFiat
                            inflowRecord.isChange = true
                            // TODO inherit the most recent date of all parents
                        }
                    }
                    else  // Wallet received money
                    {
                        for (inflow in txdata.value.inflow)  // The outputs that went to myself (change) should get the cost basis of the inputs
                        {
                            val inflowRecord = paymentHistory[inflow]!!
                            inflowRecord.priceWhenIssued = getPrice(inflowRecord.date, priceWhatFiat)
                            inflowRecord.priceWhatFiat = priceWhatFiat
                            inflowRecord.isChange = false
                        }
                    }

                    // remove this tx from txdata since its finished processing
                    txdataIter.remove()
                    progress += 1
                }

            }
            if (progress == 0)
            {
                LogIt.warning("No progress")
                giveUp += 1
                if (giveUp == 5)
                {
                    // TODO (doesn't work with complex tx, see top comment)
                    LogIt.severe("No progress resolving wallet incoming/outgoing flows: cost basis will be inaccurate")
                    return
                }
            }
        }

         */
    }

    /** print debugging data to the log */

    fun debugDump()
    {
        val s = StringBuilder()
        synchronized(dataLock) {
            s.append("Wallet " + name + " Balance: Confirmed: " + balanceConfirmed + " Unconfirmed: " + balanceUnconfirmed + "\n")
            s.append("  " + "Unspent:\n")
            for ((k, v) in allTxos)
            {
                s.append("    " + v.amount + " on " + v.addr + " outpoint " + k.toHex() + "\n")
            }

            s.append("  " + "Unconfirmed:\n")

            for ((k, v) in allTxos)
            {
                // Skip if the output was subsequently spent by an unconfirmed tx
                if ((v.spentHeight == -1L) && (v.spendableUnconfirmed > 0) && (v.spentUnconfirmed == false))
                {
                    val tx = v.commitTx
                    s.append("    " + v.amount + " on " + v.addr + " outpoint " + k.toHex() + " prevout " + v.priorOutScript.toString())
                    if (tx != null)
                    {
                        val SperB = CurrencyDecimal(tx.fee) / CurrencyDecimal(tx.size)
                        val feeWarning = if (SperB < BigDecimal.fromDouble(MinFeeSatPerByte)) " !!UNRELAYABLE FEE!! " else ""
                        s.append(" fee: " + tx.fee + " sat/byte: " + bchFormat.format(SperB) + feeWarning + "\n")
                        s.append("      TX: " + tx.idem + " inputs: " + tx.inputs.size + " outputs: " + tx.outputs.size)
                        val txhex = tx.toHex()
                        s.append("      TX hex (sz ${txhex.length}: $txhex")
                    }
                    s.append("\n")
                }
            }
        }

        // Java log line is limited to 1000 chars
        val logLines = s.toString().split("\n")
        for (line in logLines)
        {
            for (chunkedLine in line.chunked(900))  // Log max length is 1000 bytes, this gives room for the log prefix
                LogIt.info(chunkedLine)
        }
    }
}


sealed class WalletStartup
object NEW_WALLET : WalletStartup()
//object LOAD_WALLET : WalletStartup()

/** Helper class that saves/loads data needed by the Bip44Wallet */
class Bip44WalletData() : BCHserializable
{
    var id: String = ""

    var secretWords: String = ""

    //var secret: ByteArray = byteArrayOf()
    var maxAddress: Int = 0

    var chainSelector: ChainSelector = ChainSelector.NEXA

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        return BCHserialized(format) + id + secretWords + BCHserialized.uint32(maxAddress.toLong()) + BCHserialized.uint16(chainSelector.v.toLong())
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        id = stream.deString()
        secretWords = stream.deString()
        maxAddress = stream.deint32()
        try  // TODO remove after a bit
        {
            chainSelector = ChainSelectorFromValue(stream.deuint16().toByte())
        }
        catch (e: Exception)
        {
            // old save format so use default chainselector
        }
        return stream
    }
}

fun GetWalletData(name: String, db: KvpDatabase): Bip44WalletData
{
    //LogIt.info("deserialize " + name)
    val wd: Bip44WalletData = Bip44WalletData()
    wd.BCHdeserialize(BCHserialized(db.get("bip44wallet_" + name), SerializationType.DISK))
    //LogIt.info("done deserialize " + name)
    return wd
}


/** Return the Bip44 address number based on this blockchain */
fun Bip44AddressDerivationByChain(chainSelector: ChainSelector): Long
{
    return when (chainSelector)
    {
        ChainSelector.NEXA -> AddressDerivationKey.NEXA
        ChainSelector.NEXATESTNET -> AddressDerivationKey.NEXA
        ChainSelector.NEXAREGTEST -> AddressDerivationKey.NEXA

        ChainSelector.BCH -> AddressDerivationKey.BCH
        ChainSelector.BCHTESTNET -> AddressDerivationKey.BCH
        ChainSelector.BCHREGTEST -> AddressDerivationKey.BCH
    }
}

/** This wallet uses a single piece of random data, deriving new private keys and addresses using the technique described in [BIP-0032](https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki) with key derivation paths
 *  specified by [BIP-043](https://github.com/bitcoin/bips/blob/master/bip-0043.mediawiki) and [BIP-044](https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki).
 *  In essence, key derivation is:  m/<purpose=44>'/<coinType=0x91 for BCH>'/<account>'/<change=0 or 1>/index
 */
class Bip44Wallet(name: String, chainSelector: ChainSelector, var wdb: KvpDatabase) : CommonWallet(name, chainSelector)
{
    companion object
    {
        val COMMON_IDENTITY_SEED = ""

        /** The seed used to specify an identity used across all web sites where the user didn't choose a unique identity */
        val SECRET_SIZE = 16
        val SEED_SIZE = 64
        val MIN_RESAVE_TIME = 10000
    }

    /** Wallet secret seed */
    public var secretWords: String = ""

    /** User enters this to unlock the wallet */
    var passCode: String = ""

    /** Wallet seed derived from words + password */
    var secret: ByteArray = ByteArray(0) // initialize with a secret of the wrong size so it can't be accidentally used uninitialized // TODO keep this encrypted until needed

    /** No addresses have be calculated beyond this index */
    var maxAddress: Int = 0

    /** when was this wallet last saved */
    @OptIn(kotlin.time.ExperimentalTime::class)
    public var lastSave = TimeSource.Monotonic.markNow()

    var addressDerivationCoin: Long = Bip44AddressDerivationByChain(chainSelector)


    data class HdDerivationPath(val secret: ByteArray?, val purpose: Long, val coinType: Long, val account: Long, val change: Boolean, var index: Int)

    /** This variable captures HD derivation paths that should be monitored and spent but never offered as new payment destinations */
    val retrieveOnlyDerivationPaths = mutableListOf<HdDerivationPath>()

    public override fun toString(): String
    {
        val ret = StringBuilder(super.toString())
        ret.append(", maxAddress=$maxAddress\n")
        return ret.toString()
    }

    init
    {
        walletDb = wdb
    }

    /** Load an existing wallet */
    constructor(wdb: KvpDatabase, wd: Bip44WalletData) : this(wd.id, wd.chainSelector, wdb)
    {
        secretWords = wd.secretWords
        maxAddress = wd.maxAddress
        secret = generateBip39Seed(secretWords, passCode)
        loadWalletTx(wdb)
        // This is a patch that puts all identity addresses into the receiving because that was not happening upon creation
        fillReceivingWithIdentities()
    }

    /** Load an existing wallet */
    constructor(wdb: KvpDatabase, name: String) : this(wdb, GetWalletData(name, wdb))

    /** Create a new wallet with a random secret */
    constructor(wdb: KvpDatabase, name: String, chainSelector: ChainSelector, wop: WalletStartup) : this(name, chainSelector, wdb)
    {
        when (wop)
        {
            is NEW_WALLET ->
            {
                secretWords = GenerateBip39SecretWords(GenerateEntropy(SECRET_SIZE * 8))
                secret = generateBip39Seed(secretWords, passCode)
                fillReceivingWithRetrieveOnly()
                fillReceivingWithIdentities()
                saveBip44Wallet()
            }
            else ->
            {
                throw WalletException("Do not use this constructor unless you are making a new wallet")
            }
        }


        if (DEBUG)
        {
            // LogIt.info(sourceLoc() + " " + name + ": secret words: '" + secretWords + "' secret: " + secret.toHex())
        }
    }

    /** Create a new wallet given secret words */
    constructor(wdb: KvpDatabase, name: String, chainSelector: ChainSelector, secretWordList: String, maxAddr: Int = -1) : this(name, chainSelector, wdb)
    {
        secretWords = secretWordList
        secret = generateBip39Seed(secretWords, passCode)
        if (maxAddr == -1)  // TODO Search for addresses
        {
            maxAddress = 0
        }
        else
        {
            maxAddress = maxAddr
            // TODO add all prior addresses into this wallet and find balances
        }
        fillReceivingWithRetrieveOnly()
        fillReceivingWithIdentities()
        saveBip44Wallet()
    }

    public fun saveBip44Wallet()
    {
        val wd = Bip44WalletData()
        wd.id = name
        wd.secretWords = secretWords
        wd.maxAddress = maxAddress
        wd.chainSelector = chainSelector
        wdb.set("bip44wallet_" + name, wd.BCHserialize(SerializationType.DISK).flatten())  // walletDb must be inited by the app before any wallets are created
    }

    override fun getRetrieveOnlyDestinations(): MutableList<PayDestination>
    {
        val ret = mutableListOf<PayDestination>()
        for (path in retrieveOnlyDerivationPaths)
        {
            for (idx in 0..path.index)
            {
                val secret = path.secret ?: secret  // If the secret isn't specified, then use this account's secret
                val newSecret = libnexa.deriveHd44ChildKey(secret, path.purpose, path.coinType, path.account, path.change, idx)
                val dest = if (chainSelector.hasTemplates)
                    Pay2PubKeyTemplateDestination(chainSelector, UnsecuredSecret(newSecret.first))
                else Pay2PubKeyHashDestination(chainSelector, UnsecuredSecret(newSecret.first))
                LogIt.info(sourceLoc() + " " + name + ": retrieve only: " + dest.address.toString())
                ret.add(dest)
            }
        }
        return ret
    }

    @OptIn(kotlin.time.ExperimentalTime::class)
    override fun save(force: Boolean)
    {
        dataLock.lock {
            if (force || lastSave.elapsedNow().inWholeMilliseconds > MIN_RESAVE_TIME)
            {
                lastSave = TimeSource.Monotonic.markNow()
                saveBip44Wallet()
                super.save(force)
            }
        }
    }

    /** Forget all transaction and blockchain state, and the redo the search for wallet transactions.
     */
    @cli(Display.User, "Forget all transaction and blockchain state, and the redo the search for wallet transactions.")
    override fun rediscover(forgetAddresses: Boolean, noPrehistory: Boolean): Unit
    {
        synchronized(dataLock) {
            if (forgetAddresses) maxAddress = 0
            super.rediscover(forgetAddresses, noPrehistory)
        }
    }

    /** Forget all transaction and blockchain state, regenerate the first N addresses
     */
    public fun rediscoverAddresses(lastAddr: Long): Unit
    {
        LogIt.info(sourceLoc() + ": Generating ${lastAddr} addresses")
        synchronized(dataLock)
        {
            maxAddress = 0
            for (i in 0..lastAddr)
            {
                val dest = generateDestination()
                val destaddr = dest.address
                if (destaddr == null) throw WalletImplementationException("Generated destination needs to be representable as an address")
                receiving[destaddr] = dest
            }
            maxAddress = lastAddr.toInt()
        }
        LogIt.info(sourceLoc() + ": rediscovering tx in blockchain")
        rediscover(false)
    }

    override fun generateDestination(): PayDestination
    {
        val tmp = maxAddress
        maxAddress += 1
        // LogIt.info(sourceLoc() + " " + name + ": secret " + secret.toHex())
        val newSecret = libnexa.deriveHd44ChildKey(secret, AddressDerivationKey.BIP44, addressDerivationCoin, 0, false, tmp)
        val dest = if (chainSelector.hasTemplates)
            Pay2PubKeyTemplateDestination(chainSelector, UnsecuredSecret(newSecret.first))
        else Pay2PubKeyHashDestination(chainSelector, UnsecuredSecret(newSecret.first))  // Note, if multiple destination types are allowed, the wallet load/save routines must be updated
        // LogIt.info(sourceLoc() + " $name: New Destination ${newSecret.second}")
        return dest
    }

    override fun destinationFor(seed: String): PayDestination
    {
        val index = if (seed == COMMON_IDENTITY_SEED) 0  // Common identity uses index 0
            else  // Otherwise create a probabilistically unique identity for this seed
            {
                // To prevent someone from engineering a collision with a particular site, we get some info that is unique (and secret) to this wallet
                val uniquifier = libnexa.deriveHd44ChildKey(secret, AddressDerivationKey.BIP44, AddressDerivationKey.ANY, 0, false, 0x7fffffff)
                // Then we hash that data with the seed string & grab 4 bytes from that to get the unique identity
                val hash = libnexa.sha256(seed.toByteArray() + uniquifier.first)
                hash[0].toPositiveLong() + hash[1].toPositiveLong()*256L + hash[2].toPositiveLong()*256L*256L + (hash[3].toPositiveLong() and 0x7f) * 256L*256L*256L
            }

        return getDestinationAtIndex(index.toInt())
    }

    public fun getDestinationAtIndex(addrIndex: Int): PayDestination
    {
        assert(addrIndex >= 0)
        val privateKey = libnexa.deriveHd44ChildKey(secret, AddressDerivationKey.BIP44, AddressDerivationKey.ANY, 0, false, addrIndex)

        // We should use P2PKH destinations for now because 3rd party (BTC and BCH) message signature creation/verification libraries want P2PKH
        //if (chainSelector.hasTemplates)
        //  return Pay2PubKeyTemplateDestination(chainSelector, UnsecuredSecret(privateKey))
        //else

        return Pay2PubKeyHashDestination(chainSelector, UnsecuredSecret(privateKey.first))
    }


    /** Search a variety of derivation paths for unspent utxos and add them into this wallet if discovered.
     * These utxos are not swept, so may be forgotten if you recreate this wallet from a recovery key.
     */
    @cli(Display.Simple, "Search various derivation paths and add unspent coins into the wallet.  Provide the quantity (and starting point) of accounts and addresses.")
    fun recoverUnspent(wallet: Wallet, numAddrToSearch: Int, numAccountToSearch: Int = 1, startAddrIndex: Int = 0, startAccountIndex: Int = 0): List<Spendable>
    {
        val w = wallet as CommonWallet
        val hdw = wallet as Bip44Wallet

        val ec = w.blockchain.net.getElectrum()

        val ret = mutableListOf<Spendable>()

        for (actI in IntRange(startAccountIndex, startAccountIndex + numAccountToSearch - 1))
        {
            for (index in IntRange(startAddrIndex, startAddrIndex + numAddrToSearch - 1))
            {
                val act = actI.toLong()
                // println("account $act index $index")
                val derivs = listOf(
                  Pair("BIP44 hardened BTC (m/44'/0'/$act'/0/$index)", { idx: Int ->
                      libnexa.deriveHd44ChildKey(hdw.secret, AddressDerivationKey.BIP44, AddressDerivationKey.BTC, act, false, idx)
                  }),
                  Pair("BIP44 hardened BTC change (m/44'/$act'/0'/1/$index)", { idx: Int ->
                      libnexa.deriveHd44ChildKey(hdw.secret, AddressDerivationKey.BIP44, AddressDerivationKey.BTC, act, true, idx)
                  }),
                  Pair("BIP44 hardened common", { idx: Int ->  // Identity derivation
                      libnexa.deriveHd44ChildKey(hdw.secret, AddressDerivationKey.BIP44, AddressDerivationKey.ANY, act, false, idx)
                  }),
                  Pair("BIP44 hardened common", { idx: Int ->  // Identity change
                      libnexa.deriveHd44ChildKey(hdw.secret, AddressDerivationKey.BIP44, AddressDerivationKey.ANY, act, true, idx)
                  }),
                  Pair("BIP44 hardened BCH (m/44'/${AddressDerivationKey.BCH}'/$act'/0/$index)", { idx: Int ->
                      libnexa.deriveHd44ChildKey(hdw.secret, AddressDerivationKey.BIP44, AddressDerivationKey.BCH, act, false, idx)
                  }),
                  Pair("BIP44 hardened BCH change (m/44'/${AddressDerivationKey.BCH}'/$act'/1/$index)", { idx: Int ->
                      libnexa.deriveHd44ChildKey(hdw.secret, AddressDerivationKey.BIP44, AddressDerivationKey.BCH, act, true, idx)
                  }),
                )
                for (derivation in derivs)
                {
                    while (true)
                    {
                        val sk = derivation.second(index).first
                        val dest = Pay2PubKeyHashDestination(w.chainSelector, UnsecuredSecret(sk))
                        try
                        {
                            val utxos: List<Spendable> = ec.listUnspent(dest, 60000)

                            if (utxos.size > 0)
                            {
                                println("Found ${utxos.size} coins at derivation path: ${derivation.first} index: $index address: ${dest.address}")
                                ret += utxos
                            }
                            break
                        }
                        catch (e: ElectrumRequestTimeout)
                        {
                            println("Index $index, electrum timeout for ${dest.address}, retrying")
                        }
                    }
                }
            }
        }

        w.injectUnspent(*ret.toTypedArray())
        return ret
    }
}

