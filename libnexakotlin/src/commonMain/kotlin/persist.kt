package org.nexa.libnexakotlin

import BchDb.BchBlockHeaderTbl
import BchDb.BchDb
import NexaDb.NexaBlockHeaderTbl
import NexaDb.NexaDb
import WalletDb.WalletDb
import app.cash.sqldelight.db.QueryResult
import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.db.SqlSchema
import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign
private val LogIt = GetLog("BU.persist")

val CHAINTIP_MARKER:ByteArray = byteArrayOf(0)

fun BigInteger.toSizedByteArray(sz:Int):ByteArray
{
    val ba = this.toByteArray()
    if (ba.size == sz) return ba
    if (ba.size > sz) throw IllegalArgumentException("Desired byte array of $sz bytes cannot store BigInteger $this")
    val ret = ByteArray(sz)
    ba.copyInto(ret, sz-ba.size)
    return ret
}

interface KvpDatabase
{
    /** update or insert a key value pair into the database */
    fun set(key: ByteArray, value: ByteArray):Boolean

    /** look up the passed key, throwing DataMissingException if it does not exist */
    fun get(key: ByteArray): ByteArray

    /** look up the passed key, returning the value or null if it does not exist */
    fun getOrNull(key: ByteArray): ByteArray?

    /** update or insert a key value pair into the database */
    fun set(key: String, value: ByteArray) = set(key.encodeUtf8(), value)

    /** look up the passed key, returning the value or throwing DataMissingException */
    fun get(key: String): ByteArray = get(key.encodeUtf8())

    /** look up the passed key, returning the value or null if it does not exist */
    fun getOrNull(key: String): ByteArray? = getOrNull(key.encodeUtf8())

    /** delete a record */
    fun delete(key: String) = delete(key.encodeUtf8())

    /** delete a record */
    fun delete(key: ByteArray)
}

// common type for Nexa and Bch header DAOs
interface BlockHeaderDatabase
{
    /** Get a main chain (a chain with the most work) header by its height */
    fun getHeader(height: Long): iBlockHeader?
    /** If a header identified by this hash does not exist, return null */
    fun getHeader(hash: ByteArray): iBlockHeader?
    /** Multiple chains may exist at this height so a list is returned */
    fun getHeadersAtHeight(height: Long): List<iBlockHeader>
    // Multiple chain tips might have the same total work so a list is returned
    fun getMostWorkHeaders(): List<iBlockHeader>
    // If a header identified by this hash does not exist, return null
    fun getHeader(hash: Hash256): iBlockHeader?

    // For quick access, the current main chain tip can be specially stored
    fun setCachedTipHeader(header: iBlockHeader)
    // Get the current main chain tip.
    fun getCachedTipHeader(): iBlockHeader?
    // Add a header into the DB.
    fun insertHeader(header: iBlockHeader)

    // Update or insert multiple headers into the database
    fun upsert(headers: Collection<iBlockHeader>)

    // Add a header into the DB, update the existing header if it does not match what is passed.  This limits database writes.
    fun diffUpsert(header: iBlockHeader)   // Read header, if different or nonexistent insert or update
    // Delete all headers.
    fun clear()

    // close this database, releasing any resources
    fun close()
}

val ZERO_BA32 = ByteArray(32, {0})
fun NexaBlockHeader.Companion.fromDb(db: NexaBlockHeaderTbl):NexaBlockHeader
{
    val ret = NexaBlockHeader()
    if (db.hash.size == 32) ret.assignHashData(Hash256(db.hash))  // otherwise its probably the tip -- anyway its bad and needs recalc.
    ret.hashAncestor = Hash256(db.hashAncestor ?: ZERO_BA32)
    ret.hashPrevBlock = Hash256(db.hashPrevBlock ?: ZERO_BA32)
    ret.diffBits = db.diffBits
    ret.hashMerkleRoot = Hash256(db.hashMerkleRoot ?: ZERO_BA32)
    ret.time = db.time
    ret.height = db.height
    ret.chainWork =  BigInteger.fromByteArray(db.chainWork, Sign.POSITIVE)
    ret.size = db.size
    ret.txCount = db.txCount
    ret.feePoolAmt = db.feePoolAmt
    ret.utxoCommitment = db.utxoCommitment  ?: byteArrayOf()
    ret.minerData = db.minerData ?: byteArrayOf()
    ret.nonce = db.nonce ?: byteArrayOf()
    return ret
}

fun BchBlockHeader.Companion.fromDb(db: BchBlockHeaderTbl):BchBlockHeader
{
    val ret = BchBlockHeader()
    ret.assignHashData(Hash256(db.hash))
    ret.hashAncestor = Hash256(db.hashAncestor ?: ZERO_BA32)
    ret.hashPrevBlock = Hash256(db.hashPrevBlock ?: ZERO_BA32)
    ret.diffBits = db.diffBits
    ret.hashMerkleRoot = Hash256(db.hashMerkleRoot ?: ZERO_BA32)
    ret.time = db.time
    ret.height = db.height
    ret.chainWork =  BigInteger.fromByteArray(db.chainWork, Sign.POSITIVE)
    ret.size = db.size
    ret.txCount = db.txCount
    ret.nonce = db.nonce
    return ret
}

class NexaBlockHeaderDb(val name: String, val sql: SqlDriver): BlockHeaderDatabase
{
    val db = NexaDb(sql)

    protected val EMPTY_BA_STAND_IN = byteArrayOf(0)
    // https://github.com/touchlab/SQLiter/issues/42
    // https://github.com/touchlab/SQLiter/pull/102/files
    fun avoid42(header: iBlockHeader):NexaBlockHeader
    {
        val h = header as NexaBlockHeader
        h.calcHash()  // we need to calc the real hash (if needed) before we put fake values in the empty fields
        val ret = NexaBlockHeader(h)
        if (ret.nonce.size == 0) ret.nonce = EMPTY_BA_STAND_IN
        if (ret.utxoCommitment.size == 0) ret.utxoCommitment = EMPTY_BA_STAND_IN
        if (ret.minerData.size == 0) ret.minerData = EMPTY_BA_STAND_IN
        return ret
    }

    fun undoAvoid42(bh: NexaBlockHeader)
    {
        if (bh.nonce contentEquals EMPTY_BA_STAND_IN) bh.nonce = byteArrayOf()
        if (bh.utxoCommitment contentEquals EMPTY_BA_STAND_IN) bh.utxoCommitment = byteArrayOf()
        if (bh.minerData contentEquals EMPTY_BA_STAND_IN) bh.minerData = byteArrayOf()
    }

    override fun getHeader(height: Long): iBlockHeader?
    {
        val q = db.nexaHeaderDatabaseQueries.getAtHeight(height)
        val item = q.executeAsOneOrNull()
        if (item == null) return null
        val ret = NexaBlockHeader.fromDb(item)
        undoAvoid42(ret)
        return ret
    }

    override fun getHeader(hash: ByteArray): iBlockHeader?
    {
        //LogIt.info("get db ${hash.toHex()}")
        val q = db.nexaHeaderDatabaseQueries.getByHash(hash)
        val item = q.executeAsOneOrNull()
        if (item == null) return null
        val ret = NexaBlockHeader.fromDb(item)
        undoAvoid42(ret)
        return ret
    }

    override fun getHeader(hash: Hash256): iBlockHeader? = getHeader(hash.hash)

    override fun getHeadersAtHeight(height: Long): List<iBlockHeader>
    {
        val q = db.nexaHeaderDatabaseQueries.getAtHeight(height)
        val lst =  q.executeAsList().map { NexaBlockHeader.fromDb(it)}
        lst.forEach { undoAvoid42(it)}
        return lst
    }

    override fun getMostWorkHeaders(): List<iBlockHeader>
    {
        val q = db.nexaHeaderDatabaseQueries.getByMostWork()
        val lst =  q.executeAsList().map { NexaBlockHeader.fromDb(it)}
        lst.forEach { undoAvoid42(it)}
        return lst
    }

    override fun setCachedTipHeader(header: iBlockHeader)
    {
        val n = avoid42(header)
        db.nexaHeaderDatabaseQueries.upsert(CHAINTIP_MARKER, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.hashTxFilter.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.feePoolAmt, n.utxoCommitment, n.minerData, n.nonce, n.hashMerkleRoot.hash, n.txCount)
    }

    override fun getCachedTipHeader(): iBlockHeader?
    {
        val q = db.nexaHeaderDatabaseQueries.getByHash(CHAINTIP_MARKER)
        val item = q.executeAsOneOrNull()
        if (item == null) return null
        val ret = NexaBlockHeader.fromDb(item)
        undoAvoid42(ret)
        return ret
    }

    override fun insertHeader(header: iBlockHeader)
    {
        //LogIt.info("insert db ${(header as NexaBlockHeader).hashData?.toHex() ?: 0} ")
        val n = avoid42(header)
        // do not recalc the hash for n, because avoid42 has to stick fake data in to avoid a bug
        // (hash, hashPrevBlock, diffBits, hashAncestor, hashTxFilter, time, height, chainWork, size, feePoolAmt, utxoCommitment, minerData, nonce, hashMerkleRoot, txCount)
        db.nexaHeaderDatabaseQueries.insert(header.hash.hash, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.hashTxFilter.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.feePoolAmt, n.utxoCommitment, n.minerData, n.nonce, n.hashMerkleRoot.hash, n.txCount)
    }

    override fun upsert(headers: Collection<iBlockHeader>)
    {
        db.nexaHeaderDatabaseQueries.transaction {
            for (h in headers)
            {
                val n = avoid42(h)
                db.nexaHeaderDatabaseQueries.upsert(n.hash.hash, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.hashTxFilter.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.feePoolAmt, n.utxoCommitment, n.minerData, n.nonce, n.hashMerkleRoot.hash, n.txCount)

            }
        }
    }

    override fun diffUpsert(header: iBlockHeader)
    {
        val n = avoid42(header)
        db.nexaHeaderDatabaseQueries.upsert(header.hash.hash, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.hashTxFilter.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.feePoolAmt, n.utxoCommitment, n.minerData, n.nonce, n.hashMerkleRoot.hash, n.txCount)
    }

    override fun clear()
    {
        db.nexaHeaderDatabaseQueries.deleteAll()
    }

    override fun close()
    {
    }

}

class BchBlockHeaderDb(val name: String, val sql: SqlDriver): BlockHeaderDatabase
{
    val db = BchDb(sql)

    protected val EMPTY_BA_STAND_IN = byteArrayOf(0)
    // https://github.com/touchlab/SQLiter/issues/42
    // https://github.com/touchlab/SQLiter/pull/102/files
    fun avoid42(header: iBlockHeader):BchBlockHeader
    {
        val h = header as BchBlockHeader
        return h
        //val ret = BchBlockHeader(h)
        //if (ret.nonce.size == 0) ret.nonce = EMPTY_BA_STAND_IN
        //if (ret.utxoCommitment.size == 0) ret.utxoCommitment = EMPTY_BA_STAND_IN
        //if (ret.minerData.size == 0) ret.minerData = EMPTY_BA_STAND_IN
        // if (ret..size == 0) ret. = EMPTY_BA_STAND_IN
        //return ret
    }

    fun undoAvoid42(bh: BchBlockHeader)
    {
        //if (bh.nonce contentEquals EMPTY_BA_STAND_IN) bh.nonce = byteArrayOf()
        //if (bh.utxoCommitment contentEquals EMPTY_BA_STAND_IN) bh.utxoCommitment = byteArrayOf()
        //if (bh.minerData contentEquals EMPTY_BA_STAND_IN) bh.minerData = byteArrayOf()
    }

    override fun getHeader(height: Long): iBlockHeader?
    {
        val q = db.bchHeaderDatabaseQueries.getAtHeight(height)
        val item = q.executeAsOneOrNull()
        if (item == null) return null
        val ret = BchBlockHeader.fromDb(item)
        undoAvoid42(ret)
        return ret
    }

    override fun getHeader(hash: ByteArray): iBlockHeader?
    {
        val q = db.bchHeaderDatabaseQueries.getByHash(hash)
        val item = q.executeAsOneOrNull()
        if (item == null) return null
        val ret = BchBlockHeader.fromDb(item)
        undoAvoid42(ret)
        return ret
    }

    override fun getHeader(hash: Hash256): iBlockHeader? = getHeader(hash.hash)

    override fun getHeadersAtHeight(height: Long): List<out iBlockHeader>
    {
        val q = db.bchHeaderDatabaseQueries.getAtHeight(height)
        val lst =  q.executeAsList().map { BchBlockHeader.fromDb(it)}
        lst.forEach { undoAvoid42(it)}
        return lst
    }

    override fun getMostWorkHeaders(): List<iBlockHeader>
    {
        val q = db.bchHeaderDatabaseQueries.getByMostWork()
        val lst =  q.executeAsList().map { BchBlockHeader.fromDb(it)}
        lst.forEach { undoAvoid42(it)}
        return lst
    }

    override fun setCachedTipHeader(header: iBlockHeader)
    {
        val n = avoid42(header)
        db.bchHeaderDatabaseQueries.upsert(CHAINTIP_MARKER, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.nonce, n.hashMerkleRoot.hash, n.txCount)
    }

    override fun getCachedTipHeader(): iBlockHeader?
    {
        val q = db.bchHeaderDatabaseQueries.getByHash(CHAINTIP_MARKER)
        val item = q.executeAsOneOrNull()
        if (item == null) return null
        val ret = BchBlockHeader.fromDb(item)
        undoAvoid42(ret)
        return ret
    }

    override fun insertHeader(header: iBlockHeader)
    {
        val n = avoid42(header)
        val tmp = n.calcHash()
        // (hash, hashPrevBlock, diffBits, hashAncestor, hashTxFilter, time, height, chainWork, size, feePoolAmt, utxoCommitment, minerData, nonce, hashMerkleRoot, txCount)
        db.bchHeaderDatabaseQueries.insert(tmp.hash, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.nonce, n.hashMerkleRoot.hash, n.txCount)
    }

    override fun upsert(headers: Collection<iBlockHeader>)
    {
        TODO("Not yet implemented")
    }

    override fun diffUpsert(header: iBlockHeader)
    {
        val n = avoid42(header)
        val tmp = n.calcHash()
        db.bchHeaderDatabaseQueries.upsert(tmp.hash, n.hashPrevBlock.hash, n.diffBits, n.hashAncestor.hash, n.time, n.height, n.chainWork.toSizedByteArray(32), n.size, n.nonce, n.hashMerkleRoot.hash, n.txCount)
    }

    override fun clear()
    {
        db.bchHeaderDatabaseQueries.deleteAll()
    }

    override fun close()
    {
    }

}


class SqldelightKvpDatabase(val name: String, val sql: SqlDriver): KvpDatabase
{
    val db = WalletDb(sql)
    override fun set(key: ByteArray, value: ByteArray): Boolean
    {
        db.kvpDatabaseQueries.upsert(key, value)
        return true
    }

    override fun get(key: ByteArray): ByteArray
    {
        val v = db.kvpDatabaseQueries.get(key).executeAsOneOrNull()
        if (v == null) throw DataMissingException(key.toHex())
        return v.valu
    }

    override fun getOrNull(key: ByteArray): ByteArray?
    {
        val v = db.kvpDatabaseQueries.get(key).executeAsOneOrNull()
        if (v == null) return null
        return v.valu
    }

    override fun delete(key: ByteArray)
    {
        db.kvpDatabaseQueries.delete(key)
    }

}

/*  ROOM db type converters kept here in case they are needed at some point
class Hash256Converters
{
    @TypeConverter
    fun fromByteArray(value: ByteArray?): Hash256?
    {
        return value?.let { Hash256(it) }
    }

    @TypeConverter
    fun toByteArray(bid: Hash256?): ByteArray?
    {
        return bid?.hash
    }
}

class BigIntegerConverters
{
    @TypeConverter
    fun fromByteArray(value: ByteArray?): BigInteger?
    {
        return value?.let {
            var ret = 0.toBigInteger()
            for (b in it)
            {
                ret = ret.shiftLeft(8)
                ret += b.toPositiveInt().toBigInteger()
            }
            ret
        }
    }

    @TypeConverter
    fun toByteArray(bid: BigInteger?): ByteArray?
    {
        if (bid == null) return null
        var ret = ByteArray(32)
        var value: BigInteger = bid
        for (i in 1..32)  // By converting by hand we are sure that the byte order means that lexicographical compare is equivalent to numerical compare
        {
            ret[32 - i] = value.and(255.toBigInteger()).toByte()
            value = value.shiftRight(8)

        }
        return ret
    }
}

 */

expect fun createDbDriver(dbname: String, schema: SqlSchema<QueryResult.Value<Unit>>): SqlDriver
//expect fun createNexaDbDriver(dbname: String): SqlDriver
//expect fun createKvpDbDriver(dbname: String): SqlDriver
fun openNexaDB(name: String): BlockHeaderDatabase?
{
    val drvr = createDbDriver(name + ".db", NexaDb.Schema)
    return  NexaBlockHeaderDb(name, drvr)
}
fun openBchDB(name: String): BlockHeaderDatabase?
{
    val drvr = createDbDriver(name + ".db", BchDb.Schema)
    return  BchBlockHeaderDb(name, drvr)
}

fun openKvpDB(name: String): KvpDatabase?
{
    val drvr = createDbDriver(name + ".db", WalletDb.Schema)
    return  SqldelightKvpDatabase(name, drvr)
}
