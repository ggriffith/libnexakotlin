package org.nexa.libnexakotlin

import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign
import com.ionspin.kotlin.bignum.integer.toBigInteger
import kotlin.concurrent.Volatile

interface iBlockHeader: BCHserializable
{
    /** Return this block's hash.  Uses a cached value if one exists.  If you change the block be sure to call calcHash() or invalidateHash() to update this value either greedily or lazily respectively */
    @cli(Display.Simple, "block hash")
    val hash: Hash256

    /** Force recalculation of hash. To access the hash just use #hash, to force recalc call "changed()" */
    abstract fun calcHash(): Hash256

    /** Communicate that this object has been changed, so recalculation of cached data is needed */
    abstract fun changed()

    /** run checks (such as verifying POW claim) & return true if the block header is consistent */
    abstract fun validate(cs: ChainSelector): Boolean

    @cli(Display.Simple, "previous block hash")
    var hashPrevBlock: Hash256

    @cli(Display.Simple, "difficulty in 'bits' representation")
    var diffBits: Long

    @cli(Display.Simple, "ancestor")
    var hashAncestor: Hash256 // For more rapid rewind, an ancestor is stored when this block is saved

    @cli(Display.Simple, "merkle root hash")
    var hashMerkleRoot: Hash256

    @cli(Display.Simple, "block timestamp")
    var time: Long

    @cli(Display.Simple, "block height")
    var height: Long

    @cli(Display.Simple, "cumulative work in the chain")
    var chainWork: BigInteger

    @cli(Display.Simple, "block size in bytes")
    var size: Long

    @cli(Display.Simple, "number of transactions in block")
    var txCount: Long

    @cli(Display.Simple, "expected # of hashes to solve this block")
    val work: BigInteger
        get()
        {
            val work = libnexa.getWorkFromDifficultyBits(diffBits)
            return BigInteger.fromByteArray(work, Sign.POSITIVE)
        }
}

abstract class CommonBlockHeader: iBlockHeader
{
    constructor() : super()
    constructor(h: CommonBlockHeader):this()
    {
        hashData = h.hashData
        hashPrevBlock = h.hashPrevBlock
        diffBits = h.diffBits
        hashAncestor = h.hashAncestor
        hashMerkleRoot = h.hashMerkleRoot
        time = h.time
        height = h.height
        chainWork = h.chainWork
        size = h.size
        txCount = h.txCount
    }

    //protected var hexHash = String()
    //protected
    var hashData: Hash256? = null

    /** Return this block's hash.  Uses a cached value if one exists.  If you change the block be sure to call calcHash() or invalidateHash() to update this value either greedily or lazily respectively */
    @cli(Display.Simple, "block hash")
    override val hash: Hash256
        get()
        {
            var temp = hashData
            if (temp == null) // ((temp == null) || (temp == Hash256()))
            {
                temp = calcHash()
                hashData = temp
            }
            return temp
        }

    /** Communicate that this object has been changed, so recalculation of cached data is needed */
    override fun changed()
    {
        hashData = null
    }

    // For efficiency only, this manually assigns the hash rather than recalculating it.  Used in disk load functions
    fun assignHashData(h: Hash256) { hashData = h }


    @cli(Display.Simple, "previous block hash")
    override var hashPrevBlock = Hash256()

    @cli(Display.Simple, "difficulty in 'bits' representation")
    override var diffBits: Long = 0L // nBits

    @cli(Display.Simple, "ancestor")
    override open var hashAncestor = Hash256() // For more rapid rewind, an ancestor is stored when this block is saved

    @cli(Display.Simple, "merkle root hash")
    override var hashMerkleRoot = Hash256()

    @cli(Display.Simple, "block timestamp")
    override var time: Long = 0L

    @cli(Display.Simple, "block height")
    override var height: Long = -1L

    @cli(Display.Simple, "cumulative work in the chain")
    override var chainWork: BigInteger = 0.toBigInteger()

    @cli(Display.Simple, "block size in bytes")
    override var size: Long = -1L

    @cli(Display.Simple, "number of transactions in block")
    override var txCount: Long = 0L
}

interface iMerkleBlock: iBlockHeader
{
    abstract val txHashes: MutableSet<Hash256>
    abstract val txes: MutableList<out iTransaction>
    @cli(Display.Simple, "Returns true if all transactions defined in this merkle block have arrived (via calls to txArrived())")
    abstract fun complete(): Boolean

    fun txArrived(tx: iTransaction): Boolean

}

/** Reference to a UTXO */
@cli(Display.Simple, "UTXO reference")
interface iTxOutpoint : BCHserializable
{
    @cli(Display.Simple, "return the serialized hex representation")
    abstract fun toHex(): String
}

/** defines new UTXOs */
interface iTxOutput : BCHserializable
{
    var amount: Long //!< Amount in satoshis assigned to this output
    var script: SatoshiScript  // !< Constraints to spend this output
}

/** defines what UTXOs are being spent and proves ability to spend */
interface iTxInput : BCHserializable
{
    var spendable: Spendable
    var script: SatoshiScript

    /** Make a shallow copy of this object */
    fun copy(): iTxInput
}

interface iTransaction: BCHserializable
{
    @cli(Display.Simple, "Which blockchain is this transaction for")
    /** Which blockchain is this transaction for */
    val chainSelector: ChainSelector

    /** inputs to this transaction */
    @cli(Display.Simple, "transaction inputs")
    val inputs:MutableList<out iTxInput>

    /** Add an input to this transaction (at the end) */
    fun add(input: iTxInput):iTransaction

    /** Overwrite all inputs with the provided list */
    fun setInputs(inputs: MutableList<out iTxInput>):iTransaction

    /** outputs of this transaction */
    @cli(Display.Simple, "transaction outputs")
    val outputs: MutableList<out iTxOutput>

    /** Add an output to this transaction (at the end) */
    fun add(output: iTxOutput):iTransaction

    /** Overwrite all outputs with the provided list */
    fun setOutputs(outputs: MutableList<out iTxOutput>):iTransaction

    /** transaction outpoints (handles to the UTXO entries the outputs will generate) */
    @cli(Display.Dev, "transaction outpoints")
    val outpoints: Array<out iTxOutpoint>

    /** transaction version */
    @cli(Display.Simple, "version")
    var version: Int

    /** transaction lock time */
    @cli(Display.Simple, "transaction lock time")
    var lockTime: Long

    /** transaction size in bytes */
    @cli(Display.Simple, "transaction size in bytes")
    val size: Long

    /** Return this transaction's idem. If you change the transaction be sure to call changed() to update this value.
     * Returns the id for blockchains that do not have an idem. */
    @cli(Display.Simple, "transaction idem")
    val idem: Hash256

    /** Return this transaction's id. If you change the transaction be sure to call changed() to update this value */
    @cli(Display.Simple, "transaction id")
    val id: Hash256

    /** Communicate that this transaction has been changed, so recalculation of id and idem are needed */
    abstract fun changed()

    /** return true if this is a coinbase transaction */
    @cli(Display.Simple, "return true if this is a coinbase transaction")
    abstract fun isCoinbase(): Boolean

    /** Return true if this is an ownership challenge transaction.  This means that this transaction is invalid and so can never be actually committed to the blockchain. */
    @cli(Display.Simple, "Return true if this is an ownership challenge transaction.  This means that this transaction is invalid and so can never be actually committed to the blockchain.")
    abstract fun isOwnershipChallenge(): Boolean

    /** Return the serialized hex representation */
    @cli(Display.Simple, "Return the serialized hex representation")
    abstract fun toHex(): String

    /** Serialize this transaction into a byte array (using network serialization) */
    fun toByteArray(): ByteArray = BCHserialize(SerializationType.NETWORK).flatten()

    /** What this transaction will be paying in transaction fees (might be calculated each time) */
    @cli(Display.Simple, "transaction fee")
    abstract val fee: Long

    /** Transaction fee rate in satoshi/byte */
    @cli(Display.Simple, "transaction fee rate in satoshi/byte")
    val feeRate: Double
        get()
        {
            return fee.toDouble() / size.toDouble()
        }

    /** Total quantity of satoshis spent (input into this transaction), including grouped satoshis */
    val inputTotal: Long
        get()
        {
            var total = 0L
            for (i in inputs) total += i.spendable.amount
            return total
        }

    /** Total quantity of satoshis sent (output from this transaction), including grouped satoshis */
    val outputTotal: Long
        get()
        {
            var total = 0L
            for (i in outputs) total += i.amount
            return total
        }

    /** Return the sighash that allows new inputs and/or outputs to be appended to the transaction (but commits to all existing inputs and outputs) */
    abstract fun appendableSighash(extendInputs:Boolean = true, extendOutputs:Boolean=true): ByteArray

    /** Dump this transaction to stdout */
    abstract fun debugDump()

}

class Spendable(val chainSelector: ChainSelector) : BCHserializable
{
    val SPENDABLE_ID = 0x58581.toInt()
    var secret: Secret? = null
    var outpoint:iTxOutpoint? = null
    var priorOutScript = SatoshiScript(chainSelector)   // The script that constrains spending these coins
    var addr: PayAddress? = null  // We could get this from the priorOutScript, but for ease of use its replicated here.  This could be null, post initialization, if priorOutscript is anyone-can-spend.
    var amount: Long = -1
    var redeemScript = SatoshiScript(chainSelector) //ByteArray( 0)

    val prevout:iTxOutput
        get() = txOutputFor(chainSelector, amount, priorOutScript)

    val groupInfo: GroupInfo?
        get() = priorOutScript.groupInfo(amount)

    // TODO: secret and addr could/should be replaced by a PayDestination object in
    // BCHspendable.  This is transitional
    var backingPayDestination: PayDestination? = null  // RAM only
    val payDestination: PayDestination?
        get()
        {
            // P2SH multisig is an interesting problem here.  The P2SH address will depend on external data (the pubkeys of cooperating wallets)
            // so the wallet cannot determine if an address is spendable by this wallet without knowing cooperating wallets' pubkeys.

            // Allow override
            if (backingPayDestination != null) return backingPayDestination
            // Otherwise try to figure out the script type to construct the appropriate destination
            // We can short-cut this if we already know the address
            var a = addr
            if (a == null)
            {
                a = priorOutScript.address  // We can attempt to recognize the prior out script.  In this case we can extract an address from it.
            }

            if (a != null)  // Now use that address to determine the script type to create a payment destination
            {
                val sec = secret ?: return null // Can't build a payment if don't know the secret
                if (a.type == PayAddressType.P2PKH)
                {
                    backingPayDestination = Pay2PubKeyHashDestination(chainSelector, sec)
                    return backingPayDestination
                }
                if (a.type == PayAddressType.P2PKT)
                {
                    backingPayDestination = Pay2PubKeyTemplateDestination(chainSelector, sec)
                    return backingPayDestination
                }

                // We can't construct the other types because we don't know info (like the redeem script)
                // We could guess the redeem script if there are P2SH redeem script types.  One common type that would be useful is to wrap every P2PKH into P2SH
                // to increase payment anonymity for those using p2sh (and make the UTXO set a tiny bit smaller per UTXO)
            }
            return null
        }

    var commitHeight: Long = -1
    var commitBlockHash: Guid = Guid()
    var commitTxIdem: Guid = Guid()
    var commitTx: iTransaction? = null

    var spentHeight: Long = -1
    var spentBlockHash: Guid = Guid()
    var spentTxHash: Guid = Guid()
    var spentUnconfirmed: Boolean = false  // True if an unconfirmed spend exists. False if spend tx exists and is confirmed or if spend tx does not exist.
    var spendableUnconfirmed: Long = 0 // > 0 if this UTXO is unconfirmed.  Number indicates the length of the longest spend chain

    var reserved: Long = 0 //*< Has an in-progress (being readied by this wallet) transaction used this?  0 means no, > 0 means yes.  RAM ONLY

    /** Return true if this txo is unspent */
    val isUnspent: Boolean
        get() {
            return ((spentUnconfirmed == false)&&(spentHeight==-1L))
        }

    constructor(chainSelector: ChainSelector, out_point: iTxOutpoint, amount: Long) : this(chainSelector)
    {
        // Throw something if the wrong outpoint type is given
        if (chainSelector.isNexaFamily)
        {
            (out_point as NexaTxOutpoint)
        }
        if (chainSelector.isBchFamily)
        {
            (out_point as BchTxOutpoint)
        }

        outpoint = out_point
        this.amount = amount
    }

    // Serialization
    constructor(chainSelector: ChainSelector, data: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(data)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        if ((format == SerializationType.NETWORK) || (format == SerializationType.HASH))
        {
            return outpoint!!.BCHserialize(format)
        }
        else if (format == SerializationType.DISK)
        {
            val sec = secret?.getSecret() ?: byteArrayOf()
            val a = addr ?: PayAddress(chainSelector, PayAddressType.NONE, byteArrayOf())
            val ret = BCHserialized(format) + SPENDABLE_ID + variableSized(sec) + outpoint!!.BCHserialize(format)
            ret.add(priorOutScript)
            ret.addInt64(amount)
            ret.add(redeemScript)
            ret.addInt64(commitHeight)
            ret.add(commitBlockHash)
            ret.add(commitTxIdem)
            /* commitTx +  since the commitTx contains this BCHinput, that would recurse -- when loading you need to restore this from an alternate source using the commitTxIdem */
            ret.addUint64(spentHeight).add(spentBlockHash)
            ret.add(spentTxHash)
            ret.add(spentUnconfirmed)
            ret.addUint64(spendableUnconfirmed)
            assert(ret.format == format)
            ret.add(a.BCHserialize(format))
            return ret
        }
        else
        {
            throw NotImplementedError()
        }
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        if (stream.format == SerializationType.NETWORK)
        {
            outpoint = outpointFor(chainSelector, stream)
        }
        else if (stream.format == SerializationType.DISK)
        {
            val id = stream.deint32()
            if (id != SPENDABLE_ID)
            {
                // LogIt.info("deserialization corruption!")
                throw DeserializationException("Spendable identifier incorrect")
            }
            val secBytes = stream.deByteArray()
            if (secBytes.size != 0) secret = UnsecuredSecret(secBytes)
            else secret = null
            outpoint = outpointFor(chainSelector, stream)
            priorOutScript.BCHdeserialize(stream)
            amount = stream.deuint64()
            redeemScript.BCHdeserialize(stream)
            commitHeight = stream.deuint64()
            commitBlockHash.BCHdeserialize(stream)
            commitTxIdem.BCHdeserialize(stream)
            spentHeight = stream.deuint64()
            spentBlockHash.BCHdeserialize(stream)
            spentTxHash.BCHdeserialize(stream)
            spentUnconfirmed = stream.deboolean()
            spendableUnconfirmed = stream.deuint64()
            val p = PayAddress(stream)
            addr = if (p.type != PayAddressType.NONE) p else null
        }
        else
        {
            throw NotImplementedError()
        }
        return stream
    }

    override fun toString(): String
    {
        return amount.toString() + " from UTXO " + outpoint.toString()
    }

    @cli(Display.Simple, "Return group token information, or null if not grouped")
    fun groupInfo(): GroupInfo? = priorOutScript.groupInfo(amount)
}


interface iBlock: iBlockHeader
{
    /** list of transactions in the block */
    val txes: List<iTransaction>
}

// common type for Nexa and Bch header DAOs
interface BlockHeaderPersist
{
    /** Get a main chain (a chain with the most work) header by its height */
    fun getHeader(height: Long): iBlockHeader?
    /** If a header identified by this hash does not exist, return null */
    fun getHeader(hash: ByteArray): iBlockHeader?
    /** Multiple chains may exist at this height so a list is returned */
    fun getHeadersAtHeight(height: Long): List<iBlockHeader>
    // Multiple chain tips might have the same total work so a list is returned
    fun getMostWorkHeaders(): List<iBlockHeader>
    // If a header identified by this hash does not exist, return null
    fun getHeader(hash: Hash256): iBlockHeader?

    // For quick access, the current main chain tip can be specially stored
    fun setCachedTipHeader(header: iBlockHeader)
    // Get the current main chain tip.
    fun getCachedTipHeader(): iBlockHeader?
    // Add a header into the DB.
    fun insertHeader(header: iBlockHeader)
    // Add a header into the DB, update the existing header if it does not match what is passed.  This limits database writes.
    fun diffUpsert(header: iBlockHeader)   // Read header, if different or nonexistent insert or update
    // Delete all headers.
    fun clear()
}