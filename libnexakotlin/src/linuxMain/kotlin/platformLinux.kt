package org.nexa.libnexakotlin

import io.ktor.http.encodeURLParameter
import io.ktor.http.encodeURLPath
import kotlinx.datetime.*
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1


/*  Defined in nativeMain
actual val lineSeparator: String = "\n"

actual fun show(p: KProperty1<*, *>, obj: Any): String
{
    TODO()
}
actual fun showProperties(obj: Any, cls: KClass<*>, level: Display, style: DisplayStyles, depth: Int, indent: Int): String
{
    TODO()
}

 */


class NativeLinuxLogging(override val module: String):iLogging
{
    override fun error(s: String)
    {
       println("[$module] ERROR:    $s")
    }

    override fun warning(s:String)
    {
        println("[$module] WARNING: $s")
    }
    override fun info(s:String)
    {
        println("[$module] INFO:    $s")
    }
}

/** Returns seconds since the epoch */
actual fun epochSeconds(): Long = Clock.System.now().epochSeconds

/** Returns milliseconds since the epoch */
actual fun epochMilliSeconds(): Long = Clock.System.now().toEpochMilliseconds()

actual fun String.urlEncode(): String
{
    return encodeURLParameter(true)
}

actual fun sourceLoc(): String
{
    return ""
}

actual fun GetLog(module: String): iLogging = NativeLinuxLogging(module)

actual fun generateBip39Seed(wordseed: String, passphrase: String, size: Int): ByteArray
{
    val salt = ("mnemonic".toCharArray() + passphrase.toCharArray())
    return salt.map { it.code.toByte() }.toByteArray()
        .let { saltb ->
            PBEKeySpecCommon(wordseed.toCharArray(), saltb, 2048, 512).let { pbeKeySpec ->
                SecretKeyFactoryCommon.getInstance("PBKDF2WithHmacSHA512", FallbackProvider()).let { keyFactory ->
                    keyFactory.generateSecret(pbeKeySpec).encoded.also {
                        pbeKeySpec.clearPassword()
                    }
                }
            }
        }
}

/** Return false if this node has no internet connection, or true if it does or null if you can't tell */
actual fun iHaveInternet(): Boolean?
{
    return null
}

private var isInitialized = false
