
import kotlin.test.*
import kotlinx.coroutines.*
import org.nexa.libnexakotlin.*
import org.nexa.threads.*
import com.ionspin.kotlin.bignum.integer.BigInteger
import io.ktor.http.Url
import io.ktor.http.fullPath

import kotlin.coroutines.CoroutineContext

val Log = GetLog("utilityTests.kt")

// assert is sometimes compiled "off" but in tests we never want to skip the checks so create a helper function
fun check(v: Boolean?)
{
    if (v==null) throw AssertionError("check failed")
    if (!v) throw AssertionError("check failed")
}

class UtilityTests
{
    companion object {
        init {
            initializeLibNexa()
        }
    }

    @Test fun date()
    {
        val s = epochToDate(1692217522)
        println(s)
    }

    @Test fun haveInternet()  // You can manually check by turning off the internet on your machine
    {
        val ret = iHaveInternet()
        if (ret == true) println("I have internet")
        else if (ret == false) println("I do not have internet")
        else println("I don't know about internet connectivity")

        // But we will assume for this test that its on
        check (ret == true || ret == null)
    }

    @Test
    fun works()
    {
        Log.error("try error log")
        check(1 == 1)
    }

    @Test
    fun testCodec()
    {
        Log.error("testCodec start")
        var data = byteArrayOf(1,2,3,4,5,6,7,8,9,10, 0, -1, 127, -127 )
        var str = Codec.encode64(data)
        Log.error(str)
        var data2 = Codec.decode64(str)
        check(data contentEquals data2)
        Log.error("testCodec complete")
    }

    @Test
    fun testSerialization()
    {
        Log.error("testSerialization start")
        val bser = BCHserialized(SerializationType.NETWORK)
        bser.add("this is a test")
        bser.flatten()
        var s = bser.deString()
        check(s == "this is a test")

        val u = Url("test:abcedfghij")
        check(u.protocol.name == "test")
        check(u.pathSegments[0] == "abcedfghij")
        check(u.fullPath == "/abcedfghij")
        Log.error("testSerialization complete")
    }

    /*
    @Test
    fun testCoCond()
    {
        val coCtxt: CoroutineContext = Executors.newFixedThreadPool(2).asCoroutineDispatcher()
        val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)

        runBlocking {
            var c1 = CoCond<Nothing?>(coScope)
            var v = 1;
            val cor = GlobalScope.launch { c1.yield(); v = 3 }
            c1.wake(null)
            cor.join()
            Assert.assertEquals(v,3)
        }

        runBlocking {

            var c1 = CoCond<Int>(coScope)
            var v = 1
            val cor = GlobalScope.launch { v = c1.yield()!!; }
            //val cor2 = GlobalScope.launch { v = c1.yield()!!; }
            c1.wake(3)
            //c1.wake(4)
            cor.join()
            //cor2.join()
            check(v == 3 || v == 4)
        }
    }

     */

    @Test
    fun testSynchronized()
    {
        Log.error("testSynchronized start")
        val tmp = Mutex()
        tmp.synchronized {
            Log.error("testSynchronized")
        }
        Log.error("testSynchronized complete")

    }

    /*
    @Test
    fun testConverters()
    {
        // Test big integer conversion
        val bic = BigIntegerConverters()
        val test = 12345678.toBigInteger()
        Assert.assertEquals(bic.fromByteArray(bic.toByteArray(test)), test)

        val rnd = Random()
        for (i in 1..1000)
        {
            val bi = BigInteger(256, rnd)
            Assert.assertEquals(bic.fromByteArray(bic.toByteArray(bi)), bi)
        }

        // Test Hash256 converter
        val hac = Hash256Converters()
        for (i:Int in 1..1000)
        {
            val b = byteArrayOf(i.and(255).toByte(), ((i shr 8) and 255).toByte(), ((i shr 16) and 255).toByte())
            val v = Hash256(libnexa.sha256(b))
            Assert.assertEquals(hac.fromByteArray(hac.toByteArray(v)), v)
        }
    }
    */

    @Test
    fun testFormatter()
    {
        Log.error("testFormatter start")
        //assertEquals(mBchFormat.format(CurrencyDecimal("12345.67891")), "12,345.67891")

        // TODO native platform formatters are not handling commas, and significant digits
        var tmp = mBchFormat.format(CurrencyDecimal("12345.67891"))
        assert (tmp == "12,345.67891" || tmp == "12345.67891")

        tmp = nexFormat.format(CurrencyDecimal("12345.678"))
        //println(tmp)
        assert (tmp == "12,345.68" || tmp == "12345.68")

        tmp = nexFormat.format(CurrencyDecimal("12345.6"))
        //println(tmp)
        assert (tmp == "12,345.60" || tmp == "12345.6")
        Log.error("testFormatter complete")
    }

    @Test
    fun testCli()
    {
        Log.error("testCli start")
        val tx=NexaTransaction(ChainSelector.NEXA)
        cliDump(tx)
        Log.error("testCli completed")
    }


    @Test
    fun testResolveDomain()
    {
        Log.error("testResolveDomain start")
        val data = resolveDomain("127.0.0.1")
        println(data.map { it.toHex()})
        val data1 = resolveDomain("www.google.com")
        println(data1.map { it.toHex()})
        Log.error("testResolveDomain completed")
    }

    @Test fun testKvpDb()
    {
        Log.error("testKvpDb start")
        // Since we cant initialize the android application context because that is platform specific,
        // we can't use any databases so have to just avoid this test
        val kv = try
        {
            openKvpDB("kvpunittest.db") ?: throw Exception("Can't open database")
        }
        catch (e: org.nexa.libnexakotlin.LibNexaException)
        {
            if (e.message?.contains("with the Android application context") ?: false) return
            else throw e
        }

        try
        {
            for (i in 1 until 1000) kv.set(byteArrayOf(i.toByte(), (i / 256).toByte()), "test $i".encodeUtf8())
        }
        catch(e:Exception)
        {
            println(e)
            e.printStackTrace()
        }

        try
        {
            for (i in 1 until 1000)
            {
                val v = kv.get(byteArrayOf(i.toByte(), (i / 256).toByte())).decodeUtf8()
                val (s, n) = v.split(' ')
                check(s == "test")
                check(n == i.toString())
            }
        }
        catch(e:Exception)
        {
            println(e)
            e.printStackTrace()
        }
        Log.error("testKvpDb completed")
    }

    @Test fun testNexaHeadersDb()
    {
        // Since we cant initialize the android application context because that is platform specific,
        // we can't use any databases so have to just avoid this test
        val hdb = try
        {
            openNexaDB("nexaunittest.db") ?: throw Exception("Can't open database")
        }
        catch (e: org.nexa.libnexakotlin.LibNexaException)
        {
            if (e.message?.contains("with the Android application context") ?: false) return
            else throw e
        }

        hdb.clear()  // clear any data from the prior run
        if (true)
        {
            var bhl = hdb.getMostWorkHeaders()
            check(bhl.size == 0)  // Check that clear cleared and that getMostWorkHeaders returns an empty list if no headers
        }
        try
        {
            // Fill it up with some fake data
            for (i in 1 until 100)
            {
                println(i)
                val bh = NexaBlockHeader()
                bh.nonce = byteArrayOf(1,2)
                bh.hashMerkleRoot = Hash256(libnexa.hash256(byteArrayOf(i.toByte())))
                bh.height = i.toLong()
                bh.chainWork = BigInteger.fromLong(i*1000L)
                bh.changed()
                hdb.insertHeader(bh)
                // Check that getMostWorkHeaders returns the right one
                var bhl = hdb.getMostWorkHeaders()
                check(bhl.size == 1)
                val mw = bhl[0]
                if (mw.height != i.toLong())
                {
                    println("ERROR")
                }
            }
        }
        catch(e:Exception)
        {
            println("exception storing headers")
            println(e)
            e.printStackTrace()
            throw e
        }

        try
        {
            // Check that the fake headers I put in come out
            for (i in 1L until 100L)
            {
                println(i)
                val bh = hdb.getHeader(i)
                val nbh = bh as NexaBlockHeader
                check(nbh.height == i)
                check(nbh.hashMerkleRoot == Hash256(libnexa.hash256(byteArrayOf(i.toByte()))))
                check(nbh.nonce contentEquals byteArrayOf(1,2))
                //check(nbh.utxoCommitment.size == 0)
                check(nbh.minerData.size == 0)
            }
        }
        catch(e:Exception)
        {
            println("exception reading headers")
            println(e)
            e.printStackTrace()
            throw e
        }

        try {
            var bhl = hdb.getMostWorkHeaders()
            for (b in bhl)
            {
                // If your DB chainwork comparison is lexagraphic and not zero-padding the number, your best height will be 65
                check(b.height == 99.toLong())
            }

            // put in a competing most-work block
            val bh = NexaBlockHeader()
            bh.nonce = byteArrayOf(1,3)
            bh.hashMerkleRoot = Hash256(libnexa.hash256(byteArrayOf(0)))
            bh.height = 99L
            bh.chainWork = BigInteger.fromLong(99*1000L)
            bh.changed()
            hdb.diffUpsert(bh)

            // Check that a tie for the most work properly returns 2 tips
            bhl = hdb.getMostWorkHeaders()
            check(bhl.size == 2)

        }
        catch(e:Exception)
        {
            println("exception testing getMostWorkHeaders")
            println(e)
            e.printStackTrace()
            throw e
        }

    }


    @Test fun testBchHeadersDb()
    {
        // Since we cant initialize the android application context because that is platform specific,
        // we can't use any databases so have to just avoid this test
        val hdb = try
        {
            openBchDB("bchunittest.db") ?: throw Exception("Can't open database")
        }
        catch (e: org.nexa.libnexakotlin.LibNexaException)
        {
            if (e.message?.contains("with the Android application context") ?: false) return
            else throw e
        }

        hdb.clear()  // clear any data from the prior run
        if (true)
        {
            var bhl = hdb.getMostWorkHeaders()
            check(bhl.size == 0)  // Check that clear cleared and that getMostWorkHeaders returns an empty list if no headers
        }
        try
        {
            // Fill it up with some fake data
            for (i in 1 until 100)
            {
                println(i)
                val bh = BchBlockHeader()
                bh.nonce = 1
                bh.hashMerkleRoot = Hash256(libnexa.hash256(byteArrayOf(i.toByte())))
                bh.height = i.toLong()
                bh.chainWork = BigInteger.fromLong(i*1000L)
                bh.changed()
                hdb.insertHeader(bh)
                // Check that getMostWorkHeaders returns the right one
                var bhl = hdb.getMostWorkHeaders()
                check(bhl.size == 1)
                val mw = bhl[0]
                if (mw.height != i.toLong())
                {
                    println("ERROR")
                }

            }
        }
        catch(e:Exception)
        {
            println("exception storing headers")
            println(e)
            e.printStackTrace()
            throw e
        }

        try
        {
            // Check that the fake headers I put in come out
            for (i in 1L until 100L)
            {
                println(i)
                val bh = hdb.getHeader(i)
                val nbh = bh as BchBlockHeader
                check(nbh.height == i)
                check(nbh.hashMerkleRoot == Hash256(libnexa.hash256(byteArrayOf(i.toByte()))))
                check(nbh.nonce == 1L)
            }
        }
        catch(e:Exception)
        {
            println("exception reading headers")
            println(e)
            e.printStackTrace()
            throw e
        }

        try {
            var bhl = hdb.getMostWorkHeaders()
            for (b in bhl)
            {
                // If your DB chainwork comparison is lexagraphic and not zero-padding the number, your best height will be 65
                check(b.height == 99.toLong())
            }

            // put in a competing most-work block
            val bh = BchBlockHeader()
            bh.nonce = 2
            bh.hashMerkleRoot = Hash256(libnexa.hash256(byteArrayOf(0)))
            bh.height = 99L
            bh.chainWork = BigInteger.fromLong(99*1000L)
            bh.changed()
            hdb.diffUpsert(bh)

            // Check that a tie for the most work properly returns 2 tips
            bhl = hdb.getMostWorkHeaders()
            check(bhl.size == 2)

        }
        catch(e:Exception)
        {
            println("exception testing getMostWorkHeaders")
            println(e)
            e.printStackTrace()
            throw e
        }

    }

    @Test
    fun docExample()
    {
        // This is the Nexa genesis block
        val hdr = blockFromHex(ChainSelector.NEXA, "00000000000000000000000000000000000000000000000000000000000000000000011e0000000000000000000000000000000000000000000000000000000000000000cdafb522e41f4b94618c8ee546f0fab8aaae7057f80e4730ea32df145dec73910000000000000000000000000000000000000000000000000000000000000000c0b2b16200ffffff00000000000000000000000000000000000000000000000000000000007201000000000000010000000403001700010000020000000000000000000100000000000000000000a06a00023b1c4c99526575746572733a204a6170616e20504d204b697368696461206261636b7320424f4a20756c7472612d6561737920706f6c696379207768696c652079656e20776f7272696573206d6f756e74204254433a3734313731313a3030303030303030303030303030303030303037356634626330386531643738613361623361663832373464313333333463306163326465323533303937363800000000")
        println("Height ${hdr.height} Size ${hdr.size} Chain Work ${hdr.chainWork}")
        val tx = hdr.txes[0]
        println("Tx 0 inputs: ${tx.inputs.size}")
        println("Tx 0 outputs: ${tx.outputs.size}")

        // I'm cheating here a little because I know this is the genesis block so I know that the last output
        // of the last transaction is text within an OP_RETURN script.
        val output0Asm = tx.outputs.last().script.toAsm(" ")
        println("tx 0 output 0: $output0Asm")
        val stmts = tx.outputs.last().script.parsed()
        println("\nNexa genesis block says:" + OP.parse(stmts.last()).data!!.decodeUtf8())

        // Create a regtest address object
        val addr = PayAddress("nexareg:nqtsq5g53q3sqyhhp45a86792a7wkkm3gyw9gylhp7kr703l")
    }
}