#!/usr/bin/env bash
# Try: ANDROID_NDK=/home/stone/Android/Sdk/ndk/21.4.7075529 ARCH=arm64-v8a TOOLCHAIN=linux-x86_64
set -e

echo "build-linux.sh: Reproduce this build on the command line via:"
echo "ARCH=$ARCH TOOLCHAIN=$TOOLCHAIN TARGET=$TARGET" ./build-linux.sh

echo "CXX=$CXX CC=$CC"

[[ -z "$ARCH" ]] && echo "Please set the ARCH variable" && exit 1
[[ -z "$TOOLCHAIN" ]] && echo "Please set the TOOLCHAIN variable" && exit 1

if [ "$ARCH" == "host" ]; then
    SYS=
elif [ "$ARCH" == "x86_64" ]; then
    SYS=x86_64
elif [ "$ARCH" == "x86" ]; then
    SYS=i686
elif [ "$ARCH" == "arm64-v8a" ]; then
    SYS=aarch64
elif [ "$ARCH" == "armeabi-v7a" ]; then
    SYS=armv7a
else
    echo "Unsupported ARCH: $ARCH"
    exit 1
fi

if [ "$ARCH" == "host" ]; then
    echo "local toolchain"
else
TARGET=$SYS-linux-
TOOLTARGET=$TARGET-
if [ "$SYS" == "armv7a" ]; then
    TARGET=armv7a-linux-
    TOOLTARGET=arm-linux-
fi
fi

if [ "COMPILER" == "clang" ]; then
export TARGET=${TARGET}
export CXX=$TOOLCHAIN/bin/${TARGET}clang++
export CC=$TOOLCHAIN/bin/${TARGET}clang
export LD=$TOOLCHAIN/bin/${TOOLTARGET}ld
export AR=$TOOLCHAIN/bin/llvm-ar
export AS=$TOOLCHAIN/bin/${TOOLTARGET}as
export RANLIB=$TOOLCHAIN/bin/${TOOLTARGET}ranlib
export STRIP=$TOOLCHAIN/bin/${TOOLTARGET}strip
else
# TODO if you use your hosts g++ then you will get stc++ link errors because the libnexalight.a compiler version does not match the final link
# You have to use the IDE's default compiler.  TODO find this from gradle...
#export TOOLCHAIN=/home/stone/.konan/dependencies/x86_64-unknown-linux-gnu-gcc-8.3.0-glibc-2.19-kernel-4.9-2
#export TARGET=x86_64-unknown-linux-gnu-

export CXX=$TOOLCHAIN/bin/${TARGET}g++
export CC=$TOOLCHAIN/bin/${TARGET}gcc
export LD=$TOOLCHAIN/bin/${TARGET}ld
export AR=$TOOLCHAIN/bin/${TARGET}ar
export AS=$TOOLCHAIN/bin/${TARGET}as
export RANLIB=$TOOLCHAIN/bin/${TARGET}ranlib
export STRIP=$TOOLCHAIN/bin/${TARGET}strip
fi

echo TARGET=${TARGET}
export CXXFLAGS=-DHAVE_CONFIG_H
export PLATFORM=LINUX
make -j 8

exit 0

