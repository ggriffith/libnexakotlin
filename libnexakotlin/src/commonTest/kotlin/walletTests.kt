package org.nexa.libnexakotlin
import kotlin.test.*

import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign
import org.nexa.threads.microsleep
import org.nexa.threads.millisleep

// If your test environment's regtest full node is NOT located on localhost, you must define
// this variable to be the IP address of your full node (otherwise use null)
private var FULL_NODE_IP:String? = "127.0.0.1"

private val LogIt = GetLog("BlockchainTests.kt")

/** Database name prefix, empty string for mainnet, set for testing */
private var dbPrefix = "nexa_unittest_"

class WalletTests
{
    companion object
    {
        init
        {
            initializeLibNexa()
        }
    }

    @BeforeTest
    fun setup()
    {
        REG_TEST_ONLY = true
    }
    @Test
    fun create()  // expects that the REGTEST blockchain has produced a block within the last hour (or it will hang)
    {
        org.nexa.threads.DefaultThreadExceptionHandler = {
            println(it)
            handleThreadException(it)
        }
        try
        {
            deleteDatabase(dbPrefix + "wal")
        }
        catch(e: org.nexa.libnexakotlin.LibNexaException)
        {
            if (e.message?.contains("with the Android application context") ?: false) return
            else throw e
        }
        val wal = newWallet(dbPrefix +"wal", ChainSelector.NEXAREGTEST)
        // point to your hard-coded regtest full node
        if (FULL_NODE_IP != null)
        {
            LogIt.info("running in exclusive node mode to $FULL_NODE_IP")
            wal.blockchain.req.net.exclusiveNodes(setOf(FULL_NODE_IP!!))
        }

        var matched = 0
        while (!wal.synced())
        {
            println("wal not synced! at: ${wal.chainstate?.syncedHeight}  chain at: ${wal.blockchain.curHeight}")
            if (wal.chainstate?.syncedHeight == wal.blockchain.curHeight)
            {
                matched++
                if (matched > 5) println("hang?  you probably haven't produced a regtest block in an hour...")
            }
            millisleep(1000U)
        }
    }
}